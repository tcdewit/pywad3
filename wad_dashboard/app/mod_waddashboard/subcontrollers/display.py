# Import flask dependencies
from flask import Blueprint, request, render_template, \
     flash, session, redirect, url_for, Markup

try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs import libdisplay
    from app.libs.shared import dbio_connect, Button, bytes_as_string, string_as_bytes
except ImportError:
    from wad_dashboard.app.mod_auth.controllers import login_required
    from wad_dashboard.app.libs import HTML as simplehtml
    from wad_dashboard.app.libs import libdisplay
    from wad_dashboard.app.libs.shared import dbio_connect, Button, bytes_as_string, string_as_bytes
dbio = dbio_connect()

import json
import jsmin

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_display import DisplayForm

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_blueprint = Blueprint('display', __name__, url_prefix='/waddashboard')

@mod_blueprint.route('/admin/display/default', methods=['GET', 'POST'])
@login_required
def default():
    # not for normal users
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >1: 
        return(redirect(url_for('admin.default')))
        
    # display and allow editing of modules table
    _sid = int(request.args['sid']) if 'sid' in request.args else None
    if _sid is None:
        if len(dbio.DBSelectors().select())>0:
            _sid = dbio.DBSelectors().select()[0].id
        else:
            return(redirect(url_for('admin.default')))

    stuff = libdisplay.get_display(dbio, _sid)
    stuff = sorted(stuff, key=lambda x: x['display_position'])
    
    #item = {'description':None, 'display_level':0, 'display_name':None, 'units':None, 'constraint_is_active':True, 'display_position':0 }
    level_choices = {
        '0': 'admin',
        '1': 'key-user',
        '2': 'user'}

    table = simplehtml.Table(header_row=['name', 'display_name', 'description', 'units', 'decimals', 'isactive', 'level', 'position'], tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        table.rows.append([data['name'], data['display_name'], data['description'], data['units'], 
                           data['decimals'], data['constraint_is_active'],
                           level_choices[str(data['display_level'])], str(data['display_position']), 
                           Button('edit',url_for('.modify', sid=_sid, did=data['name']))
                           ])
    newbutton = ''#Button('New', url_for('.modify'))

    # make a dropdown selector
    picker = '<select id="selector" name="selector">'
    for r in dbio.DBSelectors.select().order_by(dbio.DBSelectors.name):
        picker += '<option %s value="%s">%s</option>'%('selected' if _sid == r.id else '', r.id, r.name)
    picker += '</select>'
    picker += '<script> jQuery(function () { jQuery("#selector").change(function () {'\
        'location.href = "?sid="+jQuery(this).val(); }) '\
        ' }) </script>'

    page = picker+str(table)+'<div>'+newbutton+'</div>'
    
    return render_template("waddashboard/generic.html", title='Display Results', msg='', html=Markup(page))


"""
copy from wad_admin/subcontrollers/selectors.py
"""
def update_meta_if_changed(selector_id, result_name, field_dict):
    """
    Check if old constraints different from new constraints
    """
    equal = True

    # 1. get stored constraints
    oldlimits = libdisplay.get_limits_from_json(dbio, selector_id, result_name)

    # 2. generate new constraints from form data
    newlimits = {key:[] for key in oldlimits.keys()} # same dict/list structure as oldlimits

    # equals
    oldnames = [ p['lim_name'] for p in oldlimits['constraint_equals'] ]
    for i in range(0,len(oldlimits['constraint_equals'])):
        prefix = 'equals-%d-'%i
        pname = prefix+'lim_name'
        # sanity check. when a different config is picked, there is no guarantee that the same results are present
        if pname in field_dict and field_dict[pname] in oldnames:
            limval = field_dict[prefix+'lim_val']
            newlimits['constraint_equals'].append({
                'lim_name':field_dict[pname],
                'lim_val': None if limval == '' else limval,
            })

    # period
    oldnames = [ p['lim_name'] for p in oldlimits['constraint_period'] ]
    for i in range(0,len(oldlimits['constraint_period'])):
        prefix = 'periods-%d-'%i
        pname = prefix+'lim_name'
        # sanity check. when a different config is picked, there is no guarantee that the same results are present
        if pname in field_dict and field_dict[pname] in oldnames:
            limval = field_dict[prefix+'lim_val']
            newlimits['constraint_period'].append({
                'lim_name':field_dict[pname],
                'lim_val': None if limval == '' else int(limval),
            })
            
    # minlowhighmax
    oldnames = [ p['lim_name'] for p in oldlimits['constraint_minlowhighmax'] ]
    for i in range(0,len(oldlimits['constraint_minlowhighmax'])):
        prefix  = 'minlowhighmaxs-%d-'%i
        prefix2 = 'refminlowhighmaxs-%d-'%i
        pname  = prefix+'lim_name'
        pname2 = prefix2+'lim_name'
        # sanity check. when a different config is picked, there is no guarantee that the same results are present
        if pname in field_dict and (field_dict[pname] in oldnames or field_dict[pname2] in oldnames):
            newlimits['constraint_minlowhighmax'].append({
                'lim_name':field_dict[pname]})
            for f in ['min_val', 'low_val', 'high_val', 'max_val']:
                fval = field_dict[prefix+f]
                newlimits['constraint_minlowhighmax'][-1][f] = None if fval == '' else float(fval)

    # refminlowhighmax
    oldnames = [ p['lim_name'] for p in oldlimits['constraint_refminlowhighmax'] ]
    has_ref = False # does not have relative field
    for i in range(0,len(oldlimits['constraint_refminlowhighmax'])):
        prefix  = 'refminlowhighmaxs-%d-'%i
        prefix2 = 'minlowhighmaxs-%d-'%i
        pname  = prefix+'lim_name'
        pname2 = prefix2+'lim_name'
        # sanity check. when a different config is picked, there is no guarantee that the same results are present
        if pname in field_dict and (field_dict[pname] in oldnames or field_dict[pname2] in oldnames):
            if field_dict[prefix+'ref_val'] == '':
                continue
            has_ref = True
            newlimits['constraint_refminlowhighmax'].append({
                'lim_name':field_dict[pname]})
            for f in ['ref_val', 'min_val', 'low_val', 'high_val', 'max_val']:
                fval = field_dict[prefix+f]
                newlimits['constraint_refminlowhighmax'][-1][f] = None if fval == '' else float(fval)


    # 3. compare old to new
    limits_key_fields = {
        'constraint_equals': ['lim_val'],
        'constraint_period': ['lim_val'],
    }
    if has_ref:
        limits_key_fields['constraint_refminlowhighmax'] = ['ref_val', 'min_val', 'low_val', 'high_val', 'max_val']
    else:
        limits_key_fields['constraint_minlowhighmax'] = ['min_val', 'low_val', 'high_val', 'max_val']

    for key, fields in limits_key_fields.items():
        for oldv, newv in zip(oldlimits[key], newlimits[key]):
            for f in fields:
                if not oldv[f] == newv[f]:
                    logger.info('Different {}: {} ({})'.format(oldv['lim_name'], oldv[f], newv[f]) )
                    equal = False
                    break
                    
                if not equal:
                    break

    if not equal:
        meta = dbio.DBSelectors.get_by_id(selector_id).module_config.meta

        #1. read the meta from the database
        blob = json.loads(jsmin.jsmin(bytes_as_string(meta.val)))

        if not 'results' in blob:
            blob['results'] = {}

        for key, fields in limits_key_fields.items():
            for newv in newlimits[key]:
                name = newv['lim_name']
                if not name in blob['results']:
                    blob['results'][name] = {}

                if len(fields) == 1:
                    if newv[fields[0]] is None:
                        if key in blob['results'][name].keys(): # check if delete needed
                            del blob['results'][name][key]
                    else:
                        blob['results'][name][key] = newv[fields[0]]
                else:
                    if sum([ newv[f] is None for f in fields ]) == len(fields):
                        if key in blob['results'][name].keys(): # check if delete needed
                            del blob['results'][name][key]
                    else:
                        blob['results'][name][key] = [ newv[f] for f in fields ]

                if has_ref:
                    if 'constraint_minlowhighmax' in blob['results'][name]:
                        del blob['results'][name]['constraint_minlowhighmax']
                else:
                    if 'constraint_refminlowhighmax' in blob['results'][name]:
                        del blob['results'][name]['constraint_refminlowhighmax']
            
        meta.val = json.dumps(blob)
        meta.save()
    

@mod_blueprint.route('/admin/display/modify', methods=['GET', 'POST'])
@login_required
def modify():
    # not for normal users
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >1: 
        return(redirect(url_for('admin.default')))

    # show a table with all details of a result
    _sid = int(request.args['sid']) if 'sid' in request.args else None
    _did = str(request.args['did']) if 'did' in request.args else None

    # try to get a list of constraints
    constraints = libdisplay.get_limits_from_json(dbio, _sid, _did)
    equals = constraints.get('constraint_equals', None)
    periods = constraints.get('constraint_period', None)
    minlowhighmaxs = constraints.get('constraint_minlowhighmax', None)
    refminlowhighmaxs = constraints.get('constraint_refminlowhighmax', None)

    # invalid table request are to be ignored
    formtitle = 'Modify display of result'

    form = DisplayForm(None if request.method=="GET" else request.form,
                       equals = equals,
                       periods = periods,
                       minlowhighmaxs = minlowhighmaxs,
                       refminlowhighmaxs = refminlowhighmaxs)
    if not _sid is None and not _did is None:
        item = libdisplay.get_display(dbio, _sid, _did)
        if isinstance(item, list): # item not found in list of display; probably means not enabled for user
            msg = 'Result {} not found in meta. First add it to the display of the wanted access level, then try modify again.'.format(_did)
            return render_template("waddashboard/generic.html", title=formtitle, msg=msg, html='')

        form.sid.data = _sid
        form.did.data = _did
        form.display_name.data = item['display_name']
        form.description.data = item['description']
        form.units.data = item['units']
        form.decimals.data = item.get('decimals', None)
        form.constraint_is_active.data = item['constraint_is_active']
        form.display_level.data = item['display_level']
        form.display_position.data = item['display_position']

    newentry = False
    existingnames = []
    if form.did is None or form.did.data == '' or form.did.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        newentry = True
        formtitle = 'New result to display'
        if _sid is None:
            existingnames = []
        else:
            existingnames = [ s['name'] for s in libdisplay.get_display(dbio,_sid) ]

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if newentry and hasattr(form, 'name') and form.name.data in existingnames:
            flash('Display name already exists!', 'error')
            valid = False
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            update_meta_if_changed(form.sid.data, form.did.data, field_dict)
            doModifyDisplay(form.sid.data, field_dict)
            return redirect(url_for('.default',sid=form.sid.data))
            
    return render_template("waddashboard/display.html", form=form, action='modify', 
                           title=formtitle, msg='Fill out the fields and click Submit')

def doModifyDisplay(sid, field_dict):
    new_item = {'name': field_dict['did'],
                'description': field_dict['description'],
                'display_name': field_dict['display_name'],
                'units': field_dict['units'],
                'decimals': field_dict['decimals'],
                'constraint_is_active': True if 'constraint_is_active' in field_dict else False,
                'display_level': field_dict['display_level'],
                'display_position': field_dict['display_position'],
                }
    display = libdisplay.get_display(dbio, sid)
    found = False
    for i, od in enumerate(display):
        if od['name'] == new_item['name']:
            found = True
            if od == new_item:
                # nothing changed, just return
                return
            else:
                display[i] = new_item
                break
    
    if not found:
        # could not find that result, just return
        return
    
    # if we reach this point, changes should be saved to the blob in the existing meta
    meta = dbio.DBSelectors.get_by_id(sid).module_config.meta
    # construct a new config blob
    #1. read the meta from the database
    blob = json.loads(jsmin.jsmin(bytes_as_string(meta.val)))
    for dis in display:
        name = dis['name']
        del dis['name']
        for key,val in dis.items():
            blob['results'][name][key] = val

    meta.val = json.dumps(blob)
    meta.save()
