# Import Form 
from flask_wtf import FlaskForm 
from flask import Markup
# Import Form elements such as TextField 
from wtforms import HiddenField, BooleanField, TextField, SelectField, IntegerField, FieldList, FormField, StringField, FloatField

# Import Form validators
from wtforms.validators import Required, NoneOf, NumberRange, Optional

level_choices = [(0, 'admin'), (1, 'key-user'), (2, 'user')]

class EqualsEntryForm(FlaskForm):
    lim_name = HiddenField()
    lim_val = StringField('equals', validators=(Optional(),))
    
class PeriodEntryForm(FlaskForm):
    lim_name = HiddenField()
    lim_val = IntegerField('period', validators=(Optional(),))

class MinLowHighMaxEntryForm(FlaskForm):
    lim_name = HiddenField()
    min_val = FloatField('min', validators=(Optional(),))
    low_val = FloatField('low', validators=(Optional(),))
    high_val = FloatField('high', validators=(Optional(),))
    max_val = FloatField('max', validators=(Optional(),))

class RefMinLowHighMaxEntryForm(FlaskForm):
    lim_name = HiddenField()
    ref_val = FloatField('ref', validators=(Optional(),))
    min_val = FloatField('min%', validators=(Optional(),))
    low_val = FloatField('low%', validators=(Optional(),))
    high_val = FloatField('high%', validators=(Optional(),))
    max_val = FloatField('max%', validators=(Optional(),))

class DisplayForm(FlaskForm):
    sid = HiddenField('sid', []) # id of selector
    did = HiddenField('did', []) # name of result
    display_name = TextField('display_name')
    description = TextField('description')
    units = TextField('units')
    decimals = IntegerField('decimals', validators=(Optional(),))
    
    constraint_is_active = BooleanField('constraint_is_active')
    equals = FieldList(FormField(EqualsEntryForm))
    minlowhighmaxs = FieldList(FormField(MinLowHighMaxEntryForm))
    periods = FieldList(FormField(PeriodEntryForm))
    
    refminlowhighmaxs = FieldList(FormField(RefMinLowHighMaxEntryForm)) # relative

    display_level = SelectField(choices = level_choices, coerce=int)
    display_position = IntegerField('position', [NumberRange(message='Position cannot be empty!', min=0)])

