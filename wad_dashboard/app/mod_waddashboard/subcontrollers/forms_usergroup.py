# Import Form 
from flask_wtf import FlaskForm 
from flask import Markup
# Import Form elements such as TextField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, TextField, SelectField, IntegerField, RadioField

# Import Form validators
from wtforms.validators import Required, NoneOf

class GroupEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    grp_id = HiddenField()
    grp_name = HiddenField()
    grp_description = HiddenField()
    grp_selected = BooleanField('include in group')

class UserGroupForm(FlaskForm):
    gid = HiddenField('gid', [])
    groups = FieldList(FormField(GroupEntryForm))
    include_what = RadioField('make visible', choices=[('selection','selection'), ('all','all'), ('none', 'none')])

