# Import flask dependencies
from flask import Blueprint, request, render_template, \
     flash, session, redirect, url_for, Markup

try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button
    from ..models import WDUsers, role_names, WDUserGroups, WDMainGroups, WDMainItems, WDSections, WDSubGroups, WDSubItems
except ImportError:
    from wad_dashboard.app.mod_auth.controllers import login_required
    from wad_dashboard.app.libs import HTML as simplehtml
    from wad_dashboard.app.libs.shared import dbio_connect, Button
    from wad_dashboard.app.mod_waddashboard.models import WDUsers, role_names, WDUserGroups, WDMainGroups, WDMainItems, WDSections, WDSubGroups, WDSubItems
dbio = dbio_connect()
import os.path
from wad_qc.connection import bytes_as_string

# Import modify forms
from .forms_group import GroupForm, SectionForm, SubGroupForm
from .forms_usergroup import UserGroupForm

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_blueprint = Blueprint('admin', __name__, url_prefix='/waddashboard')

# Set the route and accepted methods
@mod_blueprint.route('/admin/default', methods=['GET', 'POST'])
@login_required
def default():
    """
    Just present a menu with choices; should all be require authorization
    """
    # not everything for normal users
    role = 2
    if session.get('logged_in'):
        role = session.get('role')

    adminrole = (role == 0)

    menus = [
    ]
    
    groupingmenu=[
        { 'label':'Sections', 'href':url_for('admin.sections') }, 
        { 'label':'SubGroups', 'href':url_for('admin.subgroups') },
        { 'label':'MainGroups', 'href':url_for('admin.groups') },
    ]

    displaymenu=[
        { 'label':'Results', 'href':url_for('display.default') }, 
    ]

    resourcesmenu=[
        { 'label':'User Management', 'href':url_for('auth_users.default') }
    ]

    # not for normal users
    if role==0: 
        displaymenu.append( { 'label':'UserGroups', 'href':url_for('admin.usergroups') } )
        
        menus.append({'title':'Grouping', 'items':groupingmenu})

    if role<2:
        menus.append({'title':'Display', 'items':displaymenu})

    menus.append({'title':'Resources', 'items':resourcesmenu})
    return render_template('waddashboard/home.html', menus=menus, title='WAD-QC Dashboard Admin Menu')

@mod_blueprint.route('/admin/sections', methods=['GET', 'POST'])
@login_required
def sections():
    """
    Display and allow editing of sections table
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    subtitle = "Sections are the toplevel menus on the homepage of WAD Dashboard. They are placeholders for MainGroups."
    
    stuff = WDSections.select().order_by(WDSections.name)

    table = simplehtml.Table(header_row=['id', 'name', 'description', 'position', '#maingroups'], tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        table.rows.append([data.id, data.name, data.description, 
                           data.position, len(data.maingroups),
                           Button('delete',url_for('.deletesection', gid=data.id)),
                           Button('edit',url_for('.modifysection', gid=data.id))
                           ])
    newbutton = Button('New', url_for('.modifysection'))
    page = str(table)+'<div>'+newbutton+'</div>'
    
    return render_template("waddashboard/generic.html", title='Sections', msg='', subtitle=subtitle, html=Markup(page))

@mod_blueprint.route('/admin/groups', methods=['GET', 'POST'])
@login_required
def groups():
    """
    Display and allow editing of groups table
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    subtitle = "MainGroups are placeholders to group SubGroups."

    stuff = WDMainGroups.select().order_by(WDMainGroups.name)

    table = simplehtml.Table(header_row=['id', 'name', 'description', 'section', '#subgroups'], tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        sg = WDMainItems.select().where(WDMainItems.maingroup == data.id)
        table.rows.append([data.id, data.name, data.description, 
                           data.section.name, len(sg),
                           Button('delete',url_for('.deletegroup', gid=data.id)),
                           Button('edit',url_for('.modifygroup', gid=data.id))
                           ])
    newbutton = Button('New', url_for('.modifygroup'))
    page = str(table)+'<div>'+newbutton+'</div>'
    
    return render_template("waddashboard/generic.html", title='MainGroups', msg='', subtitle=subtitle, html=Markup(page))

@mod_blueprint.route('/admin/subgroups', methods=['GET', 'POST'])
@login_required
def subgroups():
    """
    Display and allow editing of subgroups table
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    subtitle = ("SubGroups are groups of Selectors, for which the Results are joined. "  
         "MainGroups consist of SubGroups only, so even if the MainGroup is to contain " 
         "only one selector, a SubGroup should be defined for that Selector.")

    stuff = WDSubGroups.select().order_by(WDSubGroups.name)

    table = simplehtml.Table(header_row=['id', 'name', 'description', '#maingroups', '#selectors'], tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        mg = WDMainItems.select().where(WDMainItems.subgroup == data.id)
        table.rows.append([data.id, data.name, data.description, 
                           len(mg), len(data.subitems),
                           Button('delete',url_for('.deletesubgroup', gid=data.id)),
                           Button('edit',url_for('.modifysubgroup', gid=data.id))
                           ])
    newbutton = Button('New', url_for('.modifysubgroup'))
    page = str(table)+'<div>'+newbutton+'</div>'
    
    return render_template("waddashboard/generic.html", title='SubGroups', msg='', subtitle=subtitle, html=Markup(page))


@mod_blueprint.route('/admin/usergroups', methods=['GET', 'POST'])
@login_required
def usergroups():
    """
    Allow display of MainGroups for given users
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if not role == 0: 
        return(redirect(url_for('admin.default')))

    subtitle = "MainGroups visibility for Users."

    stuff = WDUsers.select().order_by(WDUsers.username)

    table = simplehtml.Table(header_row=['id', 'username', 'role', '#maingroups'], tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        if data.role == 0: #admin sees everything
            continue
        sg = WDUserGroups.select().where(WDUserGroups.user == data)
        table.rows.append([data.id, data.username, 
                           role_names[data.role], len(sg),
                           Button('edit',url_for('.modifyusergroup', gid=data.id))
                           ])
    page = str(table)
    
    return render_template("waddashboard/generic.html", title='UserGroups', msg='', subtitle=subtitle, html=Markup(page))

@mod_blueprint.route('/admin/section/delete')
@login_required
def deletesection():
    """
    delete given id of given table from iqc db
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        WDSections.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.sections'))

@mod_blueprint.route('/admin/group/delete')
@login_required
def deletegroup():
    """
    delete given id of given table from iqc db
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        WDMainGroups.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.groups'))

@mod_blueprint.route('/admin/subgroup/delete')
@login_required
def deletesubgroup():
    """
    delete given id of given table from iqc db
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        WDSubGroups.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.subgroups'))

# Set the route and accepted methods
@mod_blueprint.route('/admin/section/modify', methods=['GET', 'POST'])
@login_required
def modifysection():
    """
    Show a table with all details of a section
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    _gid = int(request.args['gid']) if 'gid' in request.args else None
    _xtramsg = request.args.get('xtramsg', None)

    subtitle = None
    if _xtramsg == 'maingroup':
        subtitle = "A Main group must be part of a Section. You must define a Section first!"
    elif _xtramsg == 'subgroup':
        subtitle = "A sub group must be part of a Main group, and a Main group must be part of a Section. You must define a Section first!"
        
    # invalid table request are to be ignored
    formtitle = 'Modify section'
    form = SectionForm(None if request.method=="GET" else request.form)
    if not _gid is None:
        section = WDSections.get_by_id(_gid)
        form.gid.data = _gid
        form.name.data = section.name
        form.position.data = section.position
        form.description.data = section.description

    newentry = False
    if form.gid is None or form.gid.data == '' or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        newentry = True
        formtitle = 'New section'

    existingnames = [ s.name for s in WDSections.select() ]
    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if newentry and form.name.data in existingnames:
            flash('Section name already exists!', 'error')
            valid = False
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            doModifySection(field_dict)
            return redirect(url_for('.sections')) #doExport(field_dict)
            
    return render_template("waddashboard/section.html", form=form, action='modify', 
                           title=formtitle, subtitle=subtitle, msg='Fill out the fields and click Submit')
    
# Set the route and accepted methods
@mod_blueprint.route('/admin/group/modify', methods=['GET', 'POST'])
@login_required
def modifygroup():
    """
    Show a table with all details of a group
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    # if no sections defined, first go to new section
    if len(WDSections.select()) == 0:
        _xtramsg = request.args.get('xtramsg', "maingroup")
        return redirect(url_for('.modifysection', xtramsg=_xtramsg))

    _gid = int(request.args['gid']) if 'gid' in request.args else None

    subtitle = None

    # invalid table request are to be ignored
    formtitle = 'Modify MainGroup'
    selectors = None
    subgroups = getSubgroups(_gid)
    form = GroupForm(None if request.method=="GET" else request.form, selectors=selectors, subgroups=subgroups)
    form.section.choices = sorted([(m.id, m.name) for m in WDSections.select()], key=lambda x: x[1])

    if not _gid is None:
        group = WDMainGroups.get_by_id(_gid)
        form.gid.data = _gid
        form.name.data = group.name
        form.section.data = group.section.id
        form.description.data = group.description
        
    newentry = False
    if form.gid is None or form.gid.data == '' or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        newentry = True
        formtitle = 'New MainGroup'

    existingnames = [ s.name for s in WDMainGroups.select() ]
    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if newentry and form.name.data in existingnames:
            flash('MainGroup name already exists!', 'error')
            valid = False
        if not newentry:
            group = WDMainGroups.get_by_id(form.gid.data)
            if not group.name == form.name.data and form.name.data in existingnames:
                flash('MainGroup name already exists!', 'error')
                valid = False
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            doModifyGroup(field_dict)
            return redirect(url_for('.groups')) #doExport(field_dict)
            
    return render_template("waddashboard/group.html", form=form, action='modify', 
                           title=formtitle, subtitle=subtitle, msg='Select SubGroups to group for main display and click Submit')

# Set the route and accepted methods
@mod_blueprint.route('/admin/subgroup/modify', methods=['GET', 'POST'])
@login_required
def modifysubgroup():
    """
    Show a table with all details of a group
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Modify SubGroup'
    selectors = sorted(getSelectors(groupid=None, subgroupid=_gid), key=lambda x: x['sel_name'])
    form = SubGroupForm(None if request.method=="GET" else request.form, selectors=selectors)
        
    if not _gid is None:
        subgroup = WDSubGroups.get_by_id(_gid)
        form.gid.data = _gid
        form.name.data = subgroup.name
        form.description.data = subgroup.description
        
    newentry = False
    if form.gid is None or form.gid.data == '' or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        newentry = True
        formtitle = 'New SubGroup'

    existingnames = [ s.name for s in WDSubGroups.select() ]
    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if newentry and form.name.data in existingnames:
            flash('SubGroup name already exists!', 'error')
            valid = False
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            doModifySubGroup(field_dict)
            return redirect(url_for('.subgroups')) #doExport(field_dict)
            
    return render_template("waddashboard/subgroup.html", form=form, action='modify', 
                           title=formtitle, msg='Select selectors to group for sub group display and click Submit')

# Set the route and accepted methods
@mod_blueprint.route('/admin/user/modify', methods=['GET', 'POST'])
@login_required
def modifyusergroup():
    """
    Show a table with MainGroups visible to each user
    """
    # only for admin
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >0: 
        return(redirect(url_for('admin.default')))

    _gid = int(request.args['gid']) if 'gid' in request.args else None

    subtitle = None

    # invalid table request are to be ignored
    formtitle = 'Modify UserGroup'
    selectors = None
    groups = getUsergroups(_gid)
    form = UserGroupForm(None if request.method=="GET" else request.form, groups=groups, include_what='selection')

    if not _gid is None: # only modify
        form.gid.data = _gid # userid
        user = WDUsers.get_by_id(_gid)
    
    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            doModifyUserGroup(field_dict)
            return redirect(url_for('.usergroups')) #doExport(field_dict)
            
    return render_template("waddashboard/usergroup.html", form=form, action='modify', 
                           title=formtitle, subtitle=subtitle, msg='Select MainGroups to visualise for User "{}" and click Submit'.format(user.username))

def getSelectors(groupid, subgroupid=None):
    """
    Return a list of Selectors in dictionary format, belonging to the given MainGroup or SubGroup
    """
    if groupid is None:
        gid   = subgroupid
        model = "subgroups"
    else:
        gid = groupid
        model = "maingroups"
        
    if gid is None:
        in_group = []
    else:
        in_group =[]
        if model == "subgroups":
            items = WDSubGroups.get_by_id(gid).subitems
        else:
            items = WDMainGroups.get_by_id(gid).mainitems
            
        for item in items:
            try:
                in_group.append(item.selector.id)
            except dbio.DBSelectors.DoesNotExist: # happens if a selector was deleted in WAD_Admin
                item.delete_instance(recursive=False)
                
    result = []
    for sel in dbio.DBSelectors.select():
        result.append({'sel_name':sel.name,
                       'sel_description':sel.description,
                       'sel_isactive':sel.isactive,
                       'sel_selected':sel.id in in_group,
                       'sid': sel.id})
        
    return result

def getSubgroups(gid):
    """
    Return a list of Subgroups in dictionary format, belonging to the given MainGroup
    """
    result = []
    if gid is None:
        in_group = []
    else:
        group = WDMainGroups.get_by_id(gid)
        in_group = [it.subgroup.id for it in WDMainItems.select().where(WDMainItems.maingroup == group)]

    for sub in WDSubGroups.select():
        result.append({'sub_name':sub.name,
                       'sub_description':sub.description,
                       'sub_selected':sub.id in in_group,
                       'sub_id': sub.id})
        
    return sorted(result, key=lambda x:x['sub_name'])

def getUsergroups(gid):
    """
    Return a list of Subgroups in dictionary format, belonging to the given MainGroup
    """
    result = []
    if gid is None:
        in_group = []
    else:
        user = WDUsers.get_by_id(gid)
        in_group = [it.maingroup.id for it in WDUserGroups.select().where(WDUserGroups.user == user)]

    for sub in WDMainGroups.select():
        result.append({'grp_name':sub.name,
                       'grp_description':sub.description,
                       'grp_selected':sub.id in in_group,
                       'grp_id': sub.id})
        
    return sorted(result, key=lambda x:x['grp_name'])

def doModifySection(field_dict):
    if field_dict['gid'] == '':
        # create new
        mid = WDSections.create(name=field_dict['name'], description=field_dict['description'], position=field_dict['position'])
    else:
        gid = int(field_dict['gid'])
        section = WDSections.get_by_id(gid)
        section.name = field_dict['name']
        section.description = field_dict['description']
        section.position = field_dict['position']
        section.save()

def doModifyGroup(field_dict):
    sidsuffix = '-sub_id'
    subsuffix = '-sub_selected'
    sub_ids = []
    for key, entry in field_dict.items():
        if key.endswith(subsuffix): # it's either marked and present or not present
            sidkey = key[:(len(key)-len(subsuffix))]+sidsuffix
            sub_ids.append(int(field_dict[sidkey]))
    
    sid = int(field_dict['section'])
    if sid == 0:
        # create new
        sid = WDSections.create(name=field_dict['newsectionname'], description=field_dict['newsectiondescription'])
    else:
        sid = WDSections.get_by_id(sid)
        
    if field_dict['gid'] == '':
        # create new
        mid = WDMainGroups.create(name=field_dict['name'], description=field_dict['description'], section=sid)
        for i in sub_ids:
            WDMainItems.create(subgroup=i, maingroup=mid)
    else:
        gid = int(field_dict['gid'])
        group = WDMainGroups.get_by_id(gid)
        group.section = sid
        group.name = field_dict['name']
        group.description = field_dict['description']
        group.save()
        for i in WDMainItems.select().where(WDMainItems.maingroup == group):
            i.delete_instance(recursive=False)
                
        for i in sub_ids:
            WDMainItems.create(subgroup=i, maingroup=group)
            
def doModifySubGroup(field_dict):
    sidsuffix = '-sid'
    selsuffix = '-sel_selected'
    sel_ids = []
    for key, entry in field_dict.items():
        if key.endswith(selsuffix): # it's either marked and present or not present
            sidkey = key[:(len(key)-len(selsuffix))]+sidsuffix
            sel_ids.append(int(field_dict[sidkey]))
    
    if field_dict['gid'] == '':
        # create new
        mid = WDSubGroups.create(name=field_dict['name'], description=field_dict['description'])
        for i in sel_ids:
            WDSubItems.create(selector=i, subgroup=mid)
    else:
        gid = int(field_dict['gid'])
        subgroup = WDSubGroups.get_by_id(gid)
        subgroup.name = field_dict['name']
        subgroup.description = field_dict['description']
        subgroup.save()
        for i in subgroup.subitems:
            i.delete_instance(recursive=False)
                
        for i in sel_ids:
            WDSubItems.create(selector=i, subgroup=gid)
            
def doModifyUserGroup(field_dict):
    iw = field_dict['include_what']
    if iw == "all":
        grp_ids = [ f.id for f in WDMainGroups.select() ]
    elif iw == "none":
        grp_ids = []
    else: #iw == "selection":
        sidsuffix = '-grp_id'
        grpsuffix = '-grp_selected'
        grp_ids = []
        for key, entry in field_dict.items():
            if key.endswith(grpsuffix): # it's either marked and present or not present
                sidkey = key[:(len(key)-len(grpsuffix))]+sidsuffix
                grp_ids.append(int(field_dict[sidkey]))

    gid = int(field_dict['gid'])
    user = WDUsers.get_by_id(gid)
    for i in WDUserGroups.select().where(WDUserGroups.user == user):
        i.delete_instance(recursive=False)
            
    for i in grp_ids:
        WDUserGroups.create(user=user, maingroup=WDMainGroups.get_by_id(i))
            
