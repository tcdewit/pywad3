"""
Routines for deleting and downloading of datasets.

This should be a collection of functional stuff for interfacing with WAD-QC Results through the web interface.

Changelog:
  20180212: added relative limits
  20180130: changed red color for readability
  20171121: Initial version
"""
from __future__ import print_function
import os
import json
import jsmin
from .dummylogger import DummyLogger

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

COLOR_CRITICAL = '#FF3333'# 'red'
COLOR_WARNING  = 'yellow'
COLOR_OK       = 'yellowgreen'

def is_constraint_active(name, display):
    if name in display:
        dis = display[name]
        if 'constraint_is_active' in dis and not dis['constraint_is_active']:
            return False
    return True

def get_status(result, display, static_constraints, old_status=None):
    # determine if all results are within limits
    end_status = None
    
    static_equals = static_constraints.get('constraint_equals', {})
    static_periods = static_constraints.get('constraint_period', {})
    static_minlowhighmaxs = static_constraints.get('constraint_minlowhighmax', {})
    static_refminlowhighmaxs = static_constraints.get('constraint_refminlowhighmax', {})

    status_order = {'ok':0, 'warning':1, 'incomplete':2, 'critical':3}
    # check strings
    p_status = None
    for res in result.strings:
        if not is_constraint_active(res.name, display): continue
        lim =  static_equals.get(res.name, getattr(res, 'val_equal', None))  
        if not lim is None:
            if p_status is None:
                p_status = 'ok'
            if not str(res.val) == str(lim):
                p_status = 'critical'
                break

    # if p_status more severe than current end_status, raise end_status
    if not p_status is None:
        if end_status is None or status_order[p_status]>status_order[end_status]:
            end_status = p_status
    
    # check bools
    p_status = None
    for res in result.bools:
        if not is_constraint_active(res.name, display): continue
        lim =  static_equals.get(res.name, getattr(res, 'val_equal', None))  
        if not lim is None:
            if p_status is None:
                p_status = 'ok'
            if not res.val == lim:
                p_status = 'critical'
                break
    
    # if p_status more severe than current end_status, raise end_status
    if not p_status is None:
        if end_status is None or status_order[p_status]>status_order[end_status]:
            end_status = p_status


    # check floats equals
    p_status = None
    for res in result.floats:
        if not is_constraint_active(res.name, display): continue
        lim_eq =  static_equals.get(res.name, getattr(res, 'val_equal', None))  
        lim_ref = None
        if res.name in static_minlowhighmaxs:
            lim_min, lim_low, lim_high, lim_max =  static_minlowhighmaxs[res.name]
        elif res.name in static_refminlowhighmaxs:
            lim_ref, lim_min, lim_low, lim_high, lim_max =  static_refminlowhighmaxs[res.name]
        else:
            lim_ref  = getattr(res, 'val_ref', None)
            lim_min  = getattr(res, 'val_min', None)
            lim_low  = getattr(res, 'val_low', None)
            lim_high = getattr(res, 'val_high', None)
            lim_max  = getattr(res, 'val_max', None)

        if not lim_ref is None:
            lim_min  = None if lim_min is None else lim_ref*(1.+lim_min/100.)   # lim_min in pct
            lim_low  = None if lim_low is None else lim_ref*(1.+lim_low/100.)   # lim_low in pct
            lim_high = None if lim_high is None else lim_ref*(1.+lim_high/100.) # lim_high in pct
            lim_max  = None if lim_max is None else lim_ref*(1.+lim_max/100.)   # lim_max in pct

        if not lim_eq is None or not lim_min is None or not lim_low is None or not lim_high is None or not lim_max is None:
            if p_status is None:
                p_status = 'ok'
        if not lim_eq is None:
            if not str(res.val) == str(lim_eq):
                p_status = 'critical'
                break

        if not lim_min is None:
            if res.val is None:
                p_status = 'critical' # raising to critical is end stage
                break
            elif res.val < lim_min:
                p_status = 'critical' # raising to critical is end stage
                break

        if not lim_max is None:
            if res.val is None:
                p_status = 'critical' # raising to critical is end stage
                break
            elif res.val > lim_max:
                p_status = 'critical' # raising to critical is end stage
                break
            
        if not lim_low is None:
            if res.val is None:
                p_status = 'critical' # raising to critical is end stage
                break
            elif res.val < lim_low:
                p_status = 'warning'

        if not lim_high is None:
            if res.val is None:
                p_status = 'critical' # raising to critical is end stage
                break
            if res.val > lim_high:
                p_status = 'warning'

    # if p_status more severe than current end_status, raise end_status
    if not p_status is None:
        if end_status is None or status_order[p_status]>status_order[end_status]:
            end_status = p_status

    # check if given end_status already worse
    if not old_status is None:
        if end_status is None or status_order[old_status]>status_order[end_status]:
            end_status = old_status
        
    if end_status == 'ok':
        status_color = COLOR_OK
    elif end_status == 'warning':
        status_color = COLOR_WARNING
    elif end_status == 'incomplete':
        status_color = COLOR_CRITICAL
    elif end_status == 'critical':
        status_color = COLOR_CRITICAL
    else:
        return end_status, end_status
    
    return '<div style="background-color:%s">%s</div>'%(status_color, end_status), end_status


def get_display(dbio, sid, result_name=None):
    #1. read the meta from the database
    selector = dbio.DBSelectors.get_by_id(int(sid))
    try:
        meta = json.loads(jsmin.jsmin(bytes_as_string(selector.module_config.meta.val)))
    except json.JSONDecodeError:
        return {}
    except TypeError: # will occur if meta does not exist
        return {}
        

    display = []
    ordered = sorted(list(meta['results'].keys()))
    for i, name in enumerate(ordered):
        res = meta['results'][name]
        item = {'name': name}
        
        for key, defval, vtype in [ ('description', None, 'string'), 
                          ('display_level', 0, 'int'), 
                          ('display_name', None, 'string'), 
                          ('units', None, 'string'), 
                          ('decimals', None, 'int'), 
                          ('constraint_is_active', True, 'bool'), 
                          ('display_position', 1000+i, 'int')]:
            if key in res.keys():
                item[key] = res[key]
            else:
                item[key] = None

            # fix empty fields
            if item[key] is None:
                item[key] = defval

            if not (vtype == 'string') and item[key] in ['', u'']:# fix for both python2 and python3
                item[key] = defval

            if not item[key] is None:
                if vtype == 'int':
                    item[key] = int(item[key])
                elif vtype == 'bool':
                    item[key] = bool(item[key])

        # restrict display_level to allowable values
        if not item['display_level'] is None:
            item['display_level'] = min(max(item['display_level'], 0), 2)

        if item['display_level'] < 2: # set constraints of non-users items to inactive
            item['constraint_is_active'] = False

        display.append(item)
        if result_name == name:
            return item

    return display

def get_limits_from_json(dbio, selector_id, result_name):
    """
    Return dict of constraints for given result_name
    1. read the config from the database for given config
    2. extract the limit_types (Equals, MinLowHighMax, Period)
    """
    result = {} # use this if want to display only supplied values (instead of possible values)
    result = {'constraint_equals': [ {'lim_name':result_name, 'lim_val': None } ],
              'constraint_period': [ {'lim_name':result_name, 'lim_val': None } ],
              'constraint_minlowhighmax': [{'lim_name':result_name, 
                                            'min_val': None, 'low_val': None, 'high_val': None, 'max_val': None} ],
              'constraint_refminlowhighmax': [{'lim_name':result_name, 'ref_val': None, 
                                               'min_val': None, 'low_val': None, 'high_val': None, 'max_val': None} ],
              }
    if selector_id is None or result_name is None:
        return result

    #1. read the meta from the database
    meta = dbio.DBSelectors.get_by_id(selector_id).module_config.meta.val

    if meta is None:
        return result
    try:
        meta = json.loads(jsmin.jsmin(bytes_as_string(meta)))
    except json.JSONDecodeError:
        return result

    #2. extract the limits block
    res = meta['results'].get(result_name, {} )
    if 'constraint_equals' in res:
        result['constraint_equals'] = [ {'lim_name':result_name, 'lim_val': res['constraint_equals'] } ]
    if 'constraint_period' in res:
        result['constraint_period'] = [ {'lim_name':result_name, 'lim_val': int(res['constraint_period']) } ]
    if 'constraint_minlowhighmax' in res:
        result['constraint_minlowhighmax'] = [ 
            { l:None if f is None or f in ['', u''] else float(f) for l, f in zip( ['min_val', 'low_val', 'high_val', 'max_val'], res['constraint_minlowhighmax'])} ]
        result['constraint_minlowhighmax'][-1]['lim_name'] = result_name
    if 'constraint_refminlowhighmax' in res:
        result['constraint_refminlowhighmax'] = [ 
            { l:None if f is None or f in ['', u''] else float(f) for l, f in zip( ['ref_val', 'min_val', 'low_val', 'high_val', 'max_val'], res['constraint_refminlowhighmax'])} ]
        result['constraint_refminlowhighmax'][-1]['lim_name'] = result_name
    return result
    
def get_all_limits_from_json(dbio, selector_id):
    """
    Return dict of constraints for given result_name
    1. read the config from the database for given config
    2. extract the limit_types (Equals, MinLowHighMax, Period)
    """
    result = {} # use this if want to display only supplied values (instead of possible values)
    result = {'constraint_equals': {},
              'constraint_period': {},
              'constraint_minlowhighmax': {},
              'constraint_refminlowhighmax': {},
              }
    if selector_id is None:
        return result

    #1. read the meta from the database
    meta = dbio.DBSelectors.get_by_id(selector_id).module_config.meta.val

    if meta is None:
        return result
    try:
        meta = json.loads(jsmin.jsmin(bytes_as_string(meta)))
    except json.JSONDecodeError:
        return result

    #2. extract the limits block
    for key,res in meta['results'].items():
        if 'constraint_equals' in res:
            result['constraint_equals'][key] = res['constraint_equals']
        if 'constraint_period' in res:
            result['constraint_period'][key] = int(res['constraint_period'])
        if 'constraint_minlowhighmax' in res:
            result['constraint_minlowhighmax'][key] = [ None if f is None or f in ['', u''] else float(f) 
                                                        for f in res['constraint_minlowhighmax'] ]
        if 'constraint_refminlowhighmax' in res:
            result['constraint_refminlowhighmax'][key] = [ None if f is None or f in ['', u''] else float(f) 
                                                        for f in res['constraint_refminlowhighmax'] ]

    return result
    
