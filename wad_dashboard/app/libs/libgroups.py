"""
Routines for deleting and downloading of datasets.

This should be a collection of functional stuff for interfacing with WAD-QC Results through the web interface.

Changelog:
  20171121: Initial version
"""
from __future__ import print_function
import os
import json
import jsmin
from .dummylogger import DummyLogger
from . import libdisplay

import datetime
from dateutil.relativedelta import relativedelta


import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

def getCombinedAcqResults(selectors, logger=None):
    """
    Return a list of (date, { sel1: (acqdatetime, result, acqdatetimeresult) })
      date: date of these resultst.
      acqdatetime: datetime based on *datetime* results. If no *datetime* result given, fall back to created_time
      result: corresponding DBResults object
      acqname: name of DBDateTime corresponding to acqdatetime
    """
    
    if logger is None:
        logger = DummyLogger()

    # make dictionary of needed info for each selector:
    #   a list of (acqdate, result, DBDateTime) 
    multi_data = {}
    for selector in selectors:
        acq_dates = []
        for result in selector.results:
            if len(result.datetimes)>0:
                acq_dates.append(max([(p.val, result, p) for p in result.datetimes]))
            else:
                acq_dates.append( (result.created_time, result, None) ) # no period, no name
                
        multi_data[selector.name] = sorted(acq_dates, key=lambda x: x[0], reverse=True)


    # combine results of selectors per day
    data_per_day = []

    # keep track of next element of each selector
    index = { name: 0 for name in multi_data.keys() }
    
    stop = False
    while stop == False:
        # find the latest result of the next results of the selectors
        cmpdates = []
        for key, acq in multi_data.items():
            if len(acq)>0 and index[key]<len(acq):
                cmpdates.append(acq[index[key]][0])
        if len(cmpdates) == 0: # all results of all selectors are exhausted
            stop = True

        if stop == False:
            dadate = max(cmpdates).date() # drop time, just check date
            daydata = {} # results for this day
            for key, acq in multi_data.items():
                if len(acq) == 0 or index[key]==len(acq): # this selector is exhausted
                    daydata[key] = None
                elif acq[index[key]][0].date() == dadate: # only use the result if the date matches
                    daydata[key] = acq[index[key]]
                    index[key] += 1
                else:
                    daydata[key] = None
                    
            data_per_day.append( (dadate, daydata) )

    return data_per_day

def get_good_days(data_per_day, min_date, max_date=None):
    """
    return good_days, lastid
      good_days: a set of distinct dates after min_date when all tests were done
      last_id: id of latest result included
    """
    #data_per_day is ordered lates first
    latest_id = None
    good_days = set()
    for i, (ld, dpd) in enumerate(data_per_day):
        if not max_date is None and ld > max_date:
            continue
        if ld < min_date: 
            break

        if latest_id is None:
            latest_id = i

        good = True
        for d in dpd.values():
            if d is None or len(d) == 0:
                good = False
                break
        if good:
            good_days.add(ld)
    
    return good_days, latest_id


def getLatestCombinedAcqResults(dbio, selectors, logger=None):
    """
    return latest_id, latest_date, production, latest_status for latest combined results of given selectors.
      latest_id: id of used DBResults; not used in groups, only useful for single selector
      latest_date: datetime of latest result shown, presented as a color coded HTML <DIV>
      production: number of times all tests were done wrt to expected number of times in last six months, 
        presented as a color coded HTML <DIV>
      latest_status: status of all included tests as compared to given constraints, 
        presented as a color coded HTML <DIV>
    """
    if logger is None:
        logger = DummyLogger()

    production_days = int(366./2+.5) # 6 months
    production_limit = (datetime.datetime.now()-relativedelta(days=production_days)).date()
    production = 0

    data_per_day = getCombinedAcqResults(selectors, logger)
    
    good_days = set()
    # only check the latest combination of result
    latest_date = None
    latest_id = None
    production = None
    latest_status = None
    status_code = None
    date_code = 0 # no status, OK, ERROR
    min_period = None
    if len(data_per_day)>0:
        # count number of distinct days in the last 6 months when all tests were done 
        good_days,_ = get_good_days(data_per_day, production_limit)
            
        latest_day,latest_combi =  data_per_day[0] # latest data row
        
        for key, data in latest_combi.items():
            if data is None:
                # if not all selectors have a result for this row, the data is incomplete. only critical is worse
                if status_code != 'critical':
                    status_code = 'incomplete'
                    latest_status = '<div style="background-color:%s">%s</div>'%(libdisplay.COLOR_CRITICAL, status_code)
                continue   
            
            ## data is not None
            # find selector, display, static_constraints
            selector = dbio.DBSelectors.get_by_name(key)
            display =  { k['name']: k for k in libdisplay.get_display(dbio, selector.id) } # needed to check if constraint is active
            static_constraints = libdisplay.get_all_limits_from_json(dbio, selector)        # needed for status check
            static_periods = static_constraints.get('constraint_period', {}) # needed for period check

            # find period acqname
            acqdate, result, acqdt = data
            period = None
            if not acqdt is None:
                period = static_periods.get(acqdt.name, acqdt.val_period)

            # find full date of latest_date
            if latest_date is None:
                latest_date = data[0]
                latest_id = result.id # not used in groups, only useful for single selector
            else:
                latest_date = max(latest_date, data[0])

            # find status of last qc
            latest_status, status_code = libdisplay.get_status(result, display, static_constraints, status_code)

            # was last qc in time
            if not period is None:
                time_limit = (datetime.datetime.now()+datetime.timedelta(-period)).date() # just check day
                if not acqdt is None and libdisplay.is_constraint_active(acqdt.name, display):
                    if time_limit<data[0].date():
                        date_code = max(date_code, 1) # in time
                    else:
                        date_code = max(date_code, 2) # too long ago
                    if min_period is None:
                        min_period = period
                    else:
                        min_period = min(period, min_period)
        
    # add some color to date: if latest data was within period of now
    if date_code == 1:
        latest_date = '<div style="background-color:%s">%s</div>'%(libdisplay.COLOR_OK, latest_date)
    elif date_code == 2:
        latest_date = '<div style="background-color:%s">%s</div>'%(libdisplay.COLOR_CRITICAL, latest_date)

    # production
    production = len(good_days)
    if not min_period is None and min_period>0:
        min_production = int(production_days/min_period)
        if production<min_production:
            prod_color = libdisplay.COLOR_CRITICAL
        else:
            prod_color = libdisplay.COLOR_OK
        production = '<div style="background-color:{}">{}/{}</div>'.format(prod_color, production, min_production)

    return latest_id, latest_date, production, latest_status

def getStatusDiv(dbio, data_per_day, rid):
    """
    Generate a colored HTM DIV with the combined status of the given row
    """
    latest_day, latest_combi =  data_per_day[rid] # latest data row

    latest_status = None
    status_code = None
    for key, data in latest_combi.items():
        if data is None:
            # if not all selectors have a result for this row, the data is incomplete. only critical is worse
            if status_code != 'critical':
                status_code = 'incomplete'
                latest_status = '<div style="background-color:%s">%s</div>'%(libdisplay.COLOR_CRITICAL, status_code)
            continue   
        
        ## data is not None
        acqdate, result, acqdt = data

        # find selector, display, static_constraints
        selector = dbio.DBSelectors.get_by_name(key)
        display =  { k['name']: k for k in libdisplay.get_display(dbio, selector.id) } # needed to check if constraint is active
        static_constraints = libdisplay.get_all_limits_from_json(dbio, selector)        # needed for status check

        # find status of last qc
        latest_status, status_code = libdisplay.get_status(result, display, static_constraints, status_code)

    return latest_status

def getTimeLine(dbio, selectors, days, logger=None):
    # determine the latest result based on *datetime* results. Fallback to created_time
    # determine production as the ration of the number of results in the last xx months, over the expected number
    if logger is None:
        logger = DummyLogger()

    selector = selectors[0]
    rows = []
    period = 0

    # determine if latest result to obtain period
    static_constraints = libdisplay.get_all_limits_from_json(dbio, selector)
    static_periods = static_constraints.get('constraint_period', {})

    data_per_day = getCombinedAcqResults(selectors, logger)
    if len(data_per_day)==0:
        return rows, period

            
    # find the periodicity of the tests
    period = None
    latest_day,latest_combi =  data_per_day[0] # latest data row
    for key, data in latest_combi.items():
        if data is None:
            continue   
        
        ## data is not None
        # find selector, display, static_constraints
        selector = dbio.DBSelectors.get_by_name(key)
        display =  { k['name']: k for k in libdisplay.get_display(dbio, selector.id) } # needed to check if constraint is active
        static_constraints = libdisplay.get_all_limits_from_json(dbio, selector)        # needed for status check
        static_periods = static_constraints.get('constraint_period', {}) # needed for period check

        # find period acqname
        acqdate, result, acqdt = data
        if not acqdt is None:
            period = static_periods.get(acqdt.name, acqdt.val_period)
        if not period is None:
            break

    # now build rows starting half a year ago
    start_date = datetime.datetime.now().date()
    delta_days = datetime.timedelta(days)
    delta_period = datetime.timedelta(period)
    finish_date = start_date-delta_days
    end_date = start_date-delta_period

    while end_date>finish_date:
        good_days, latest_id = get_good_days(data_per_day, min_date=end_date, max_date=start_date)
        status = None if latest_id is None else getStatusDiv(dbio, data_per_day, latest_id)
        if len(good_days) < 1:
            submissions_color = libdisplay.COLOR_CRITICAL
        else:
            submissions_color = libdisplay.COLOR_OK
        
        if period is None: # no check on in_time
            submissions = '<div>%s</div>'%(len(good_days))
        else:
            submissions = '<div style="background-color:%s">%s</div>'%(submissions_color, len(good_days))
        
        rows.append( (end_date, start_date, submissions, status) )
        start_date -= delta_period
        end_date   -= delta_period

    return rows, period
