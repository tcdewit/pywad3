from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, session
from werkzeug.security import generate_password_hash, check_password_hash
try:
    from app.mod_auth.controllers import login_required
    from app.mod_waddashboard.models import WDUsers, role_names
    from app.libs import HTML as simplehtml
    from app.libs.shared import Button
    from app import AUTO_REFRESH
except ImportError:
    from wad_dashboard.app.mod_auth.controllers import login_required
    from wad_dashboard.app.mod_waddashboard.models import WDUsers, role_names
    from wad_dashboard.app.libs import HTML as simplehtml
    from wad_dashboard.app.libs.shared import Button
    from wad_dashboard.app import AUTO_REFRESH

# Import modify forms
from .forms_usermanager import ModifyForm

mod_blueprint = Blueprint('auth_users', __name__, url_prefix='/auth')

@mod_blueprint.route('/users', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow editing of modules table
    stuff = WDUsers.select().order_by(WDUsers.username)
    subtitle = "Only inactive users can be deleted."

    role = 2
    if session.get('logged_in'):
        role = session.get('role')
        currentid = session.get('user_id')

    table = simplehtml.Table(header_row=['username', 'email', 'role', 'refresh', 'enabled', 'passwordless'], 
                             tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        if (role < data.role or data.id == currentid):
            table.rows.append([data.username, data.email, 
                               role_names[data.role], data.refresh,
                               data.status == 1,
                               check_password_hash(data.password, str("")),
                               
                               Button('edit',url_for('.modify', gid=data.id)) if (role < data.role or data.id == currentid) else '',
                               Button('delete',url_for('.delete', gid=data.id)) if data.status == 0 and role==0 else '',
                               ])
    page = str(table)
    if role == 0:
        newbutton = Button('New', url_for('.modify'))
        page += '<div>'+newbutton+'</div>'
    
    return render_template("waddashboard/generic.html", title='Users', subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/users/modify', methods=['GET', 'POST'])
@login_required
def modify():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    role = 2
    if session.get('logged_in'):
        role = session.get('role')
        currentid = session.get('user_id')
    adminrole = (role==0)

    # invalid table request are to be ignored
    formtitle = 'Modify User'
    form = ModifyForm(None if request.method=="GET" else request.form, role=role)

    if form.gid.data is None or len(form.gid.data)==0:
        if _gid is None:
            if not role == 0: # only admin
                return redirect(url_for('waddashboard.home'))
            formtitle = 'New User'
            form.refresh.data = AUTO_REFRESH
            form.role.data = 2 # default to normal user
        else:
            user = WDUsers.get(WDUsers.id==_gid)
            if not (role<user.role or currentid == user.id): 
                # Only allowed modifications of lower accounts
                # normal user can only change their own accounts
                return redirect(url_for('waddashboard.home'))
    
            form.username.data = str(user.username)
            form.password.data = str(user.password)
            form.role.data = int(user.role)
            form.isenabled.data = (user.status == 1)
            form.nopassword.data = check_password_hash(user.password, str(""))
            form.email.data = str(user.email)
            form.refresh.data = user.refresh

            form.gid.data = _gid
            
    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            irole = int(form.role.data)
            pdata = str(form.password.data)
            pless = int(form.nopassword.data)
            if form.gid.data is None or len(form.gid.data)==0: # new user
                if irole<2:
                    if len(pdata)==0 or pless==1:
                        valid = False
                        flash("Empty password not allowed for {}".format(role_names[irole]), 'error')
                        
                # safety check: make sure the new role is not lower than the current user role
                if role>irole:
                    form.role.data = 2

                if valid:
                    if pless == 1:
                        pdata = str("")
                    WDUsers.create(username = str(form.username.data), 
                                password = generate_password_hash(pdata), 
                                email = str(form.email.data),
                                role = int(form.role.data),
                                refresh = int(form.refresh.data),
                                status = int(form.isenabled.data)
                            )
                    return render_template("waddashboard/generic.html", title='New User', msg="Successfully created new user {}.".format(str(form.username.data)))
            else:
                if irole<2:
                    if pless==1:
                        valid = False
                        flash("Empty password not allowed for {}".format(role_names[irole]), 'error')

                # safety check: make sure the new role is not lower than the current user role
                if role>irole:
                    form.role.data = 2

                if valid:
                    user = WDUsers.get(WDUsers.id==form.gid.data)
                    if pless==1:
                        pdata = str("")
                        user.password = generate_password_hash(pdata)
                    else:
                        if len(form.password.data)>0: #only change if not empty
                            user.password = generate_password_hash(str(form.password.data))

                    user.username = str(form.username.data)
                    user.email = str(form.email.data)
                    user.role = int(form.role.data)
                    user.refresh = int(form.refresh.data)
    
                    if session.get('logged_in') == True and session.get('user_id') == user.id:
                        session['refresh'] = user.refresh
    
                    if adminrole: # only admin can enable/disable
                        user.status = int(form.isenabled.data)
                    user.save()
                    flash('Changes applied successfully', 'error') # it is not an error, just used as a trigger for display
            
    return render_template("auth/usermanager_modify.html", form=form, admin=adminrole,
                           title=formtitle, msg='Fill out the fields and click Submit')


@mod_blueprint.route('/users/delete', methods=['GET', 'POST'])
@login_required
def delete():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role>0:
        return redirect(url_for('waddashboard.home'))
        
    user = WDUsers.get(WDUsers.id==_gid)
    user.delete_instance()

    return redirect(url_for('auth_users.default'))
    
