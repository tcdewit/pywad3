# Import Form
from flask_wtf import FlaskForm

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import IntegerField, StringField, PasswordField, HiddenField, SelectField, BooleanField, validators
try:
    from app.mod_waddashboard.models import role_names
except ImportError:
    from wad_dashboard.app.mod_waddashboard.models import role_names

role_choices = [ (k, v) for k,v in sorted(role_names.items()) if k>1]

class ModifyForm(FlaskForm):
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email    = StringField('Email Address', [validators.Length(min=6, max=35)])
    role     = SelectField("Role", choices = role_choices, coerce=int )
    refresh  = IntegerField('refresh (s)', [validators.NumberRange(min=1)])
    isenabled = BooleanField('Enabled')

    nopassword = BooleanField('No password')

    password = PasswordField('New Password', [
        #validators.DataRequired('Password is required'),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    gid = HiddenField('gid', [])
    
    confirm = PasswordField('Repeat Password')

    def __init__(self, *args, **kwargs):
        super(FlaskForm, self).__init__(*args, **kwargs)
        role_choices = [ (k, v) for k,v in sorted(role_names.items()) if k >= kwargs['role'] ]
        self.role.choices = role_choices
    