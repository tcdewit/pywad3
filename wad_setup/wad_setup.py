#!/usr/bin/env python
from __future__ import print_function
# -*- coding: iso-8859-1 -*-

__version__ = '20180710'

"""
Workflow:
 1. read json file with instructions
 2. do it!

Changelog:
  20180710: Missed some errors in actions; changes for docker; fix broken python36x; added support for multiple postgresql clusters
  20180706: Replace deprecated imp. move wad_setup.py and actions.py to scripts folder. errno no longer in os. 
            install python3-pip when running python3
  20180629: Added pip install -r requirements for base system
  20180209: Added virtualenv fix for systems with only python3 installed
  20170921: Added setuptools as requirements
  20170503: Added custom ports for PostgreSQL and Orthanc.
  20170418: Fixes for Windows
  20170314: New links for timeout fixed precompiled Orthanc; only store headers in Orthanc PostgreSQL (raw data on filesystem);
            added QR for WADQC; changed default name
  20170221: Move initial dependencies inside main
  20170209: Initial version
"""
import os
import sys
import argparse
import logging

from scripts import actions
from scripts.defaults import LOGGERNAME
from scripts.helpers import setup_logging
from scripts.addtoenv import addtoenv

def _exit(success):
    # shutdown logging, closing all file handles and flushing all output
    logging.shutdown()
    exit(success)

def check_install_pip():
    # check if pip ist installed. If not, install it now.
    try:
        import pip
    except ImportError:
        print("Installing pip:...")
        pkg = 'python-pip'
        if sys.version_info >= (3, 0): # python3
            pip = 'python3-pip'

        result, msg = actions.apt_install([pkg]) # this works only on debian/ubuntu systems
        if result == "ERROR":
            print("{}: {}".format(result, msg))
            exit(False)
        print("{}".format(result))

def check_install_modules():
    # check if required modules for this installer are installed. If not, do install them.
    try:
        import jsmin
        import simplejson as json
        import requests
    except ImportError:
        reqs = ['setuptools', 'jsmin', 'simplejson', 'requests']
        print("Installing packages ({}):...".format(', '.join(reqs)))
        result, msg = actions.pip_install(reqs)
        if result == "ERROR":
            print("{}: {}".format(result, msg))
            exit(False)
        print("{}".format(result))

        """
        Make sure the newly installed modules can be imported
        The proper way is to do:
        # rescan path to pick up new modules
        import site
        try:
            reload(site)
        except NameError as e: # moved to importlib in python3
            from importlib import reload
            try:
                reload(site)
            except TypeError as e: # try to fix broken python 3.6.x
                fix_python36x(site)
    
        but this is broken for python 3.6.x. It can be fixed by overriding site.abs_paths(),
        but a simpler fix is just adding the folder $HOME.local/lib/python3.6/site-packages
        """
        import site
        if site.ENABLE_USER_SITE: # not for virtualenv
            print("Making the just installed packages importable...")
            user_site = site.getusersitepackages()
            if not user_site in sys.path:
                sys.path.append(user_site)

def check_create_virtualenv(venvpath, **kwargs):
    # <venv_home>/<venv_name>/bin
    if not os.path.basename(venvpath) == 'bin':
        logger.error('virtualenv parameter should be of the form <venv_home>/<venv_name>/bin')
        _exit(False)
    venvname = os.path.basename(os.path.dirname(venvpath))
    venvhome = os.path.dirname(os.path.dirname(venvpath))
    if not os.path.exists(venvpath):
        logger.info("Creating a new virtualenv...")
        actions.create_virtualenv(venvname, venvhome, python3=True, activate_on_login=False, **kwargs)
        logger.info("Done.\nNow exit this shell, start a new one and execute\n  workon {}\nThen run wad_setup again to finish installation".format(venvname))
        exit()
    
    if 'VIRTUAL_ENV' in os.environ and not os.environ['VIRTUAL_ENV'] == os.path.dirname(venvpath):
        logger.error('Current virtualenv is {} but requested one is {}. \nExecute\n  workon {}\nand try again.'.format(os.path.basename(os.environ['VIRTUAL_ENV']), venvname, venvname))
        _exit(False)
    
    if not 'VIRTUAL_ENV' in os.environ:
        logger.error('Not running in requested virtualenv {}. \nExecute\n  workon {}\nand try again.'.format(venvname, venvname))
        _exit(False)
    
if __name__ == "__main__":
    # do not run as root! the script will ask for permission if it needs root
    if os.name == 'nt':
        pass
    elif os.geteuid() == 0:
        print("Do not run wad_setup as root! The script will ask you for root permission if it needs it! Exit.")    
        exit(False)

    # check if pip is installed, if not, install it through apt:
    check_install_pip()
    
    # check if packages for this setup script are available
    check_install_modules()

    # import (new) modules
    import jsmin
    import simplejson as json
    import requests
    
    setup_logging('INFO', LOGGERNAME, logfile_only=False)
    logger = logging.getLogger(LOGGERNAME)
    setupfile = 'recipes/dummy.json'#None
    parser = argparse.ArgumentParser(description='WAD Setup')
    parser.add_argument('-r','--recipe',
                        default=setupfile,
                        type=str,
                        help='the json file with setup instructions [{}].'.format(setupfile),
                        dest='setupfile')

    args = parser.parse_args()
    if args.setupfile is None or args.setupfile == setupfile:
        parser.print_help()
        _exit(False)

    # sanity checks 
    if not os.path.exists(args.setupfile):
        logger.error('Setup file "{}" does not exist. Exit.'.format(args.setupfile))
        _exit(False)
    try:
        with open(args.setupfile) as f:
            validjson = jsmin.jsmin(f.read()) # strip comments and stuff from more readable json
        setup = json.loads(validjson)

    except Exception as e:
        logger.error('Setup file "{}" is not a valid json file. {}, Exit.'.format(args.setupfile, str(e)))
        _exit(False)


    setup['global_params']['installation_root'] = os.path.abspath(
        os.path.expanduser(setup['global_params']['installation_root']))
    setup['global_params']['__recipe_path'] = os.path.abspath(
        os.path.expanduser(args.setupfile))
    setup['global_params']['__setup_folder'] = os.path.abspath(
        os.path.expanduser(os.path.dirname(os.path.realpath(__file__))))

    # make sure the given virtualenv exists
    if 'virtualenv' in setup['global_params'] and not setup['global_params']['virtualenv'].strip() == "":
        venvpath = os.path.abspath(
            os.path.expanduser(setup['global_params']['virtualenv']))
        setup['global_params']['virtualenv'] = venvpath
        check_create_virtualenv(venvpath, **setup['global_params'])

    # need to set installation_root as WADROOT now! Every script needs it!
    result2, msg2 = addtoenv( {'WADROOT':setup['global_params']['installation_root']} )

    logger.info('== Starting WAD Setup with recipe {} =='.format(args.setupfile))
    errors = 0
    for act in setup['actions']:
        if not hasattr(actions, act['cmd']):
            logger.error('Unknown command "{}". Skipping.'.format(act['cmd']))
            continue
        kwargs = act['kwargs']
        for k,v in setup['global_params'].items():
            if not k in kwargs.keys():
                kwargs[k] = v
        result, msg = getattr(actions, act['cmd'])(**act['kwargs'])
        logger.info('{}: {}. {}'.format(act['cmd'], result, msg)) if result == 'OK' else logger.error('{}: {}. {}'.format(act['cmd'], result, msg))
        if not result == "OK": 
            logger.error('Error: {}'.format(msg))
            errors += 1
            break

    if errors>0:
        logger.info('== {} Errors for WAD Setup with recipe {} =='.format(errors, args.setupfile))
        logger.info('Inspect {} for ERRORs. Rerun this recipe after fixing the ERRORS or close this shell and open a new one for the environment changes to take effect.'.format(LOGGERNAME))
    else:
        logger.info('== Successfully finished WAD Setup with recipe {} =='.format(args.setupfile))
        logger.info('Close this shell and open a new one for the environment changes to take effect.')
    _exit(True)

