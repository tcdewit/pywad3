function OnStableStudy(studyId, tags, metadata)
   if (metadata['ModifiedFrom'] == nil and
       metadata['AnonymizedFrom'] == nil) then

      print('This study is now stable: ' .. studyId)
      
      -- Call WAD_Collector
      os.execute('__WADROOT__/orthanc/lua/wadselector.py --source WADQC --studyid ' .. studyId .. ' --inifile __WADROOT__/WAD_QC/wadconfig.ini --logfile_only')
   end
end
