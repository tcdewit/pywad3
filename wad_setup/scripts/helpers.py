import os
import errno
import subprocess
import time
import logging
import logging.handlers
import sys
import platform

#----string/bytes conversion support for python2 and python3
import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

try:
    from scripts.defaults import LOGGERNAME
except:
    from defaults import LOGGERNAME

def setup_logging(levelname, loggername, logfile_only):
    # create logger to log to screen and to file
    #logging.basicConfig(format='%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=level)
    loglevel = getattr(logging, levelname)
    logfile = '{}.log'.format(loggername)

    logger = logging.getLogger(loggername)
    logger.setLevel(loglevel)

    # restrict handlers to one of each only
    has_filehandler   = False
    has_streamhandler = False
    for hand in logger.handlers:
        if isinstance(hand, logging.FileHandler):
            has_filehandler = True
        elif isinstance(hand, logging.StreamHandler):
            has_streamhandler = True
            
    if not has_filehandler:
        formatter = logging.Formatter('%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s')
        fh = logging.handlers.RotatingFileHandler(logfile, mode='a', maxBytes=2*1024*1024, backupCount=5) #append mode # max 6*2 MB
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    if not logfile_only and not has_streamhandler:
        formatter = logging.Formatter('[%(levelname)s]: %(message)s')
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)


def external_call(cmd, returnoutput=False, background=False, opt={}):
    """
    helper function to make system calls
    """
    result = 'OK'
    msg = ''
    try:
        if background:
            with open(os.devnull, "w") as f: # never pipe the output of a background process, as it will break eventually!
                proc = subprocess.Popen(cmd, stdout=f, stderr=f, close_fds=( not platform.system() == 'Windows' ), **opt)
                time.sleep(2)
                if proc.poll():
                    result = 'ERROR'
        else:
            # Now we can wait for the child to complete
            if opt.get('shell', False):
                cmd = ' '.join(cmd)
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **opt)
            (output, error) = proc.communicate()
            if returnoutput:
                if error: # this is true if any output to stderr is produced, e.g. by a numpy warning. 
                          #  to trigger only on real errors do if proc.returncode and returnoutput
                    msg = bytes_as_string(error.strip())
                    if 'Extracting templates from packages' in msg: # message from dpkg-preconfigure which is not an error
                        msg = bytes_as_string(output.strip())
                        result = 'OK'
                    else:
                        result = 'ERROR'
                else:
                    msg = bytes_as_string(output.strip())
                    result = 'OK'
            else:
                result ='OK' if proc.returncode==0 else 'ERROR'

    except OSError as e:
        if e.errno != errno.ENOENT:
            msg = str(e)
            result = 'ERROR'
        else:
            msg = str(e)
            result = 'ERROR'

    except subprocess.CalledProcessError as e:
        msg = str(e)
        result = 'ERROR'

    return result, msg

def apt_update(**kwargs):
    """
    Apt update
    """
    logger = logging.getLogger(LOGGERNAME)
    msg = 'Updating the system-wide packages list needs root permission. If root permissions are needed, you will be prompted for your password.'
    logger.info(msg)
    cmd = ['sudo', 'apt-get', 'update']
    return external_call(cmd, returnoutput=True)

def apt_install(pkgs, **kwargs):
    """
    Apt install pkgs
    kwargs must contain a list 'pkgs'
    """
    logger = logging.getLogger(LOGGERNAME)
    
    # make sure apt list is updated; prevents errors like packeges not found
    # actually this is only needed the very first time apt_install is requested.
    result, msg = apt_update(**kwargs) 
    if result == "ERROR":
        return result, msg
    
    msg = 'Installing system-wide packages needs root permission. If root permissions are needed, you will be prompted for your password.'
    logger.info(msg)
    cmd = ['sudo', 'apt-get', 'install', '-y']
    cmd.extend(pkgs)
    return external_call(cmd, returnoutput=True)

def yum_install(pkgs, **kwargs):
    """
    Yum install pkgs
    kwargs must contain a list 'pkgs'
    """
    logger = logging.getLogger(LOGGERNAME)
    msg = 'Installing system-wide packages needs root permission. If root permissions are needed, you will be prompted for your password.'
    logger.info(msg)
    cmd = ['sudo', 'yum', 'install', '-y']
    cmd.extend(pkgs)
    return external_call(cmd, returnoutput=True)

def get_latest_pkg(pkg):
    # helper to find the name of the latest version of the given package in a local folder
    import fnmatch

    folder = os.path.dirname(pkg)
    pattern = os.path.basename(pkg).replace('latest','*')
    
    matches = sorted(fnmatch.filter(os.listdir(folder), pattern))
    return os.path.join(folder, matches[-1])
    
def pip_install(pkglist, **kwargs):
    #pip install --upgrade ~/wadinstall/dist/wad_qc-0.1.0-py2.py3-none-any.whl
    if isinstance(pkglist, str):
        pkglist = [pkglist]

    logger = logging.getLogger(LOGGERNAME)
    pip = 'pip'
    if sys.version_info >= (3, 0): # python3
        pip = 'pip3'

    result, msg = ("OK", "")
    for pkg in pkglist:
        if 'latest' in pkg:
            pkg = get_latest_pkg(pkg)

        cmd = [pip, 'install']
        # check if we need to install in user's home
        if not "virtualenv" in kwargs.keys() or kwargs['virtualenv'].strip() == "": 
            if not 'VIRTUAL_ENV' in os.environ:
                cmd.append('--user') 
                if not '.local/bin' in os.environ['PATH']:
                    from scripts.addtoenv import addtoenv
                    addtoenv({'PATH': os.path.expanduser('~/.local/bin')})

        cmd.extend( ['--upgrade', pkg] )
        result, msg = external_call(cmd, returnoutput=True, background=False, opt={})
        if result == 'ERROR':
            if 'install --upgrade pip' in msg:
                pip_install(['pip'], **kwargs)
                result = "OK"
            elif 'entry deserialization failed' in msg:
                # ignore caching problems, just means that it will be redownloaded
                result = "OK"
            else:
                return result, msg

    return result, msg

def pip_install_requirements(**kwargs):
    """
    pip install -r requirements.txt
    This will create a replicated, tested base environment

    """
    logger = logging.getLogger(LOGGERNAME)
    pip = 'pip'
    requirements = 'requirements2.txt'
    
    if sys.version_info >= (3, 0): # python3
        pip = 'pip3'
        requirements = 'requirements3.txt'
    result, msg = ("OK", "")

    requirements = os.path.join(kwargs['__setup_folder'], requirements)

    cmd = [pip, 'install', '-r', requirements]

    # check if we need to install in user's home
    if not "virtualenv" in kwargs.keys() or kwargs['virtualenv'].strip() == "": 
        if not 'VIRTUAL_ENV' in os.environ:
            cmd.append('--user') 
            if not '.local/bin' in os.environ['PATH']:
                from scripts.addtoenv import addtoenv
                addtoenv({'PATH': os.path.expanduser('~/.local/bin')})

    result, msg = external_call(cmd, returnoutput=True, background=False, opt={})
    if result == 'ERROR':
        if 'pip install --upgrade pip' in msg:
            pip_install(['pip'], **kwargs)
            result = "OK"
        else:
            return result, msg

    return result, msg

def unpack_from_url(pkgurl, dstfolder, remove_old=False):
    """
    Download a file from the given url, and unpack in given destination folder, optionally remove the old contents.
    """
    import requests
    import tarfile #zipfile zipfile does not preserve permissions
    import shutil
    from io import BytesIO

    logger = logging.getLogger(LOGGERNAME)

    result, msg = ('OK', '')

    try:
        # get stream handle
        pkg = requests.get(pkgurl, stream=True)
        total_length = pkg.headers.get('content-length')

        if remove_old:
            # remove old installation
            if os.path.exists(dstfolder):
                shutil.rmtree(p, ignore_errors=True)

        with BytesIO() as f:
            if total_length is None: # no content length header
                f.write(pkg.content)
            else:
                dl = 0
                total_length = int(total_length)
                for data in pkg.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    logger.info("downloading... %.2f" % (100*dl/total_length))

            f.seek(0)
            zfile = tarfile.open(fileobj=f, mode='r:gz')
            logger.info('extracting package...')
            zfile.extractall(path=os.path.expanduser(dstfolder))

    except Exception as e:
        result = 'ERROR'
        msg = str(e)
        return result, msg

    return result, msg

def port_available(port):
    """
    Check if tcp port is in use
    """
    import socket

    logger = logging.getLogger(LOGGERNAME)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.bind(('localhost', port))
    except socket.error:
        return False

    s.close()

    return True
