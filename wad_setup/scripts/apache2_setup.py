import os
import sys
import logging
from .helpers import external_call, apt_install, yum_install
from .folders_settings import copy_replaces
from .which import which
from .defaults import LOGGERNAME
logger = logging.getLogger(LOGGERNAME)

"""
Output files for running flask websites in apache2
Will need sudo.
"""

def enable_apache2(mode, **kwargs):
    """
    Apache2 is not enabled by default
    """
    result,msg = ("OK","")
    
    #1. install apache2 and mods
    if sys.version_info >= (3, 0): # python3
        mod = 'libapache2-mod-wsgi-py3'
    else:
        mod = 'libapache2-mod-wsgi'

    result,msg = apt_install(['apache2', mod])

    if result == "ERROR":
        return result, msg

    #2. enable dormant apache2
    if mode == 'systemd': # default for Ubuntu 16.04 and later
        cmds = [
            ['sudo', 'systemctl', 'enable', 'apache2'],
            ['sudo', 'systemctl', 'start', 'apache2'],
        ]
    else:
        result = "ERROR"
        msg = "Only systemd is implemented right now!"
        return result, msg

    for cmd in cmds:
        result, msg = external_call(cmd, returnoutput=True, background=False)
        mustquit = (not result == "OK")
        if mustquit:
            if 'apache2.service is not a native service' in msg or 'Synchronizing state of apache2.service' in msg:
                result = "OK"
            else:
                errormsg = 'ERROR! Could not enable apache2 for {}! '.format(mode)
                return result, errormsg+msg
    
    return result, msg

def enable_httpd(mode, **kwargs):
    """
    Apache2 is httpd on CentOS 7
    httpd is not enabled by default
    """
    result,msg = ("OK","")
    
    #1. install httpd / apache2 and mods
    if sys.version_info >= (3, 0): # python3
        mod = 'mod_wsgi' # NOTE: not verified to work with python3!
    else:
        mod = 'mod_wsgi'

    result,msg = yum_install(['httpd', mod])

    if result == "ERROR":
        return result, msg

    #2. enable dormant apache2 / httpd
    if mode == 'systemd': # default for CentOS7
        cmds = [
            ['sudo', 'systemctl', 'enable', 'httpd'],
            ['sudo', 'systemctl', 'start', 'httpd']
        ]
    else:
        result = "ERROR"
        msg = "Only systemd is implemented right now!"
        return result, msg

    for cmd in cmds:
        result, msg = external_call(cmd, returnoutput=True, background=False)
        mustquit = (not result == "OK")
        if mustquit:
            if 'httpd.service is not a native service' in msg:
                result = "OK"
            else:
                errormsg = 'ERROR! Could not enable httpd (apache2) for {}! '.format(mode)
                return result, errormsg+msg
    
    return result, msg

def apache2_deploy_sites(sitelist, portlist, installation_root, **kwargs):
    #http://flask.pocoo.org/docs/0.12/deploying/mod_wsgi/
    import getpass
    user = getpass.getuser() # gets the name of the user running this shell

    result,msg = ("OK","")

    varpath = '/var/www/wadqc'
    etcpath = '/etc/apache2/sites-available'

    pos = { k:i for i,k in enumerate(sitelist)}

    # make sure var folder exists
    cmds = [['sudo', 'mkdir', '-p', varpath]]
    cmds.append(['sudo', 'a2dissite', '000-default']) # disable default
    
    
    if not "virtualenv" in kwargs.keys() or kwargs['virtualenv'].strip() == "": 
        venvbin = 'None' #Needs to be a string for replace!
    else:
        venvbin = '"{}"'.format(os.path.abspath(os.path.expanduser(kwargs['virtualenv']))) # note the quotes!

    if 'wad_admin' in sitelist:
        # create proper paths in wsgi
        xtra_paths = [ os.path.dirname(p) for p in [which('wadcontrol'), which('Orthanc'), which('pg_config')]]
        dest = os.path.join(installation_root, 'admin_wadqc.wsgi')
        inlist  = ['__WADROOT__',   '__XTRAPATHS__', '__VENVBIN__', '\\']
        outlist = [installation_root, str(xtra_paths),    venvbin,       '/']
        copy_replaces(src=os.path.join('scripts', 'templates', 'admin_wadqc.wsgi'), 
                      dest=dest, 
                      inlist=inlist, 
                      outlist=outlist) 

        cmds.append(['sudo', 'mv', dest, os.path.join(varpath,os.path.basename(dest))])

        # create proper paths in conf
        dest = os.path.join(installation_root, 'admin_wadqc.conf')
        inlist  = ['__USER__',   '__GROUP__', '__PORT__',            '\\']
        outlist = [user,          user,       str(portlist[pos['wad_admin']]),'/']
        copy_replaces(src=os.path.join('scripts', 'templates', 'admin_wadqc.conf'), 
                          dest=dest, 
                          inlist=inlist, 
                          outlist=outlist) 
        cmds.append(['sudo', 'mv', dest, os.path.join(etcpath,os.path.basename(dest))])

        cmds.append(['sudo', 'a2ensite', 'admin_wadqc'])
        
    if 'wad_dashboard' in sitelist:
        # create proper paths in wsgi
        dest = os.path.join(installation_root, 'dashboard_wadqc.wsgi')
        inlist  = ['__WADROOT__',   '__VENVBIN__', '\\']
        outlist = [installation_root, venvbin,     '/']
        copy_replaces(src=os.path.join('scripts', 'templates', 'dashboard_wadqc.wsgi'), 
                          dest=dest, 
                          inlist=inlist, 
                          outlist=outlist) 
    
        cmds.append(['sudo', 'mv', dest, os.path.join(varpath,os.path.basename(dest))])

        # create proper paths in conf
        dest = os.path.join(installation_root, 'dashboard_wadqc.conf')
        inlist  = ['__USER__',   '__GROUP__', '__PORT__',                 '\\']
        outlist = [user,          user,       str(portlist[pos['wad_dashboard']]),'/']
        copy_replaces(src=os.path.join('scripts', 'templates', 'dashboard_wadqc.conf'), 
                          dest=dest, 
                              inlist=inlist, 
                              outlist=outlist) 
        cmds.append(['sudo', 'mv', dest, os.path.join(etcpath,os.path.basename(dest))])

        cmds.append(['sudo', 'a2ensite', 'dashboard_wadqc'])

    cmds.append(['sudo', 'apachectl', 'restart']) # restart apache2 with updated site list


    for cmd in cmds:
        result, msg = external_call(cmd, returnoutput=True)
        mustquit = (not result == "OK")
        if mustquit:
            if 'Could not reliably determine the server' in msg:
                result = "OK"
                msg = ""
            else:
                errormsg = 'ERROR! Could not deploy_sites sites on apache2! '.format(cmd)
                return result, errormsg+msg

    return result, msg

def httpd_deploy_sites(sitelist, portlist, installation_root, **kwargs):
    #http://flask.pocoo.org/docs/0.12/deploying/mod_wsgi/
    import getpass
    user = getpass.getuser() # gets the name of the user running this shell

    result,msg = ("OK","")

    # CentOS doesn't have the a2ensite / a2dissite commands
    # just copy to /etc/httpd/conf.d instead
    varpath = '/var/www/wadqc'
    etcpath = '/etc/httpd/conf.d'

    pos = { k:i for i,k in enumerate(sitelist)}

    # make sure var folder exists
    cmds = [['sudo', 'mkdir', '-p', varpath]]
    
    if not "virtualenv" in kwargs.keys() or kwargs['virtualenv'].strip() == "": 
        venvbin = 'None' #Needs to be a string for replace!
    else:
        venvbin = '"{}"'.format(os.path.abspath(os.path.expanduser(kwargs['virtualenv']))) # note the quotes!

    if 'wad_admin' in sitelist:
        # create proper paths in wsgi
        xtra_paths = [ os.path.dirname(p) for p in [which('wadcontrol'), which('Orthanc'), which('pg_config')]]
        dest = os.path.join(installation_root, 'admin_wadqc.wsgi')
        inlist  = ['__WADROOT__',   '__XTRAPATHS__', '__VENVBIN__', '\\']
        outlist = [installation_root, str(xtra_paths),    venvbin,       '/']
        copy_replaces(src=os.path.join('scripts', 'templates', 'admin_wadqc.wsgi'), 
                      dest=dest, 
                      inlist=inlist, 
                      outlist=outlist) 

        cmds.append(['sudo', 'mv', dest, os.path.join(varpath,os.path.basename(dest))])

        # create proper paths in conf
        dest = os.path.join(installation_root, 'admin_wadqc.conf')
        inlist  = ['__USER__',   '__GROUP__', '__PORT__',            '\\']
        outlist = [user,          user,       str(portlist[pos['wad_admin']]),'/']
        copy_replaces(src=os.path.join('scripts', 'templates', 'admin_wadqc.conf'), 
                          dest=dest, 
                          inlist=inlist, 
                          outlist=outlist) 
        cmds.append(['sudo', 'mv', dest, os.path.join(etcpath,os.path.basename(dest))])
        # for CentOS mv doesn't work, even if permissions are set. Easy solution is to copy as root
        #cmds.append(['sudo', 'cp', dest, os.path.join(etcpath,os.path.basename(dest))])

    if 'wad_dashboard' in sitelist:
        # create proper paths in wsgi
        dest = os.path.join(installation_root, 'dashboard_wadqc.wsgi')
        inlist  = ['__WADROOT__',   '__VENVBIN__', '\\']
        outlist = [installation_root, venvbin,     '/']
        copy_replaces(src=os.path.join('scripts', 'templates', 'dashboard_wadqc.wsgi'), 
                          dest=dest, 
                          inlist=inlist, 
                          outlist=outlist) 
    
        cmds.append(['sudo', 'mv', dest, os.path.join(varpath,os.path.basename(dest))])

        # create proper paths in conf
        dest = os.path.join(installation_root, 'dashboard_wadqc.conf')
        inlist  = ['__USER__',   '__GROUP__', '__PORT__',                 '\\']
        outlist = [user,          user,       str(portlist[pos['wad_dashboard']]),'/']
        copy_replaces(src=os.path.join('scripts', 'templates', 'dashboard_wadqc.conf'), 
                          dest=dest, 
                              inlist=inlist, 
                              outlist=outlist) 
        cmds.append(['sudo', 'mv', dest, os.path.join(etcpath,os.path.basename(dest))])
        # for CentOS mv doesn't work, even if permissions are set. Easy solution is to copy as root
        #cmds.append(['sudo', 'cp', dest, os.path.join(etcpath,os.path.basename(dest))])

    cmds.append(['sudo', 'systemctl', 'restart', 'httpd']) # restart httpd with updated site list


    for cmd in cmds:
        result, msg = external_call(cmd, returnoutput=True)
        mustquit = (not result == "OK")
        if mustquit:
            if 'Could not reliably determine the server' in msg:
                result = "OK"
                msg = ""
            else:
                errormsg = 'ERROR! Could not deploy_sites sites on httpd! '.format(cmd)
                return result, errormsg+msg

    return result, msg
