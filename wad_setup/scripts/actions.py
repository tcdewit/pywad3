import os
import subprocess
import time
import logging
import sys
import errno

from . import helpers
from .defaults import LOGGERNAME

def apt_install(pkgs, **kwargs):
    # replace python-dev and friends by python3-dev etc when running python3
    if sys.version_info >= (3, 0): # python3
        pkgs = [p.replace('python-', 'python3-') for p in pkgs]
    
    return helpers.apt_install(pkgs, **kwargs)

def yum_install(pkgs, **kwargs):
    # replace python-dev and friends by python3-dev etc when running python3
    if sys.version_info >= (3, 0): # python3
        pkgs = [p.replace('python-', 'python3-') for p in pkgs]
    
    return helpers.yum_install(pkgs, **kwargs)

def pip_install(pkglist, **kwargs):
    return helpers.pip_install(pkglist, **kwargs)

def pip_install_requirements(**kwargs):
    return helpers.pip_install_requirements(**kwargs)

def create_folders_settings(**kwargs):
    """
    Create the WADQC Root folder, and all subfolders. Create settings files for WADQC services.
    """
    from . import folders_settings as fs

    logger = logging.getLogger(LOGGERNAME)

    result, msg = fs.create_folders(**kwargs)
    if result == "ERROR":
        return result, msg

    result, msg = fs.create_scripts(database='postgresql', **kwargs)
    if result == "ERROR":
        return result, msg

    return result, msg

def postgresql_install(source, **kwargs):
    """
    Install postgresql. Several options, depending on source
    """
    logger = logging.getLogger(LOGGERNAME)

    # check the requested port for postgresql
    pg_port = kwargs.get('pgsql_port', 5432)
    # not configurable for bigsql so use default port
    if source.startswith('bigsql'):
        pg_port = 5432
    if not helpers.port_available(pg_port):
        result = 'ERROR'
        msg = 'Requested postgresql port ({}) is not available!'.format(pg_port)
        return result, msg

    if source.startswith('bigsql'):
        """
        Install from BiqSQL
        """
        pgver = 'pg96'
        if source.endswith('95'):
            pgver = 'pg95'
        elif source.endswith('10'):
            pgver = 'pg10'

        # bigsql scripts are fixed for python3, no need for kludges
        bigsqldir = os.path.join(kwargs['installation_root'], 'bigsql')
        from .addtoenv import addtoenv
        addtoenv({'PATH': os.path.join(bigsqldir,pgver,'bin')})

        cmd = ['python', '-c', '"$(curl -fsSL http://s3.amazonaws.com/pgcentral/install.py)"']
        result, msg = helpers.external_call(cmd, returnoutput=True, background=False, opt={'cwd': kwargs['installation_root'], 'shell': True})
        if result == 'ERROR':
            return result, msg

        cmd = ['./pgc', 'install', pgver]
    
        result, msg = helpers.external_call(cmd, returnoutput=True, background=False, opt={'cwd': bigsqldir})
        if result == 'ERROR':
            return result, msg
        

    elif source == 'apt_systemd':
        # install postgresql from apt
        result, msg = helpers.apt_install(['postgresql'])
        if result == "ERROR":
            return result, msg
        
        import getpass
        user = getpass.getuser() # gets the name of the user running this shell

        if pg_port == 5432:
            # disable systemd service
            cmds = [
                ['sudo', 'usermod', '-a', '-G', 'postgres', user], # add wad user to the postgres group
                ['sudo', 'systemctl', 'stop', 'postgresql'],
                ['sudo', 'systemctl', 'disable', 'postgresql'], # no longer auto start
                ['sudo', 'systemctl', 'mask', 'postgresql'], # make it invisible
                ['sudo', 'chown', '-R', '{}:{}'.format(user,user), '/var/run/postgresql', '/var/log/postgresql'], # make sure wad user can write here initially
            ]
            for cmd in cmds:
                result, msg = helpers.external_call(cmd)
                if result == "ERROR":
                    return result, msg
            
    elif source == 'yum_systemd':
        # install postgresql from yum
        result, msg = helpers.yum_install(['postgresql-server'])
        if result == "ERROR":
            return result, msg
        
        import getpass
        user = getpass.getuser() # gets the name of the user running this shell

        if pg_port == 5432:
            # disable systemd service
            # --> service comes disabled upon install
            cmds = [
                ['sudo', 'usermod', '-a', '-G', 'postgres', user], # add wad user to the postgres group
                ['sudo', 'systemctl', 'mask', 'postgresql'], # make it invisible
                ['sudo', 'mkdir', '-p', '/var/log/postgresql'], # create log dir
                ['sudo', 'chown', '-R', '{}:{}'.format(user,user), '/var/run/postgresql', '/var/log/postgresql'], # make sure wad user can write here initially
            ]
            for cmd in cmds:
                result, msg = helpers.external_call(cmd)
                if result == "ERROR":
                    return result, msg
            
    else:
        raise ValueError('Unknown source {}'.format(source))
    return result, msg

def orthanc_install(source, **kwargs):
    logger = logging.getLogger(LOGGERNAME)

    if source.startswith('dropbox_'):
        if source.endswith('Lin64_Ubuntu1604'):
            #pkgurl = 'https://www.dropbox.com/s/a99subavuy79rpb/orthanc120fix_Lin64_Ubuntu1604.tar.gz?dl=1' #Orthanc 1.2.0 with dcmtk-3.6.0 time-out fix
            pkgurl = 'https://www.dropbox.com/s/2dvcqpf2m4v83ot/orthanc130fix_Lin64_Ubuntu1604.tar.gz?dl=1' #Orthanc 1.3.0 with dcmtk-3.6.2 threads fix and PostgreSQL 9.6.1 with boost 1.64.0 fix
        elif source.endswith('Lin64_Ubuntu1610'):
            #pkgurl = 'https://www.dropbox.com/s/lsmd7z22pkghdzr/orthanc_Lin64_Ubunto1610.tar.gz?dl=1'
            pkgurl = 'https://www.dropbox.com/s/0k9l5cn8ftw4nup/orthanc120fix_Lin64_Ubuntu1610.tar.gz?dl=1' #Orthanc 1.2.0 with dcmtk-3.6.0 time-out fix
        elif source.endswith('Lin64_Ubuntu1704'):
            #pkgurl = 'https://www.dropbox.com/s/g6erzp46fw0c3ux/orthanc120fix_Lin64_Ubuntu1704.tar.gz?dl=1' #Orthanc 1.2.0 with dcmtk-3.6.0 time-out fix
            pkgurl = 'https://www.dropbox.com/s/5zkrxx6k2qqbl5x/orthanc130fix_Lin64_Ubuntu1704.tar.gz?dl=1' #Orthanc 1.3.0 with dcmtk-3.6.2 threads fix and PostgreSQL 9.6.1 with boost 1.64.0 fix
        elif source.endswith('Lin64_Ubuntu1710'):
            pkgurl = 'https://www.dropbox.com/s/mcwlef1udm7txea/orthanc130fix_Lin64_Ubuntu1710.tar.gz?dl=1' #Orthanc 1.3.0 with dcmtk-3.6.2 threads fix and PostgreSQL 9.6.1 with boost 1.64.0 fix
        elif source.endswith('Lin64_CentOS7'):
            pkgurl = 'https://www.dropbox.com/s/ssdqnyce3ien7d6/orthanc130_Lin64_CentOS7.tar.gz?dl=1' #Orthanc 1.3.0
        else:
            raise ValueError('Unknown version {}'.format(source))
        
        dstfolder = kwargs['installation_root']
        from .addtoenv import addtoenv
        addtoenv({'PATH': os.path.join(dstfolder, 'orthanc', 'bin')})

        result,msg = helpers.unpack_from_url(pkgurl, dstfolder, remove_old=False)
        if result == 'ERROR':
            return result, msg

        
    elif source == 'apt_systemd':
        # install orthanc from apt
        pkgs = ['orthanc']

        # find out if repo has 'orthanc-postgresql'
        psq_pkg = 'orthanc-postgresql'
        cmd = ['apt-cache', 'search', psq_pkg]
        result, msg = helpers.external_call(cmd, returnoutput=True)
        if msg.strip().startswith(psq_pkg):
            pkgs.append(psq_pkg)

        result, msg = helpers.apt_install(pkgs)
        if result == "ERROR":
            return result, msg
        
        # disable systemd service
        cmds = [
            ['sudo', 'systemctl', 'stop', 'orthanc'],
            ['sudo', 'systemctl', 'disable', 'orthanc'], # no longer auto start
            ['sudo', 'systemctl', 'mask', 'orthanc'], # make it invisible
        ]
        for cmd in cmds:
            result, msg = helpers.external_call(cmd)
            if result == "ERROR":
                return result, msg
            
    else:
        raise ValueError('Unknown source {}'.format(source))

    return result, msg


def create_postgresql_datadir(installation_root, **kwargs):
    """
    If PostgreSQL is installed from a standard repository (Ubuntu apt-get) this step is not needed.
    When BigSQL is used, a root PostgreSQL datadir is not created automatically, and this step is
    needed before create_databases databases can be called.
    
    create_folders_settings must have been run first
    """
    from . import database_setup as ds
    pgport = kwargs.get('pgsql_port', 5432)
    result, msg = ds.create_postgresql_datadir(installation_root, pgport) # create_folders_settings must have been run first

    return result, msg
    
def create_databases(installation_root, **kwargs):
    """
    If PostgreSQL is installed from a standard repository (Ubuntu apt-get) this step is not needed.
    When BigSQL is used, a root PostgreSQL datadir is not created automatically, and this step is
    needed before create_databases databases can be called.
    
    create_folders_settings must have been run first
    """
    from . import database_setup as ds
    pgport = kwargs.get('pgsql_port', 5432)
    result, msg = ds.create_databases(installation_root, pgport) # create_folders_settings must have been run first

    return result, msg
    

def initialize_wadqc(installation_root, **kwargs):
    """
    initialize database
    
    create_folders_settings must have been run first
    """
    from . import database_setup as ds
    result, msg = ds.initialize_wadqc(installation_root) # create_folders_settings must have been run first

    return result, msg

def enable_apache2(mode, **kwargs):
    """
    Setup apache2 for given init mode; right now only systemd
    """
    from . import apache2_setup as act

    logger = logging.getLogger(LOGGERNAME)
    result, msg = ("OK", "")

    result, msg = act.enable_apache2(mode, **kwargs)

    return result, msg

def enable_httpd(mode, **kwargs):
    """
    Setup apache2/httpd for given init mode; right now only systemd
    """
    from . import apache2_setup as act

    logger = logging.getLogger(LOGGERNAME)
    result, msg = ("OK", "")

    result, msg = act.enable_httpd(mode, **kwargs)

    return result, msg

def apache2_deploy_sites(sitelist, portlist, installation_root, **kwargs):
    """
    Deploy sitelist on apache2. Make sure apache2 is enabled before running this action
    """
    from . import apache2_setup as act

    logger = logging.getLogger(LOGGERNAME)
    result, msg = ("OK", "")

    result, msg = act.apache2_deploy_sites(sitelist, portlist, installation_root, **kwargs)

    return result, msg

def httpd_deploy_sites(sitelist, portlist, installation_root, **kwargs):
    """
    Deploy sitelist on httpd. Make sure httpd is enabled before running this action
    """
    from . import apache2_setup as act

    logger = logging.getLogger(LOGGERNAME)
    result, msg = ("OK", "")

    result, msg = act.httpd_deploy_sites(sitelist, portlist, installation_root, **kwargs)

    return result, msg

def create_start_systemd(service, installation_root, **kwargs):
    """
    Create a systemd startup script for given service, and startup now
    """
    from . import systemd_setup as act

    logger = logging.getLogger(LOGGERNAME)
    result, msg = ("OK", "")

    result, msg = act.create_start_systemd(service, installation_root, **kwargs)

    return result, msg

def create_virtualenv(name, workon_home, python3, activate_on_login, **kwargs):
    """
    Install requirements for virtualenv, create Envs home, make an environment named <name> of python3 (True)
    """
    from .addtoenv import addtoenv, addtobash, removefrombash
    from .which import which

    result, msg = ("OK", "")

    # cannot run this script from within a virtualenv
    if 'VIRTUAL_ENV' in os.environ:
        result = "ERROR"
        msg = "Cannot create a virtualenv from within a virtualenv. Run this script outside a virtualenv (deactivate first)"
        return result, msg
    
    # pip install packages
    result, msg = helpers.pip_install(['virtualenv', 'virtualenvwrapper'])
    if not result == "OK":
        return result, msg
        
    # add .local/bin to PATH if not already added
    if not '.local/bin' in os.environ['PATH']:
        addtoenv({'PATH': os.path.expanduser('~/.local/bin')})

    # setup virtualenv stuff
    workon_home = os.path.abspath(os.path.expanduser(workon_home))
    addtoenv({'WORKON_HOME': workon_home})

    if which('python') is None: # fix for installations with only python3
        addtoenv({'VIRTUALENVWRAPPER_PYTHON': which('python3')})
             
    line = 'source $HOME/.local/bin/virtualenvwrapper.sh'

    if activate_on_login:
        line = '{}\nworkon {}'.format(line, name)
    else:
        removefrombash('workon {}'.format(name))
    addtobash(line)
        
    if python3:
        pyexe = which('python3')
    else:
        pyexe = which('python')

    # execute make a virtualenv
    try:
        os.makedirs(kwargs['installation_root'])
    except OSError as e: 
        if e.errno == errno.EEXIST and os.path.isdir(kwargs['installation_root']):
            pass
        else:
            result = 'ERROR'
            msg = str(e)

    # just make it easier and execute a bash
    dest = os.path.join(kwargs['installation_root'], "mkv.sh")
    with open(dest, 'w') as f:
        if which('python') is None: # fix for installations with only python3
            f.write('export VIRTUALENVWRAPPER_PYTHON={}\n'.format(which('python3')))
        f.write('source $HOME/.local/bin/virtualenvwrapper.sh\n')
        f.write('mkvirtualenv {} --no-site-packages --python={}'.format(name, pyexe))

    bexe = which('bash') # must be bash for 'source' to work!
    result, msg = helpers.external_call([bexe, dest], returnoutput=True)
    os.remove(dest)
    return result, msg
