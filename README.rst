WAD-QC 2.0: version 2 of the WAD-QC server written in python
============================================================

An open source implementation in python of the `WAD-QC 
Server <https://github.com/wadqc>`_: a server for automated analysis of 
medical images for quality control, by the `Society for Medical Physics of the Netherlands 
(NVKF) <http://www.nvkf.nl>`_. 

----

This project contains:
 * wad_core: processor, selector, and analyser
 * wad_qc: module interfaces and APIs for IO of database (dbio) and PACS (pacsio)
 * wad_admin: command line and web interface for administration of the WAD Server
 * wad_dashboard: web interface for reporting of analysis results
 * wad_setup: command line installer
 * Dox: documentation of the WAD Server
 * Tools (for creating uploadable analysis modules and for creating config files for analysis modules)
 * Tests (of the core functionality)
 * OrthancBuild (scripts for building a local, static `Orthanc <http://www.orthanc-server.com>`_)
 
This project does **not** contain any analysis modules. Most python analysis modules from WAD-QC 1.0 have 
been ported to WAD-QC 2.0 already, so please ask for read access to the new analysis modules if you need them.


Current status is **beta**. Major changes after version 0.6.2 will be highlighted in the changelog on the `Wiki <https://bitbucket.org/rvrooij/pywad3/wiki>`_.

At present pacsio only supports `Orthanc <http://www.orthanc-server.com>`_.

Only the SQLite and the PostgreSQL backends for the WAD-QC database and `Orthanc <http://www.orthanc-server.com>`_ have been tested.

----
 
For installation of the WAD-QC Server on Ubuntu 16.x, use the script in wad_setup with a recipe from wad_setup/recipes, or follow the manual installation instructions from the `Wiki <https://bitbucket.org/rvrooij/pywad3/wiki>`_.

For installation on Windows, follow the instructions in Dox/installation_windows.txt.