import os
from peewee import SqliteDatabase, PostgresqlDatabase, Model, CharField, IntegerField, BooleanField, ForeignKeyField, BlobField, DateTimeField, DoubleField, OperationalError, Check
from datetime import datetime

import logging
import stat
import zipfile
import shutil
import tempfile

db = SqliteDatabase(None)

def retry(times, func, *args, **kwargs):
    for n in range(times):
        try:
            return func(*args, **kwargs)
        except OperationalError as e:
            logging.error('Database locked, will try again later.')
            
    return func(*args, **kwargs)


class DBModel(Model):
    class Meta:
        database = db

    def save(self, *args, **kwargs):
        return retry(10, super(DBModel, self).save, *args, **kwargs)
                
    @classmethod
    def get_by_name(cls, name):
        # convenience function
        return cls.get(cls.name == name) if hasattr(cls, 'name') else None

    @classmethod
    def get_by_id(cls, id):
        # convenience function
        return cls.get(cls.id == id)
    

class DBVariables(DBModel):
    # define fields
    name = CharField(unique=True, max_length=64) # identifier
    val  = CharField(max_length=512)
    
class DBSourceTypes(DBModel):
    # define fields
    name = CharField(unique=True, max_length=64) # identifier

class DBDataTypes(DBModel):
    # define fields
    name = CharField(unique=True, max_length=50) # identifier


class DBDataSources(DBModel):
    # define fields
    name       = CharField(unique=True, max_length=64) # identifier
    source_type = ForeignKeyField(DBSourceTypes, backref='datasources') 
    protocol   = CharField(max_length=64) # access protocol for credentials below 'http', 'https', 'samba', ...
    aetitle    = CharField(max_length=64, null=True) # for dicomqr
    host       = CharField(max_length=250) # 'localhost', '127.0.0.1', ...
    port       = IntegerField(null=True) # access port for protocol above: 8042, 11112, ...
    user       = CharField(max_length=250, null=True) 
    pswd       = CharField(max_length=250, null=True) 
    
    @classmethod
    def create(cls, *args, **kwargs):
        if 'typename' in kwargs and 'source_type' not in kwargs:
            kwargs['source_type'] = DBSourceTypes.get(DBSourceTypes.name == kwargs['typename'])
        return super(DBDataSources, cls).create(*args, **kwargs)

    def as_dict(self):
        fields = [f for f in DBDataSources._meta.sorted_field_names]
        field_dict = {field: getattr(self, field) for field in fields}
        field_dict['typename'] = self.source_type.name
        return field_dict
    
class DBModules(DBModel):
    # define fields
    name        = CharField(unique=True, max_length=250) # identifier
    description = CharField(max_length=250) 
    filename    = CharField(max_length=250) # path to executable relative to root/Module/name
    foldername  = CharField(max_length=250, unique=True) # what is the length of the tempfolder dir name?
    origin      = CharField(max_length=32, default='user') # either factory or user
    repo_version = CharField(max_length=32, default="" )  # if installed from repo
    repo_url     = CharField(max_length=250, default=""  ) # if installed from repo
    
    def delete_instance(self, *args, **kwargs):
        # also delete uploaded folder
        module_dir = DBVariables.get(DBVariables.name=='modules_dir').val
        # remove the folder if modifying an existing module
        shutil.rmtree(os.path.join(module_dir, self.foldername), ignore_errors=True)
        return super(DBModules, self).delete_instance(*args, **kwargs)
    
    @classmethod
    def create(cls, *args, **kwargs):
        # this dstfolder should be unique, and can only exist if it is coupled to the current pk
        # however, since we are deleting folders, we need to check if there is not another reference to it.
        if len(DBModules.select().where(DBModules.name == kwargs['name']))>0:
            raise ValueError('Uniqueness constraint failed on module name %s'%kwargs['name'])
 
        # create unique foldername
        module_dir = DBVariables.get(DBVariables.name=='modules_dir').val
        if not os.path.exists(module_dir):
            os.makedirs(module_dir)
        kwargs['foldername'] = os.path.basename(tempfile.mkdtemp(dir=module_dir,prefix='mod'))
        
        module = super(DBModules, cls).create(*args, **kwargs)
        if 'uploadfilepath' in kwargs:
            module.upload_module(kwargs['uploadfilepath'])
        
        return module

    def save(self, *args, **kwargs):
        if 'uploadfilepath' in kwargs:
            self.upload_module(kwargs['uploadfilepath'])
            del kwargs['uploadfilepath']
        return super(DBModules, self).save(*args, **kwargs)

    def upload_module(self, uploadfilepath):
        """
        mechanism to upload new or revised module. After uploading, a call to dbio is needed for inclusion in the wad db
        """
        module_dir = DBVariables.get(DBVariables.name == 'modules_dir').val
        # a module is always placed in a subfolder with the name 'modulename'
        dstfolder = os.path.join(module_dir, self.foldername)
        
        # remove the folder if modifying an existing module
        if os.path.exists(dstfolder):
            shutil.rmtree(dstfolder, ignore_errors=True)
    
        # create the dstfolder (no error checking, since we just deleted it)
        os.makedirs(dstfolder)
    
        # if the uploaded file is a zip file, unpack it
        ext = os.path.splitext(os.path.basename(uploadfilepath))[1]
        if ext.lower() == '.zip':
            # unzip uploaded zip to destination folder and remove zip file
            with zipfile.ZipFile(uploadfilepath, 'r') as z:
                z.extractall(dstfolder)
            exename = os.path.join(dstfolder,self.filename)
        else: # not a zipfile, take upload as is
            # destination file
            exename = os.path.join(dstfolder, os.path.basename(self.filename))
            # copy uploaded file to proper location
            shutil.copyfile(uploadfilepath, exename)
    
        try: # make module executable
            os.chmod(exename, os.stat(exename).st_mode | stat.S_IEXEC)
        except Exception as e:
            logging.debug('cannot make %s executable'%exename)
        

class DBMetaConfigs(DBModel):
    # define fields
    val           = BlobField(default='{"results": {}}')

class DBModuleConfigs(DBModel):
    # define fields
    name          = CharField(max_length=250)
    description   = CharField(max_length=250) 
    module        = ForeignKeyField(DBModules, backref='module_configs') # backref is for reference from DBModules
    data_type     = ForeignKeyField(DBDataTypes, backref='module_configs') 
    val           = BlobField()
    meta          = ForeignKeyField(DBMetaConfigs, backref='module_configs') # display stuff, limits, ...
    origin        = CharField(max_length=32, default='user') # either factory or user or result
    
    @classmethod
    def getConfigsByName(cls, name):
        return list(cls.select().where(cls.name == name).order_by(cls.name))
    
    @classmethod
    def create(cls, *args, **kwargs): # for a new moduleconfig, make sure to supply a new meta as well!
        if 'datatypename' in kwargs and 'data_type' not in kwargs:
            kwargs['data_type'] = DBDataTypes.get(DBDataTypes.name == kwargs['datatypename'])
        if 'modulename' in kwargs and 'module' not in kwargs:
            kwargs['module'] = DBModules.get(DBModules.name == kwargs['modulename'])
        return super(DBModuleConfigs, cls).create(*args, **kwargs)
    
    def clone(self):
        fields = [f for f in DBModuleConfigs._meta.sorted_field_names if f is not 'id']
        field_dict = {field: getattr(self, field) for field in fields}
        nw_meta = DBMetaConfigs.create(val=self.meta.val)
        field_dict['meta'] = nw_meta.id
        return DBModuleConfigs.create(**field_dict)

    def delete_instance(self, *args, **kwargs):
        # also delete connected meta
        mta = self.meta
        res = super(DBModuleConfigs, self).delete_instance(*args, **kwargs)
        if len(mta.module_configs) == 0: # only if no references left
            mta.delete_instance(recursive=False)
        return res


class DBSelectors(DBModel):
    # define fields
    name              = CharField(unique=True, max_length=250) # identifier
    description       = CharField(max_length=250) 
    isactive          = BooleanField(default=False)
    module_config     = ForeignKeyField(DBModuleConfigs, backref='selectors') 
    
    @classmethod
    def create(cls, *args, **kwargs):
        if 'configname' in kwargs and 'module_config' not in kwargs:
            kwargs['module_config'] = DBModuleConfigs.get(DBModuleConfigs.name == kwargs['configname'])
        #copy base config, but supply a new meta
        cfg = DBModuleConfigs.get(DBModuleConfigs.id == kwargs['module_config'])
        fields = [f for f in DBModuleConfigs._meta.sorted_field_names if not f == 'id']
        field_dict = {field: getattr(cfg, field) for field in fields}
        nw_meta = DBMetaConfigs.create(val=cfg.meta.val)
        field_dict['meta'] = nw_meta.id
        nw_cfg = DBModuleConfigs.create(**field_dict)
        nw_cfg.origin = 'result' # flag it, so it is possible to distinguish between user/factory and old coupled configs
        nw_cfg.save()
        
        kwargs['module_config'] = nw_cfg
        return super(DBSelectors, cls).create(*args, **kwargs)
    
    def addRule(self, dicomtag, logicname, values):
        rule = DBSelectorRules.create(selector=self, dicomtag=dicomtag, logic=DBSelectorLogics.get(DBSelectorLogics.name == logicname))
        for val in values:
            DBRuleValues.create(rule=rule, val=val)
        return rule

    def clone(self):
        #copy fields
        fields = [f for f in DBSelectors._meta.sorted_field_names if f is not 'id']
        field_dict = {field: getattr(self, field) for field in fields}

        # make selector name unique
        existing_names = [m.name for m in DBSelectors.select()]
        i = 0
        while 'copy_%d of %s'%(i,self.name) in existing_names:
            i += 1
        field_dict['name'] = 'copy_%d of %s'%(i,self.name)

        # make selector
        nw_sel = DBSelectors.create(**field_dict)
        
        # copy rules
        for rule in self.rules:
            values = [v.val for v in rule.values]
            nw_sel.addRule(rule.dicomtag, rule.logic.name, values)

        return nw_sel


class DBSelectorLogics(DBModel):
    # define fields
    name = CharField(unique=True, max_length=50) # identifier


class DBSelectorRules(DBModel):
    # define fields
    dicomtag          = CharField(max_length=20) 
    selector          = ForeignKeyField(DBSelectors, backref='rules') 
    logic             = ForeignKeyField(DBSelectorLogics, backref='rules') 


class DBRuleValues(DBModel):
    # define fields
    val               = CharField(max_length=250) 
    rule              = ForeignKeyField(DBSelectorRules, backref='values') 


class DBProcessStatus(DBModel):
    # define fields
    name = CharField(unique=True, max_length=250) # identifier


class DBProcesses(DBModel):
    # define fields
    selector          = ForeignKeyField(DBSelectors, backref='processes')
    module_config     = ForeignKeyField(DBModuleConfigs, backref='processes')
    data_id           = CharField(max_length=64) # study_id or series_id or instance_id
    process_status    = ForeignKeyField(DBProcessStatus, backref='processes')
    data_source       = ForeignKeyField(DBDataSources, backref='processes')
    created_time      = DateTimeField()
    process_log       = BlobField(null=True)

    @classmethod
    def getProcessesByStatus(cls, status):
        if isinstance(status, str):
            status = [status]
        return cls.select().join(DBProcessStatus).where(DBProcessStatus.name.in_(status))
        
    @classmethod
    def create(cls, *args, **kwargs):
        if 'selectorname' in kwargs and 'selector' not in kwargs:
            kwargs['selector'] = DBSelectors.get(DBSelectors.name == kwargs['selectorname'])
        if 'processstatusname' in kwargs and 'process_status' not in kwargs:
            kwargs['process_status'] = DBProcessStatus.get(DBProcessStatus.name == kwargs['processstatusname'])
        if 'created_time' not in kwargs:
            kwargs['created_time'] = datetime.now()
        if 'datasourcename' in kwargs and 'data_source' not in kwargs:
            kwargs['data_source'] = DBDataSources.get(DBDataSources.name == kwargs['datasourcename'])
            
        kwargs['module_config'] = kwargs['selector'].module_config
        
        return super(DBProcesses, cls).create(*args, **kwargs)

class DBResults(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    selector          = ForeignKeyField(DBSelectors, backref='results')
    module_config     = ForeignKeyField(DBModuleConfigs, backref='results') 
    data_id           = CharField(max_length=64) # study_id or series_id or instance_id
    data_source       = ForeignKeyField(DBDataSources, backref='results')
    created_time      = DateTimeField()
    process_log       = BlobField(null=True)
    
    @classmethod
    def finishedProcess(cls, process):
        fields = [f for f in DBResults._meta.sorted_field_names if f is not 'id']
        field_dict = {field: getattr(process, field) for field in fields}
        return super(DBResults, cls).create(**field_dict)
    
    
    def getResults(self):
        result_fields = ['strings', 'floats', 'bools', 'objects', 'datetimes']
        all_results = [x for field in result_fields for x in getattr(self, field)]
        return all_results


class DBResultDateTimes(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='datetimes') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    val             = DateTimeField()
    val_equal       = DateTimeField(null=True)
    val_period      = IntegerField(null=True) # days between results (intended as acq interval)


class DBResultStrings(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='strings') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    val             = CharField(max_length=100)
    val_equal       = CharField(max_length=100, null=True)


class DBResultFloats(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='floats') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    val             = DoubleField()
    val_equal       = DoubleField(null=True) 
    val_min         = DoubleField(null=True) 
    val_max         = DoubleField(null=True) 
    val_low         = DoubleField(null=True) 
    val_high        = DoubleField(null=True)
    val_ref         = DoubleField(null=True)


class DBResultBools(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='bools') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    val             = BooleanField()
    val_equal       = BooleanField(null=True) 


class DBResultObjects(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='objects') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    filetype        = CharField(max_length=10) 
    val             = BlobField()


DBTables = [
    DBVariables,
    DBSourceTypes,
    DBDataSources,
    DBModules,
    DBMetaConfigs,
    DBModuleConfigs, 
    DBDataTypes,
    DBSelectors,
    DBSelectorLogics,
    DBSelectorRules, 
    DBRuleValues, 
    DBProcessStatus,
    DBProcesses,
    DBResults, 
    DBResultDateTimes, 
    DBResultStrings, 
    DBResultFloats,
    DBResultBools, 
    DBResultObjects
]
