import os
import logging
import shutil

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

from .dbio_models import *

"""
Only update the versionnumber if the database (tables, fields) changes.
Use versionnumber as "20160531" or "20171020.1", indicating the date as the major versionnumber,
 and an additional point release if needed.
"""
__version__ = '20180316' # only update this versionnumber if the database (tables, fields) changes and then write upgrade script!

"""
Changelog:
  20180417: only auto delete of coupled meta on delete of config, if no more configs can refer to same meta. cfg clone: create nw meta
  20180316: added repo info to DBModules
  20180209: added val_ref to DBResultFloats for relative constraints
  20180131: peewee3 no longer supports set_autocommit and truncate_tables
  20171020: DBModuleConfigs.origin now one of ['factory', 'user', 'result']
  20170428: separate table for meta; 
  20160906: added clone to dbselectors
  20160705: corrected selector creation: first copy config, then create selector; 
            removed redundant field data_type from results and processes
  20160623: changed module subfolder to module.id instead of module.name; removed changename
  20160622: added clone() and blobfield meta to dbmoduleconfigs; fixed modulepath renaming on changename
  20160620: removed quantity and units: now part of config blob
  20160610: moved data_type to config; removed frequency and instead added DBResultsDataTime
  20160609: added 'origin' field to modules and configs, to distinguish between user made stuff and factory stuff
  20160606: configmodule without versionnumber; frequency moved to configmodule; 
            dbconfig only with db credentials; db_create with dbconfig and other
  20160531: restructured; merge from db_tables and dbio; added upload_modules; 
    multiple data_sources;  removed to_field reference because that replaces an reference to an object with 
    a copy of the contents of that field
  20160520: replace pydal with peewee
"""


default_content = {
    # wadqc_versions is special: it has names and values
    DBVariables: [
        ('iqc_db_version',__version__),
    ],
    DBSourceTypes: [
        (1, 'orthanc')
    ],
    DBSelectorLogics:[
        (1, 'equals'),
        (2, 'contains'),
        (3, 'starts with'),
        (4, 'ends with'),
        (5, 'is empty'),
        (6, 'not equals'),
        (7, 'not is empty'),
        (8, 'not contains'),
    ],
    DBProcessStatus:[
        (1, 'new'),
        (2, 'queued'),
        (3, 'busy'),
        (4, 'waiting for input'),
        (5, 'finished'),
        (6, 'module error'),
        (7, 'analyser failed'),
    ],
    DBDataTypes:[
        (1, 'dcm_series'),
        (2, 'dcm_study'),
        (3, 'dcm_instance'),
        (4, 'dcm_patient'),
    ],
}

def _init_db(dbconfig):
    dbtype = dbconfig['TYPE']
    global db
    if dbtype == 'postgresql':
        db = PostgresqlDatabase(dbconfig['DBASE'], user=dbconfig['USER'], password=dbconfig['PSWD'], host=dbconfig['HOST'], port=dbconfig['PORT'])
    elif dbtype == 'sqlite': 
        dbname = os.path.abspath(os.path.expanduser(dbconfig['DBASE']))
        db = SqliteDatabase(dbname, threadlocals=True)
    else:
        raise(NotImplementedError('%s not implemented'%dbtype))
    
    # We need this kludge cause BlobField is broken with dbproxy
    for tb in DBTables:
        tb._meta.database = db
    
def db_create_only(inifile, setupfile):
    dbconfig = get_dict_from_inifile(inifile)['iqc-db']
    dbvariables = get_variables_dict_from_inifile(setupfile)
    _init_db(dbconfig)
    db.connect()
    db.create_tables(DBTables)
    _defaultcontent(dbvariables)
    db.close()
    
def db_connect(inifile):
    dbconfig = get_dict_from_inifile(inifile)['iqc-db']
    _init_db(dbconfig)
    db.connect()
    return db

def db_truncate(setupfile):
    # prepare for restart, discarding all old stuff
    dbvariables = get_variables_dict_from_inifile(setupfile)
    try:
        oldroot = DBVariables.get(DBVariables.name == 'modules_dir').val
        shutil.rmtree(oldroot, ignore_errors=True)
    except DBVariables.DoesNotExist:
        logging.debug('truncate called on db without proper Variables table')
        
    with db.atomic() as txn:
        # truncate_tables not supported in peewee3
        # db.truncate_tables(DBTables, cascade=True)
        db.drop_tables(DBTables, safe=True, cascade=True)
        db.create_tables(DBTables)
    _defaultcontent(dbvariables) # fill with basic stuff

def _defaultcontent(dbvariables):
    # Fill tables with default content where applicable
    # to be called only from db_create_only and db_truncate
    with db.atomic() as txn:
        for model, data in default_content.items():
            if model.select().count() == 0:
                if model == DBVariables:
                    for n, v in data:
                        model.create(name = n, val = v)
                else:
                    for i, rel in data:
                        model.create(name = rel, id = i)
        # set wadqcroot variable
        for key, value in dbvariables.items():
            if key == 'sources':
                # add a datasource
                for source in value:
                    DBDataSources.create(**source)
            elif key == 'wadqcroot':
                DBVariables.create(name='wadqcroot', val=value)
                DBVariables.create(name='modules_dir', val=os.path.join(value, 'Modules'))
                DBVariables.create(name='temp_dir', val=os.path.join(value, 'Temp'))
            else:
                DBVariables.create(name=key, val=value)


# --- initialization helpers
#-----------------------
#----config file helpers
def get_dict_from_inifile(inifile):
    # only needed if the class is initialized with a inifile instead of a dict
    # read the inputconfig as a config, and cast it into a dict
    config = configparser.SafeConfigParser()
    with open(inifile,'r') as f:
        config.readfp(f)

    config_dict = {}
    for section in config.sections():
        config_dict[section] = {}
        for option in config.options(section):
            config_dict[section][option.upper()] = config.get(section, option)
    return config_dict

def get_variables_dict_from_inifile(setupfile):
    results = {}

    # turn ini file into dict
    variables = get_dict_from_inifile(setupfile)

    # wadqcroot
    results['wadqcroot'] = os.path.abspath(os.path.expanduser(variables['iqc-storage']['WADQCROOT']))

    # datasouces, ordered on section-number
    sources_dicts = {}
    for k, v in variables.items():
        if k.startswith('iqc-source-'):
            key = int(k[len('iqc-source-'):])
            sources_dicts[key] = {kk.lower():vv for kk,vv in v.items()} # turn capitals in key names into lowercase
        
    results['sources'] = [sources_dicts[k] for k in sorted(sources_dicts.keys())]
    
    # iqc-taskmanager
    results['processor_interval'] = variables['iqc-processor']['INTERVAL'] # seconds
    results['processor_workers'] = variables['iqc-processor']['WORKERS']
    results['processor_timeout'] = variables['iqc-processor']['TIMEOUT'] # seconds
    
    # logging
    results['iqc_logging'] = variables['iqc-debug']['VERBOSE']
    # verbose defines the amount of print statements in core components. Choose: DEBUG, INFO, WARNING, ERROR, CRITICAL
    
    return results
