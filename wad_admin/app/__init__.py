# Import flask and template operators
from flask import Flask, render_template, session, url_for, redirect
from flask_wtf import CSRFProtect

# auto refresh timeout in seconds
AUTO_REFRESH = 60

# create the database for WA* models
try:
    from app.mod_wadconfig.models import init_models
except ImportError:
    from wad_admin.app.mod_wadconfig.models import init_models
init_models()

# Define the WSGI application object
flask_app = Flask(__name__)
CSRFProtect(flask_app)
# Configurations
try:
    flask_app.config.from_object('config')
except ImportError:
    flask_app.config.from_object('wad_admin.config')

# Sample HTTP error handling
@flask_app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

# Import a module / component using its blueprint handler variable (mod_auth)
try:
    from app.mod_auth.controllers import mod_auth as auth_module
    from app.mod_auth.subcontrollers.usermanager import mod_blueprint as auth_users_module
    from app.mod_wadconfig.controllers import mod_blueprint as wadconfig_module
    from app.mod_wadconfig.subcontrollers.modules import mod_blueprint as wadconfig_modules_module
    from app.mod_wadconfig.subcontrollers.configs import mod_blueprint as wadconfig_configs_module
    from app.mod_wadconfig.subcontrollers.metas import mod_blueprint as wadconfig_metas_module
    from app.mod_wadconfig.subcontrollers.selectors import mod_blueprint as wadconfig_selectors_module
    from app.mod_wadconfig.subcontrollers.processes import mod_blueprint as wadconfig_processes_module
    from app.mod_wadconfig.subcontrollers.results import mod_blueprint as wadconfig_results_module
    from app.mod_wadconfig.subcontrollers.wadprocessor import mod_blueprint as wadconfig_wadprocessor_module
    from app.mod_wadconfig.subcontrollers.importexport import mod_blueprint as wadconfig_importexport_module
    from app.mod_wadconfig.subcontrollers.backuprestore import mod_blueprint as wadconfig_backuprestore_module
    from app.mod_wadconfig.subcontrollers.dbtool import mod_blueprint as wadconfig_dbtool_module
    from app.mod_wadconfig.subcontrollers.sources import mod_blueprint as wadconfig_sources_module
    from app.mod_wadconfig.subcontrollers.ruletags import mod_blueprint as wadconfig_ruletags_module
    from app.mod_wadconfig.subcontrollers.factorymodules import mod_blueprint as wadconfig_factorymodules_module
except ImportError:
    from wad_admin.app.mod_auth.controllers import mod_auth as auth_module
    from wad_admin.app.mod_auth.subcontrollers.usermanager import mod_blueprint as auth_users_module
    from wad_admin.app.mod_wadconfig.controllers import mod_blueprint as wadconfig_module
    from wad_admin.app.mod_wadconfig.subcontrollers.modules import mod_blueprint as wadconfig_modules_module
    from wad_admin.app.mod_wadconfig.subcontrollers.configs import mod_blueprint as wadconfig_configs_module
    from wad_admin.app.mod_wadconfig.subcontrollers.metas import mod_blueprint as wadconfig_metas_module
    from wad_admin.app.mod_wadconfig.subcontrollers.selectors import mod_blueprint as wadconfig_selectors_module
    from wad_admin.app.mod_wadconfig.subcontrollers.processes import mod_blueprint as wadconfig_processes_module
    from wad_admin.app.mod_wadconfig.subcontrollers.results import mod_blueprint as wadconfig_results_module
    from wad_admin.app.mod_wadconfig.subcontrollers.wadprocessor import mod_blueprint as wadconfig_wadprocessor_module
    from wad_admin.app.mod_wadconfig.subcontrollers.importexport import mod_blueprint as wadconfig_importexport_module
    from wad_admin.app.mod_wadconfig.subcontrollers.backuprestore import mod_blueprint as wadconfig_backuprestore_module
    from wad_admin.app.mod_wadconfig.subcontrollers.dbtool import mod_blueprint as wadconfig_dbtool_module
    from wad_admin.app.mod_wadconfig.subcontrollers.sources import mod_blueprint as wadconfig_sources_module
    from wad_admin.app.mod_wadconfig.subcontrollers.ruletags import mod_blueprint as wadconfig_ruletags_module
    from wad_admin.app.mod_wadconfig.subcontrollers.factorymodules import mod_blueprint as wadconfig_factorymodules_module

# Register blueprint(s)
flask_app.register_blueprint(auth_module)
flask_app.register_blueprint(auth_users_module)
flask_app.register_blueprint(wadconfig_module)
flask_app.register_blueprint(wadconfig_modules_module)
flask_app.register_blueprint(wadconfig_configs_module)
flask_app.register_blueprint(wadconfig_metas_module)
flask_app.register_blueprint(wadconfig_selectors_module)
flask_app.register_blueprint(wadconfig_processes_module)
flask_app.register_blueprint(wadconfig_results_module)
flask_app.register_blueprint(wadconfig_wadprocessor_module)
flask_app.register_blueprint(wadconfig_importexport_module)
flask_app.register_blueprint(wadconfig_backuprestore_module)
flask_app.register_blueprint(wadconfig_dbtool_module)
flask_app.register_blueprint(wadconfig_sources_module)
flask_app.register_blueprint(wadconfig_ruletags_module)
flask_app.register_blueprint(wadconfig_factorymodules_module)
# flask_app.register_blueprint(xyz_module)
# ..
@flask_app.route('/', defaults={'path': ''})
@flask_app.route('/<path:path>')
def catch_all(path):
    return redirect(url_for('wadconfig.home')) 
