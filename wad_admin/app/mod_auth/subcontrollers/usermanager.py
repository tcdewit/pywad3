from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, session
from werkzeug.security import generate_password_hash
try:
    from app.mod_auth.controllers import login_required
    from app.mod_wadconfig.models import WAUsers
    from app.libs import HTML as simplehtml
    from app.libs.shared import Button
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.mod_wadconfig.models import WAUsers
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import Button

# Import modify forms
from .forms_usermanager import ModifyForm

mod_blueprint = Blueprint('auth_users', __name__, url_prefix='/auth')

@mod_blueprint.route('/users', methods=['GET', 'POST'])
@login_required
def default():
    _gid = 1 # there is only 1 user 

    # invalid table request are to be ignored
    formtitle = 'Modify User'
    form = ModifyForm(None if request.method=="GET" else request.form)

    if form.gid.data is None or len(form.gid.data)==0 or form.gid.data== "":
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call

        user = WAUsers.get(WAUsers.id==_gid)

        form.username.data = str(user.username)
        form.password.data = str(user.password)
        form.email.data = str(user.email)
        form.refresh.data = user.refresh
        
        form.gid.data = _gid

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            user = WAUsers.get(WAUsers.id==form.gid.data)
            user.username = str(form.username.data)
            user.refresh = int(form.refresh.data)

            # if this is user is the logged in user, update refresh
            if session.get('logged_in') == True and session.get('user_id') == user.id:
                session['refresh'] = user.refresh
                
            if len(form.password.data)>0:
                user.password = generate_password_hash(str(form.password.data))
            user.save()
            flash('Changes applied successfully', 'error') # it is not an error, just used as a trigger for display
            
    return render_template("auth/usermanager_modify.html", form=form, action='.',
                           title=formtitle, msg='Fill out the fields and click Submit')

