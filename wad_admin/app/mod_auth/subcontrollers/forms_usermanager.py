# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import IntegerField, StringField, PasswordField, HiddenField, validators

class ModifyForm(FlaskForm):
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email    = StringField('Email Address', [validators.Length(min=6, max=35)])
    password = PasswordField('New Password', [
        #validators.DataRequired('Password is required'),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    refresh = IntegerField('refresh (s)', [validators.NumberRange(min=1)])
    
    gid = HiddenField('gid', [])
    
    confirm = PasswordField('Repeat Password')