from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, session
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button, getIpAddress, bytes_as_string, string_as_bytes
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button, getIpAddress, bytes_as_string, string_as_bytes

dbio = dbio_connect()

mod_blueprint = Blueprint('wadconfig_processes', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/processes/')
@login_required
def default():
    # display and allow handling of processes DBProcesses table
    add_refresh = int(request.args.get('refresh', session.get('refresh')))

    subtitle=''
    stuff = dbio.DBProcesses.select().order_by(dbio.DBProcesses.id.desc())

    date_hdr = 'created_at'+20*'&nbsp;'
    table = simplehtml.Table(header_row=['id', 'selector', 
                                         'module', 'config', 
                                         'datatype', 'source', 'data_id', 
                                         'status', 
                                         date_hdr],
                             tclass='tablesorter-wadred', tid='sortTable')

    pacs_url = {}
    url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}
    num_status = {}
    for data in stuff:
        if not data.process_status.name in num_status:
            num_status[data.process_status.name] = 0
        num_status[data.process_status.name] += 1

        if not data.data_source.name in pacs_url:
            pacs_url[data.data_source.name] = None
            if data.data_source.source_type.name == 'orthanc': # we know how to construct that url
                # determine the outside address of this server
                ip = data.data_source.host
                if ip == 'localhost':
                    ip = getIpAddress(1) # dummy param
                pacs_url[data.data_source.name] = '%s://%s:%s/app/explorer.html'%(data.data_source.protocol,
                                                                                  ip, 
                                                                                  data.data_source.port,
                                                                                  )
        if pacs_url[data.data_source.name] is None:
            data_id = data.data_id
        else:
            url = '%s#%s?uuid=%s'%(pacs_url[data.data_source.name], url_part[data.module_config.data_type.name], data.data_id)
            data_id = simplehtml.link(data.data_id, url)

        # http://localhost:8042/app/explorer.html#series?uuid=3144fcf6-833a17ba-5454f959-d37e6d60-c2ac12ca
        table.rows.append([data.id, data.selector.name,
                           data.module_config.module.name, data.module_config.name,
                           data.module_config.data_type.name, data.data_source.name, data_id,
                           data.process_status.name, data.created_time.strftime('%Y-%m-%d %H:%M:%S'),
                           Button('show log',url_for('.showlog', gid=data.id)),
                           Button('delete',url_for('.delete', gid=data.id)),
                           Button('redo',url_for('.redo', gid=data.id)),
                           ])
    page = str(table)
    
    for key,val in num_status.items():
        subtitle += '<p> %s: %d'%(key, val)

    return render_template("wadconfig/generic.html", title='Processes', subtitle=Markup(subtitle), 
                           msg='', html=Markup(page), add_refresh=add_refresh )

@mod_blueprint.route('/processes/log/')
@login_required
def showlog():
    """
    show log of process if available
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if _gid is None:
        return redirect(url_for('.default'))

    try:
        msg = bytes_as_string(dbio.DBProcesses.get_by_id(_gid).process_log)
    except: # maybe the log does not exist, or this process just finished
        msg = ''

    # go back to overview page
    return render_template("wadconfig/generic.html", title='Process log', msg=msg)

@mod_blueprint.route('/processes/delete/')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        dbio.DBProcesses.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/processes/redo/')
@login_required
def redo():
    """
    reset process status to new
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        new_status = dbio.DBProcessStatus.get_by_name('new')
        proc = dbio.DBProcesses.get_by_id(_gid)
        proc.process_status = new_status
        proc.save()
        

    # go back to overview page
    return redirect(url_for('.default'))


