from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash
import os
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button
    from ..models import WARuleTags
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button
    from wad_admin.app.mod_wadconfig.models import WARuleTags

dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_ruletags import ModifyForm

mod_blueprint = Blueprint('wadconfig_ruletags', __name__, url_prefix='/wadadmin')

"""
show list of current tags, check boxes for delete
drop down add_tag of all tags (not in current)
add tag freefield

"""
@mod_blueprint.route('/ruletags/', methods=['GET', 'POST'])
@login_required
def default():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Tags to pick for Selector Rules'
    curtags, opttags = getTags()
    form = ModifyForm(None if request.method=="GET" else request.form, currenttags=curtags, optionaltags=opttags)
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            error, msg = applyChanges(field_dict)
            if not error:
                return(redirect(url_for('wadconfig.home')))
            else: # error
                return render_template("wadconfig/generic.html", title='Modify RuleTags', subtitle='ERROR! Cannot apply proposed changes!', msg='', html=Markup(msg))

    return render_template("wadconfig/ruletags_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Make some changes and click Sumbit')


def getTags():
    try:
        import pydicom as dicom
    except ImportError:
        import dicom
    currenttags = [{'tag_name':t.name, 'tag_val':t.val, 'cid':t.id} for t in WARuleTags.select()]

    curvals = [t.val for t in WARuleTags.select()]
    #0008,0060
    optionaltags = []
    for k,t in dicom._dicom_dict.DicomDictionary.items():
        val = ('%x'%k).zfill(8)
        val = '%s,%s'%(val[:4], val[4:])
        if not val in curvals:
            optionaltags.append({'tag_name':t[-1], 'tag_val':'%s'%val, 'cid':0})

    return currenttags, optionaltags


@mod_blueprint.route('/ruletags/delete/')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        dbio.DBModules.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.default'))


def applyChanges(field_dict):
    """
    delete all selected rules in Current Selections
    add all selected rules in Optional Selections
    """
    error, msg = False, ''
    
    # check if a new Private Tag is entered. If so, check if name and value not already in  (optional) lists.
    priv_name  = field_dict.get('priv_name', None)
    priv_value = field_dict.get('priv_value', None)

    _curtags, _opttags = getTags() # get lists of {'tag_name', 'tag_val'} for check with new priv tag
    inuse_names  = [c['tag_name'].lower() for c in _curtags]
    inuse_values = [c['tag_val'].lower() for c in _curtags]

    inuse_names.extend(  [c['tag_name'].lower() for c in _opttags] )
    inuse_values.extend( [c['tag_val'].lower() for c in _opttags] )

    if not priv_name is None:
        if priv_name.strip() == '':
            priv_name = None
        
    if not priv_value is None:
        if priv_value.strip() == '':
            priv_value = None

    if not priv_name is None and not priv_value is None:
        logger.info("proposing new private tage name: {}, value: {}".format(priv_name, priv_value))
        if priv_name.lower() in inuse_names:
            error = True
            msg += "New private tag name {} already exists in known tag names! ".format(priv_name)
        if priv_value.lower() in inuse_values:
            error = True
            msg += "New private value {} already exists in known tag values! ".format(priv_value)
            
        if error:
            return error, msg
        else:
            WARuleTags.create(name=priv_name,val=priv_value)
            
    elif priv_name is None and priv_value is None:
        pass
    else:
        error = True
        msg = "Need to supply both name and value for new private tag!"
        return error, msg

    
    # currenttags-0-cid
    cidsuffix = '-cid'
    selsuffix = '-tag_selected'

    prefix = 'currenttags-'
    del_ids = []
    for key, entry in field_dict.items():
        if key.startswith(prefix):
            if key.endswith(selsuffix): # it's either marked and present or not present
                cidkey = key[:(len(key)-len(selsuffix))]+cidsuffix
                del_ids.append(int(field_dict[cidkey]))
    
    # optionaltags-0-tag_selected
    add_tags = []
    prefix = 'optionaltags-'
    for key, entry in field_dict.items():
        if key.startswith(prefix):
            if key.endswith(selsuffix): # it's either marked and present or not present
                namekey = key[:(len(key)-len(selsuffix))]+'-tag_name'
                valkey = key[:(len(key)-len(selsuffix))]+'-tag_val'
                add_tags.append((field_dict[namekey], field_dict[valkey]))
                
    for k in del_ids:
        WARuleTags.get(WARuleTags.id == k).delete_instance()
        
    for n,v in add_tags:
        WARuleTags.create(name=n,val=v)
        
    return error, msg

    
