from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash
import os
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button
dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_sources import ModifyForm

mod_blueprint = Blueprint('wadconfig_sources', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/sources/')
@login_required
def default():
    # display and allow editing of modules table
    subtitle='WARNING! Deleting a data source sets off a cascade: It also deletes all entries '\
           'that directly reference that data source, and all other entries that reference those entries. '\
           'That includes: processes, results. '\
           'The number of references is shown in #references.'

    stuff = dbio.DBDataSources.select()

    table = simplehtml.Table(header_row=['id', 'name', 'source_type', '#references'], tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        table.rows.append([data.id, simplehtml.link(data.name, url_for('.inspect', gid=data.id)), data.source_type.name,
                           len(data.processes)+len(data.results),
                           Button('delete',url_for('.delete', gid=data.id)),
                           Button('edit',url_for('.modify', gid=data.id))
                           ])
    newbutton = Button('New', url_for('.modify'))
    page = str(table)+'<div>'+newbutton+'</div>'
    
    return render_template("wadconfig/generic.html", title='Data Sources', subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/sources/delete/')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        dbio.DBDataSources.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/sources/modify/', methods=['GET', 'POST'])
@login_required
def modify():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    # for now only orthanc is supported
    formtitle = 'Modify datasource'
    form = ModifyForm(None if request.method=="GET" else request.form)
    if not _gid is None:
        src = dbio.DBDataSources.get_by_id(_gid)
        form.name.data = src.name
        form.currentname.data = src.name
        form.source_type.data = src.source_type #dropdown
        form.protocol.data = src.protocol # dropdown
        form.aetitle.data = src.aetitle
        form.host.data = src.host
        form.port.data = src.port
        form.user.data = src.user
        form.pswd.data = src.pswd
        form.gid.data = _gid
    if form.gid is None or form.gid.data == '' or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        formtitle = 'New data source'
    existingnames = [v.name for v in dbio.DBDataSources.select()]

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.currentname.data is None or form.currentname.data == 'None': # yes, new form
            if form.name.data in existingnames:
                flash('A data source with this name already exist.', 'error')
                valid = False
        else: # not a new form
            if existingnames.count(form.name.data)> (0 if not form.currentname.data == form.name.data else 1) :
                flash('A data source with this name already exist.', 'error')
                valid = False
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            
            igid = 0
            if 'gid' in field_dict:
                try:
                    igid = int(field_dict['gid'])
                except:
                    pass
                
            if igid>0: # update, not create
                src = dbio.DBDataSources.get_by_id(igid)
                src.name = field_dict['name']
                src.source_type = field_dict['source_type']
                src.protocol = field_dict['protocol']
                src.aetitle = field_dict['aetitle']
                src.host = field_dict['host']
                src.port = field_dict['port']
                src.user = field_dict['user']
                if len(form.pswd.data)>0:
                    src.pswd = field_dict['pswd']
                src.save()
            else:
                dbio.DBDataSources.create(**field_dict)
            return redirect(url_for('.default'))
            
    return render_template("wadconfig/sources_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Fill out the fields and click Submit')
        
@mod_blueprint.route('/sources/inspect', methods=['GET', 'POST'])
@login_required
def inspect():
    # display and allow editing of modules table
    subtitle='WARNING! Deleting a data source sets off a cascade: It also deletes all entries '\
           'that directly reference that data source, and all other entries that reference those entries. '\
           'That includes: processes, results. '\
           'The number of references is shown in #references.'

    _gid   = int(request.args['gid']) if 'gid' in request.args else None
    # invalid table request are to be ignored
    if _gid is None:
        # go back to overview page
        return redirect(url_for('.default'))

    _level  = request.args['level'] if 'level' in request.args else 'patients'
    _patuid = request.args['patuid'] if 'patuid' in request.args else ''
    _stuuid = request.args['stuuid'] if 'stuuid' in request.args else ''
    _seruid = request.args['seruid'] if 'seruid' in request.args else ''
        
    # make a PACS connection
    from wad_qc.connection.pacsio import PACSIO
    pacsconfig = dbio.DBDataSources.get_by_id(_gid).as_dict()
    
    # 1. make sure a local pacs is running and that it has the required demo data
    try:
        pacsio = PACSIO(pacsconfig)
    except Exception as e:
        msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
        logger.error(msg)
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)
        

    picker = '<select id="pacstable" name="pacstable">'
    for tn in ['patients', 'studies', 'series', 'instances']:
        picker += '<option %s value="%s">%s</option>'%('selected' if _level == tn else '', tn, tn)
    picker += '</select>'
    picker += '<script> jQuery(function () { jQuery("#pacstable").change(function () {'\
        'location.href = "?gid=%d&patuid=%s&stuuid=%s&seruid=%s&level="+jQuery(this).val(); }) '\
        ' }) </script>'%(_gid, _patuid, _stuuid, _seruid)

    # make table
    rows = []
    try:
        if _level == 'patients':
            """
            # in future want something like this:
            ids = pacsio.getPatientIds()
            for i in ids:
                stuff.append(pacsio.getPatient(i))
            display = ['ID', 'MainDicomTags/PatientName', 'MainDicomTags/PatientID', 'Studies'] 
            """
            display = ['PatientUID'] 
            ids = pacsio.getPatientIds()
            for i in ids:
                rows.append( [
                    simplehtml.link(i, url_for('.inspect', gid=_gid, level='studies', patuid=i)),
                    ] )

        elif _level == 'studies':
            display = ['PatientUID', 'StudyUID']
            ids = pacsio.getStudyIds(patientid=_patuid if not _patuid == '' else None)
            for i in ids:
                rows.append( [
                    simplehtml.link(_patuid, url_for('.inspect', gid=_gid, level='patients')),
                    simplehtml.link(i, url_for('.inspect', gid=_gid, level='series', patuid=_patuid, stuuid=i)),
                    ] )

        elif _level == 'series':
            display = ['StudyUID', 'SeriesUID']
            ids = pacsio.getSeriesIds(studyid=_stuuid if not _stuuid == '' else None)
            for i in ids:
                rows.append( [
                    simplehtml.link(_stuuid, url_for('.inspect', gid=_gid,level='studies', patuid=_patuid, stuuid=_stuuid, seruid=i)),
                    simplehtml.link(i, url_for('.inspect', gid=_gid,level='instances', patuid=_patuid, stuuid=_stuuid, seruid=i)),
                    ] )

        elif _level == 'instances':
            display = ['SeriesUID', 'instanceUID']
            ids = pacsio.getInstancesIds(seriesid=_seruid if not _seruid == '' else None)
            for i in ids:
                rows.append( [
                    simplehtml.link(_seruid, url_for('.inspect', gid=_gid,level='series', patuid=_patuid, stuuid=_stuuid, seruid=_seruid)),
                    i] )

    except Exception as e:
        logger.info(' '.join(['Cannot access at level %s; error'%(_level),str(e)]))
    
    #return render_template("wadconfig/generic.html", title='PACS Access', subtitle='entries: %d'%len(stuff), msg=msg, html=Markup(page))

    table = simplehtml.Table(header_row=display, tclass='tablesorter-wadred', tid='sortTable')
    for row in rows:
        table.rows.append(row)

    page = picker+str(table)
    
    return render_template("wadconfig/generic.html", title='PACS Access', subtitle='entries: %d'%len(rows), msg='', html=Markup(page))

