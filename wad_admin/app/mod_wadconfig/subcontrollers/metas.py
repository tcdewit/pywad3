from flask import Blueprint, render_template, Markup, url_for, redirect, request, send_file, flash
import os
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button, upload_file, bytes_as_string, string_as_bytes
    from app.libs import configmaintenance
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button, upload_file, bytes_as_string, string_as_bytes
    from wad_admin.app.libs import configmaintenance
dbio = dbio_connect()

# Import modify forms
from .forms_metas import ModifyForm

import json
from io import BytesIO

mod_blueprint = Blueprint('wadconfig_metas', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/metas/')
@login_required
def default():
    # display and allow editing of modules table
    subtitle='WARNING! Deleting a meta_config sets off a cascade: It also deletes the module_config '\
           'that references that meta_config, and all other entries that reference that module_config, and so on. '\
           'A meta_config coupled to a Selector cannot be deleted.'

    """
    Loop over all module_configs, find the meta reference and check if config coupled to selector
    """

    table = simplehtml.Table(header_row=['id', 'value', 'coupled&nbsp;module_config', 'coupled&nbsp;selector'],
                             tclass='tablesorter-wadred', tid='sortTable')

    stuff = dbio.DBMetaConfigs.select().order_by(dbio.DBMetaConfigs.id)
    for data in stuff:
        selname = ''
        modname = ''
        if len(data.module_configs) >0:
            modname = data.module_configs[0].name
            if len(data.module_configs[0].selectors) >0:
                selname = data.module_configs[0].selectors[0].name

        table.rows.append([data.id, simplehtml.Link("meta.json", url_for('.download', metaid=data.id)), 
                           modname, selname,
                           Button('delete',url_for('.delete', metaid=data.id)),
                           Button('edit',url_for('.modify', metaid=data.id)),
                           ])
    page = str(table)
    
    return render_template("wadconfig/generic.html", title='Metas', subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/metas/delete/')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _metaid = int(request.args['metaid']) if 'metaid' in request.args else None

    # invalid table request are to be ignored
    if not _metaid is None:
        meta = dbio.DBMetaConfigs.get_by_id(_metaid)
        
        if len(meta.module_configs) == 0 or (len(meta.module_configs) >0 and len(meta.module_configs[0].selectors) == 0):
            meta.delete_instance(recursive=True)
        else:
            selname = meta.module_configs[0].selectors[0].name
            page = 'Meta {} is coupled to Selector "{}". '.format(meta.id, selname)
            page += 'Deleting this Meta would also delete that Selector and all its Results. '
            page += 'If you do want to do that, delete Selector "{}". '.format(selname)
            page += 'Else, couple the Selector "{}" to a different Config, and then delete this Meta.'.format(selname)
            return render_template("wadconfig/generic.html", title='Delete Meta', subtitle='ERROR! Cannot delete Meta!', msg='', html=Markup(page))

    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/metas/download/')
@login_required
def download():
    """
    download given id of given table from iqc db
    """
    _metaid = int(request.args['metaid']) if 'metaid' in request.args else None
    
    # invalid table request are to be ignored
    if _metaid is None:
        return redirect(url_for('.default'))

    row = dbio.DBMetaConfigs.get_by_id(_metaid)
    # find the elements of the config.json to delete
    filename = 'meta.json'
    blob = row.val #str(base64.b64decode(row['val']))
    
    # make a bit more pretty
    blob = json.loads(bytes_as_string(blob))
    blob = string_as_bytes(json.dumps(blob, sort_keys=True, indent=4))

    return send_file(BytesIO(blob), as_attachment=True, attachment_filename=filename)#mimetype=None

@mod_blueprint.route('/metas/modify/', methods=['GET', 'POST'])
@login_required
def modify():
    _metaid = int(request.args['metaid']) if 'metaid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Modify meta config'
    form = ModifyForm(None if request.method=="GET" else request.form)

    if _metaid is None:
        return redirect(url_for('.default'))

    # Verify the form
    valid = True
    if form.validate_on_submit():
        blob = None
        if 'metafile' in request.files and len(request.files['metafile'].filename) > 0:
            outname = upload_file(request.files['metafile'])
            try:
                with open(outname,'r') as fcfg: blob = fcfg.read()
            except Exception as e:
                flash('{} is not a valid meta.json'.format(request.files['metafile'].filename), 'error')
                valid = False
                msg = ""
            os.remove(outname)
        else:
            flash('no meta.json selected', 'error')
            valid = False
            msg = ""
            
        if valid:
            valid, msg = configmaintenance.validate_json('metafile', blob)
            if not valid:
                flash('{} {}'.format(request.files['metafile'].filename, msg), 'error')

        if valid:
            meta = dbio.DBMetaConfigs.get_by_id(_metaid)
                
            if not meta is None:
                meta.val = blob
            meta.save()

            return redirect(url_for('.default'))
            
    return render_template("wadconfig/metas_modify.html", form=form, action=url_for('.modify', metaid=_metaid),
                           title=formtitle, msg='Fill out the fields and click Submit')
        

