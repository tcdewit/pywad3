from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, send_file, session
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button, Image, getIpAddress, bytes_as_string, string_as_bytes
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button, Image, getIpAddress, bytes_as_string, string_as_bytes

import io

dbio = dbio_connect()

mod_blueprint = Blueprint('wadconfig_results', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/results/')
@login_required
def default():
    # display and allow handling of processes DBResults table
    add_refresh = int(request.args.get('refresh', session.get('refresh')))

    numentries = [('latest {}'.format(n), n) for n in [50, 100, 200, 500, 1000] ]
    numentries.append( ('all', 0) ) # display all

    _gid = int(request.args['gid']) if 'gid' in request.args else numentries[0][1]

    total_results = dbio.DBResults.select().count()
    if _gid > 0: 
        stuff = dbio.DBResults.select().order_by(dbio.DBResults.id.desc()).limit(_gid)
    else:
        stuff = dbio.DBResults.select().order_by(dbio.DBResults.id.desc())

    date_hdr = 'created_at'+20*'&nbsp;'
    table = simplehtml.Table(header_row=['id', 'selector', 
                                         'module', 'config', 
                                         'datatype', 'source', 'data_id', 
                                         '#results', date_hdr],
                             tclass='tablesorter-wadred', tid='sortTable')

    pacs_url = {}
    url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}

    for data in stuff:
        if not data.data_source.name in pacs_url:
            pacs_url[data.data_source.name] = None
            if data.data_source.source_type.name == 'orthanc': # we know how to construct that url
                # determine the outside address of this server
                ip = data.data_source.host
                if ip == 'localhost':
                    ip = getIpAddress(1) # dummy param
                pacs_url[data.data_source.name] = '%s://%s:%s/app/explorer.html'%(data.data_source.protocol,
                                                                                  ip, 
                                                                                  data.data_source.port,
                                                                                  )
        if pacs_url[data.data_source.name] is None:
            data_id = data.data_id
        else:
            url = '%s#%s?uuid=%s'%(pacs_url[data.data_source.name], url_part[data.module_config.data_type.name], data.data_id)
            data_id = simplehtml.link(data.data_id, url)

        table.rows.append([data.id, data.selector.name,
                           data.module_config.module.name, data.module_config.name,
                           data.module_config.data_type.name, data.data_source.name, data_id,
                           len(data.getResults()), 
                           simplehtml.link(data.created_time.strftime('%Y-%m-%d %H:%M:%S'), url_for('.showresults', rid=data.id)),
                           Button('show log',url_for('.showlog', gid=data.id)),
                           Button('delete',url_for('.delete', gid=data.id)),
                           Button('redo',url_for('.redo', gid=data.id)),
                           ])
    page = str(table)
    subtitle='Showing latest %d out of %d successful analyses in the database'%(len(table.rows), total_results)
    
    # add a limit_number picker
    picker = '<select id="maxlimit" name="maxlimit">'
    for tn in numentries:
        picker += '<option %s value="%s">%s</option>'%('selected' if _gid == tn[1] else '',tn[1], tn[0])
    picker += '</select>'
    picker += '<script> jQuery(function () { jQuery("#maxlimit").change(function () {'\
        'location.href = "?gid="+jQuery(this).val(); }) '\
        ' }) </script>'
    page = picker+page
    
    return render_template("wadconfig/generic.html", title='Results', subtitle=subtitle, msg='', 
                           html=Markup(page), add_refresh=add_refresh )

@mod_blueprint.route('/results/log/')
@login_required
def showlog():
    """
    show log of process if available
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if _gid is None:
        return redirect(url_for('.default'))

    try:
        msg = bytes_as_string(dbio.DBResults.get_by_id(_gid).process_log)
    except: # maybe the log does not exist, or this process just finished
        msg = ''

    # go back to overview page
    return render_template("wadconfig/generic.html", title='Results log', msg=msg)

@mod_blueprint.route('/results/delete/')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        dbio.DBResults.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/results/redo/')
@login_required
def redo():
    """
    turn selected result into new process, and delete all this result
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        result = dbio.DBResults.get_by_id(_gid)
        field_dict = {
            'selectorname':result.selector.name, 
            'datasourcename':result.data_source.name, 
            'data_id':result.data_id, 
            'processstatusname':'new'
        }
        dbio.DBProcesses.create(**field_dict)
        result.delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.default'))

# Set the route and accepted methods
@mod_blueprint.route('/results/show', methods=['GET', 'POST'])
@login_required
def showresults():
    # show a table with all details of a result
    _rid = int(request.args['rid']) if 'rid' in request.args else None
    if _rid is None:
        return(redirect(url_for('wadconfig.home')))
    
    result   = dbio.DBResults.get_by_id(_rid)
    selector = dbio.DBSelectors.get_by_id(result.selector.id)

    msg =''
    subtitle = '%s: %s'%(selector.name, selector.description) # name + description of selector
    stuff = result.getResults()
        
    table = simplehtml.Table(header_row=[
        'name', 'value',
        'equals', 'period', 'min', 'low', 'high', 'max'
        ],
        tclass='tablesorter-wadred', tid='sortTable')

    tableObj = simplehtml.Table(header_row=[
        'name', 'thumbnail'
        ],
        tclass='tablesorter-wadred')

    constraint_labels = ['val_equal','val_period', 'val_min', 'val_low','val_high', 'val_max']
    for data in sorted(stuff, key=lambda x: x.name):
        if isinstance(data, dbio.DBResultObjects):
            datatype='object'
            row = [data.name,
                   simplehtml.link(Image(label=data.name, src=url_for('.getobject',did=data.id), width=120), 
                                   url_for('.showobject',did=data.id), newpage=True)
                   ]
            tableObj.rows.append(row)
            continue
        elif isinstance(data, dbio.DBResultStrings):
            datatype='string'
        elif isinstance(data, dbio.DBResultBools):
            datatype='bool'
        elif isinstance(data, dbio.DBResultDateTimes):
            datatype='datetime'
        elif isinstance(data, dbio.DBResultFloats):
            datatype='float'

        # make sure all displayed elements are (empty) strings
        constraints = {f: getattr(data, 'val_equal', None) for f in  constraint_labels }
        for k,v in constraints.items():
            if v is None:
                constraints[k] = ''
            else:
                constraints[k] = str(v)

        row = [data.name,
               str(getSingleStatus(data)), #data.val,
            ]
        row.extend([ constraints[f] for f in constraint_labels ])

        table.rows.append(row)
        
    page = str(table)
    if len(tableObj.rows)>0:
        page += str(tableObj)
    
    return render_template('wadconfig/generic.html', title='WAD-QC Results', subtitle=subtitle, msg=msg, html=Markup(page))

@mod_blueprint.route('/results/getobject')
@login_required
def getobject():
    """Serves the image."""
    _did = int(request.args['did']) if 'did' in request.args else None
    if _did is None:
        return redirect(url_for('.showresults'))

    res = dbio.DBResultObjects.get_by_id(_did)
    
    return send_file(io.BytesIO(res.val),
                     attachment_filename='%s.%s'%(res.name, res.filetype),
                     mimetype='image/%s'%res.filetype)

@mod_blueprint.route('/results/object')
@login_required
def showobject():
    _did = int(request.args['did']) if 'did' in request.args else None
    if _did is None:
        return redirect(url_for('.showresults'))

    msg =''

    res = dbio.DBResultObjects.get_by_id(_did)
    selector = dbio.DBSelectors.get_by_id(res.result.selector.id)
    img = Image(label=res.name, src=url_for('.getobject',did=_did))
    
    subtitle = '%s: %s'%(selector.name, selector.description) # name + description of selector
    dt = res.result.created_time

    page = img
    
    return render_template('wadconfig/generic.html', title='%s: %s'%(res.name,dt), subtitle=subtitle, msg=msg, html=Markup(page))

def getSingleStatus(result):
    # determine if all results are within limits
    status = None
    
    """
    if status is None:
        if not getattr(result, 'val_period', None) is None:
            status = 'ok'
    """        
    # check equals
    if status is None:
        if not getattr(result, 'val_equal', None) is None:
            status = 'ok'
            if not result.val == result.val_equal:
                status = 'critical'

    # check floats min_low_high_max
    if status is None:
        for lim in ['val_min', 'val_low', 'val_high', 'val_max']:
            if not getattr(result, lim, None) is None:
                status = 'ok'
                break;
            
        if not getattr(result, 'val_low', None) is None:
            if result.val < result.val_low:
                status = 'warning'

        if not getattr(result, 'val_high', None) is None:
            if result.val > result.val_high:
                status = 'warning'


        if not getattr(result, 'val_min', None) is None:
            if result.val < result.val_min:
                status = 'critical'

        if not getattr(result, 'val_max', None) is None:
            if result.val > result.val_max:
                status = 'critical'

    if status == 'ok':
        status_color = 'yellowgreen'
    elif status == 'warning':
        status_color = 'yellow'
    elif status == 'critical':
        status_color = 'red'
    else:
        return result.val
    
    return '<div style="background-color:%s">%s</div>'%(status_color, result.val)

