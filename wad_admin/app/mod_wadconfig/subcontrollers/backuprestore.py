from flask import Blueprint, render_template, Markup, url_for, redirect, request, send_file, send_from_directory, flash
import os
import tempfile
from io import BytesIO
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button, upload_file
    from app.libs.exchange import export_selectors, import_selectors
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button, upload_file
    from wad_admin.app.libs.exchange import export_selectors, import_selectors
dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_backuprestore import BackupForm, RestoreForm

mod_blueprint = Blueprint('wadconfig_backuprestore', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/backup/', methods=['GET', 'POST'])
@login_required
def backingup():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Export selectors with module and configuration'
    selectors = getSelectors()
    form = BackupForm(request.form, selectors=selectors)
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            return doBackup(field_dict)
            
    return render_template("wadconfig/backup.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Select selectors to backup (modules and configs will be added automatically) and click Backup')

@mod_blueprint.route('/restore/', methods=['GET', 'POST'])
@login_required
def restoring():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Restore selectors with modules and configurations'
    form = RestoreForm(None if request.method=="GET" else request.form)
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if not 'file' in request.files or len(request.files['file'].filename) == 0:
            flash('No valid zip file chosen.', 'error')
            valid = False
        if valid:
            inname = upload_file(request.files['file'])
            skip_modules = form.skip_modules.data
            msg = doRestore(inname, skip_modules)
            logger.info(msg)
            if  msg == 'Success':
                msg = 'Restored {}'.format(request.files['file'].filename)
                logger.info(msg)
                return render_template("wadconfig/generic.html", title='Restore', subtitle='Success', msg=msg)
            else:
                flash(msg, 'error')

    msg = 'Restore recreates selectors with config and modules. It does not couple existing results or processes to restored selectors. '
    msg += '<BR>Select package with backup of selectors with configs and modules to import and click Restore'
    return render_template("wadconfig/restore.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg=Markup(msg))

def doRestore(inname, skip_modules):
    """
    return success or not.
    check if zip
    unpack zip
      check if manifest.ini
      check consistency of manifest
      check consistency of manifest with folders modules and configs; check if exe file present in folder
      check uniqueness of new module names (if not, suggest user renames current modules, refuse restore)
      check uniqueness of new selector names (if not, suggest user renames current selector, refuse restore)
        create each module (zip for upload?!)
        create each config coupled to correct module
        create each selector, coupled to correct config, with rules
    """
    valid, msg = import_selectors(dbio, inname, skip_modules, logger)

    # remove zip
    os.remove(inname)
    
    return msg

def getSelectors(active_only=False):
    stuff = dbio.DBSelectors.select().order_by(dbio.DBSelectors.id)
    
    results = []
    for sel in stuff:
        if active_only:
            if not sel.isactive:
                continue
        results.append(
            {'sel_name':sel.name, 
             'sel_description':sel.description,
             'sel_isactive': sel.isactive,
             'cid':sel.id})
    return results
    
def doBackup(field_dict):
    """
    Pack all selected selectors and connected configs and modulea with a manifest.ini file and send
    Export:
      manifest.json
      selectors/selector_1.json
               /selector_2.json
      modules/module_1/
              module_2/
      configs/config_1.json
             /config_2.json
    """
    cidsuffix = '-cid'
    selsuffix = '-sel_selected'
    
    # make a list of all selected selectors
    sel_ids = []
    for key, entry in field_dict.items():
        if key.endswith(selsuffix): # it's either marked and present or not present
            cidkey = key[:(len(key)-len(selsuffix))]+cidsuffix
            sel_ids.append(int(field_dict[cidkey]))
    
    # make the Backup location
    modules_dir = dbio.DBVariables.get_by_name('modules_dir').val
    temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)

    # make a temporary file; either stream it of write it to disk
    with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
        error, msg = export_selectors(dbio, tmpf, sel_ids, logger)
        
        if error:
            redirect(url_for('.default', gid=1))
            
        # Reset file pointer
        tmpf.seek(0)

        return send_file(BytesIO(tmpf.read()), as_attachment=True, attachment_filename='backup.zip')#mimetype=None
