from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash
import os
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button, upload_file
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button, upload_file
dbio = dbio_connect()

# Import modify forms
from .forms_confirm import ConfirmForm
from .forms_modules import ModifyForm

mod_blueprint = Blueprint('wadconfig_modules', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/modules/')
@login_required
def default():
    # display and allow editing of modules table
    subtitle='WARNING! Deleting a module sets off a cascade: It also deletes all entries '\
           'that directly reference that module, and all other entries that reference those entries. '\
           'That includes: module_configs, selectors, rules, processes, results. '\
           'The number of references is shown in #configs. '\
           'Clicking delete will ask for confirmation.'
    stuff = dbio.DBModules.select().order_by(dbio.DBModules.id)

    table = simplehtml.Table(header_row=['id', 'name', 'description', 'executable', 'origin', '#configs', '#coupled selectors'], tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        sels = sum([1 if len(cfg.selectors) else 0 for cfg in data.module_configs ])
        table.rows.append([data.id, data.name, data.description, data.filename, data.origin,
                           len(data.module_configs), sels,
                           Button('delete',url_for('.delete', gid=data.id), _class='btn btn-danger') if sels==0 else '',
                           Button('edit',url_for('.modify', gid=data.id))
                           ])
    newbutton = Button('New', url_for('.modify'))
    page = str(table)+'<div>'+newbutton+'</div>'
    
    return render_template("wadconfig/generic.html", title='Modules', subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/modules/delete/', methods=['GET', 'POST'])
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if _gid is None:
        return redirect(url_for('.default'))

    mod = dbio.DBModules.get_by_id(_gid)

    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete Module {}'.format(mod.name)
    msg = 'Deleting a module sets off a cascade: It also deletes all entries '\
          'that directly reference that module, and all other entries that reference those entries. '\
          'That includes: module_configs, selectors, rules, processes, results. '\
          'However, you cannot delete a Module that is coupled to a Selector.'
    msg += '<BR><BR>Tick confirm and click Submit to proceed.'
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            sels = sum([1 if len(cfg.selectors) else 0 for cfg in mod.module_configs ])
            if not sels == 0:
                page = 'Module {} is coupled to Selectors. Deleting this Module would break those Selectors. '\
                    'First delete those Selectors, or couple them to different Modules, and then delete this '\
                    'Module.'.format(mod.name)
                return render_template("wadconfig/generic.html", title='Delete Module', subtitle='ERROR! Cannot delete Module!', msg='', html=Markup(page))

            # do stuff
            modname = mod.name
            
            # delete orphaned meta files
            del_meta = [ c.meta for c in mod.module_configs ]
            mod.delete_instance(recursive=True)
            
            for mt in del_meta:
                try:
                    mt.delete_instance(recursive=False)
                except:
                    pass

            subtitle='Deleted Module {}'.format(modname)
            return render_template("wadconfig/generic.html", title='Delete Module', subtitle=subtitle, msg='', html="")

    return render_template("wadconfig/confirm.html", form=form, action=url_for('.delete', gid=_gid),
                           title=formtitle, msg=Markup(msg))

@mod_blueprint.route('/modules/modify/', methods=['GET', 'POST'])
@login_required
def modify():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Modify module'
    form = ModifyForm(None if request.method=="GET" else request.form)
    if not _gid is None:
        module = dbio.DBModules.get_by_id(_gid)
        form.name.data = module.name
        form.currentname.data = module.name
        form.description.data = module.description
        form.gid.data = _gid
        form.executable.data = module.filename
    if form.gid is None or form.gid.data == '' or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        formtitle = 'New module'

    existingnames = [v.name for v in dbio.DBModules.select()]

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.currentname.data is None or form.currentname.data == 'None': # yes, new form
            if form.name.data in existingnames:
                flash('A module with this name already exist.', 'error')
                valid = False
            if not 'file' in request.files or len(request.files['file'].filename) == 0:
                flash('No file chosen.', 'error')
                valid = False
        else: # not a new form
            if existingnames.count(form.name.data)> (0 if not form.currentname.data == form.name.data else 1) :
                flash('A module with this name already exist.', 'error')
                valid = False

        field_dict = {k:v for k,v in request.form.items()}
        outname = None
        if valid:
            if 'file' in request.files and not len(request.files['file'].filename) == 0:
                outname = upload_file(request.files['file'])
                if not outname is None:
                    field_dict['uploadfilepath'] = outname
    
            igid = 0
            if 'gid' in field_dict:
                try:
                    igid = int(field_dict['gid'])
                except:
                    pass
                
            if igid>0: # update, not create
                module = dbio.DBModules.get_by_id(igid)
                module.name = field_dict['name']
                module.description = field_dict['description']
                module.filename = os.path.basename(field_dict['executable'])
                # make clear that this module is not a factory module anymore
                module.origin = 'user'
                module.repo_version = ""
                module.repo_url = ""
                if not outname is None:
                    module.save(uploadfilepath=outname)
                else:
                    module.save()
            else:
                field_dict['filename'] = os.path.basename(field_dict['executable'])
                # make clear that this module is not a factory module
                field_dict['origin'] = 'user'
                field_dict['repo_version'] = ""
                field_dict['repo_url'] = ""
                dbio.DBModules.create(**field_dict)
            if not outname is None:
                os.remove(outname)
            return redirect(url_for('.default'))
            
    return render_template("wadconfig/modules_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Fill out the fields and click Submit')
        
