# Import Form 
from flask_wtf import FlaskForm 
from flask import Markup
# Import Form elements such as TextField 
from wtforms import StringField, HiddenField, SelectField, BooleanField, IntegerField, FieldList, FormField, FloatField

# Import Form validators
from wtforms.validators import Required, NoneOf, NumberRange

try:
    from ..models import WARuleTags
    from app.libs.shared import dbio_connect
except ImportError:
    from wad_admin.app.mod_wadconfig.models import WARuleTags
    from wad_admin.app.libs.shared import dbio_connect, Button, upload_file
dbio = dbio_connect()

logic_choices = [(m.id, m.name) for m in dbio.DBSelectorLogics.select()]
tag_choices = [(m.val, m.name) for m in WARuleTags.select()]

class EqualsEntryForm(FlaskForm):
    lim_name = HiddenField()
    lim_val = StringField()
    
class PeriodEntryForm(FlaskForm):
    lim_name = HiddenField()
    lim_val = IntegerField()

class MinLowHighMaxEntryForm(FlaskForm):
    lim_name = HiddenField()
    min_val = FloatField()
    low_val = FloatField()
    high_val = FloatField()
    max_val = FloatField()

class ParamEntryForm(FlaskForm):
    action = HiddenField()
    param = HiddenField()
    value = StringField()
    info = HiddenField()

class RuleEntryForm(FlaskForm):
    tag    = SelectField(choices = tag_choices)
    logic  = SelectField(choices = logic_choices, coerce=int)
    value0 = StringField()
    value1 = StringField()
    value2 = StringField()
    value3 = StringField()
    value4 = StringField()
    delrule = BooleanField('delete rule')
    ruleid = HiddenField()
 
    def __init__(self, *args, **kwargs):
        super(RuleEntryForm, self).__init__(*args, **kwargs)
        self.delrule.label = 'delete'

class ModifyForm(FlaskForm):
    name = StringField('name', [Required(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    isactive = BooleanField('isactive')
    description = StringField('description', [])
    module_config = SelectField('config', coerce=int )
    currentname = HiddenField('currentname', [])
    gid = HiddenField('gid', [])
    cid = HiddenField('cid', [])
    params = FieldList(FormField(ParamEntryForm))
    rules = FieldList(FormField(RuleEntryForm))
    addrule = BooleanField('add new rule')
    equals = FieldList(FormField(EqualsEntryForm))
    minlowhighmaxs = FieldList(FormField(MinLowHighMaxEntryForm))
    periods = FieldList(FormField(PeriodEntryForm))
