from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash
import os.path
import subprocess
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button
dbio = dbio_connect()

# Import modify forms
from .forms_wadprocessor import ModifyForm

mod_blueprint = Blueprint('wadconfig_wadprocessor', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/wadprocessor/', methods=['GET', 'POST'])
@login_required
def default():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Modify WAD Processor parameters'
    form = ModifyForm(None if request.method=="GET" else request.form)
    #if not _gid is None:
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call

        form.interval.data = int(dbio.DBVariables.get_by_name('processor_interval').val)
        form.timeout.data = float(dbio.DBVariables.get_by_name('processor_timeout').val)
        form.workers.data = int(dbio.DBVariables.get_by_name('processor_workers').val)
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            wadcontrol('processor_interval', form.interval.data)
            wadcontrol('processor_timeout', form.timeout.data)
            wadcontrol('processor_workers', form.workers.data)
            flash('Changes applied successfully', 'error') # it is not an error, just used as a trigger for display
            return redirect(url_for('.default', gid=1))
            
    return render_template("wadconfig/wadprocessor_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Fill out the fields and click Submit')

def wadcontrol(varname, newval):
    # check if new value is different from old value, if so, apply change and store in db
    var = dbio.DBVariables.get_by_name(varname)

    if newval == var.val: # no change, return
        return
    
    # call wadcontrol to change selected setting
    wadqcroot = dbio.DBVariables.get_by_name('wadqcroot').val
    cmd = ['wadcontrol']
    if varname == 'processor_interval':
        cmd.extend(['setQueryInterval', str(newval)])
        ret = subprocess.check_call(cmd, cwd=wadqcroot)
    elif varname == 'processor_workers':
        cmd.extend(['setWorkers', str(newval)])
        subprocess.check_call(cmd, cwd=wadqcroot)
    elif varname == 'processor_timeout':
        pass # analyzer reads directly from db
        
    # store new setting in db
    var.val = str(newval)
    var.save()
    
    
    