# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import BooleanField

# Import Form validators
from wtforms.validators import Required, NoneOf

class ConfirmForm(FlaskForm):
    confirm = BooleanField('confirm')
    
