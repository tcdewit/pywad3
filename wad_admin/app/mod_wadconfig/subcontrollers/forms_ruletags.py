# Import Form 
from flask_wtf import FlaskForm 
from flask import Markup
# Import Form elements such as TextField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, StringField

# Import Form validators
from wtforms.validators import Required, NoneOf, NumberRange

class TagEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    tag_name = HiddenField()
    tag_val = HiddenField()
    cid = HiddenField()
    tag_selected = BooleanField('pick')

class ModifyForm(FlaskForm):
    gid = HiddenField('gid', [])
    currenttags = FieldList(FormField(TagEntryForm))
    optionaltags = FieldList(FormField(TagEntryForm))
    
    priv_name  = StringField('name',  [])
    priv_value = StringField('value', [])
    
    
    

