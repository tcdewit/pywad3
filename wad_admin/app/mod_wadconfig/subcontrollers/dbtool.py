from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, send_file
from peewee import BlobField
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button, INIFILE, bytes_as_string, string_as_bytes, getIpAddress
    from app.libs import dbmaintenance
    from app.libs import dbupgrade as libdbupgrade
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button, INIFILE, bytes_as_string, string_as_bytes, getIpAddress
    from wad_admin.app.libs import dbmaintenance
    from wad_admin.app.libs import dbupgrade as libdbupgrade
dbio = dbio_connect()
    
from wad_qc.connection.pacsio import PACSIO
from wad_core.selector import Selector

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)
import os
from datetime import datetime
import tempfile
from io import BytesIO

# Import modify forms
from .forms_confirm import ConfirmForm
from .forms_qrdelete import QRDForm

mod_blueprint = Blueprint('wadconfig_dbtool', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/dbtool')
@login_required
def default():
    """
    Just present a menu with choices; should all be require authorization
    """
    menus = [
    ]
    menuA =[
        { 'label':'WAD-QC database inspector', 'href':url_for('.inspector') }, 
    ]

    menuC = [
        { 'label':'Check for datasets in Sources but missing in Processes/Results', 'href':url_for('.consistency',mode='misses') }, 
    ]
    menuB =[
        { 'label':'upgrade WAD-QC database', 'href':url_for('.dbupgrade') }, 
        { 'label':'truncate results WAD-QC database', 'href':url_for('.truncate') }, 
        { 'label':'empty whole WAD-QC database', 'href':url_for('.cleanstart') }, 
        { 'label':'manage Selectors', 'href':url_for('.manageselector') }, 
    ]

    menuD = [
        { 'label':'download or permanently delete studies from Sources', 'href':url_for('.qrd') }, 
    ]
    
    menus.append({'title':'Monitoring', 'items':menuA})
    menus.append({'title':'Consistency', 'items':menuC})
    menus.append({'title':'Database Maintenance', 'items':menuB})
    menus.append({'title':'Sources Maintenance', 'items':menuD})
    return render_template("wadconfig/home.html", menus=menus, title='WAD-QC DBTool')

def get_source_datatable(sources, dtype, filters, fields, start_date=None, end_date=None):
    """
    helper function to return an embedable table of selected data ids.
    Returns html table, {sourcename:{data_type:[data_ids]}}, number of results
    """
    keys = [ f[1] for f in fields ]
    table = simplehtml.Table(header_row=[ f[0] for f in fields ],
                             tclass='tablesorter-wadred', tid='sortTableNoFilter')
    
    results = {}
    numresults = 0
    for source in sources:
        pacsconfig = source.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

        results[source.id] = { dtype : []}
        if dtype == 'dcm_series':
            for dataid in pacsio.getSeriesIds(None): # loop over all possible series
                do_include, hdrs = dbmaintenance.source_select_data(pacsio, (pacsconfig['name'], dtype, dataid), filters, return_headers=True, logger=logger)
                if 'series_date' in hdrs:
                    try:
                        series_date = datetime.strptime(hdrs['series_date'], '%Y%m%d').date()
                    except:
                        print(dataid)
                        raise
                    if do_include:
                        if not start_date is None:
                            do_include = series_date >= start_date
                    if do_include:
                        if not end_date is None:
                            do_include = series_date <= end_date
                    
                if do_include:
                    results[source.id][dtype].append(dataid)
                    numresults += 1
                    row = []
                    for k in keys:
                        if k == 'source_name':
                            row.append( pacsconfig['name'] )
                        elif k == 'dtype':
                            row.append( dtype )
                        elif k == 'uid':
                            row.append( simplehtml.link(dataid, url_for('.showtags',gid=dataid,level=dtype, source=pacsconfig['name'])) )
                        else:
                            row.append( hdrs[k] )
                    table.rows.append(row)
    
    page = str(table)
    return page, results, numresults

def _delete_datasets(data):
    """
    data[source_id] = {dtype: [data_id]}
    Permanently delete list of files
    """
    error, msg = dbmaintenance.sources_delete_datasets(dbio, data, logger)
    if error:
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

    else:
        return render_template("wadconfig/generic.html", title='Delete from Sources', subtitle='Success', msg=msg, html="")

def _download_datasets(data):
    """
    data[source_name] = {dtype: [data_id]}
    Download list of files
    """
    # make sure temp dir exists
    temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)
    
    # make a temporary file; either stream it of write it to disk
    with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
        error, msg, resnum = dbmaintenance.sources_download_datasets(dbio, tmpf, data, logger)

        if error:
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)
            
        if resnum == 0:
            # no data at all
            subtitle='NO datasets selected for download'
            return render_template("wadconfig/generic.html", title='Download data from Sources', subtitle=subtitle, msg='', html="")
            
        logger.info(msg)
        # Reset file pointer
        tmpf.seek(0)

        return send_file(BytesIO(tmpf.read()), as_attachment=True, attachment_filename='{}_dcm.zip'.format('selected_'))#mimetype=None
    


@mod_blueprint.route('/dbtool/db_upgrade_latest')
@login_required
def db_upgrade_latest():
    """
    The real upgrade routine. Ask for some extra params.
    """
    running = request.args.get('running', None)
    latest  = request.args.get('latest', None)
    if running is None or latest is None:
        return url_for('.dbupgrade')

    title = "Upgrade WAD-QC Database"
    subtitle = ""
    page = ""
    msg = ""

    error, msgs = libdbupgrade.upgrade(dbio, logger)
    
    msg = '\n'.join(msgs)
    if error == False:
        subtitle = "Success! Now restart all WAD services for the changes to take effect."

    return render_template("wadconfig/generic.html", title=title, subtitle=subtitle, msg=msg, html=Markup(page))
   

@mod_blueprint.route('/dbtool/dbupgrade')
@login_required
def dbupgrade():
    """
    upgrade database to latest version. 
    ? show a list of defined upgrades, buttons for force redo (make sure this doesn't break)
    show currect version from db, version in dbio, if not same offer upgrade buttons
    """
    title = "Upgrade WAD-QC Database"
    subtitle = ""
    page = ""
    msg = ""

    # versions
    hdr = "<H2>Versions</H2>"
    col_ok   = {'bgcolor':'yellowgreen'}
    col_bad =  {'bgcolor':'red'}

    running_version = libdbupgrade.get_running_version(dbio, logger)
    latest_version  = libdbupgrade.get_latest_version(dbio, logger)
    running_label = '.'.join([str(v) for v in running_version])
    latest_label  = '.'.join([str(v) for v in latest_version])
    
    col_running = col_ok if libdbupgrade.version_equal(running_version, latest_version) else col_bad
    col_latest = col_bad if libdbupgrade.version_smaller(latest_version, running_version) else col_ok

    table = simplehtml.Table(header_row=['Item',   'Running', 'Latest'])
    row = ['WAD-QC DataBase']
    row.append(simplehtml.TableCell(running_label, **col_running))
    row.append(simplehtml.TableCell(latest_label, **col_latest))
    
    if libdbupgrade.version_smaller(running_version, latest_version):
        btn_upgrade = Button('Upgrade', url_for('.db_upgrade_latest', running=running_label, latest=latest_label), _class='btn btn-primary')
        row.append(btn_upgrade)
        subtitle = "The running WAD-QC database needs to be upgraded. Press the upgrade button."
    elif libdbupgrade.version_equal(running_version, latest_version):
        subtitle = "The running WAD-QC database is at the latest version. No upgrade needed."
    else:
        subtitle = "The running WAD-QC database is incompatible with the installed version of the WAD-QC server. Install the latest version of WAD-QC server!"
        
        
    table.rows.append(row)
    table = hdr + str(table)

    page = str(table)
    
    return render_template("wadconfig/generic.html", title=title, subtitle=subtitle, msg=msg, html=Markup(page))
    
@mod_blueprint.route('/dbtool/qrd', methods=['GET', 'POST'])
@login_required
def qrd():
    # invalid table request are to be ignored
    formtitle = 'Select data from Sources.'
    msg = 'For delete you must check the confirm box!'
    form = QRDForm(None if request.method=="GET" else request.form)
            
    possible_filters = [ # table header, dicom header field, form element
        ('modality', 'modality', 'modality'), ('description', 'series_desc', 'seriesdesc'), ('protocol','prot_name', 'protocol'),
        ('station', 'stat_name', 'station'), ('date', 'series_date'), ('aet', 'aet', 'aet')]

    filters = {}
    for pf in possible_filters:
        if len(pf)>2:
            elem = getattr(form,pf[2],None)
            if elem is None or elem.data is None or elem.data == '' or len(elem.data) == 0:
                continue
            filters[pf[1]] = elem.data
        
    source = getattr(form, 'sourcename', None)
    if not source is None:
        source = source.data
    sources = dbio.DBDataSources.select()
    if not (source is None or source == '' or len(source) == 0):
        sources = [ s for s in sources if source.lower() in s.name.lower() ]

    elem = getattr(form, 'seriesdate_min', None)
    if elem is None or elem.data is None or elem.data == '':
        start_date = None
    else:
        start_date = elem.data

    elem = getattr(form, 'seriesdate_max', None)
    if elem is None or elem.data is None or elem.data == '':
        end_date = None
    else:
        end_date = elem.data

    dtype = 'dcm_series'
    fields = [ ('Source', 'source_name'), # special
              ('data_type', 'dtype'), # special
              ('uid', 'uid'),# special
              ('modality', 'modality'),
              ('series desc', 'series_desc'),
              ('protocol', 'prot_name'),
              ('station', 'stat_name'),
              ('aet', 'aet'),
              ('series date', 'series_date'),
              ] # table header, field key
    if len(filters.keys())>0 or not start_date is None or not end_date is None or not source is None:
        table, results, numresults = get_source_datatable(sources, dtype, filters, fields, start_date, end_date)
        page = '<p><pre> {} datasets in selection. </pre>'.format(numresults)+str(table)
    else:
        table, results, numresults = ('', [], 0)
        page = '<p><pre> will not make a selection without filters! </pre>'+str(table)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.todo.data == 2 and form.confirm.data is False:
            flash('Must tick confirm to delete!', 'error')
            valid = False

        form.confirm.data = False # whatever action is peformed, make sure confirm is not ticked when reloading!
        if valid and form.todo.data == 1: # download
            if numresults == 0:
                flash('No data in selection, nothing to download!', 'error')
            else:
                return _download_datasets(results)
            
        if valid and form.todo.data == 2: # delete
            if numresults == 0:
                flash('No data in selection, nothing to delete!', 'error')
            else:
                return _delete_datasets(results)

    return render_template("wadconfig/qrd.html", form=form, action=url_for('.qrd'),
                           title=formtitle, msg=Markup(msg), html=Markup(page))
    

@mod_blueprint.route('/dbtool/inspector')
@login_required
def inspector():
    # display and allow editing of modules table
    subtitle=''
    
    tablenames = { model._meta.table_name : model for model in dbio.DBTables } # peewee3: db_table -> table_name
    _gid = request.args['gid'] if 'gid' in request.args else list(sorted(tablenames.keys()))[0]
    
    model = tablenames[_gid]
    stuff = model.select()
    fields = [f for f in model._meta.sorted_field_names]
    
    date_hdr = 'created_at'+20*'&nbsp;'
    table = simplehtml.Table(header_row=fields,
                             tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        table.rows.append([getvalue(data, f) for f in fields ])
    
    page = str(table)

    picker = '<select id="dbtable" name="dbtable">'
    for tn in sorted(tablenames.keys()):
        picker += '<option %s value="%s">%s</option>'%('selected' if _gid == tn else '',tn,tn)
    picker += '</select>'
    picker += '<script> jQuery(function () { jQuery("#dbtable").change(function () {'\
        'location.href = "?gid="+jQuery(this).val(); }) '\
        ' }) </script>'

    page = picker+page
    return render_template("wadconfig/generic.html", title='DataBase Inspector', subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/dbtool/consistency')
@login_required
def consistency():
    # display and allow editing of modules table
    _mode = request.args.get('mode', None)
    if not _mode in ['misses']:
        return redirect(url_for('.default'))

    possible_filters = ['modality','pat_name', 'study_desc', 'study_date', 'series_desc', 'prot_name', 'stat_name', 'series_date', 'aet']
    filters = {}
    for filt in possible_filters:
        if filt in request.args:
            filters[filt] = request.args.get(filt, None)
        
    # aet is only not empty for instances, so do not show
    table = simplehtml.Table(header_row=[
        'Source', 'data_type', 'uid',
        'patient', 'modality', 'study desc', 'series desc', 'protocol', 'station', 'aet', 'study date', 'series date'],
        tclass='tablesorter-wadred', tid='sortTable')

    used_dtypes = []
    for pacs in dbio.DBDataSources.select():
        pacsconfig = pacs.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

        # _mode == 'misses':
        results = dbmaintenance.source_find_unpicked(dbio, pacsio, pacsconfig['name'], datalevel='all', logger=logger)
            
        # populate results table
        for srcname, dtype, dataid in results:
            do_include, hdrs = dbmaintenance.source_select_data(pacsio, (srcname, dtype, dataid), filters, return_headers=True, logger=logger)

            # restrict output
            if do_include == False: 
                continue

            if not dtype in used_dtypes:
                used_dtypes.append(dtype)
            
            btnDel    = Button('Delete From PACS', url_for('.delete_pacs', src=srcname, datatype=dtype, dataid=dataid), _class='btn btn-danger')

            # _mode == 'misses':
            btnResend = Button('Resend', url_for('.resend_selector', src=srcname, datatype=dtype, dataid=dataid), _class='btn btn-primary')
            table.rows.append([pacsconfig['name'], dtype, 
                               simplehtml.link(dataid, url_for('.showtags',gid=dataid,level=dtype,source=srcname)),
                               hdrs['pat_name'], hdrs['modality'], hdrs['study_desc'], hdrs['series_desc'],
                               hdrs['prot_name'], hdrs['stat_name'], hdrs['aet'], hdrs['study_date'], hdrs['series_date'],
                               btnResend, btnDel])
            
    page = str(table)
    # _mode == 'misses':
    # find items in datasources without a match in processes/results
    title = 'Consistency: Not in Processes/Results'
    subtitle='All datasets in all datasources have been picked up.'

    if len(table.rows) >0:
        subtitle = 'These %d datasets have not been picked up. You could try resending them to the WAD Selector, '%len(table.rows)
        subtitle +='or delete them from the datasource. Clicking delete will ask for confirmation.'
        btns = ''
        for dt in ['dcm_study', 'dcm_series', 'dcm_instance']:
            if dt in used_dtypes:
                btns += Button('Send not picked up {} datasets to WADSelector'.format(dt), url_for('.send_unpicked', datatype=dt, **filters), _class="btn btn-primary")
        page += '<div>'+btns+'</div>'
        page += '<BR>'
        btns = ''
        for dt in ['dcm_study', 'dcm_series', 'dcm_instance']:
            if dt in used_dtypes:
                btns += Button('Delete not picked up {} datasets from PACS'.format(dt), url_for('.delete_unmatched', datatype=dt, mode=_mode, **filters), _class='btn btn-danger')
        page += '<div>'+btns+'</div>'

    return render_template("wadconfig/generic.html", title=title, subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/dbtool/showtags')
@login_required
def showtags():
    # display and allow editing of modules table
    _source = request.args['source'] if 'source' in request.args else None
    _level = request.args['level'] if 'level' in request.args else None
    _gid = request.args['gid'] if 'gid' in request.args else None
    if not _level in ['dcm_study', 'dcm_series', 'dcm_instance']:
        return redirect(url_for('.default'))
    if _gid is None:
        return redirect(url_for('.default'))
    if _source is None:
        return redirect(url_for('.default'))

    try:
        # 1. make sure a local pacs is running and that it has the required demo data
        pacsconfig = dbio.DBDataSources.get_by_name(_source).as_dict()
        pacsio = PACSIO(pacsconfig)
    except Exception as e:
        msg = ' '.join(['Cannot access PACS %s with provided credentials'%_source,str(e)])
        logger.error(msg)
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

    tags = []
    if _level == 'dcm_study':
        tags = pacsio.getSharedStudyHeaders(_gid)
    elif _level == 'dcm_series':
        tags = pacsio.getSharedStudyHeaders(_gid)
    elif _level == 'dcm_instance':
        tags = pacsio.getInstanceHeaders(_gid)
    
    msg = ''
    table = simplehtml.Table(header_row=[
        'Tag', 'Name', 'Type', 'Value'],
        tclass='tablesorter-wadred', tid='sortTable')
    
    for key,value in sorted(tags.items()):
        table.rows.append([key,value['Name'],value['Type'],value['Value']])

    page = str(table)

    subtitle = '%s/%s/%s'%(_source, _level, _gid)

    if pacsconfig['typename'] == 'orthanc': # we know how to construct that url
        url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}
        # determine the outside address of this server
        ip = pacsconfig['host']
        if ip == 'localhost':
            ip = getIpAddress(1) # dummy param
        pacs_url = '%s://%s:%s/app/explorer.html'%(pacsconfig['protocol'],
                                                   ip, 
                                                   pacsconfig['port'])
        url = '%s#%s?uuid=%s'%(pacs_url, url_part[_level], _gid)
        btnPACS = Button('open in PACS', url)
        page = btnPACS+page

    #subtitle += str(pacsconfig)
    return render_template("wadconfig/generic.html", title='ShowTags', subtitle=subtitle, msg=msg, html=Markup(page))

@mod_blueprint.route('/dbtool/consistency/resend_selector')
@login_required
def resend_selector():
    _datatype = request.args.get('datatype', None)
    _srcname = request.args.get('src', None)
    _dataid = request.args.get('dataid', None)
    if not _datatype in ['dcm_series', 'dcm_study', 'dcm_instance']:
        return redirect(url_for('.default'))
    
    deleted = False
    # 1. make sure a local pacs is running and that it has the required demo data
    try:
        pacs = dbio.DBDataSources.get_by_name(_srcname)
        pacsio = PACSIO(pacs.as_dict())
    except Exception as e:
        msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacs.name, str(e)])
        logger.error(msg)
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

    wsel = Selector(INIFILE, logfile_only=True)
    wsel.run(_srcname, _dataid, dryrun=False, datalevel=_datatype, include_inactive=False)

    page = 'Did send 1 item of data_type "%s" to WAD Selector'%(_datatype)

    return render_template("wadconfig/generic.html", title='Resend to WAD Selector', subtitle='', msg='', html=Markup(page))

@mod_blueprint.route('/dbtool/consistency/send_unpicked')
@login_required
def send_unpicked():
    _datatype = request.args['datatype'] if 'datatype' in request.args else None
    if not _datatype in ['dcm_series', 'dcm_study', 'dcm_instance']:
        return redirect(url_for('.default'))
    
    # accept restrictions on pacs data
    possible_filters = ['modality','pat_name', 'study_desc', 'study_date', 'series_desc', 'prot_name', 'stat_name', 'series_date', 'aet']
    filters = {}
    for filt in possible_filters:
        if filt in request.args:
            filters[filt] = request.args.get(filt, None)
    
    wsel = Selector(INIFILE, logfile_only=True)
    num = 0

    for pacs in dbio.DBDataSources.select():
        pacsconfig = pacs.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

        results = dbmaintenance.source_find_unpicked(dbio, pacsio, pacsconfig['name'], datalevel=_datatype, logger=logger) # only requested type
    
        for srcname, dtype, dataid in results:
            do_include, hdrs = dbmaintenance.source_select_data(pacsio, (srcname, dtype, dataid), 
                                                                filters, return_headers=False, logger=logger)

            # restrict output
            if do_include == False: 
                continue

            num += 1
            wsel.run(srcname, dataid, dryrun=False, datalevel=dtype, include_inactive=False)

    msg = 'Did send %d items of data_type "%s" to WAD Selector'%(num, _datatype)
    return render_template("wadconfig/generic.html", title='Send Unpicked', subtitle='Message', msg=msg)


@mod_blueprint.route('/dbtool/consistency/delete_pacs', methods=['GET', 'POST'])
@login_required
def delete_pacs():
    _datatype = request.args.get('datatype', None)
    _srcname = request.args.get('src', None)
    _dataid = request.args.get('dataid', None)
    if not _datatype in ['dcm_series', 'dcm_study', 'dcm_instance']:
        return redirect(url_for('.default'))
    
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete one {} from {}'.format(_datatype, _srcname)
    msg = 'This will permanently delete the selected data set from the QC PACS.'
    msg += '<BR>Tick confirm and click Submit to proceed.'
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            src_id = dbio.DBDataSources.get_by_name(_srcname).id
            return _delete_datasets({src_id: {_datatype: [_dataid]}})

    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.delete_pacs', src=_srcname, datatype=_datatype, dataid=_dataid),
                           title=formtitle, msg=Markup(msg))

@mod_blueprint.route('/dbtool/consistency/delete_unmatched', methods=['GET', 'POST'])
@login_required
def delete_unmatched():
    _mode = request.args.get('mode', None)
    _datatype = request.args.get('datatype', None)
    if not _datatype in ['dcm_series', 'dcm_study', 'dcm_instance']:
        return redirect(url_for('.default'))
    if not _mode in ['misses']:
        return redirect(url_for('.default'))
    
    # accept restrictions on pacs data
    possible_filters = ['modality','pat_name', 'study_desc', 'study_date', 'series_desc', 'prot_name', 'stat_name', 'series_date', 'aet']
    filters = {}
    for filt in possible_filters:
        if filt in request.args:
            filters[filt] = request.args.get(filt, None)

    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete multiple data sets from PACS'
    msg = 'This will permanently delete the selected data sets from the QC PACS.'
    msg += '<BR>Tick confirm and click Submit to proceed.'
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            wsel = Selector(INIFILE, logfile_only=True)
            error, emsg = dbmaintenance.sources_delete_unmatched(dbio, wsel, datatype=_datatype, mode=_mode, filters=filters, logger=logger)
            if error:
                return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=emsg)
            else:
                return render_template("wadconfig/generic.html", title='Delete from Sources', subtitle='Success', msg=emsg, html="")


    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.delete_unmatched', datatype=_datatype, mode=_mode, **filters),
                           title=formtitle, msg=Markup(msg))


def getvalue(model, field):
    names = ['data_type', 'data_source', 'process_status', 'module', 'selector', 'logic', 'source_type']
    m = getattr(model, field)
    if field == 'pswd':
        return('*****')
    if isinstance(m, bytes):
        return ('[blob]')
    if field in names:
        return getattr(m, 'name')
    if hasattr(m, 'id'): #foreign key
        return getattr(m, 'id')
    if field == 'val' and isinstance(m, memoryview):
        return bytes_as_string(m)
    return m
 
@mod_blueprint.route('/dbtool/cleanstart', methods=['GET', 'POST'])
@login_required
def cleanstart():
    """
    Remove whole WAD-QC database, and create a new one.
    """
    # invalid table request are to be ignored
    formtitle = 'Confirm action: empty WAD-QC database.'
    msg = 'Empty WAD-QC database will recreate an empty WAD-QC database: all results, configuration, modules and selectors will be deleted. (DICOM data in PACS will be untouched).'
    msg += '<BR>Tick confirm and click Submit to proceed.'
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            error, emsg = dbmaintenance.waddb_cleanstart(dbio, INIFILE, logger)
        
            if error:
                return render_template("wadconfig/generic.html", subtitle='ERROR', msg=emsg, title='WAD-QC DBTool')
            else:
                return render_template("wadconfig/generic.html", subtitle='Success', msg="Successfully created empty WAD-QC database", title='WAD-QC DBTool')
            
    return render_template("wadconfig/confirm.html", form=form, action=url_for('.cleanstart'),
                           title=formtitle, msg=Markup(msg))


@mod_blueprint.route('/dbtool/truncate', methods=['GET', 'POST'])
@login_required
def truncate():
    """
    Truncate most tables in WAD-QC database (deleting all results).
    """
    # invalid table request are to be ignored
    formtitle = 'Confirm action: truncate results tables.'
    msg = 'Truncate results tables will empty all results and processes, but leaves modules and selectors intact. (DICOM data will be untouched in PACS).'
    msg += '<BR>Tick confirm and click Submit to proceed.'
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            error, emsg = dbmaintenance.waddb_truncate(dbio, logger)

            if error:
                return render_template("wadconfig/generic.html", subtitle='ERROR', msg=msg, title='WAD-QC DBTool')
            else:
                return render_template("wadconfig/generic.html", subtitle='Success', msg="Successfully truncated WAD-QC database", title='WAD-QC DBTool')
            
    return render_template("wadconfig/confirm.html", form=form, action=url_for('.truncate'),
                           title=formtitle, msg=Markup(msg))

@mod_blueprint.route('/manageselector/')
@login_required
def manageselector():
    """
    Show number of Processes/Results per selector, and offer to delete them all.
    """
    # display and allow editing of modules table
    subtitle  = '<p>"Delete All" will remove all Processes and Results from the WAD-QC database. '\
        'The datasets will not be deleted from the DataSources. Use the Consistency tool to Resend or Delete the data.</p>'
    subtitle += '<p>"Dryrun" will recheck all DataSources for datasets that could be matched by the Selector. </p>'
    subtitle += '<p>"Resend All" will resend all datasets of all DataSources to the WAD Selector for this Selector only. '\
        'Previous Results/Processes will NOT be removed, so double entries could occur.</p>'
    subtitle += '<p>"Download" will download all already selected datasets of this Selector. </p>'
    include_inactive=True
    stuff = dbio.DBSelectors.select().order_by(dbio.DBSelectors.id)
    if not include_inactive: 
        stuff = stuff.where(dbio.DBSelectors.isactive == True)

    table = simplehtml.Table(header_row=[
        'id', 'name', 'isactive', 'description', 'module_name', 
        'config_name', '#rules', '#processes', '#results'],
        tclass='tablesorter-wadred', tid='sortTable')

    for data in stuff:
        table.rows.append([data.id, data.name, 'True' if data.isactive==True else 'False', data.description,
                           data.module_config.module.name,
                           data.module_config.name,
                           len(data.rules),
                           len(data.processes), len(data.results),
                           Button('Dryrun',url_for('wadconfig_selectors.dryrun', gid=data.id), _class='btn btn-primary'),
                           Button('Resend All',url_for('wadconfig_selectors.realrun', gid=data.id), _class='btn btn-warning'),
                           Button('Delete All',url_for('.delete_from_processor', gid=data.id), _class='btn btn-danger'),
                           Button('Download',url_for('wadconfig_selectors.download', gid=data.id), _class='btn btn-primary'),
                           ])
    page = str(table)
    
    return render_template("wadconfig/generic.html", title='Manage Selectors', subtitle=Markup(subtitle), msg='', html=Markup(page))

@mod_blueprint.route('/delete_from_processor/')
@login_required
def delete_from_processor():
    """
    Delete all Processes/Results of a Selector
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    if _gid is None:
        return url_for('.default')

    processes = 0
    results   = 0
    sel = dbio.DBSelectors.get_by_id(_gid)
    for item in sel.processes:
        item.delete_instance(recursive=True)
        processes += 1
    for item in sel.results:
        item.delete_instance(recursive=True)
        results += 1

    # display and allow editing of modules table
    subtitle = ''
    msg  = '<p>Deleted {} Processes</p>'.format(processes)
    msg += '<p>Deleted {} Results</p>'.format(results)
    page = ''
    
    return render_template("wadconfig/generic.html", title='Delete From Selector', subtitle=Markup(subtitle), msg=Markup(msg), html=Markup(page))

