from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, send_file
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button, INIFILE, bytes_as_string, string_as_bytes
    from ..models import WARuleTags
    from app.libs import selectormaintenance
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button, INIFILE, bytes_as_string, string_as_bytes
    from wad_admin.app.mod_wadconfig.models import WARuleTags
    from wad_admin.app.libs import selectormaintenance
dbio = dbio_connect()

from wad_core.selector import Selector
from wad_qc.connection.pacsio import PACSIO
import os
import tempfile
from io import BytesIO
from peewee import IntegrityError

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_confirm import ConfirmForm
from .forms_selectors import ModifyForm

mod_blueprint = Blueprint('wadconfig_selectors', __name__, url_prefix='/wadadmin')

"""
TODO:
 o only one action header form params
 o adding limits not allowed; is that wanted behaviour?
 
"""
@mod_blueprint.route('/selectors/')
@login_required
def default():
    # display and allow editing of selectors table
    subtitle='WARNING! Deleting a selector sets off a cascade: It also deletes all entries '\
           'that directly reference that selector, and all other entries that reference those entries. '\
           'That includes: processes, results. The number of references is shown in #processes and #results.'
    include_inactive=True
    stuff = dbio.DBSelectors.select().order_by(dbio.DBSelectors.id)
    if not include_inactive: 
        stuff = stuff.where(dbio.DBSelectors.isactive == True)

    table = simplehtml.Table(header_row=[
        'id', 'name', 'isactive', 'description', 'module_name', 
        'config_name', 'meta', '#rules', '#processes', '#results'],
        tclass='tablesorter-wadred', tid='sortTable')

    for data in stuff:
        meta = simplehtml.Link('meta', url_for('wadconfig_metas.download', metaid=data.module_config.meta.id))
        table.rows.append([data.id, data.name, 'True' if data.isactive==True else 'False', data.description,
                           data.module_config.module.name,
                           data.module_config.name,
                           meta,
                           len(data.rules),
                           len(data.processes), len(data.results),
                           Button('copy',url_for('.duplicate', gid=data.id)),
                           Button('delete',url_for('.delete', gid=data.id), _class='btn btn-danger'),
                           Button('edit',url_for('.modify', gid=data.id, cid=data.module_config.id)),
                           Button('dryrun',url_for('.dryrun', gid=data.id)),
                           ])
    newbutton = Button('New', url_for('.modify'))
    page = str(table)+'<div>'+newbutton+'</div>'
    
    return render_template("wadconfig/generic.html", title='Selectors', subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/selectors/download')
@login_required
def download():
    # download all data matching with given selector
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    if _gid is None:
        return url_for('.default')

    sel = dbio.DBSelectors.get_by_id(_gid)
    datatype = sel.module_config.data_type.name
    
    # create downloadfolder
    downloadfolder = os.path.join(dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val, 'Download')
    if not os.path.exists(downloadfolder):
        os.mkdir(downloadfolder)
    outfolder = tempfile.mkdtemp(dir=downloadfolder,prefix='sel')
    
    resnum = 0
    results = {}
    # find all matches in results
    for res in sel.results:
        if not res.data_source.id in results.keys():
            results[res.data_source.id] = set() # use sets to avoid double copies of twice processed sets
        results[res.data_source.id].add(res.data_id) 

    # add all matches in processes
    for res in sel.processes:
        if not res.data_source.id in results.keys():
            results[res.data_source.id] = set() # use sets to avoid double copies of twice processed sets
        results[res.data_source.id].add(res.data_id) 
        
    # make sure temp dir exists
    temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)
    
    # make a temporary file; either stream it of write it to disk
    with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
        error, msg, resnum = selectormaintenance.sources_download_datasets(dbio, tmpf, results, datatype, logger)

        if error:
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)
            
        if resnum == 0:
            # no data at all
            subtitle='NO datasets selected for {}'.format(sel.name)
            return render_template("wadconfig/generic.html", title='Download data from Sources', subtitle=subtitle, msg='', html="")
            
        logger.info(msg)
        # Reset file pointer
        tmpf.seek(0)
        return send_file(BytesIO(tmpf.read()), as_attachment=True, attachment_filename='{}_dcm.zip'.format(sel.name))#mimetype=None


@mod_blueprint.route('/selectors/dryrun')
@login_required
def dryrun():
    # display what data would have been selected for the current PACS for a given selector
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    if _gid is None:
        return url_for('.default')

    sel = dbio.DBSelectors.get_by_id(_gid)
    datatype = sel.module_config.data_type.name

    subtitle='If this selector were active, NO datasets would have been selected.'

    include_inactive=True

    wsel = Selector(INIFILE, logfile_only=True)
    for pacs in dbio.DBDataSources.select():
        pacsconfig = pacs.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

        table = simplehtml.Table(header_row=[
            'Source', datatype],
            tclass='tablesorter-wadred', tid='sortTable')

        for studyid in pacsio.getStudyIds():
            wsel.run(pacsconfig['name'], studyid, dryrun=True, datalevel='dcm_study',
                     selectornames=[sel.name], 
                     include_inactive=include_inactive)
            stuff = wsel.getDryrun()
            for data in stuff:
                dataid = data['data_id']
                table.rows.append([pacsconfig['name'], 
                                   simplehtml.link(dataid, url_for('.showtags',gid=dataid,level=datatype,source=pacsconfig['name']))])

    page = str(table)
    if len(table.rows) >0:
        subtitle='If this selector were active, these %d unprocessed datasets would have been selected.'%len(table.rows)
    
    return render_template("wadconfig/generic.html", title=sel.name, subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/selectors/realrun')
@login_required
def realrun():
    # Resend all dataset of all DataSources to the WAD Selector for matching with given Selector only
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    if _gid is None:
        return url_for('.default')

    sel = dbio.DBSelectors.get_by_id(_gid)
    datatype = sel.module_config.data_type.name

    if not sel.isactive:
        subtitle='Selector "{}" is not active. Nothing to do.'.format(sel.name)
        return render_template("wadconfig/generic.html", title=sel.name, subtitle=subtitle, msg='', html='')

    subtitle='All datasets of all DataSources were resend to the WAD Selector for matching with Selector "{}".'.format(sel.name)

    include_inactive=False # real run, so no inactive selectors!

    wsel = Selector(INIFILE, logfile_only=True)
    for pacs in dbio.DBDataSources.select():
        pacsconfig = pacs.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

        for studyid in pacsio.getStudyIds():
            wsel.run(pacsconfig['name'], studyid, dryrun=False, datalevel='dcm_study',
                     selectornames=[sel.name], 
                     include_inactive=include_inactive)

    page = ''
    
    return render_template("wadconfig/generic.html", title=sel.name, subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/selectors/showtags')
@login_required
def showtags():
    # display dicom tags of a given dataset
    _source = request.args['source'] if 'source' in request.args else None
    _level = request.args['level'] if 'level' in request.args else None
    _gid = request.args['gid'] if 'gid' in request.args else None
    if not _level in ['dcm_study', 'dcm_series', 'dcm_instance']:
        return redirect(url_for('.default'))
    if _gid is None:
        return redirect(url_for('.default'))
    if _source is None:
        return redirect(url_for('.default'))

    try:
        # 1. make sure a local pacs is running and that it has the required demo data
        pacsconfig = dbio.DBDataSources.get_by_name(_source).as_dict()
        pacsio = PACSIO(pacsconfig)
    except Exception as e:
        msg = ' '.join(['Cannot access PACS %s with provided credentials'%_source,str(e)])
        logger.error(msg)
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='ERROR', msg=msg)

    tags = []
    if _level == 'dcm_study':
        tags = pacsio.getSharedStudyHeaders(_gid)
    elif _level == 'dcm_series':
        tags = pacsio.getSharedStudyHeaders(_gid)
    elif _level == 'dcm_instance':
        tags = pacsio.getInstanceHeaders(_gid)
    
    msg = ''
    table = simplehtml.Table(header_row=[
        'Tag', 'Name', 'Type', 'Value'],
        tclass='tablesorter-wadred', tid='sortTable')
    
    for key,value in sorted(tags.items()):
        table.rows.append([key,value['Name'],value['Type'],value['Value']])

    page = str(table)
    subtitle = '%s/%s/%s'%(_source,_level,_gid)
    return render_template("wadconfig/generic.html", title='ShowTags', subtitle=subtitle, msg=msg, html=Markup(page))

@mod_blueprint.route('/selectors/duplicate/')
@login_required
def duplicate():
    """
    copy given id to new selector, changing name
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        nw_sel = dbio.DBSelectors.get_by_id(_gid).clone() # clone already makes new name unique
        nw_sel.save()

    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/selectors/delete/', methods=['GET', 'POST'])
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    if _gid is None:
        return redirect(url_for('.default'))
    try:
        selectorname = dbio.DBSelectors.get_by_id(_gid).name
    except:
        return redirect(url_for('.default'))
        
    # confirmation dialog
    formtitle = 'Confirm action: delete Selector {}'.format(selectorname)
    msg = 'This will delete the Selector and all its Results.'
    msg += '<BR>Tick confirm and click Submit to proceed.'
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            title = 'Delete Selector'
            subtitle = 'Successfully deleted Selector "{}". '.format(selectorname)
            msg = ""
            with dbio.db.atomic() as trx:
                try:
                    dbio.DBSelectors.get_by_id(_gid).delete_instance(recursive=True)
                except IntegrityError as e:
                    trx.rollback()
                    subtitle='Error deleting Selector {}'.format(selectorname)
                    msg = str(e)
                    if "violates foreign key constraint" in str(e):
                        subtitle += "<BR><BR>This Selector is referenced from a table from an external service (e.g. WAD Dashboard). "\
                            "Use that service to remove the reference and try again. "  
            return render_template("wadconfig/generic.html", title='Delete Selector', subtitle=Markup(subtitle), msg=msg, html="")

    return render_template("wadconfig/confirm.html", form=form, action=url_for('.delete', gid=_gid),
                           title=formtitle, msg=Markup(msg))

@mod_blueprint.route('/selectors/modify/', methods=['GET', 'POST'])
@login_required
def modify():
    _gid = None
    if 'gid' in request.args and not request.args['gid'] is '':
        _gid = int(request.args['gid'])
    _cid = int(request.args['cid']) if 'cid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Modify Selector'
    msg = 'Fill out the fields and click Submit. For a different config, first change only the config and submit before making additional changes.'
    params = None
    rules = None
    limits = {'constraint_equals': None, 'constraint_period': None, 'constraint_minlowhighmax': None}
    if not _gid is None:
        params = selectormaintenance.paramsblock(dbio, _cid, logger=logger)
        rules = selectormaintenance.rulesblock(dbio, _gid, logger=logger)
        limits = selectormaintenance.limitsblocks(dbio, _cid, logger=logger)

    form = ModifyForm(None if request.method=="GET" else request.form, params=params, rules=rules, 
                      equals=limits['constraint_equals'], periods=limits['constraint_period'], 
                      minlowhighmaxs=limits['constraint_minlowhighmax'])
    if _gid is None and (form.gid is None or form.gid.data == '' or form.gid.data is None): #define here, to avoid wrong label on redisplaying a form with errors
        formtitle = 'New Selector'
        msg = 'Fill out the fields and click Submit. Afterwards modify the new Selector for SelectorRules etc.'
    
    #form.config.choices = [(m.id, '%s_%s'%(m.name,m.versionnumber)) for m in dbio.DBModuleConfigs.select()]
    # make a list of (id, name), where name is either default + configname or selector+configname, but use on the biggest id if multiple
    default_configs = []
    selector_configs = {}
    for m in dbio.DBModuleConfigs.select():
        if len(m.selectors)>0:
            cname = 'selector: %s'%(m.selectors[0].name)
            if cname in selector_configs:
                selector_configs[cname] = max(selector_configs[cname], m.id)
            else:
                selector_configs[cname] = m.id
        else:
            # exclude configs coupled to results, but not to a current selector (meaning replaced)
            if not m.origin == 'result': 
                default_configs.append((m.id , '%s: %s'%(m.origin, m.name)))
            
    if not _gid is None:
        cname = None
        for key,val in selector_configs.items():
            if val == _cid:
                cname = key
                break
        if not cname is None:
            del(selector_configs[cname])
            selector_configs['<current config>'] = _cid
    
    default_configs = sorted(default_configs, key=lambda x: x[1])
    for key in sorted(selector_configs.keys()):
        default_configs.append((selector_configs[key], key))
    form.module_config.choices = default_configs

    # set tags_choices to updated set
    if not _gid is None:
        sel = dbio.DBSelectors.get_by_id(_gid)
        for dtag in sel.rules:
            if not dtag.dicomtag in [m.val for m in WARuleTags.select()]:
                WARuleTags.create(name=dtag.dicomtag,val=dtag.dicomtag)

    tag_choices = [(m.val, m.name) for m in WARuleTags.select()]
    for rules_entry in form.rules:
        rules_entry.tag.choices = tag_choices
    
    
    if not _gid is None:
        sel = dbio.DBSelectors.get_by_id(_gid)
        form.name.data = sel.name
        form.currentname.data = sel.name
        form.description.data = sel.description
        form.gid.data = _gid
        form.cid.data = _cid
        form.module_config.data = _cid
        form.isactive.data = sel.isactive

    existingnames = [v.name for v in dbio.DBSelectors.select()]

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.currentname.data is None or form.currentname.data == 'None': # yes, new form
            if form.name.data in existingnames:
                flash('A selector with this name already exist.', 'error')
                valid = False
        else: # not a new form
            if existingnames.count(form.name.data)> (0 if not form.currentname.data == form.name.data else 1) :
                flash('A selector with this name already exist.', 'error')
                valid = False

        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            
            # check if new or update
            igid = 0
            if 'gid' in field_dict:
                try:
                    igid = int(field_dict['gid'])
                except:
                    pass
            # the elements of the config
            cfg_id = int(field_dict['module_config'])
            if igid>0: # update, not create
                if cfg_id == dbio.DBSelectors.get_by_id(igid).module_config.id: # if we changed the config, ignore cfg and meta changes
                    cfg_id = selectormaintenance.update_config_if_changed(dbio, cfg_id, field_dict, logger=logger)
                    selectormaintenance.update_meta_if_changed(dbio, cfg_id, field_dict, logger=logger)
                else: # changed config: clone config, and make new meta
                    # clone the config
                    nw_config = dbio.DBModuleConfigs.get_by_id(cfg_id).clone()
                    nw_meta = dbio.DBMetaConfigs.create(val=nw_config.meta.val)
                    nw_config.meta = nw_meta.id
                    nw_config.origin = 'result'
                    nw_config.save()
                    cfg_id = nw_config.id
                    
            """
            a new selector made with DBSelectors.create() already makes a clone of the config with origin set to 'result'
            """

            blob = None
                
            if igid>0: # update, not create
                sel = dbio.DBSelectors.get_by_id(igid)
                sel.name = field_dict['name']
                sel.description = field_dict['description']
                sel.module_config = cfg_id
                sel.isactive = True if 'isactive' in field_dict else False
                sel.save()

                nwtagval = WARuleTags.get(WARuleTags.id == 1).val # yeah, this will crash if there is no tag left
                if selectormaintenance.handle_rules(dbio, igid, field_dict, nwtagval, logger=logger):
                    url = url_for('.modify')
                    if igid>0:
                        url += '?gid=%d'%igid
                        url += '&cid=%d'%cfg_id
                    else:
                        url += '?cid=%d'%cfg_id
                        
                    return redirect(url)
            else:
                dbio.DBSelectors.create(**field_dict)
                
            return redirect(url_for('.default'))
            

    return render_template("wadconfig/selectors_modify.html", form=form, action='.', 
                           title=formtitle, msg=msg)
