from flask import Blueprint, render_template, Markup, url_for, redirect, request, send_file, flash
import os
try:
    from app.mod_auth.controllers import login_required
    from app.libs import HTML as simplehtml
    from app.libs.shared import dbio_connect, Button, upload_file, bytes_as_string, string_as_bytes
    from app.libs import configmaintenance
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import HTML as simplehtml
    from wad_admin.app.libs.shared import dbio_connect, Button, upload_file, bytes_as_string, string_as_bytes
    from wad_admin.app.libs import configmaintenance
dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_configs import ModifyForm

import json
from io import BytesIO

mod_blueprint = Blueprint('wadconfig_configs', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/configs/')
@login_required
def default():
    # display and allow editing of modules table
    subtitle='WARNING! Deleting a module_config sets off a cascade: It also deletes all entries '\
           'that directly reference that version of the module_config, and all other entries that reference those entries. '\
           'The number of references is shown in coupled selector, #processes and #results. '\
           'A module_config coupled to a Selector cannot be deleted.'
           
    stuff = dbio.DBModuleConfigs.select().order_by(dbio.DBModuleConfigs.id)

    table = simplehtml.Table(header_row=['id', 'name', 'description', 'data_type', 'origin', 'module_name', 'base_meta',
                                         'coupled&nbsp;selector', '#processes', '#results'],
                             tclass='tablesorter-wadred', tid='sortTable')
    for data in stuff:
        selname = ''
        if len(data.selectors) >0:
            selname = data.selectors[0].name

        meta = None
        if not data.meta is None:      
            meta = simplehtml.Link('meta', url_for('wadconfig_metas.download', metaid=data.meta.id))
        table.rows.append([data.id, simplehtml.Link(data.name, url_for('.download', cfgid=data.id)), data.description, 
                           data.data_type.name,
                           data.origin, data.module.name, meta, 
                           selname, len(data.processes), len(data.results),
                           Button('copy',url_for('.duplicate', gid=data.id)),
                           Button('delete',url_for('.delete', gid=data.id)),
                           Button('edit',url_for('.modify', gid=data.id)),
                           ])
    newbutton = Button('New', url_for('.modify'))
    page = str(table)+'<div>'+newbutton+'</div>'
    
    return render_template("wadconfig/generic.html", title='Configs', subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/configs/duplicate/')
@login_required
def duplicate():
    """
    copy given id to new config, changing name and origin
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        nw_cfg = dbio.DBModuleConfigs.get_by_id(_gid).clone()
        nw_cfg.name = 'copy of '+nw_cfg.name
        nw_cfg.origin = 'user'
        nw_cfg.save()

    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/configs/delete/')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        cfg = dbio.DBModuleConfigs.get_by_id(_gid)
        if len(cfg.selectors) == 0:
            cfg.delete_instance(recursive=True)
        else:
            page = 'Config {} is coupled to Selector "{}". Deleting this Config would also delete that Selector and all its Results. If you do want to do that, delete Selector "{}". Else, couple the Selector "{}" to a different Config, and then delete this Config.'.format(cfg.id, cfg.selectors[0].name, cfg.selectors[0].name, cfg.selectors[0].name)
            return render_template("wadconfig/generic.html", title='Delete Config', subtitle='ERROR! Cannot delete Config!', msg='', html=Markup(page))
            
    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/configs/download/')
@login_required
def download():
    """
    download given id of given table from iqc db
    """
    _gid = int(request.args['cfgid']) if 'cfgid' in request.args else None
    
    # invalid table request are to be ignored
    if _gid is None:
        return redirect(url_for('.default'))

    row = dbio.DBModuleConfigs.get_by_id(_gid)
    # find the elements of the config.json to download
    filename = row.name+'.json'
    blob = row.val #str(base64.b64decode(row['val']))

    # make a bit more pretty
    blob = json.loads(bytes_as_string(blob))
    blob = string_as_bytes(json.dumps(blob, sort_keys=True, indent=4))

    return send_file(BytesIO(blob), as_attachment=True, attachment_filename=filename)#mimetype=None

@mod_blueprint.route('/configs/modify/', methods=['GET', 'POST'])
@login_required
def modify():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Modify module config'
    form = ModifyForm(None if request.method=="GET" else request.form)

    modules = [(m.id, m.name) for m in dbio.DBModules.select()]
    form.module.choices = [(m.id, m.name) for m in dbio.DBModules.select()]
    form.datatype.choices = [(m.id, m.name) for m in dbio.DBDataTypes.select()]
    if not _gid is None:
        config = dbio.DBModuleConfigs.get_by_id(_gid)
        form.name.data = config.name
        form.currentname.data = config.name
        form.description.data = config.description
        form.gid.data = _gid
        form.module.data = config.module.id
        form.datatype.data = config.data_type.id

    new_entry = False
    if form.gid is None or form.gid.data == ''  or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        new_entry = True
        formtitle = 'New module config'
 
    existingnames = [v.name for v in dbio.DBModuleConfigs.select()]

    # Verify the form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if new_entry:
            if form.name.data in existingnames:
                flash('A config with this name already exist.', 'error')
                valid = False
            if not 'configfile' in request.files or len(request.files['configfile'].filename) == 0:
                flash('No configfile chosen.', 'error')
                valid = False
        else: # not a new form
            pass

        field_dict = {k:v for k,v in request.form.items()}
        blob = None
        meta = None
        if valid:
            if 'configfile' in request.files and len(request.files['configfile'].filename) > 0:
                outname = upload_file(request.files['configfile'])  
                if not outname is None:
                    try:
                        with open(outname,'r') as fcfg: blob = fcfg.read()
                    except Exception as e:
                        flash('{} is not a valid config.json'.format(request.files['configfile'].filename), 'error')
                        valid = False
                        msg = ""
                    os.remove(outname)
                if valid:
                    valid, msg = configmaintenance.validate_json('configfile', blob)
                    if not valid:
                        flash('{} {}'.format(request.files['configfile'].filename, msg), 'error')
                
        if valid:
            if 'metafile' in request.files and len(request.files['metafile'].filename) > 0:
                outname = upload_file(request.files['metafile'])
                try:
                    with open(outname,'r') as fcfg: meta = fcfg.read()
                except Exception as e:
                    flash('{} is not a valid meta.json'.format(request.files['metafile'].filename), 'error')
                    valid = False
                    msg = ""
                os.remove(outname)
            if valid:
                valid, msg = configmaintenance.validate_json('metafile', meta)
                if not valid:
                    flash('{} {}'.format(request.files['metafile'].filename, msg), 'error')
            
        if valid:
            igid = 0
            if 'gid' in field_dict:
                try:
                    igid = int(field_dict['gid'])
                except:
                    pass
                
            if igid>0: # update, not create
                # check if a new config should be created or not:
                #  if the config is coupled and the module or the blob has changed, 
                #  then a new config should be created. Or should a coupled config remain unalterable
                #  for ever? A blob or module change can only occur if uncoupled?
                config = dbio.DBModuleConfigs.get_by_id(igid)
                inuse = (len(config.selectors)+len(config.processes)+len(config.results))>0
                if inuse:
                    if not blob is None or not config.module.id == int(field_dict['module']) or not config.data_type.id == int(field_dict['datatype']):
                        flash('Cannot change module or datatype or upload new file for a module config that is coupled.', 'error')  
                        return render_template("wadconfig/configs_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                                           title=formtitle, msg='Fill out the fields and click Submit')
                    
                config.name = field_dict['name']
                config.description = field_dict['description']
                config.module = int(field_dict['module'])
                config.data_type = int(field_dict['datatype'])
                config.origin = 'user'
                if not blob is None:
                    config.val = blob
                if not meta is None:
                    nw_meta = dbio.DBMetaConfigs.create(val=meta)
                    config.meta = nw_meta.id
                config.save()
            else:
                field_dict['val'] = blob
                if not meta is None:
                    nw_meta = dbio.DBMetaConfigs.create(val=meta)
                else:
                    nw_meta = dbio.DBMetaConfigs.create()
                field_dict['meta'] = nw_meta.id
                field_dict['data_type'] = int(field_dict['datatype'])
                field_dict['origin'] = 'user'
                dbio.DBModuleConfigs.create(**field_dict)
            return redirect(url_for('.default'))
            
    return render_template("wadconfig/configs_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Fill out the fields and click Submit')

