# Import flask dependencies
from flask import Blueprint, request, render_template, \
     flash, session, redirect, url_for, Markup

try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import INIFILE, dbio_connect, Button
    from app.libs import HTML as simplehtml
    import wadservices_communicate as wad_com

except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs.shared import INIFILE, dbio_connect, Button
    from wad_admin.app.libs import HTML as simplehtml
    import wad_admin.wadservices_communicate as wad_com

dbio = dbio_connect() 

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_blueprint = Blueprint('wadconfig', __name__, url_prefix='/wadadmin')

# Set the route and accepted methods
@mod_blueprint.route('/home/', methods=['GET', 'POST'])
@login_required
def home():
    """
    Just present a menu with choices; should all be require authorization
    """
    menus = [
    ]
    resourcesmenu=[
        { 'label':'server status', 'href':url_for('wadconfig.whatsup') }, #'whatsup'
        { 'label':'data sources', 'href':url_for('wadconfig_sources.default') },
        { 'label':'tags for selector rules', 'href':url_for('wadconfig_ruletags.default', gid=1) },
        { 'label':'WAD Processor', 'href':url_for('wadconfig_wadprocessor.default', gid=1) }, 
        { 'label':'WAD-QC database', 'href':url_for('wadconfig_dbtool.default') }, 
        { 'label':'User manager', 'href':url_for('auth_users.default') }, 
    ]

    configmenu=[
        { 'label':'factory modules', 'href':url_for('wadconfig_factorymodules.default', gid=1) }, 
        { 'label':'import modules', 'href':url_for('wadconfig_importexport.importing', gid=1) }, 
        { 'label':'export modules', 'href':url_for('wadconfig_importexport.exporting', gid=1) }, 
        { 'label':'backup selectors', 'href':url_for('wadconfig_backuprestore.backingup', gid=1) }, 
        { 'label':'restore selectors', 'href':url_for('wadconfig_backuprestore.restoring', gid=1) }, 
        { 'label':'selectors', 'href':url_for('wadconfig_selectors.default') }, 
    ]


    processmenu=[
        { 'label':'processes', 'href':url_for('wadconfig_processes.default') },
        { 'label':'results', 'href':url_for('wadconfig_results.default') }, 
    ]

    advancedmenu=[
        { 'label':'analysis modules', 'href':url_for('wadconfig_modules.default') },
        { 'label':'module configs',  'href':url_for('wadconfig_configs.default') }, 
        { 'label':'meta configs',  'href':url_for('wadconfig_metas.default') }, 
    ]

    menus.append({'title':'Resources', 'items':resourcesmenu})
    menus.append({'title':'Configuration', 'items':configmenu})
    menus.append({'title':'Processes/Results', 'items':processmenu})
    menus.append({'title':'Advanced', 'items':advancedmenu})
    return render_template("wadconfig/home.html", menus=menus, title='WAD-QC Server Administration')


@mod_blueprint.route('/whatsup/')
@login_required
def whatsup():
    # show the status of several services
    result = {}

    wp_start = Button('start', url_for('.wadcontrol', command='start'))
    wp_pause = Button('pause', url_for('.wadcontrol', command='pause'))
    wp_quit  = Button('quit', url_for('.wadcontrol', command='quit'))

    ort_start = Button('start', url_for('.orthanc', command='start'))
    ort_quit = Button('quit', url_for('.orthanc', command='quit'))

    psql_start = Button('start', url_for('.pgcontrol', command='start'))
    psql_reload = Button('reload', url_for('.pgcontrol', command='reload'))
    psql_quit = Button('quit', url_for('.pgcontrol', command='stop'))

    services = [ # process name, identifier on cmdline
                 {'name': 'postgresql', 'process':'postgresql', 'buttons':[psql_start, psql_reload, psql_quit]}, # at least for the Orthanc database
                 {'name': 'orthanc', 'process':'Orthanc', 'buttons':[ort_start, '', ort_quit]}, # preferred PACS solution 
                 {'name': 'wadprocessor', 'process':'wadprocessor', 'buttons':[wp_start, wp_pause, wp_quit]}, # wad_qc processor
                 ]    

    systemd_services = {'orthanc': 'wadorthanc', 'postgresql': 'wadpostgresql', 'wadprocessor': 'wadprocessor'}
    # set service status to NOT RUNNING
    for service in services:
        status = is_service_running(service['process'])
        if service['name'] in systemd_services.keys():
            systemd = wad_com.controlled_by_systemd(systemd_services[service['name']])
            if systemd:
                result["{} (controlled by systemd)".format(service['name'])] = {
                    'cell': simplehtml.TableCell(status[0], **status[1]) , 
                    'buttons':[]
                }
            else:
                result[service['name']] = {'cell': simplehtml.TableCell(status[0], **status[1]) , 'buttons':service['buttons']}

    table = simplehtml.Table(header_row=['Service',   'Status'])
    for service, status in sorted(result.items()):
        row = [service, status['cell']]
        row.extend(status['buttons'])
        table.rows.append(row)

    # versions
    hdr2 = "<H2>Versions</H2>"
    col_ok   = {'bgcolor':'yellowgreen'}
    col_bad =  {'bgcolor':'Red'}

    version_db   = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version').val
    version_dbio = dbio.__version__
    import pkg_resources
    version_whl = pkg_resources.get_distribution("wad_qc").version
    
    table2 = simplehtml.Table(header_row=['Item',   'Version'])
    results2 = {}
    results2['WAD-QC DataBase'] = {'val': version_db, 'col': col_ok if version_db>=version_dbio else col_bad }
    results2['wad_qc.dbio'] = {'val': version_dbio, 'col': col_ok if version_dbio>=version_db else col_bad }
    results2['wad_qc whl'] = {'val': version_whl, 'col':{} }
    
    for service, vals in sorted(results2.items()):
        row = [service, simplehtml.TableCell(vals['val'], **vals['col'])]
        table2.rows.append(row)
    table2 = hdr2 + str(table2)

    table = str(table)+str(table2)

    return render_template("wadconfig/generic.html", title='Whatsup', html=Markup(table))

def is_service_running(service):
    # show the status of several services
    # service = cli identifier
    col =  {'bgcolor':'Red'}
    if service == 'wadprocessor':
        stat = wad_com.wadcontrol('status', INIFILE)
        if stat == 'ERROR':
            stat = 'stopped'
    elif service == 'postgresql':
        stat = wad_com.postgresql('status')
    else:
        stat = wad_com.status(service)

    if stat == 'paused': 
        col = {'bgcolor':'yellow'}
    elif stat == 'running': 
        col = {'bgcolor':'yellowgreen'}
    status = (stat, col)

    return status

@mod_blueprint.route('/wadcontrol/')
@login_required
def wadcontrol():
    _command = request.args['command'] if 'command' in request.args else None
    if not _command in ['start', 'quit', 'status', 'pause']:
        return redirect(url_for('.whatsup'))

    wad_com.wadcontrol(_command, INIFILE)
    return redirect(url_for('.whatsup'))

@mod_blueprint.route('/pgcontrol/')
@login_required
def pgcontrol():
    _command = request.args['command'] if 'command' in request.args else None
    if not _command in ['start', 'status', 'reload', 'stop']:
        return redirect(url_for('.whatsup'))

    wad_com.postgresql(_command)
    return redirect(url_for('.whatsup'))

@mod_blueprint.route('/orthanc/')
@login_required
def orthanc():
    _command = request.args['command'] if 'command' in request.args else None
    if not _command in ['start', 'quit']:
        return redirect(url_for('.whatsup'))

    if _command == 'start':
        wad_com.start('Orthanc', INIFILE)
    elif _command == 'quit':
        wad_com.stop('Orthanc')

    return redirect(url_for('.whatsup'))
