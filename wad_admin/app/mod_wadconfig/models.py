# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
try:
    from app.libs.shared import dbio_connect
    from app import AUTO_REFRESH
except ImportError:
    from wad_admin.app.libs.shared import dbio_connect
    from wad_admin.app import AUTO_REFRESH
    
from peewee import CharField, IntegerField, ForeignKeyField
from werkzeug.security import generate_password_hash

"""
Only update the versionnumber if the database (tables, fields) changes.
Use versionnumber as "20160531" or "20171020.1", indicating the date as the major versionnumber,
 and an additional point release if needed.
"""
__version__ = "20180312" # only update this versionnumber if the database (tables, fields) changes and then write upgrade script!

"""
Changelog:
  20180404: added some more default tags
  20180312: added refresh to User; renamed User to WDUsers; renamed RuleTags to WARuleTags; 
            added wa_db_version to DBVariables to enable db upgrades; tables moved to wad_db
"""

dbio = dbio_connect()

# Define a User model
class WAUsers(dbio.DBModel):
    # User Name
    username = CharField(unique=True) 

    # Identification Data: email & password
    password = CharField() 
    email    = CharField()

    # Authorisation Data: role & status
    role     = IntegerField()
    status   = IntegerField()

    refresh  = IntegerField() # auto refresh timeout in seconds

    class Meta:
        order_by = ('username',)

# define the ruletags model
class WARuleTags(dbio.DBModel):
    # tag
    name   = CharField(max_length=64) 
    val    = CharField(max_length=20) 


WATables = [
    WAUsers,
    WARuleTags
]

def init_models():
    """
    Build the database: this should be run only once on first startup of WAD Admin.
    However, using safe=True prevents problems
    """
    
    # first tables need to be initialized!
    for tb in WATables:
        tb._meta.database = dbio.db

    dbio.db.create_tables(WATables, safe=True)

    if len(WAUsers.select()) == 0:
        WAUsers.create(username='root', password=generate_password_hash('waddemo'), email='root@localhost', role=100, status=1, refresh=AUTO_REFRESH)

    if len(WARuleTags.select()) == 0:
        WARuleTags.create(name = 'RemoteAET', val = 'RemoteAET')
        WARuleTags.create(name = 'StudyDescription',  val = '0008,1030')
        WARuleTags.create(name = 'SeriesDescription', val = '0008,103e')
        WARuleTags.create(name = 'Modality',        val = '0008,0060')
        WARuleTags.create(name = 'StationName',     val = '0008,1010')
        WARuleTags.create(name = 'DeviceSerialNumber', val = '0018,1000')
        WARuleTags.create(name = 'TransducerData',  val = '0018,5010')
        WARuleTags.create(name = 'FilterMaterial',  val = '0018,7050')
        WARuleTags.create(name = 'ProtocolName',    val = '0018,1030')
        WARuleTags.create(name = 'ImageType',       val = '0008,0008')
        WARuleTags.create(name = 'TubeOrientation', val = '2003,102C')
        WARuleTags.create(name = 'PatientName',     val = '0010,0010')

    if len(dbio.DBVariables.select().where(dbio.DBVariables.name == 'wa_db_version')) == 0:
        dbio.DBVariables.create(name='wa_db_version', val=__version__)
