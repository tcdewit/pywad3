"""
Routines for downloading and upgrading modules with factory modules in repositories.

This should be a collection of functional stuff for interfacing with WAD-QC through the web interface.

CHECK:
 o delete of config on upgrade for results, selectors, etc.
 o upgrade/overwrite
 
Changelog:
  20180315: Initial version
"""

from __future__ import print_function
import requests
import json
import os
import shutil
import zipfile
import tempfile

try:
    import configparser
except ImportError:
    import ConfigParser as configparser
    
from .exchange import make_factory_zip, import_configs, validate_import_manifest, add_folder_to_zip

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)


# URL of mother repository on github
GITHUBREPO = 'https://api.github.com/users/MedPhysQC/repos'
def is_release_candidate(version):
    """
    check if string resolves to a release candidate
    """
    return split_release_version(version)[2]

def is_newer_release(old, new):
    """
    Check if a new release is newer. If old release unknown, return False
    """
    try:
        old_version = split_release_version(old)
    except Exception as e:
        return False

    try:
        new_version = split_release_version(new)
    except Exception as e:
        return False

    
    # check major version
    if new_version[0]<old_version[0]:
        return False
    elif new_version[0]>old_version[0]:
        return True

    # major version same, check minor
    if new_version[1]<old_version[1]:
        return False
    elif new_version[1]>old_version[1]:
        return True

    # major and minor version same, check release candidate status
    if not old_version[2] and new_version[2]:
        return False
    elif old_version[2] and not new_version[2]:
        return True
    
    # major and minor version same, check release candidate
    if new_version[3]<old_version[3]:
        return False
    elif new_version[3]>old_version[3]:
        return True
    
    # they are the same
    return False

def split_release_version(rel):
    """
    Release "v1.0-rc.2" -> (1, 0, True, 2)
    Release "v1.0" -> (1, 0, False, 0)
    """
    major = 0
    minor = 0
    rc = False
    rcnum = 0

    try:
        # drop v
        rel = rel[1:]
    
        # get release candidate
        if '-rc.' in  rel:
            rc = True
            rel, rcnum = rel.split('-rc.')
        
        # get major, minor
        major, minor = rel.split('.')
    except Exception as e:
        raise ValueError("{}. Invalid release version number: {}".format(e, rel))

    return (int(major), int(minor), rc, int(rcnum))

def check_repo(dbio, url=GITHUBREPO, logger=None):
    """
    Query repo for modules, and return dictionary {module_name:(tag_name, published_at, zipball_url)}
    """
    #First we obtain all the repos that contain releases of analysis modules
    available_modules = {}
    msg = ''
    failed = []
    
    # allow for credentials from ini
    authfile = os.path.join(dbio.DBVariables.get_by_name('wadqcroot').val, 'repo.ini')
    try:
        config = configparser.SafeConfigParser()
        with open(authfile,'r') as f:
            config.readfp(f)
        creds = ( config.get(url, "USER"), config.get(url, "PSWD") )
    except:
        creds = None

    try:
        if creds is None:
            r = requests.get(url, headers={"User-Agent":"MedPhysQC"})
        else:
            r = requests.get(url, headers={"User-Agent":"MedPhysQC"}, auth = creds)

        json_data = json.loads(r.text)
    
        #For each repo we obtain release data
        for elem in json_data:
            tmprepo = (elem['name'], elem['releases_url'].replace('{/id}',''))
            #
            if creds is None:
                tmpr = requests.get(tmprepo[1], headers={"User-Agent":"MedPhysQC"})
            else:
                tmpr = requests.get(tmprepo[1], headers={"User-Agent":"MedPhysQC"}, auth = creds)

            release_data = json.loads(tmpr.text)

            if len(release_data) >0:
                available_modules[elem['name']] = []
            for release in release_data:
                try:
                    available_modules[elem['name']].append({'release_version': release['tag_name'],
                                                            'release_date': release['published_at'],
                                                            'release_url': release['zipball_url'],
                                                            'release_note': release['body'],
                                                            'repo_url': release['url'].split('/releases')[0],
                                                            'repo_dox': "{}/wiki".format(elem['html_url'])})
                except Exception as e:
                    failed.append(elem['name'])
                    logger.error("check_repo failed for {}: {}".format(elem['name'], e))

    except Exception as e:
        if 'message' in json_data:
            msg = json_data['message']
        else:
            msg = "ERROR! Could not query {} for modules. Please try again later.".format(url)
        logger.error(msg)

    if len(failed)>0:
        msg += "check_repo failed for: " +", ".join(failed)

    return available_modules, msg


def _url_to_module(url, repo_info, tempdir, logger=None):
    """
    helper to make a importable module in premade tempdir. checks for consistencies, returns filename. does not delete tempdir.

    return error, msg, outfile
    """

    if logger is None:
        logger = DummyLogger()
        
    outfile = None
    msg = ""
    error = False

    fname   = os.path.join(tempdir, "mod.zip")
    moddir  = os.path.join(tempdir, "mod")
    os.mkdir(moddir)
    outdir  = os.path.join(tempdir, "out")
    os.mkdir(outdir)
    try:
        # download stream
        r = requests.get( url, stream=True )

        # and save to file
        if r.status_code == 200:
            with open(fname, 'wb') as f:
                f.write(r.raw.read())


            # unpack zipfile
            with zipfile.ZipFile(fname, 'r') as z:
                z.extractall(moddir)
            
            # check if 1 manifest.json is present in archive:
            manifests = []
            for subdir, dirs, files in os.walk(moddir, topdown=True):
                for fname in files:
                    if fname == 'manifest.json':
                        manifests.append(os.path.join(subdir, fname))
    
            if not len(manifests) == 1:
                print(manifests)
                error = True
                msg = "ERROR! Number of manifest.json files should be 1. Found {}.".format(len(manifests))
                logger.error(msg)
            
            # now make and import.zip
            if not error:
                error, msg, outfile = make_factory_zip(manifests[0], 'zip_module', 
                                                       repo_info,
                                                       outdir=outdir, logger=logger)
        else:
            error = True
            msg = "ERROR! Could not download from {}. Please try again later. ".format(url)
            logger.error(msg)

    except Exception as e:
        error = True
        msg += "ERROR! {}".format(e)
        logger.error(msg)

    return error, msg, outfile


def install_from_url(dbio, url, repo_info, logger=None):
    """
    Get zipball from url, make factory module and import
    """
    if logger is None:
        logger = DummyLogger()
        
    msg = ""
    error = False

    # make tmpdir
    temp_root = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_root):
        os.makedirs(temp_root)
    tempdir = tempfile.mkdtemp(dir=temp_root)
    
    error, msg, outfile = _url_to_module(url, repo_info, tempdir, logger=logger)
    # import import.zip
    if not error:
        valid, msg = import_configs(dbio, outfile, logger)
        error = not valid

    # clean up
    shutil.rmtree(tempdir, ignore_errors=True)
    return error, msg

def replace_from_url(dbio, url, repo_info, mod_id, logger=None):
    """
    Get zipball from url, make factory module. 
    Update old module code with new zipped module.
    Delete old configs, and install new configs.
    """
    if logger is None:
        logger = DummyLogger()
        
    msg = ""
    error = False

    # the installed module to replace
    current_mod = dbio.DBModules.get_by_id(mod_id)
    current_dbversion = dbio.DBVariables.get_by_name('iqc_db_version').val
    
    # make tmpdir
    temp_root = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_root):
        os.makedirs(temp_root)
    tempdir = tempfile.mkdtemp(dir=temp_root)
    
    # make import.zip
    error, msg, outfile = _url_to_module(url, repo_info, tempdir, logger=None)

    # extract content of zipball module in module folder
    unpackdir = tempfile.mkdtemp(dir=temp_root)
    
    # unpack zipfile
    with zipfile.ZipFile(outfile, 'r') as z:
        z.extractall(unpackdir)

    # read manifest.ini and check consistency
    with open(os.path.join(unpackdir, 'manifest.json'),'r') as f:
        manifest = json.load(f)
    valid, msg = validate_import_manifest(dbio, unpackdir, check_existing_names=False, logger=logger)

    if valid:
        import_dbversion = manifest['info']['dbversion']
        modules = manifest['modules']
        configs = manifest['configs']

        if len(modules) != 1:
            valid = False
            msg = "ERROR! Expected only 1 module, but import contains {} modules.".format(len(modules))

    # all checks were valid. zip new module and upload it for modification
    if valid:
        try:
            nwmod = list(modules.values())[0]
            nwmod_folder = os.path.join(unpackdir, 'modules', nwmod['folder'])
            zipname = nwmod_folder+'.zip'
            if os.path.exists(zipname):
                os.remove(zipname)
            with zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED) as zf:
                add_folder_to_zip(zf, nwmod_folder, '', mode='zip_module')
        
            current_mod.name        = nwmod['name']
            current_mod.description = nwmod['description']
            current_mod.filename    = nwmod['executable']
            current_mod.origin      = nwmod['origin']
            current_mod.repo_url     = nwmod['repo_url']
            current_mod.repo_version = nwmod['repo_version']
            current_mod.save(uploadfilepath=zipname)
            logger.info('Replaced module {}'.format(nwmod['name']))
                        
        except Exception as e:
            msg = 'Error during import of module "%s". '\
                'DBVersion of imported package is %s (current: %s)'%(nwmod['name'], import_dbversion, current_dbversion)+str(e)
            valid = False
            
    # now remove old ModuleConfigs and upload new ModuleConfigs with MetaConfigs
    #  remove old connected ModuleConfigs, but leave those connected to Processes or Results or Selectors
    del_configs = [ cfg for cfg in current_mod.module_configs if (not len(cfg.selectors) and 
                                                              not len(cfg.processes) and 
                                                              not len(cfg.results)) ]

    for cfg in del_configs:
        cfg.delete_instance(recursive=True)

    if valid:
        try:
            for cfg in configs.values():
                fname = os.path.join(unpackdir, 'configs', cfg['filename'])
                with open(fname,'r') as fcfg: blob = fcfg.read()
                field_dict = {
                    'name': cfg['name'],
                    'description': cfg['description'],
                    'origin': cfg['origin'],
                    'modulename': modules[cfg['module']]['name'],
                    'datatypename': cfg['datatype'],
                    'val': blob
                }
                meta = None
                if 'metafilename' in cfg:
                    fname = os.path.join(unpackdir, 'metas', cfg['metafilename'])
                    with open(fname,'r') as fcfg: meta = fcfg.read()
                if not meta is None:
                    nw_meta = dbio.DBMetaConfigs.create(val=meta)
                else:
                    nw_meta = dbio.DBMetaConfigs.create()
                field_dict['meta'] = nw_meta.id

                dbio.DBModuleConfigs.create(**field_dict)
                logger.info('Imported config {}'.format(cfg['name']))
    
        except Exception as e:
            msg = 'Error during import of module_config "%s". '\
                'DBVersion of imported package is %s (current: %s)'%(cfg['name'], import_dbversion, current_dbversion)+str(e)
            valid = False


    # clean up unpacked zipfile
    shutil.rmtree(unpackdir, ignore_errors=True)
    
    # clean up
    shutil.rmtree(tempdir, ignore_errors=True)

    error = not valid
    return error, msg

    
