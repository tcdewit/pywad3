import os
from wad_qc.connection import dbio
from werkzeug import secure_filename
import tempfile

INIFILE = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

isconnected = False
def dbio_connect():
    global isconnected
    if not isconnected:
        dbio.db_connect(INIFILE)
        isconnected = True
    return dbio

def Button(label, href, _class=None):
    if _class is None:
        result = '<a href="%s"> <input type="button" value="%s" /></a>'%(href, label)
    else:
        result = '<a class="%s" href="%s" role="button">%s</a>'%(_class, href, label) # bootstrap
    return result

def Image(label, src, width=None):
    if width is None:
        result = '<img src="%s" alt="%s">'%(src, label)
    else:
        result = '<img src="%s" alt="%s" width="%d">'%(src, label, width)
    return result

def upload_file(f):
    outname = None
    if not len(f.filename) == 0:
        uploadfolder = os.path.join(dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val, 'Upload')
        ext = os.path.splitext(f.filename)[1]
        if not os.path.exists(uploadfolder):
            os.mkdir(uploadfolder)
        handle, outname = tempfile.mkstemp(dir=uploadfolder, suffix=ext) # must delete this file manually
        f.save(outname)
        os.close(handle)
    return outname

def getIpAddress(dummy):
    """
    dummy param is just to ensure this function is not available directly from the website
    """
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('1.1.1.1', 1))
    except socket.error as e:
        # No network, but we are called from a website, so the host address should be localhost
        return 'localhost'
    
    return s.getsockname()[0]
