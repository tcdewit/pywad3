"""
Routines for deleting and downloading of datasets.

This should be a collection of functional stuff for interfacing with WAD-QC through the web interface.

Changelog:
  20171021: Initial version
"""
from __future__ import print_function
import os
import json
import jsmin

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)


# validate json
def validate_json(filetype, info):
    """
    validate if the uploaded json has the correct fields
    """
    valid = True
    msg = ""
    if info is None:
        return valid, msg

    try:
        blob = json.loads(jsmin.jsmin(info))
    except ValueError as e:
        valid = False
        msg = "file cannot be decoded as json: {}".format(e)
        return valid, msg

    if filetype == 'metafile':
        """
        results:
          <name>:
            <items>

        comments:
          <items>
        """
        # check for required items
        required = ['results']
        for req in required:
            val = blob.get(req, None)
            if val is None:
                valid = False
                msg = "'{}' must be defined in meta.json".format(req)
                return valid, msg
            if not isinstance(val, dict):
                valid = False
                msg = "Key '{}' in meta.json must be a dictionary".format(req)
                return valid, msg
        
        # check for extra items
        accepted = required
        required.extend(['comments'])
        for key,val in blob.items():
            if key not in accepted:
                valid = False
                msg = "Key '{}' invalid for meta.json".format(key)
                return valid, msg
                
    elif filetype == 'configfile':
        """
        actions:
          <name>:
            params:
            
        comments:
          author:
          description:
          version:
          params:
        """
        # check for required items
        required = ['comments', 'actions']
        for req in required:
            val = blob.get(req, None)
            if val is None:
                valid = False
                msg = "'{}' must be defined in config.json".format(req)
                return valid, msg
            if not isinstance(val, dict):
                valid = False
                msg = "Key '{}' in config.json must be a dictionary".format(key)
                return valid, msg
        
        # check for extra items
        accepted = required
        for key,val in blob.items():
            if key not in accepted:
                valid = False
                msg = "Key '{}' invalid for config.json".format(key)
                return valid, msg

        # check actions
        actions = blob.get('actions', None)
        if actions is None or not isinstance(actions, dict) or len(actions.keys()) == 0:
            valid = False
            msg = "No actions defined"
            return valid, msg
            
        for key, act in actions.items():
            pars = act.get('params', None)
            if pars is None or not isinstance(pars, dict):
                valid = False
                msg = "No params defined for action {}".format(key)
                return valid, msg
            
        # check comments
        comments = blob.get('comments', None)
        for req in ['author', 'description', 'version']:
            val = comments.get(req, None)
            if val is None:
                valid = False
                msg = "Missing required field '{}' in comments".format(req)
                return valid, msg

    else:
        valid = False
        msg = "filetype '{}' is not a recognized json file to validate".format(filetype)
        logger.error(msg)

    return valid, msg