"""
Routines for deleting and downloading of datasets.

This should be a collection of functional stuff for interfacing with WAD-QC through the web interface.

Changelog:
  20180731: fix missing series_date if not found.
  20180313: removed temporary cascade fix
  20180131: peewee3 no longer supports set_autocommit
  20171021: Initial version
"""
from __future__ import print_function
import os
import shutil
import zipfile
import tempfile
from sys import version_info
from wad_qc.connection.pacsio import PACSIO
from .exchange import add_folder_to_zip
from peewee import ImproperlyConfigured

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)

# delete from pacs
def sources_delete_datasets(dbio, data, logger=None):
    """
    data[source_id] = {dtype: [data_id]}
    Permanently delete list of files
    """
    if logger is None:
        logger = DummyLogger()
        
    error = False
    msg = ""

    resnum = 0
        
    for pacs_id in data.keys():
        pacsconfig = dbio.DBDataSources.get_by_id(pacs_id).as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            error = True
            return error, msg

        for dtype in data[pacs_id].keys():
            for data_id in data[pacs_id][dtype]:
                pacsio.deleteData(data_id, dtype)
                resnum += 1

    msg ='Deleted {} data sets from Sources.'.format(resnum)
    return error, msg

# download from pacs
def sources_download_datasets(dbio, tmpf, data, logger=None):
    """
    data[source_name] = {dtype: [data_id]}
    Download list of files
    """
    if logger is None:
        logger = DummyLogger()
        
    error = False
    msg = ""

    # create downloadfolder
    downloadfolder = os.path.join(dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val, 'Download')
    if not os.path.exists(downloadfolder):
        os.mkdir(downloadfolder)
    outfolder = tempfile.mkdtemp(dir=downloadfolder,prefix='dat')
    
    resnum = 0
        
    for pacs_id in data.keys():
        pacsconfig = dbio.DBDataSources.get_by_id(pacs_id).as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            error = True
            return error, msg, resnum

        for dtype in data[pacs_id].keys():
            for data_id in data[pacs_id][dtype]:
                outf = os.path.join(outfolder, '{}'.format(resnum).zfill(6))
                os.mkdir(outf)
                pacsio.getData(data_id, dtype, outf)
                resnum += 1


    if resnum > 0:
        # now zip complete folder
        # maybe start a separate process for this? save to zip in download, and signal when ready?
        with zipfile.ZipFile(tmpf, 'w', zipfile.ZIP_DEFLATED) as zf:
            add_folder_to_zip(zf, outfolder, 'dcm', 'zip_dcm')
        
    # clean up temporary stuff
    shutil.rmtree(outfolder)

    error = False
    msg = "Retrieved {} datasets from Sources".format(resnum)
    return error, msg, resnum
        
# helper functions
def intersection_dicts(a, b):
    """
    returns the intersection of dict a an b
    """
    # for python2 and 3
    if version_info[0] < 3:
        # return { i:j for i,j in a.viewitems() & b.viewitems() } # does not work for dict of dict
        return { key: val  for key, val in a.iteritems() if b.get(key,None) == val}
    else:
        # return { i:j for i,j in a.items() & b.items() }
        return { key: val  for key, val in a.items() if b.get(key,None) == val}
        
def source_select_data(pacsio, data, filters, return_headers, logger=None):
    """
    test if a (srcname, dtype, dataid) should be include in results.
    if return_headers is True, a tuple (TrueFalse, header_dict is returned),
    else only TrueFalse, {} is returned.
    """
    if logger is None:
        logger = DummyLogger()
        
    if return_headers == False and filters == {}:
        return True, {}
    
    aettag = 'RemoteAET'.lower()
    srcname, dtype, dataid = data
    hdrs = {}
    if dtype == 'dcm_study':
        # just take the intersection of the first instances of the series, 
        # as that is faster than calculating full shared hdrs, 
        # and we don't need the complete set (like we need for the selector to be correct)
        series = pacsio.getSeriesIds(dataid)
        for ser in series:
            instances = pacsio.getInstancesIds(ser)
            if len(instances)>0:
                h =  pacsio.getInstanceHeaders(instances[0])
                if len(hdrs) == 0:
                    hdrs = h
                else:
                    hdrs = intersection_dicts(hdrs, h)

    elif dtype == 'dcm_series':
        # just take the first instance of the series, as that is faster than calculating shared hdrs,
        # and we don't need the complete set (like we need for the selector to be correct)
        instances = pacsio.getInstancesIds(dataid)
        if len(instances)>0:
            hdrs = pacsio.getInstanceHeaders(instances[0])

    elif dtype == 'dcm_instance':
        hdrs = pacsio.getInstanceHeaders(dataid)

    data_values = {
        'modality': hdrs["0008,0060"]["Value"] if "0008,0060" in hdrs else "",
        'pat_name': hdrs["0010,0010"]["Value"] if "0010,0010" in hdrs else "",
        'study_desc': hdrs["0008,1030"]["Value"] if "0008,1030" in hdrs else "",
        'study_date': hdrs["0008,0020"]["Value"] if "0008,0020" in hdrs else "",
        'series_desc': hdrs["0008,103e"]["Value"] if "0008,103e" in hdrs else "",
        'prot_name': hdrs["0018,1030"]["Value"] if "0018,1030" in hdrs else "",
        'stat_name': hdrs["0008,1010"]["Value"] if "0008,1010" in hdrs else "",
        'aet': hdrs[aettag]["Value"] if aettag in hdrs else ""}

    # SeriesDate
    for tag in ["0008,0021", "0008,0022", "0008,0012", "0008,0023", "0008,0020"]: #SeriesDate, AcquisitionDate, InstanceCreationDate, ContentDate, StudyDate
        if tag in hdrs and not hdrs[tag] == "":
            data_values['series_date'] = hdrs[tag]["Value"]
            
    if not 'series_date' in data_values.keys():
        logger.error(dtype, dataid)
        data_values['series_date'] = ""
        
    # restrict output
    for key, value in data_values.items():
        if key in filters.keys():
            if not filters[key].lower() in value.lower():
                return False, data_values

    return True, data_values

# handle data in sources, but not used in WAD-QC
def _get_for_source_level(dbio, model, sourcename, level):
    """
    return a set of items of model of sourcename at level
    """
    wadjoined = model.select().join(dbio.DBDataSources).switch(model).join(dbio.DBModuleConfigs).join(dbio.DBDataTypes)
    wads = wadjoined.where(
        ( dbio.DBDataSources.name == sourcename ) & 
        ( dbio.DBDataTypes.name == level ))
    
    results = set([s.data_id for s in wads])
    return results

def source_find_unpicked(dbio, pacsio, sourcename, datalevel, logger=None):
    """
    find all unpicked datasets of datasource of datalevel
    """
    if logger is None:
        logger = DummyLogger()
        
    results = []

    # build a list of all occuring dataids in Processes and Results
    wadids = {}
    for level in ['dcm_study', 'dcm_series', 'dcm_instance']:
        wadids[level] = _get_for_source_level(dbio, dbio.DBProcesses, sourcename, level)
        wadids[level] = wadids[level].union( _get_for_source_level(dbio, dbio.DBResults, sourcename, level) )
 
    # first check studies
    for pacs_study_id in pacsio.getStudyIds():
        matched_study = False

        # first check studies
        if pacs_study_id in wadids['dcm_study']: # found a match on study level, so skip to next study:
            matched_study = True
            continue # next study
        
        # no study match, try series
        series = [] # series to check instances of
        for pacs_series_id in pacsio.getSeriesIds(studyid=pacs_study_id):
            if pacs_series_id in wadids['dcm_series']: # found a match on series level, so matched_study is also True
                matched_study = True
                continue # next series
            else:
                series.append(pacs_series_id)

        # instances
        unmatched_series = []
        unmatched_instances = []
        for series_id in series: # all unmatched series so far
            instances = [] # will hold all instances of this series without a match
            matched_series = False
            for instance_id in pacsio.getInstancesIds(seriesid=series_id): # all instances for this series
                if instance_id in wadids['dcm_instance']: # found a match on instance level, so matched_study and match_series are also True
                    matched_study = True
                    matched_series = True
                else:
                    instances.append(instance_id)
            if not matched_series:
                unmatched_series.append(series_id) # only store the orphan series, forget instances
            else:
                unmatched_instances.extend(instances) # some instances part of series do not match
                
        # generate results
        if not matched_study: # the whole study does not match, don't need to mention series and instances
            if datalevel in ['dcm_study', 'all']:
                results.append([sourcename, 'dcm_study', pacs_study_id])
        else: # part of the study matches, but some complete series or instances do not match
            if datalevel in ['dcm_series', 'all']:
                for s in unmatched_series: # part of the study matches, but some complete series do not match
                    results.append([sourcename, 'dcm_series', s])
            if datalevel in ['dcm_instance', 'all']:
                for s in unmatched_instances:
                    results.append([sourcename, 'dcm_instance', s])
    
    return results


def source_find_unmatched(dbio, pacsio, sourcename, wsel, datalevel, logger=None):
    """
    wsel = an instance of wad_core.selector.Selector
    returns all studies in PACS without a possible match: 
      1. get a list of data not in Results/Processes, and for each item dryrun WADSelector.
    """
    if logger is None:
        logger = DummyLogger()
        
    results = []

    # build a list of all occuring dataids in Processes and Results
    wadids = {}
    for level in ['dcm_study', 'dcm_series', 'dcm_instance']:
        wadids[level] = _get_for_source_level(dbio, dbio.DBProcesses, sourcename, level)
        wadids[level] = wadids[level].union( _get_for_source_level(dbio, dbio.DBResults, sourcename, level) )
 
    # first check studies
    for pacs_study_id in pacsio.getStudyIds():
        matched_study = False

        # first check studies
        if pacs_study_id in wadids['dcm_study']: # found a match on study level, so skip to next study:
            matched_study = True
            continue # next study
        
        # no study match, try series
        series = [] # series to check instances of
        for pacs_series_id in pacsio.getSeriesIds(studyid=pacs_study_id):
            if pacs_series_id in wadids['dcm_series']: # found a match on series level, so matched_study is also True
                matched_study = True
                continue # next series
            else:
                series.append(pacs_series_id)
            
        # instances
        unmatched_series = []
        unmatched_instances = []
        for series_id in series: # all unmatched series so far
            instances = [] # will hold all instances of this series without a match
            matched_series = False
            for instance_id in pacsio.getInstancesIds(seriesid=series_id): # all instances for this series
                matched_series = False
    
                if instance_id in wadids['dcm_instance']: # found a match on instance level, so matched_study and match_series are also True
                    matched_study = True
                    matched_series = True
                else:
                    instances.append(instance_id)
            if not matched_series:
                unmatched_series.append(series_id) # only store the orphan series, forget instances
            else:
                unmatched_instances.extend(instances) # some instances part of series do not match
                
        # generate results
        if not matched_study: # the whole study does not match, don't need to mention series and instances
            # dryrun WAD Selector and add matches to wadids. then check again
            level = 'dcm_study'
            wsel.run(sourcename, pacs_study_id, dryrun=True, datalevel=level, include_inactive=False)
            matches = wsel.getDryrun() 
            
            #{'selectorname':selname, 'datasourcename':sourcename, 'data_id':data_id, 'datatype':sel['datatype_name']})
            for mat in matches:
                wadids[mat['datatype']].add(mat['data_id'])

            # check again after dryrun update
            if not pacs_study_id in wadids[level]:
                if datalevel in [level, 'all']:
                    results.append([sourcename, level, pacs_study_id])
                    
        else: # part of the study matches, but some complete series or instances do not match
            for s in unmatched_series: # part of the study matches, but some complete series do not match
                level = 'dcm_series'
                # dryrun WAD Selector and add matches to wadids. then check again
                wsel.run(sourcename, s, dryrun=True, datalevel=level, include_inactive=False)
                matches = wsel.getDryrun() 
                for mat in matches:
                    wadids[mat['datatype']].add(mat['data_id'])

                # check again after dryrun update
                if not s in wadids[level]:
                    if datalevel in [level, 'all']:
                        results.append([sourcename, level, s])
                    
            for s in unmatched_instances:
                level = 'dcm_instance'
                # possibly the instance is now matched through the umatched_series check above, so check again.
                if s in wadids[level]:
                    continue # next instance

                # dryrun WAD Selector and add matches to wadids. then check again
                wsel.run(sourcename, s, dryrun=True, datalevel=level, include_inactive=False)
                matches = wsel.getDryrun() 
                for mat in matches:
                    wadids[mat['datatype']].add(mat['data_id'])

                # check again after dryrun update
                if not s in wadids[level]:
                    if datalevel in [level, 'all']:
                        results.append([sourcename, level, s])
    
    return results

def sources_delete_unmatched(dbio, wsel, datatype, mode, filters, logger=None):
    """
    wsel = an instance of wad_core.selector.Selector
    real delete function for selection in consistency
    """
    if logger is None:
        logger = DummyLogger()
    
    error = False
    msg = ""
    
    deleted = {}
    notdeleted = {}

    selection = {}
    for pacs in dbio.DBDataSources.select():
        pacsconfig = pacs.as_dict()
        deleted[pacs.name] = 0
        notdeleted[pacs.name] = 0
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            error = True
            return error, msg

        selection[pacs.id] = {}
        if mode == 'nomatch':
            results = source_find_unmatched(dbio, pacsio, pacsconfig['name'], wsel, 
                                            datalevel=datatype, logger=logger) # only requested type
        else:
            results = source_find_unpicked(dbio, pacsio, pacsconfig['name'], 
                                           datalevel=datatype, logger=logger) # only requested type

        for srcname,dtype,dataid in results: # we know how to delete that one
            do_include, hdrs = source_select_data(pacsio, (srcname, dtype, dataid), filters, 
                                                  return_headers=False, logger=logger)
            # restrict output
            if do_include == False: 
                continue
            if not dtype in selection[pacs.id].keys():
                selection[pacs.id][dtype] = []
            selection[pacs.id][dtype].append(dataid)

    return sources_delete_datasets(dbio, selection, logger)

# clean starts
def waddb_cleanstart(dbio, inifile, logger):
    """
    Start anew. Remove everything from the WAD-QC database and create a new one.
    """
    if logger is None:
        logger = DummyLogger()

    error = False
    msg = ""
    
    # delete all modules
    for mod in dbio.DBModules.select():
        mod.delete_instance(recursive=True)
        
    # delete database if sqlite
    with dbio.db.atomic() as txn:
        dbio.db.drop_tables(dbio.DBTables, safe=True, cascade=True)

    # recreate database
    try:
        dbio.db.close()
        dbio.db_create_only(inifile, os.path.join(os.path.dirname(inifile), 'wadsetup.ini'))
        dbio.db_connect(inifile)
    except ImproperlyConfigured:
        msg = 'ERROR creating the WAD-QC database.'
        logger.info(msg)
        error = True

    return error, msg

def waddb_truncate(dbio, logger):
    # truncate all results tables in WAD-QC database
    if logger is None:
        logger = DummyLogger()

    error = False
    msg = ""

    tables = [
        dbio.DBProcesses,
        dbio.DBResultDateTimes, 
        dbio.DBResultStrings, 
        dbio.DBResultFloats,
        dbio.DBResultBools, 
        dbio.DBResultObjects,
        dbio.DBResults 
    ]
    try:
        with dbio.db.atomic() as txn:
            # truncate_tables not supported in peewee3
            #dbio.db.truncate_tables(tables, cascade=True)
            dbio.db.drop_tables(tables, safe=True, cascade=True)
            dbio.db.create_tables(tables)
    except Exception as e:
        if "unexpected keyword argument 'cascade'" in str(e):
            logger.info(' '.join(["Broken peewee 3.0; cascade not implemented:",str(e)]))
            try:
                with dbio.db.atomic() as ctx:
                    dbio.db.drop_tables(tables, safe=True)
                    dbio.db.create_tables(tables)
            except Exception as e2:
                logger.info(' '.join(["Could not drop database, assuming it does not exist:",str(e2)]))
        else:
            msg = ' '.join(["Could not truncate results tables",str(e)])
            logger.info(msg)
            error = True

    return error, msg