"""
Routines for deleting and downloading of datasets.

This should be a collection of functional stuff for interfacing with WAD-QC through the web interface.

Changelog:
  20171021: Initial version
"""
from __future__ import print_function
import os
import shutil
import zipfile
import json
import jsmin
import tempfile
from wad_qc.connection.pacsio import PACSIO
from .exchange import add_folder_to_zip

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)


# download from sources
def sources_download_datasets(dbio, tmpf, data, datatype, logger=None):
    """
    data[source_name] = {dtype: set(data_id)}
    Download list of files
    """
    if logger is None:
        logger = DummyLogger()
        
    error = False
    msg = ""

    # create downloadfolder
    downloadfolder = os.path.join(dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val, 'Download')
    if not os.path.exists(downloadfolder):
        os.mkdir(downloadfolder)
    outfolder = tempfile.mkdtemp(dir=downloadfolder,prefix='sel')
    
    resnum = 0

    for pacs_id in data.keys():
        pacsconfig = dbio.DBDataSources.get_by_id(pacs_id).as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'], str(e)])
            logger.error(msg)
            error = True
            return error, msg, resnum

        for data_id in data[pacs_id]:
            outf = os.path.join(outfolder, '{}'.format(resnum).zfill(6))
            os.mkdir(outf)
            pacsio.getData(data_id, datatype, outf)
            resnum += 1

    if resnum >0 :
        # now zip complete folder
        with zipfile.ZipFile(tmpf, 'w', zipfile.ZIP_DEFLATED) as zf:
            add_folder_to_zip(zf, outfolder, 'dcm', mode='zip_dcm')

    # clean up temporary stuff
    shutil.rmtree(outfolder)
            
    error = False
    msg = "Retrieved {} datasets from Sources".format(resnum)
    return error, msg, resnum

# handle forms as dicts
def handle_rules(dbio, selid, field_dict, nwtagval, logger=None):
    """
    Compare the details of the selector as send from a form (as a dict) to the detail in the database.
    The nwtagval is used as the tagval for a new tag.
    Returns whether the number of rules has changed (in order to refresh the page)
    """
    #
    if logger is None:
        logger = DummyLogger()

    numchanged = False
    selector = dbio.DBSelectors.get_by_id(selid)
    if 'addrule' in field_dict:
        selector.addRule(dicomtag=nwtagval, logicname='equals', values=[])
        numchanged = True

    # change field values
    for f in  field_dict.keys():
        # fill tag and logic, then save
        if f.endswith('-ruleid'):
            rule = dbio.DBSelectorRules.get_by_id(int(field_dict[f]))
            keyprefix = f.replace('-ruleid', '')
            rule.dicomtag = field_dict[keyprefix+'-tag']
            rule.logic = int(field_dict[keyprefix+'-logic'])
            rule.save()

            # check rule_values, if different
            # delete rule_values, and build new ones if not ''
            newvalues = []
            for k in range(5):
                val = field_dict[keyprefix+'-value%d'%k]
                if not val == '':
                    newvalues.append(val)
            newvalues = set(newvalues)        
            oldvalues = set([v.val for v in rule.values])
            if newvalues != oldvalues:
                for r in rule.values:
                    r.delete_instance()
                for v in newvalues:
                    dbio.DBRuleValues.create(**{'val':v, 'rule':rule.id})
        
    # see if any rules need deleting
    delrules = []
    for f in  field_dict.keys():
        #'rules-0-delrule'
        #'rules-1-ruleid': '3'
        if f.endswith('-delrule'):
            key = f.replace('-delrule','-ruleid')
            delrules.append(int(field_dict[key]))


    for d in delrules:
        dbio.DBSelectorRules.get_by_id(d).delete_instance(recursive=True)
        numchanged = True
        
    return numchanged

def paramsblock(dbio, config_id, logger=None):
    """
    1. read the config from the database for given config
    2. extract the actions
    3. for each action, extract the params
    4. Return a subpage with all actions and params
    """
    if logger is None:
        logger = DummyLogger()

    result = []
    if config_id is None:
        return result
    
    #1. read the config from the database
    config = dbio.DBModuleConfigs.get_by_id(int(config_id)).val
    try:
        config = json.loads(jsmin.jsmin(bytes_as_string(dbio.DBModuleConfigs.get_by_id(int(config_id)).val)))
    except ValueError as e:
        logger.error("config {} is not valid json: {}".format(config_id, e))
        return result
    
    #2. extract the actions
    actions_dict = config['actions']
    comments_dict = config.get('comments', {})

    actions_names = sorted(actions_dict.keys())
    #3. for each action, extract the params
    for ia,act_key in enumerate(actions_names):
        actname = act_key
        params_dict = actions_dict[act_key]['params']
        info_dict = comments_dict['params'] if 'params' in comments_dict else {}
        par_names = sorted(params_dict.keys())
        for ip, parname in enumerate(par_names): 
            parval = params_dict[parname]
            info = info_dict[parname] if parname in info_dict else ''
            result.append({'action':act_key,
                           'param':parname,
                           'value':parval,
                           'info':info})
            
    return result

def getlimits_from_json(meta):
    default_limits = {'constraint_equals':{}, 'constraint_minlowhighmax':{}, 'constraint_period':{} }
    for key,res in meta['results'].items():
        if 'constraint_equals' in res:
            default_limits['constraint_equals'][key] = res['constraint_equals']
        if 'constraint_period' in res:
            default_limits['constraint_period'][key] = res['constraint_period']
        if 'constraint_minlowhighmax' in res:
            default_limits['constraint_minlowhighmax'][key] = res['constraint_minlowhighmax']

    return default_limits
    
def limitsblocks(dbio, config_id, logger=None):
    """
    1. read the config from the database for given config
    2. extract the limit_types (Equals, MinLowHighMax, Period)
    3. for each limit_type, extract the limits
    4. Return a subpage with all limit_types and limits
    """
    if logger is None:
        logger = DummyLogger()

    result = {'constraint_equals':[], 'constraint_minlowhighmax':[], 'constraint_period':[]}
    if config_id is None:
        return result
    
    #1. read the meta from the database
    meta = dbio.DBModuleConfigs.get_by_id(int(config_id)).meta.val
    if meta is None:
        return result
    try:
        meta = json.loads(jsmin.jsmin(bytes_as_string(meta)))
    except ValueError as e:
        logger.error("meta {} is not valid json: {}".format(config_id, e))
        return result

    #2. extract the limits block
    limits_dict = getlimits_from_json(meta) #config['limits']

    limit_types = list(result.keys())
    #3. for each limit, extract the result_names
    for lim_key in limit_types:
        if not lim_key in limits_dict:
            continue

        lims_dict = limits_dict[lim_key]
        lim_names = sorted(lims_dict.keys())
        if lim_key == 'constraint_equals':
            for limname in lim_names: 
                limval = lims_dict[limname]
                result[lim_key].append({'lim_name':limname, 'lim_val':limval})
        elif lim_key == 'constraint_period':
            for limname in lim_names: 
                limval = lims_dict[limname]
                result[lim_key].append({'lim_name':limname, 'lim_val':int(limval)})
        elif lim_key == 'constraint_minlowhighmax':
            for limname in lim_names: 
                limvals = lims_dict[limname]
                result[lim_key].append({'lim_name':limname, 
                                        'min_val':float(limvals[0]),
                                        'low_val':float(limvals[1]),
                                        'high_val':float(limvals[2]),
                                        'max_val':float(limvals[3]),
                                        })
            
    return result

def update_meta_if_changed(dbio, config_id, field_dict, logger=None):
    """
    check of the params in the dict are different from the values stored in the database. if so, update the database.
    """
    if logger is None:
        logger = DummyLogger()

    equal = True
    # check limits
    oldlimits = limitsblocks(dbio, config_id, logger)

    newlimits = {key:[] for key in oldlimits.keys()}
    for i in range(0,len(oldlimits['constraint_equals'])):
        prefix = 'equals-%d-'%i
        # sanity check. when a different config is picked, there is no guartantee that the same results are present
        oldnames = [ p['lim_name'] for p in oldlimits['constraint_equals'] ]
        if prefix+'lim_name' in field_dict and field_dict[prefix+'lim_name'] in oldnames:
            newlimits['constraint_equals'].append({
                'lim_name':field_dict[prefix+'lim_name'],
                'lim_val':field_dict[prefix+'lim_val'],
            })
        
    for i in range(0,len(oldlimits['constraint_period'])):
        prefix = 'periods-%d-'%i
        # sanity check. when a different config is picked, there is no guartantee that the same results are present
        oldnames = [ p['lim_name'] for p in oldlimits['constraint_period'] ]
        if prefix+'lim_name' in field_dict and field_dict[prefix+'lim_name'] in oldnames:
            newlimits['constraint_period'].append({
                'lim_name':field_dict[prefix+'lim_name'],
                'lim_val':int(field_dict[prefix+'lim_val']),
            })
    for i in range(0,len(oldlimits['constraint_minlowhighmax'])):
        prefix = 'minlowhighmaxs-%d-'%i
        # sanity check. when a different config is picked, there is no guartantee that the same results are present
        oldnames = [ p['lim_name'] for p in oldlimits['constraint_minlowhighmax'] ]
        if prefix+'lim_name' in field_dict and field_dict[prefix+'lim_name'] in oldnames:
            newlimits['constraint_minlowhighmax'].append({
                'lim_name':field_dict[prefix+'lim_name'],
                'min_val':float(field_dict[prefix+'min_val']),
                'low_val':float(field_dict[prefix+'low_val']),
                'high_val':float(field_dict[prefix+'high_val']),
                'max_val':float(field_dict[prefix+'max_val']),
            })

    limits_key_fields = {'constraint_equals': ['lim_val'],
                  'constraint_period': ['lim_val'],
                  'constraint_minlowhighmax': ['min_val', 'low_val', 'high_val', 'max_val']}

    for key, fields in limits_key_fields.items():
        for oldv, newv in zip(oldlimits[key], newlimits[key]):
            for f in fields:
                if not oldv[f] == newv[f]:
                    logger.info('Different %s: %s (%s)'%(oldv['lim_name'],oldv[f], newv[f]) )
                    equal = False
                    break
                if not equal:
                    break

    if not equal:
        config = dbio.DBModuleConfigs.get_by_id(int(config_id))
        #1. read the meta from the database
        blob = json.loads(jsmin.jsmin(bytes_as_string(config.meta.val)))

        for key, fields in limits_key_fields.items():
            for newv in newlimits[key]:
                name = newv['lim_name']
                logger.info('==',newv)
                if len(fields) == 1:
                    blob['results'][name][key] = newv[fields[0]]
                else:
                    blob['results'][name][key] = [ newv[f] for f in fields ]

        # save meta changes
        config.meta.val = json.dumps(blob)
        config.meta.save()
    
def update_config_if_changed(dbio, config_id, field_dict, logger=None):
    """
    Check of the params in the dict are different from the values stored in the database. 
    If so, create a new config with the new params and return the id of the new config.
    If not, return the id of the current config.
    """
    if logger is None:
        logger = DummyLogger()

    equal = True
    # first check params
    oldparams = paramsblock(dbio, config_id, logger=logger)

    newparams = []
    for i in range(0,len(oldparams)):
        prefix = 'params-%d-'%i
        action = field_dict[prefix+'action']
        newparams.append({
            'action':field_dict[prefix+'action'],
            'param':field_dict[prefix+'param'],
            'value':field_dict[prefix+'value'],
        })

    for old, new in zip(oldparams, newparams):
        if not str(old['value']) == str(new['value']):
            #logger.info('Different %s %s: %s (%s)'%(old['action'],old['param'],old['value'], new['value']) )
            equal = False
            break
        
    if not equal:
        # clone the config
        config = dbio.DBModuleConfigs.get_by_id(int(config_id)).clone()
        #1. read the config from the database
        blob = json.loads(jsmin.jsmin(bytes_as_string(config.val)))

        for par in newparams:
            blob['actions'][par['action']]['params'][par['param']] = par['value']

        # make a copy of the current config, set origin to result
        config.val = json.dumps(blob)
        config.origin = 'result'
        config.save()
        
        return config.id
        
    return config_id
        
def rulesblock(dbio, config_id, logger=None):
    """
    1. read the rules with rulevalues from the database for given selector
    4. Return a subpage with all rules and logics and values
    5. Here the number of values to display is limitted to 5
    """
    if logger is None:
        logger = DummyLogger()

    result = []
    if config_id is None:
        return result
    
    #1. read the config from the database
    #Retrieve selector and rules
    selector = dbio.DBSelectors.get_by_id(config_id)
    for rule in selector.rules:
        res = {
            'tag':rule.dicomtag,
            'logic':rule.logic.id,
            'ruleid':rule.id
        }
        values = [v.val for v in rule.values]
        for i,v in enumerate(values):
            if i<5:
                res['value%d'%i] = v
        if len(values)<5:
            for i in range(len(values),5):
                res['value%d'%i] = ''
        result.append(res)
            
    return result

