#!/usr/bin/env python
from __future__ import print_function
import os
import sys
import time
import argparse

try:
    from . import wadservices_communicate as wad_com
except Exception as e:
    import wadservices_communicate as wad_com

__version__ = 20180629
"""
Changelog:
  20180629: show (more) help
  20180404: added dbupgrade, check
  20180314: based on wadservices 20180214; added drop command for wadadmin and waddashboard
"""

def ensure_postgresql(pgdata):
    """
    make sure a postgresql service is live
    """
    error = False
    msg = ""

    # start postgres
    status = wad_com.postgresql('status', pgdata)
    if not status == 'running':
        status = wad_com.postgresql('start', pgdata)
    if not status == 'running':
        error = True
        msg = "ERROR, cannot start 'postgresql' ({})".format(status)

    return error, msg

def applycmd(services, cmd, inifile, pgdata):
    """
    apply cmd to all services
    """
    if cmd in ['check', 'dbupgrade']:
        error, msg = ensure_postgresql(pgdata)
        if error:
            print("ERROR! Command {} needs a running PostgreSQL service for WAD. {}".format(cmd, msg))
            return

        try:
            import app.libs.dbupgrade as dbup
        except Exception as e:
            import wad_admin.app.libs.dbupgrade as dbup
        
        # need a dbio object
        if not wad_com.isconnected:
            try:
                wad_com.dbio_connect(inifile)
            except:
                return 'ERROR! cannot connect to WAD-QC database with {}'.format(inifile)
        
        if cmd == 'check':
            for s in services:
                if s == 'dbwadqc':
                    latest_version  = dbup.get_latest_version(wad_com.dbio, logger=None)
                    running_version = dbup.get_running_version(wad_com.dbio, logger=None)
                    if dbup.version_smaller(running_version, latest_version):
                        print("{} {}: An upgrade is available!".format(cmd, s))
                    else:
                        print("{} {}: No upgrade available.".format(cmd, s))
                    print("  Running version {}, latest version {}.".format(running_version, latest_version))
        elif cmd == 'dbupgrade':
            for s in services:
                if s == 'dbwadqc':
                    current_version = dbup.get_running_version(wad_com.dbio, logger=None)
                    error, msgs = dbup.upgrade(wad_com.dbio, logger=None)
                    if error:
                        print("Error running {} for {}! {}").format(cmd, s, msgs)
                        return
                    latest_version  = dbup.get_latest_version(wad_com.dbio, logger=None)
                    running_version = dbup.get_running_version(wad_com.dbio, logger=None)
                    print("Upgraded {} from version {} to {}. Latest version is {}.".format(s, current_version, running_version, latest_version))
                    print("  Restart all wadservices for the changes to take effect.")
        return
    
    for s in services:
        # postgres is a special case
        print('* %s %s:...'%(cmd, s), end='')
        sys.stdout.flush()
        if s == 'postgresql':
            print('%s: %s'%(s, wad_com.postgresql(cmd, pgdata)))

        # wadprocessor is a special case
        elif s == 'wadprocessor':
            print('%s: %s'%(s, wad_com.wadcontrol('quit' if cmd=='stop' else cmd, inifile)))
            
        else:
            if cmd == 'status':
                print('%s: %s'%(s, wad_com.status(s)))

            elif cmd == 'stop':
                print('%s: %s'%(s, wad_com.stop(s)))
            
            elif cmd == 'start':
                print('%s: %s'%(s, wad_com.start(s, inifile=inifile)))

            elif cmd == 'info':
                print('%s: %s'%(s, wad_com.info(s)))

            elif cmd == 'drop':    
                print('%s: %s'%(s, wad_com.drop(s, inifile=inifile)))

def main():
    # do not run as root! the script will ask for permission if it needs root
    if os.name == 'nt':
        pass
    elif os.geteuid() == 0:
        print("Do not run waddoctor as root! The script will ask you for root permission if it needs it! Exit.")    
        exit(False)

    print("waddoctor v.{}\n".format(__version__))
    print("NOTICE: Changing the state of apache2 or systemd controlled services needs root permissions.\n"
          "If root permissions are needed, you will be prompted for your password.\n\n")

    epilog = ""

    parser = argparse.ArgumentParser(description='wadservices.py version %s'%__version__, epilog=epilog)
    all_services = ['postgresql', 'orthanc', 'wadadmin', 'waddashboard', 'wadprocessor', 'all']
    all_drop = ['wadadmin', 'waddashboard']
    
    if 'WADROOT' in os.environ:
        inifile = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')
        pgdata = os.path.join(os.environ['WADROOT'], 'pgsql','data')
    else: # development
        print("WADROOT cannot be found in the environment. Add it to your .bashrc and try again from a new shell.")
        sys.exit()

    parser.add_argument('-i','--inifile',
                        type=str,
                        default=inifile,
                        help='path to wadconfig.ini file [%s]'%(inifile),dest='inifile')

    parser.add_argument('-d','--data',
                        type=str,
                        default=pgdata,
                        help='path to postgresql data folder [%s]'%(pgdata),dest='pgdata')

    parser.add_argument('--drop',
                        type=str,
                        default='',
                        help='Remove the databases of the indicated service. When the indicated service is restarted later, a new default database for that service will be generated. Valid for: {} [{}]'.format(','.join(all_drop), ''),dest='drop')

    parser.add_argument('--dbupgrade',
                        type=str,
                        default='',
                        help='Applies all available upgrades of the indicated database. Valid for: wadqc [{}]'.format(''), dest='dbupgrade')

    parser.add_argument('--check',
                        type=str,
                        default='',
                        help='Check status of indicated service. Valid for: dbwadqc [{}]'.format(''), dest='check')

    args = parser.parse_args()
    inifile = args.inifile
    pgdata = args.pgdata
    
    if len(args.drop) > 0:
        # drop database of wadadmin or waddashboard
        if not args.drop in all_drop:
            print('ERROR! Command "{}" is invalid for service "{}". Aborting, system has not changed!'.format("drop", args.drop) )
            return
        applycmd([args.drop], 'drop', inifile, pgdata)

    elif len(args.dbupgrade) > 0:
        # upgrade database of wadqc
        if not args.dbupgrade in ['dbwadqc']:
            print('ERROR! Command "{}" is invalid for database "{}". Aborting, system has not changed!'.format("dbupgrade", args.dbupgrade) )
            return
        applycmd([args.dbupgrade], 'dbupgrade', inifile, pgdata)
 
    elif len(args.check) > 0:
        # check status of service
        if not args.check in ['dbwadqc']:
            print('ERROR! Command "{}" is invalid for "{}". Aborting, system has not changed!'.format("check", args.check) )
            return
        applycmd([args.check], 'check', inifile, pgdata)

    else:
        parser.print_help()
    
if __name__ == "__main__":
    main()
