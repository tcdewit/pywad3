from __future__ import print_function
import os
import errno
import subprocess
import time
import psutil
import platform
from wad_qc.connection import bytes_as_string, dbio

isconnected = False
def dbio_connect(inifile):
    """
    helper to make one connection to wadqc db
    """
    
    global isconnected
    if not isconnected:
        dbio.db_connect(inifile)
        isconnected = True


def _external_call(cmd, returnoutput=False, background=False, opt={}):
    """
    helper function to make system calls
    """

    result = 'OK'
    try:
        if background:
            with open(os.devnull, "w") as f: # never pipe the output of a background process, as it will break eventually!
                #proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **opt)
                proc = subprocess.Popen(cmd, stdout=f, stderr=f, close_fds=( not platform.system() == 'Windows' ), **opt)
                time.sleep(3)
                if proc.poll():
                    result = 'ERROR'

        else:
            # Now we can wait for the child to complete
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **opt)
            (output, error) = proc.communicate()
            if returnoutput:
                if error:
                    result = 'ERROR'+' '+bytes_as_string(output.strip())
                    result = result.strip()+' '+bytes_as_string(error.strip())
                    result = result.strip()
                else:
                    result = bytes_as_string(output.strip())
                    if result == '':
                        result = 'OK'
            else:
                result ='OK' if proc.returncode==0 else 'Fail'

    except OSError as e:
        if e.errno != errno.ENOENT:
            print('Argh! Uncaught OSError',e.errno,str(e))
            result = 'ERROR: %s'%str(e)
        else:
            result = 'ERROR'

    except subprocess.CalledProcessError as e:
        result = 'ERROR: %s'%str(e)

    return result
    
def start(service, inifile=None, cwd=None):
    """
    start WAD-Admin or WAD-Dashboard in background
    """
    validservices = ['Orthanc', 'wadadmin', 'waddashboard', 'wadprocessor']
    if not service in validservices:
        return None

    cmd = [service]
    if service == 'Orthanc':
        if not isconnected:
            try:
                dbio_connect(inifile)
            except:
                return 'ERROR! cannot connect to WAD-QC database with %s'%inifile

        # check if systemd is used for this process
        systemdservice = 'wadorthanc'
        systemd = controlled_by_systemd(systemdservice)
        if not systemd:
            wadroot = os.path.dirname(dbio.DBVariables.get_by_name('wadqcroot').val)
            orthancjson = os.path.join(wadroot, 'orthanc', 'config', 'orthanc.json')
            logdir = os.path.join(wadroot, 'WAD_QC', 'Logs')
            cmd.append('--logdir={}'.format(logdir))
            cmd.append(orthancjson)
            result = _external_call(cmd, returnoutput=False, background=True, opt={'cwd':cwd})
        else:
            cmd = ['sudo', 'systemctl', 'start', systemdservice]
            result = _external_call(cmd, returnoutput=True)
            
    elif service == 'wadprocessor':
        # check if systemd is used for this process
        systemdservice = 'wadprocessor'
        systemd = controlled_by_systemd(systemdservice)
        if not systemd:
            cmd.extend(['-i', inifile, '--logfile_only'])
            result = _external_call(cmd, returnoutput=False, background=True, opt={'cwd':cwd})
        else:
            cmd = ['sudo', 'systemctl', 'start', systemdservice]
            result = _external_call(cmd, returnoutput=True)
            
    if service in ['wadadmin', 'waddashboard']:
        if service == 'wadadmin':
            apache2name = 'admin_wadqc'
        else:
            apache2name = 'dashboard_wadqc'
        if controlled_by_apache2(apache2name):
            cmd = ['sudo', 'a2ensite', apache2name]
            msg = _external_call(cmd, returnoutput=False)
            if not msg == "OK":
                return _external_call(cmd, returnoutput=True)
            cmd = ['sudo', 'apache2ctl', 'restart']
            result = _external_call(cmd, returnoutput=True)
            if "Could not reliably determine the server's fully qualified domain name" in result:
                result = "OK"
        else:
            result = _external_call(cmd, background=True, returnoutput=True)

    if not 'wad' in service:
        time.sleep(3) # wait 3 extra seconds for Orthanc to start
    
    return result

def status(service):
    # return status of a system process, optionally killing it
    # service = cli identifier
    if service in ['wadadmin', 'waddashboard']:
        if service == 'wadadmin':
            apache2name = 'admin_wadqc'
        else:
            apache2name = 'dashboard_wadqc'
        if controlled_by_apache2(apache2name):
            #return "controlled by apache2"
            cmd = ['a2query',  '-s', apache2name]
            result = _external_call(cmd, returnoutput=True)
            if 'enabled' in result:
                return "enabled"
            else:   
                return "disabled"
        

    return _process(service, do_kill=False)

def info(service):
    """
    return the full path to the command to use for this service
    """
    if service in ['Orthanc',  'wadprocessor']:
        if service == 'Orthanc':
            systemdservice = 'wadorthanc'
        else:
            systemdservice = service
        systemd = controlled_by_systemd(systemdservice)
        if systemd:
            return 'systemctl <command> {}'.format(systemdservice)
        else:
            return _external_call(['which', service], returnoutput=True)


    if service in ['wadadmin', 'waddashboard']:
        if service == 'wadadmin':
            apache2name = 'admin_wadqc'
        elif service == 'waddashboard':
            apache2name = 'dashboard_wadqc'
        if controlled_by_apache2(apache2name):
            return '<a2dissite/a2ensite> {}'.format(apache2name)

        else:
            return _external_call(['which', service], returnoutput=True)

    
def stop(service):
    # kill service and return status
    # service = cli identifier
    systemd = False
    if service == 'Orthanc':
        systemdservice = 'wadorthanc'
        systemd = controlled_by_systemd(systemdservice)
    elif service == 'wadprocessor':
        systemdservice = 'wadprocessor'
        systemd = controlled_by_systemd(systemdservice)

    if systemd:
        cmd = ['sudo', 'systemctl', 'stop', systemdservice]
        return _external_call(cmd, returnoutput=True)

    if service in ['wadadmin', 'waddashboard']:
        if service == 'wadadmin':
            apache2name = 'admin_wadqc'
        else:
            apache2name = 'dashboard_wadqc'
        if controlled_by_apache2(apache2name):
            cmd = ['sudo', 'a2dissite', apache2name]
            msg = _external_call(cmd, returnoutput=False)
            if not msg == "OK":
                return _external_call(cmd, returnoutput=True)
            cmd = ['sudo', 'apache2ctl', 'restart']
            result = _external_call(cmd, returnoutput=True)
            if "Could not reliably determine the server's fully qualified domain name" in result:
                result = "OK"
            return result


    return _process(service, do_kill=True)

def drop(service, inifile):
    """
    delete the database (part) of the given service.
    
    if waddashboard:
      stop waddashboard
      delete WADROOT/WAD_QC/waddashboard.db
      connect dbio
      dbio.db.drop(waddashboard.models.WDTables)
      remove dbio.db.var(wd_db_version)
    """
    if not service in ['wadadmin', 'waddashboard']:
        return "ERROR, '{}' not valid for 'drop'".format(service)

    # start postgres
    status = postgresql('status')
    if not status == 'running':
        status = postgresql('start')
    if not status == 'running':
        return "ERROR, cannot start 'postgresql' ({})".format(status)
    
    # connect to db
    try:
        dbio_connect(inifile)
    except:
        return 'ERROR! cannot connect to WAD-QC database with {}'.format(inifile)

    # get wadroot
    wadroot = os.path.dirname(dbio.DBVariables.get_by_name('wadqcroot').val)
 
    # stop service
    status = stop(service)

    #
    status = ""
    if service == "wadadmin":
        # old left-over
        fname = os.path.join(wadroot, "wadadmin.db")
        if os.path.exists(fname):
            os.unlink(fname)
            status += "deleted sqlite database"

        # drop tables in waddb
        try:
            from wad_admin.app.mod_wadconfig.models import WATables
            with dbio.db.atomic() as txn:
                dbio.db.drop_tables(WATables, safe=True, cascade=True)
                if len(status) > 0:
                    status += "; "
                status += "deleted {} tables".format(service)
        except ImportError as e: 
            pass

    elif service == "waddashboard":
        # old left-over
        fname = os.path.join(wadroot, "waddashboard.db")
        if os.path.exists(fname):
            os.unlink(fname)
            status += "deleted sqlite database"

        # drop tables in waddb
        try:
            from wad_dashboard.app.mod_waddashboard.models import WDTables
            with dbio.db.atomic() as txn:
                dbio.db.drop_tables(WDTables, safe=True, cascade=True)
                if len(status) > 0:
                    status += "; "
                status += "deleted {} tables".format(service)
        except ImportError as e: 
            pass

    if status == "":
        status = "No changes needed"
    return status


def _process(service, do_kill=False):
    # return status of a system process, optionally killing it
    # service = cli identifier

    processes = []
    # just check all running processes for matches of the identifier of a service
    for pr in psutil.process_iter():#psutil.pids():
        # python processes the total command line should be checked
        try: # work around psutil.ZombieProcess process still exists but it's a zombie
            if 'python' in pr.name():
                if '%s.py'%service in '\t'.join(pr.cmdline()):
                    processes.append(pr)
                elif '\t'.join(pr.cmdline()).endswith(service):
                    processes.append(pr)
            else:
                if service in pr.name():
                    processes.append(pr)
        except psutil.NoSuchProcess:
            pass                    
    
    dastatus = 'stopped'
    if len(processes)>0:
        dastatus = 'running'

        for pro in processes:
            if pro.status() == psutil.STATUS_ZOMBIE:
                dastatus = 'zombie'
    if do_kill:
        for pr in processes:
            try:
                pr.terminate()
                pr.wait()
            except psutil.NoSuchProcess:
                pass

        dastatus = status(service)
    return dastatus

def controlled_by_systemd(service):
    # check if systemd is used for this process
    cmd = ['systemctl', 'is-enabled', service]
    result = _external_call(cmd, returnoutput=True)
    if result == 'enabled':
        systemd = True
    else:
        systemd = False
    return systemd

def controlled_by_apache2(site):
    # check if apache2 runs this site
    cmd = ['a2query',  '-s', site]
    result = _external_call(cmd, returnoutput=True)
    if 'enabled' in result or 'disabled' in result:
        return True
    else:
        return False

def postgresql(command, pgdata=None):
    """
    send postgresql a command
    it is assumed PGDATA is set in enviroment. if not, add -D PGSDATA
    """
    global isconnected
    
    valid = ['info', 'start', 'status', 'stop', 'reload']
    if not command in valid:
        return None

    # check if systemd is used for this process
    systemdservice = 'wadpostgresql'
    systemd = controlled_by_systemd(systemdservice)

    if command == 'stop' and isconnected:
        dbio.db.close()
        isconnected = False

    if not systemd or command in ['status', 'info']:
        # find pg bindir; that is where pg_ctl is located.
        pgbindir = None
        cmd = ['pg_config', '--bindir']
        result = _external_call(cmd, returnoutput=True)
        pg_ctl = 'pg_ctl'
        if not result == "ERROR":
            pgbindir = result
            pg_ctl = os.path.join(pgbindir, pg_ctl)
        cmd = [pg_ctl]
    
        if not pgdata is None:
            pass
        elif 'WADROOT' in os.environ:
            pgdata = os.path.join(os.environ['WADROOT'], 'pgsql','data')
        elif 'PGDATA' in os.environ:
            pgdata = os.environ['PGDATA']
        else:
            raise ValueError('PGDATA not supplied and not available in environment')
        cmd.extend(['-D', pgdata])

        if command == 'info':
            return ' '.join(cmd)

        cmd.append(command)
        result = _external_call(cmd, returnoutput=True if command == 'status' else False, background=True if command=='start' else False)
        if not command == 'status' and not result == 'ERROR':
            time.sleep(3) # wait 3 seconds
        if 'is running' in result:
            result = 'running'
        
    elif systemd:
        if command == 'info':
            return 'systemctl <command> {}'.format(systemdservice)

        # for systemd we need sudo
        cmd = ['sudo', 'systemctl', command, systemdservice]
        result = _external_call(cmd, returnoutput=True)
        if command == 'start':
            time.sleep(3) # wait 3 extra seconds
            
    return result

    
def wadcontrol(command, inifile):
    if not command in ['info', 'start', 'status', 'pause', 'quit']:
        return None
    if command == 'status':
        command = 'getStatus'
    elif command == 'pause':
        command = 'stop'
        
    service = 'wadprocessor'
    # check if systemd is used for this process
    systemdservice = 'wadprocessor'
    systemd = controlled_by_systemd(systemdservice)

    if command == 'info':
        if systemd:
            return 'systemctl <command> {}'.format(systemdservice)
        else:
            return _external_call(['which', 'wadcontrol'], returnoutput=True)
        
    if not isconnected:
        try:
            dbio_connect(inifile)
        except:
            return 'ERROR! cannot connect to WAD-QC database with %s'%inifile
    
    wadqcroot = dbio.DBVariables.get_by_name('wadqcroot').val

    if not systemd or command == 'getStatus':
        if command == 'start':
            # first test if wadprocessor is online
            status = wadcontrol('status', inifile)
            if not status in ['running','paused']:
                start(service, inifile=inifile, cwd=wadqcroot)
        
        # call wadcontrol to change selected setting
        cmd = ['wadcontrol', command]
    
        result = _external_call(cmd, returnoutput=(command!='quit'), background=False, opt={'cwd':wadqcroot})

    elif systemd:
        # for systemd we need sudo
        if command == 'quit':
            command = 'stop'
        cmd = ['sudo', 'systemctl', command, systemdservice]
        result = _external_call(cmd, returnoutput=True)
        
    return result

