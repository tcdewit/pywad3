#!/usr/bin/env python
# Run a test server.

from os import environ, path, makedirs
import argparse
import platform
import sys

def _setup_logging(app, wadqcroot, levelname='INFO'):
    # create logger to log to screen and to file
    #logging.basicConfig(format='%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=level)
    import logging
    from logging.handlers import RotatingFileHandler

    loglevel = getattr(logging, levelname)
    app.logger.setLevel(loglevel)
    
    logroot = path.join(wadqcroot,'Logs')
    if not path.exists(logroot):
        makedirs(logroot)
    logfile = path.join(logroot,'wad_admin.log')
    formatter = logging.Formatter('%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s')

    # restrict handlers to one of each only
    has_filehandler   = False
    has_streamhandler = False
    for hand in app.logger.handlers:
        hand.setLevel(loglevel) # do not know why, but somehow the loglevel above does not propagate
        if isinstance(hand, logging.FileHandler):
            has_filehandler = True
        elif isinstance(hand, logging.StreamHandler):
            has_streamhandler = True

    if not has_filehandler:
        fh = RotatingFileHandler(logfile, mode='a', maxBytes=2*1024*1024, backupCount=5) #append mode # max 6*2 MB
        fh.setFormatter(formatter)
        fh.setLevel(loglevel)
        app.logger.addHandler(fh)

def main(): # define separately to allow automatic script creation
    if platform.system() == 'Windows': # needed for running this flask app from an entry_point in a whl on windows!
        if not sys.argv[0].endswith('.py'):
            sys.argv[0] += '.exe'   
    parser = argparse.ArgumentParser(description='WAD-QC Admin')

    if 'WADROOT' in environ:
        inifile = path.join(environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')

    parser.add_argument('-i', '--inifile',
                        default=inifile,
                        type=str,
                        help='the inifile file of the WAD server if not using the default location',
                        dest='inifile')

    args = parser.parse_args()
    environ['WADROOT'] = path.dirname(path.dirname(path.abspath(path.expanduser(args.inifile))))
    
    # only after setting environ import app!
    try:
        from app import flask_app
    except ImportError:
        from wad_admin.app import flask_app

    flask_app.debug = False
    reload_flag = False
    if 'WINGDB_ACTIVE' in environ:
        flask_app.debug = True

    if not flask_app.debug:
        _setup_logging(flask_app, path.expanduser(path.join(environ['WADROOT'],'WAD_QC')))
    else:
        reload_flag = True # only in development!
        
    flask_app.run(host='0.0.0.0', port=12001,use_reloader=reload_flag)# use 0.0.0.0 to make it accessible outside localhost
    #flask_app.run(host='127.0.0.1', port=12001,use_reloader=True)

if __name__ == "__main__":
    main()
