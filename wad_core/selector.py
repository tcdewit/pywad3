#!/usr/bin/env python

import os
import argparse
import logging
import logging.handlers

# this will fail unless wad_qc is already installed
from wad_qc.connection import dbio
from wad_qc.connection.pacsio import PACSIO

__version__ = '20171018'

"""
wad_selector should be the process:
  * retrieves the dicomheaders of a study_id
  * determines if the study has a matching selector
    * if yes:
      * enqueue a new processor (selectorid,moduleid, configid, studyid)

Changelog:
  20171018: do not use exit() but return
  20170726: do not add a process to the queue if it was already processed previously (avoid double results)
  20170318: move hdrs into match
  20170309: added header cache
  20170301: Split match in match_study, match_series, match_level; added datalevel argument to run to restrict analysis to certain dataset
  20161115: functional dryrun
  20160907: bugfix matched dcm_instance; added rotating file log
  20160610: updated to match changes in dbio
  20160606: split config.ini in dbconfig and setup; get pacsconfig from dbio
  20160531: multiple data_sources
  20160530: use new dbio
  20160523: go peewee
  20160513: simplified logger
  20160426: implement multi-value for rule without ';' in value
  20160425: prefer init of dbio and pacsio with a dict instead of an inifile to prevent problems due to runtime changes in inifile;
    case insenstive matching, whilst removing leading and trailing white space in rule and header and allowing ';' as an 'or' separator
  20160422: using python logging; read config only once
  20160421: wad_qc path
  20160420: renamed relation to logic; adding data_type (series, etc.); verbosity in inifile
  20160413: added reference to selector
  20160407: moved testing to selector__test.py; hide testing code in dummy dbio pacsio classes in test.py; use wadconfig inifile instead of directly pacs config
  20160316: added optional config flag for location of orthanc.json for testing
  20160311: added series shared-tags; compacted matching code; added nested tags; added more examples
  20160310: restart from scratch
  
  TODO: 
  o check all logics in matching
  o pitfalls: dicomtag in rules should match lower/upper case 
"""  

"""
Flow:
 1. Selector is started with parameter studyid
 2. Build dict {selectorid: {'datatype:': string, 'rules':[(tag,logic,value),...]}, ...}
 3. Get shared_study_headers
 4. for each selectorid: match rules to shared_study_headers; if match enqueue (moduleid,configid,studyid)
"""

LOGGERNAME = 'wad_selector'

def _setup_logging(levelname, wadqcroot, logfile_only):
    # create logger to log to screen and to file
    loglevel = getattr(logging, levelname)
    logroot = os.path.join(wadqcroot,'Logs')
    if not os.path.exists(logroot):
        os.makedirs(logroot)
    logfile = os.path.join(logroot,'%s.log'%LOGGERNAME)

    logger = logging.getLogger(LOGGERNAME)
    logger.setLevel(loglevel)

    formatter = logging.Formatter('%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s')

    # restrict handlers to one of each only
    has_filehandler   = False
    has_streamhandler = False
    for hand in logger.handlers:
        if isinstance(hand, logging.FileHandler):
            has_filehandler = True
        elif isinstance(hand, logging.StreamHandler):
            has_streamhandler = True
            
    if not has_filehandler:
        fh = logging.handlers.RotatingFileHandler(logfile, mode='a', maxBytes=2*1024*1024, backupCount=5) #append mode # max 6*2 MB
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        
    if not logfile_only and not has_streamhandler:
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)

class Selector:
    """
    Class to check if a the headers of a incoming study match the criteria of a selector
    """
    def __init__(self, inifile, logfile_only=False):
        # connect to wad_qc db
        dbio.db_connect(inifile)

        # set up logger
        wadqcroot = dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val
        loglevel = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_logging').val
        _setup_logging(loglevel, wadqcroot, logfile_only)
        self.logger = logging.getLogger(LOGGERNAME)
        self.headers_cache = {'dcm_study':{}, 'dcm_series':{}, 'dcm_instance':{}}
        
    def run(self, sourcename, dataid, dryrun=False, datalevel='dcm_study', selectornames=[], include_inactive=False):
        if not datalevel in ['dcm_study', 'dcm_series', 'dcm_instance']:
            raise ValueError('Unknown datalevel {}'.format(datalevel))

        # init PACS connection
        self.pacsio = PACSIO(dbio.DBDataSources.get(dbio.DBDataSources.name == sourcename).as_dict())

        # allows to provide dummy instances of dbio and pacsio for testing
        self.dryrun = dryrun # dryrun, only output which selector matches, do not enqueue process
        self.dryrun_processes = []

        #1. obtain selectors defs
        selectors = self.getSelectors(includeinactive=include_inactive, names=selectornames)
        if len(selectors) == 0:
            self.logger.warning('No active selectors found. Nothing to do. Bye.')
            return
        else:
            self.logger.debug('%d active selectors'%len(selectors))
            
        numprocs = 0
        for selname,sel in selectors.items():
            if datalevel == 'dcm_study':
                dataids = self.matchstudy(dataid, sel['rules'], sel['datatype_name'])
            elif datalevel == 'dcm_series':
                dataids = self.matchseries_perstudy([dataid], sel['rules'], sel['datatype_name'])
            elif datalevel == 'dcm_instance':
                dataids = self.matchinstances_perseries([dataid], sel['rules'], sel['datatype_name'])

            for data_id in dataids:
                if self.checkIfNewForSelector(selname, sourcename, data_id):
                    if dryrun:
                        self.dryrun_processes.append({'selectorname':selname, 'datasourcename':sourcename, 
                                                      'data_id':data_id, 'datatype':sel['datatype_name']})
                    else:
                        dbio.DBProcesses.create(**{'selectorname':selname, 'datasourcename':sourcename, 'data_id':data_id, 'processstatusname':'new'})
                        self.logger.info('addprocess \"%s\" %s'%(selname, data_id))
                    numprocs += 1
                else:
                    self.logger.info('Already processed {} {} by {}. Not a new process.'.format(sel['datatype_name'], data_id, selname))

        if numprocs == 0:
            self.logger.info('NO new selector matches for %s %s'%(datalevel, dataid))
        else:
            self.logger.info('%d new selector matches for %s %s'%(numprocs, datalevel, dataid))
            
        
    def getDryrun(self):
        # return a list of processes that would have been created for the study used in init
        return self.dryrun_processes
    
    def checkIfNewForSelector(self, selname, sourcename, data_id):
        # check if process to be added to queue is indeed a new process (for this selector). if not, ignore.
        # this solves the problem of reprocessing old series if a new series is added to an old study.
        # selectorname':selname, 'datasourcename':sourcename, 'data_id':data_id

        """
        this adds 2 lookups for each process. alternatively construct the query as:
        num_old_processes = dbio.DBProcesses.select().join(dbio.DBSelectors).switch(dbio.DBProcesses).join(dbio.DBDataSources).where(
            (dbio.DBSelectors.name == selname) & 
            (dbio.DBDataSources.name == sourcename) &
            (dbio.DBProcesses.data_id == data_id) 
        ).count()

        num_old_results = dbio.DBResults.select().where(
            (dbio.DBResults.selector == sel) & 
            (dbio.DBResults.data_source == ds) &
            (dbio.DBResults.data_id == data_id) 
        ).count()
        """
        sel = dbio.DBSelectors.get_by_name(selname)
        ds  = dbio.DBDataSources.get_by_name(sourcename)
        
        # first check existing processes, as the processes table is likely to be smaller
        num_old_processes = dbio.DBProcesses.select().where(
            (dbio.DBProcesses.selector == sel) & 
            (dbio.DBProcesses.data_source == ds) &
            (dbio.DBProcesses.data_id == data_id) 
        ).count()
        if num_old_processes>0:
            return False

        # check existing results
        num_old_results = dbio.DBResults.select().where(
            (dbio.DBResults.selector == sel) & 
            (dbio.DBResults.data_source == ds) &
            (dbio.DBResults.data_id == data_id) 
        ).count()
        if num_old_results>0:
            return False
        
        return True
        
     
    def getSelectors(self, includeinactive=False, names=[]):
        # return a dict of all selectors and rules and stuff:
        # {sel.name:{'datatype':datatypeid,'rules':[(tag,selector_logics_str,val)]}}

        results = {}

        # find base selection
        active_selectors = dbio.DBSelectors.select()
        selection = True
        if not includeinactive:
            selection = selection & (dbio.DBSelectors.isactive == True)
        if len(names) > 0:
            selection = selection & (dbio.DBSelectors.name << names)
        active_selectors = active_selectors.where(selection)
        
        for sel in active_selectors: 
            results[sel.name] = {
                'datatype_name':sel.module_config.data_type.name,
                'rules':[ (rule.dicomtag, rule.logic.name, [r.val for r in rule.values]) for rule in sel.rules ]
                }
            
        return results

    def getSeries(self, studyid):
        """
        Return a list of all seriesids connected to that studyid
        """
        return self.pacsio.getSeriesIds(studyid)
    
    def getInstances(self, seriesid):
        """
        Return a list of all instanceids connected to that seriesid
        """
        return self.pacsio.getInstancesIds(seriesid)
    
    def _match(self, hdrs, rule):
        """
        the actual matching.

        to implement wildcards (*,?), use fnmatch:
          from fnmatch import fnmatch, fnmatchcase
          for temp in templates:
          if fnmatchcase(text,temp):
            return True
        """
        tag,logic,values = rule
        
        # remove leading and trailing white space in rule and header
        # case insensitive matching
        rule_values = [ p.strip().lower() for p in values ]
        hdr_value = hdrs[tag]['Value'].strip().lower()
        
        self.logger.debug('[_match] %s %s %s ?'%(hdr_value, logic, str(rule_values)))
        if logic == 'equals':
            for value in rule_values:
                if hdr_value == value: # orthanc json format
                    return True
        elif logic == 'starts with':
            for value in rule_values:
                if hdr_value.startswith(value): # orthanc json format
                    return True
        elif logic == 'ends with':
            for value in rule_values:
                if hdr_value.endswith(value): # orthanc json format
                    return True
        elif logic == 'contains':
            for value in rule_values:
                if value in hdr_value: # orthanc json format
                    return True
        elif logic == 'not equals':
            for value in rule_values:
                if not hdr_value == value: # orthanc json format
                    return True
        elif logic == 'not contains':
            for value in rule_values:
                if not value in hdr_value: # orthanc json format
                    return True
        # below are rules without rule_values
        elif logic == 'is empty':
            if hdr_value == '': # orthanc json format
                return True
        elif logic == 'not is empty':
            if not hdr_value == '': # orthanc json format
                return True
        else:
            raise ValueError('[match] Unknown logic %s'%logic)
        
        return False
        

    def _matchitem(self, level, hdrs, rules):
        """
        Generic matching for all rules to one object in a level; level being study,series,instance; level only used for logtag
        Returns match,need_next_level
        if match == False: a rule did not match; does not need extra checks
        if match == True: for all tags that were present, all rules matched; maybe need extra checks
        if need_next_level == True: some tags were not present at this level
        if need_next_level == False: all tags were present at this level
        
        """

        # check if there is a full match for any object at this level
        match = True
        need_next_level = False

        for tag,logic,value in rules:
            tag = tag.lower().split(';') # force to lower case and find nested tags
            if not tag[0] in hdrs: # if the tag is not in the headers, we need to look one level deeper
                need_next_level = True 
                continue # do not break, because a match = False could still occur for another rule

            if len(tag) == 2: # allow a nested tag 1 level
                subhdr = hdrs[tag[0]]['Value'][0]
                if not tag[1] in subhdr: # if the tag is not in the headers, we need to look one level deeper
                    need_next_level = True
                    continue # do not break, because a match = False could still occur for another rule

                elif not self._match(subhdr,(tag[1],logic,value)): # if a rule does not match, go to next object
                    match = False
                    self.logger.debug('%sMisMatch on %s: %s %s %s'%(level,subhdr[tag[1]]['Name'],subhdr[tag[1]]['Value'],logic,value))
                    break # try next object

            else: # not nested
                if not self._match(hdrs,(tag[0],logic,value)): # if a rule does not match, go to next series
                    match = False
                    self.logger.debug('%sMisMatch on %s: %s %s %s'%(level,hdrs[tag[0]]['Name'],hdrs[tag[0]]['Value'],logic,value))
                    break # try next object


        return match,need_next_level

    def matchstudy(self, studyid, rules, datatype):
        """
        match the contents of hdrs to the set of rules; return list of matching data ids
        specific for study level. datatype is the datatype wanted by this selector.
        """
        implemented = ['dcm_series', 'dcm_study', 'dcm_instance']
        if not datatype in implemented:
            raise ValueError('[match] Unimplemented datatype %s'%(datatype))
            
        # get from cache if possible
        hdrs = self.getSharedStudyHeaders(studyid)

        # first check on shared study headers
        match,need_next_level = self._matchitem('  study', hdrs, rules)

        if match and not need_next_level: # this study matched all rules, and no extra checks are needed
            if datatype == 'dcm_study':
                return [studyid] # we are done
            if datatype == 'dcm_series':
                return self.getSeries(studyid) # we are done
            matches = []
            if datatype == 'dcm_instance':
                for seriesid in self.getSeries(studyid):
                    matches.extend(self.getInstances(seriesid))
            return matches # we are done

        if not match:# there was a rule that did not match
            return [] 
        
        # all rules matched for tags that could be found, but some tags need to be found at a deeper level
        self.logger.debug('  study: not all tags were found, looking deeper')

        # the implementation here leads to double checking, but could also prevent extra checks because one of the 
        # other shared-tags might not match and prevent this next stage from happening
        return self.matchseries_perstudy(self.getSeries(studyid), rules, datatype)

    def matchseries_perstudy(self, seriesids, rules, datatype):
        """
        match the contents of hdrs to the set of rules; return list of matching data ids
        specific for series level. all series belong to same study. datatype is the datatype wanted by this selector
        """
        implemented = ['dcm_series', 'dcm_study', 'dcm_instance']
        if not datatype in implemented:
            raise ValueError('[match] Unimplemented datatype %s'%(datatype))
            
        # first check on shared study headers has failed or is skipped
        studyid = self.pacsio.getStudyId(seriesid=seriesids[0])

        matches = []
        for seriesid in seriesids: # we need to check all series! and if any full match is found we're done
            hdrs = self.getSharedSeriesHeaders(seriesid) # if there is only one series in the study, this would give the same tags
            match,need_next_level = self._matchitem('   series', hdrs, rules)

            if match and not need_next_level: # there was a series that matched all rules, and no extra checks are needed
                if datatype == 'dcm_study':
                    return [studyid] # we are done
                elif datatype == 'dcm_series':
                    matches.append(seriesid)
                    continue # go to next series
                elif datatype == 'dcm_instance':
                    matches.extend(self.getInstances(seriesid))
                    continue # go to next series
            
            if not match:
                continue # there was a rule that did not match, so try the next series

            # all rules matched for tags that could be found, but some tags need to be found at a deeper level
            self.logger.debug('  series: not all tags were found, looking deeper')

            matches.extend(self.matchinstances_perseries(self.getInstances(seriesid), rules, datatype))

        return matches

    def matchseries(self, seriesids, rules, datatype):
        """
        match the contents of hdrs to the set of rules; return list of matching data ids
        specific for series level. will split in series of the same studyid.
        datatype is the datatype wanted by this selector.
        """
        implemented = ['dcm_series', 'dcm_study', 'dcm_instance']
        if not datatype in implemented:
            raise ValueError('[match] Unimplemented datatype %s'%(datatype))
            
        # first check on shared study headers has failed or is skipped

        matches = []
        # treat per studyid
        series_per_study = {}
        for seriesid in seriesids:
            studyid = self.pacsio.getStudyId(seriesid=seriesid)
            if not studyid in series_per_study.keys():
                series_per_study[studyid] = []
            series_per_study[studyid].append(seriesid)
            
        for studyid, seriesids in series_per_study.items():
            matches.extend(self.matchseries_perstudy(seriesids, rules, datatype))

        return matches
    

    def matchinstances_perseries(self, instanceids, rules, datatype):
        """
        match the contents of hdrs to the set of rules; return list of matching data ids.
        specific for instance level. all instances belong to same series. datatype is the datatype wanted by this selector.
        """
        implemented = ['dcm_series', 'dcm_study', 'dcm_instance']
        if not datatype in implemented:
            raise ValueError('[match] Unimplemented datatype %s'%(datatype))
            
        # first checks on shared study headers and series headers failed or skipped
        studyid  = self.pacsio.getStudyId(instanceid=instanceids[0])
        seriesid = self.pacsio.getSeriesId(instanceids[0])
       
        matches = []
        for instid in instanceids: # we need to check all instances!
            hdrs = self.getInstanceHeaders(instid)
            match,need_next_level = self._matchitem('    instance', hdrs, rules)
            
            if match and not need_next_level: # there was an instance that matched all rules, and no extra checks are needed
                #return True 
                if datatype == 'dcm_study':
                    return [studyid] # we are done
                elif datatype == 'dcm_series':
                    matches.append(seriesid)
                    break # go to next series
                elif datatype == 'dcm_instance':
                    matches.append(instid)
                    continue # go to next instance

            if not match:
                continue # there was a rule that did not match, so try the next series

            # at this point all rules matched for tags that could be found, 
            #  but some tags need to be found at a deeper level
            pass

        return matches
    

    def matchinstances(self, instanceids, rules, datatype):
        """
        match the contents of hdrs to the set of rules; return list of matching data ids
        specific for instance level. will split in instances of the same seriesid.
        datatype is the datatype wanted by this selector.
        """
        implemented = ['dcm_series', 'dcm_study', 'dcm_instance']
        if not datatype in implemented:
            raise ValueError('[match] Unimplemented datatype %s'%(datatype))
            
        # first check on shared series headers has failed or is skipped

        matches = []
        # treat per seriesid
        instances_per_series = {}
        for instanceid in instanceids:
            seriesid = self.pacsio.getSeriesId(instanceid)
            if not seriesid in instances_per_series.keys():
                instances_per_series[seriesid] = []
            instances_per_series[seriesid].append(instanceid)
            
        for seriesid, instanceids in instances_per_series.items():
            matches.extend(self.matchinstances_perseries(instanceids, rules, datatype))

        return matches
    

    def getSharedStudyHeaders(self, studyid):
        """
        obtain as json/dict the shared headers of a study; this should be implemented as a call to wad_pacsio
        for orthanc this would be the json result of 
          curl -u user:pswd http://localhost:8042/studies/4362aea1-3fe0cc9b-97b52b3d-208e0758-8c6cdaae/shared-tags
        """
        datalevel = 'dcm_study'
        if not studyid in self.headers_cache[datalevel].keys():
            self.headers_cache[datalevel][studyid] = self.pacsio.getSharedStudyHeaders(studyid) # result is a json

        return self.headers_cache[datalevel][studyid]
    
    def getSharedSeriesHeaders(self, seriesid):
        """
        obtain as json/dict the shared headers of a series; this should be implemented as a call to wad_pacsio
        for orthanc this would be the json result of 
          curl -u user:pswd http://localhost:8042/series/4362aea1-3fe0cc9b-97b52b3d-208e0758-8c6cdaae/shared-tags
        """
        datalevel = 'dcm_series'
        if not seriesid in self.headers_cache[datalevel].keys():
            self.headers_cache[datalevel][seriesid] = self.pacsio.getSharedSeriesHeaders(seriesid) # result is a json

        return self.headers_cache[datalevel][seriesid]

    def getInstanceHeaders(self, instanceid):
        """
        obtain as json/dict the headers of an instance; this should be implemented as a call to wad_pacsio
        for orthanc this would be the json result of 
          curl -u user:pswd http://localhost:8042/instances/61cac249-add0114b-d49fbb55-f2a78a4a-16d24344/tags
        """
        datalevel = 'dcm_instance'
        if not instanceid in self.headers_cache[datalevel].keys():
            self.headers_cache[datalevel][instanceid] = self.pacsio.getInstanceHeaders(instanceid) # result is a json

        return self.headers_cache[datalevel][instanceid]

def main(): # define separately to allow automatic script creation
    parser = argparse.ArgumentParser(description='WAD Selector')
    sourcename = None
    studyid = None
    inifile = 'wadconfig.ini'
    dryrun = False
    logfile_only = False

    parser.add_argument('-s','--studyid',
                        default=studyid,type=str,
                        help='the new study_id to be checked',dest='studyid')

    parser.add_argument('-n','--source',
                        default=sourcename,type=str,
                        help='the name of the data source (pacs) to access for the study',dest='sourcename')

    parser.add_argument('-i','--inifile',
                        default=inifile,type=str,
                        help='the inifile file of the WAD server if not using the default location',dest='inifile')

    parser.add_argument('--logfile_only',
                        default=logfile_only,action='store_true',
                        help='Suppress output to terminal, only to logfile (use when starting this from a script)',dest='logfile_only')

    parser.add_argument('--dryrun',
                        default=dryrun,action='store_true',
                        help='just give feedback and do not create jobs',dest='dryrun')

    args = parser.parse_args()
    if args.studyid is None or args.sourcename is None:
        parser.print_help()
        exit()
        
    sel = Selector(args.inifile, args.logfile_only)
    sel.run(args.sourcename, args.studyid, args.dryrun)
    

if __name__ == "__main__":
    main()
