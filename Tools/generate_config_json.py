#!/usr/bin/env python
from __future__ import print_function

"""
Changelog:
  20180616: errno no longer in os
  20180418: changed DRXSmall lowCNR to minimally 1.15; removed some warning limits for MR
  20180417: Changed skip top from 6 to 16 for Affirm
  20180329: OCR change channel "sum" to "avg"
  20180219: fixed XA siemens uniformity; changed basic name order: modality_phantom_machine
  20180209: Added extra ocr generic
  20180124: fixed DXU for XA; increased pixelsize for FD2020 Frontal to fix rotated box; increase border Frontal
  20180115: added eL18_4
  20171025: US philips hor_offset to 3, for kurtosis
  20170925: US param split between hdrs, ocr and qc; added units and images to MR
  20170905: added more OCR to US
  20170901: US philips ver_offset=2 (white line iu22); added rev_bbox to all philips L
  20170830: added action limits (meta) for CR, DR, DRX, RF; US philips hor_offset=2; added rev_bbox to US
  20170802: added parsCDCOM to CDMAM
  20170801: add DX and DXU for XA
  20170622: separate config for portable DR for DX
  20170621: added auto_suffix to RF/DDL; adjusted values for Force; adjusted values CT1
  20170616: added DRX-Revolution
  20170503: more params for US
  20170502: Split CT Force in 4 configs
  20170320: Added dummy config for yet to be implemented fBIRN ReceiveFieldTest
  20170310: Added Eleva Normi13 module
  20170227: Renamed MR stuff to FBIRN where appropriate
  20161221: Added params to US
  20161219: Added params for MG
  20161216: Added CT Siemens SOMATOM Force; CR Siemens Ysio
  20161110: meta results is now dict
  20161026: fBIRN
  20160901: OCR
  20160829: params for US
  20160822: new version and structure for DX Normi13; DX unif 5mm border
  20160622: split in config and meta
  20160620: add section results containing limits and display stuff
  20160610: add action 'acqdatetime' with limit period
  20160531: changed param back to key-value pair; moved param info to comments
  20160518: changed param value to {'value': value, 'info': info}
  20160415: added destination folder
  20160414: initial version based on generate_config_xml.py version 20160414
"""
__version__ = '20180219'
__author__ = 'aschilham'

import json
import argparse
import os.path
import errno
import sys

DISPLAY_USER = 2
DISPLAY_KEYUSER = 1
DISPLAY_ADMIN = 0

def _addparaminfo(params, comments, name, value, info):
    if name in params:
        raise KeyError('[_addparam] key "%s" already exists'%name)
    params[name] = value
    if not 'params' in comments:
        comments['params'] = {}
    comments['params'][name] = info

def _addparam(params, name, value):
    if name in params:
        raise KeyError('[_addparam] key "%s" already exists'%name)
    params[name] = value

def _addaction(actions, name, params={}, filters={}):
    if name in actions:
        raise KeyError('[_addaction] key "%s" already exists'%name)

    actions[name] = {
        'params': params,
        'filters': filters
    }

def _addresult(results, name, display_level=None, display_name=None, description=None, units=None, equals=None, minlowhighmax=None, valpaccpcrit=None, period=None):
    # all results names should be unique; multiple calls with the same name add to or overwrite the existing result

    # find results entry to add to
    # (0, 'admin'), (1, 'key-user'), (2, 'user')
    if not name in results.keys():
        results[name] = {'display_level': 0}
    res = results[name]

    # display 
    if not display_level is None: res['display_level'] = display_level
    if not display_name is None: res['display_name'] = display_name
    if not description is None: res['description'] = description
    if not units is None: res['units'] = units
        
    # constraints
    if not equals is None: res['constraint_equals'] = equals
    if not minlowhighmax is None: res['constraint_minlowhighmax'] = list(minlowhighmax)
    if not period is None: res['constraint_period'] = period
    if not valpaccpcrit is None: 
        cval, pacc, pcrit = valpaccpcrit
        res['constraint_minlowhighmax'] = [cval*(100.-pcrit)/100., cval*(100.-pacc)/100., cval*(100.+pacc)/100., cval*(100.+pcrit)/100.]

def _writeConfig(filename, confdict):
    # internal function for writing
    folder, fname = os.path.split(filename)
    metafilename = os.path.join(folder,'meta',fname)
    metadict = { f: confdict[f] for f in ['results']}
    del confdict['results']
    metadict['comments'] = {f: confdict['comments'][f] for f in ['author', 'creator', 'description']}

    with open(filename,'w') as f:
        json.dump(confdict,f,indent=4,sort_keys=True)
    print("written",filename)

    with open(metafilename,'w') as f:
        json.dump(metadict,f,indent=4,sort_keys=True)
    print("written",metafilename)

def _addfilter(filters, filtdict):
    for name,value in filtdict.items():
        if name in filters:
            raise KeyError('[_addfilter] key "%s" already exists'%name)
        filters[name] = value

def writeXXConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "US/AirReverberations module for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    if m_id == 'epiq':
        comments['description'] += 'Philips Epiq'
        comments['version'] = '20160413'
        _addparam(hdr_params, 'dummy1', 'hdr dummy parameter 1')
        _addparam(hdr_params, 'dummy2', 'hdr dummy parameter 2')
        _addaction(actions, 'header_series', params=hdr_params)

        _addparam(qc_params, 'dummy1', 'qc dummy parameter 1')
        _addparam(qc_params, 'dummy2', 'qc dummy parameter 2')
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results, 'example string',
                   display_name='example string', display_level=2, description='example result string', 
                   units='', equals='WITHIN SPECS')
        _addresult(results, 'example float',
                   display_name='example float', display_level=2, description='example result float', 
                   units='', minlowhighmax=[40., 50., 70., 80.])
        _addresult(results, 'example float2',
                   display_name='example float2', display_level=2, description='example result float2', 
                   units='', valpaccpcrit=[1., 5., 10.])

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)
    
# Start building
#
#
def writeFBIRNQCConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_QC for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime', 
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips'
        comments['version'] = '20161026'
        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)


    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)
    
def writeFBIRNB0Config(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_B0map for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime', 
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips'
        comments['version'] = '20161026'
        _addaction(actions, 'header_series', params=hdr_params)

        _addparaminfo(qc_params, comments, 'circleradiusfactor', 0.85, 'factor to adjust circle radius to stay within phantom')
        _addaction(actions, 'B0_series', params=qc_params)


    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)
    
def writeFBIRNB1Config(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_B1map for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime', 
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips'
        comments['version'] = '20161118'
        _addaction(actions, 'header_series', params=hdr_params)

        _addparaminfo(qc_params, comments, 'circleradiusfactor', 0.75, 'factor to adjust circle radius to stay within phantom')
        _addaction(actions, 'B1_series', params=qc_params)


    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)
    
def writeFBIRNSNRConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_PureSNR for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime', 
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips'
        comments['version'] = '20161026'
        _addaction(actions, 'header_series', params=hdr_params)

        _addparaminfo(qc_params, comments, 'circleradiusfactor', 0.75, 'factor to adjust circle radius to stay within phantom')
        _addaction(actions, 'snr_series', params=qc_params)


    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)
    
def writeFBIRNReceiveConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_ReceiveField for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime', 
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips (module not yet implemented)'
        comments['version'] = '20170320'


    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)
    
def writeOCRConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "OCR/Generic for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime', 
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == 'epiq':
        comments['description'] += 'Philips Epiq'
        comments['version'] = '20160901'
        _addparaminfo(qc_params, comments, 'OCR_TissueIndex:xywh', '822;22;90;25', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(qc_params, comments, 'OCR_TissueIndex:prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(qc_params, comments, 'OCR_TissueIndex:type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')
        _addparaminfo(qc_params, comments, 'OCR_MechIndex:xywh', '913;22;80;25', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(qc_params, comments, 'OCR_MechIndex:prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(qc_params, comments, 'OCR_MechIndex:type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')
        _addaction(actions,'qc_series', params=qc_params)


    if m_id == "Intevo_LEHR": # make a config with all params
        comments['description'] += 'Intevo_LEHR'
        comments['version'] = '20180209'

        # for unif only
        _addparaminfo(qc_params, comments, "channel", "avg", "which channel to take for multi-channel data; either an integer or 'avg', or 'rgb' (avg)") 
        _addparaminfo(qc_params, comments, "slicenr", -1, "which slice to analyse if enhanced dicom is used (-1)")
        _addparaminfo(qc_params, comments, "OCR_Detector:type", "float",
                      "result should be interpreted as variable of this type")
        _addparaminfo(qc_params, comments, "OCR_Detector:xywh", "25;120;100;25", 
                      "x0;y0;width;height of bounding box")
        _addparaminfo(qc_params, comments, "OCR_Detector:prefix", "Detector", 
                          "text in box starts with this and this should be stripped")

        _addparaminfo(qc_params, comments, "OCR_Integral:type", "float",
                          "result should be interpreted as variable of this type")
        _addparaminfo(qc_params, comments, "OCR_Integral:xywh", "882;294;100;25", 
                          "x0;y0;width;height of bounding box")
        _addparaminfo(qc_params, comments, "OCR_Integral:suffix", "%", 
                          "text in box ends with this and this should be stripped")

        _addparaminfo(qc_params, comments, "OCR_Label:type", "string",
                          "result should be interpreted as variable of this type")
        _addparaminfo(qc_params, comments, "OCR_Label:xywh", "64;416;200;25", 
                          "x0;y0;width;height of bounding box")

        _addaction(actions, 'qc_series', params=qc_params)


    if m_id == "ALOKAG8":
        comments['description'] += 'ALOKA G8'
        comments['version'] = '20180328'

        _addparaminfo(qc_params, comments, 'ocr_threshold', 80, 'threshold on pixel values to remove background gradient (0)')
        _addparaminfo(qc_params, comments, 'ocr_zoom', 2, 'zoom factor before ocr analysis (10)')
        _addparaminfo(qc_params, comments, 'OCR_probeID:xywh', '676;167;34;15', 'bounding box of probeID if specified, this will be used as identifier for values, else from dicom headers')
        _addparaminfo(qc_params, comments, 'OCR_probeID:type', 'string', 'contents of bounding box of OCR_probeID should be of this type')
        _addparaminfo(qc_params, comments, 'channel', 2, 'convert to RGB by using channel number 0,1,2 or "avg", or "rgb" (2)')
        _addaction(actions,'qc_series', params=qc_params)

    if not 'usage' in comments:
        comments['usage'] = []
    comments['usage'].extend([
        '  "OCR_xxx:prefix": "mm" # text in bounding box of OCR_xxx starts with this and this should be stripped',
        '  "OCR_xxx:suffix": "mm" # text in bounding box of OCR_xxx ends with this and this should be stripped',
        '\n'
    ])

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)

def writeUSConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "US/AirReverberations for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    qc_params = {}
    ocr_params = {}
    hdr_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime', 
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    # Shared Philips
    _addparaminfo(qc_params, comments, 'uni_filter', 5,   'running average of this width before peak detection')
    _addparaminfo(qc_params, comments, 'uni_delta',  0.05, 'a dip in normalized reverb pattern must be at least <delta>')
    _addparaminfo(qc_params, comments, 'uni_depth',  5, 'depth in mm of size of ROI for reverb pattern analysis from top of profile. (5)')
    _addparaminfo(qc_params, comments, 'uni_start',  2, 'offset in mm to start of ROI for reverb pattern analysis from top of profile. (2)') # 3?
    _addparaminfo(qc_params, comments, 'uni_range_model',  'skip10pct', 'method to determine y-range for uniformity profile. absolute/skip10pct/skip20pct/maxsenslimit (absolute)') # 3?
    _addparaminfo(qc_params, comments, 'sen_filter', 5,   'running average of this width for sensitivity data')
    _addparaminfo(qc_params, comments, 'sen_delta',  0.1, 'a peak in sensitivity profile must be at least <fdelta>*(max-noise)')
    _addparaminfo(qc_params, comments, 'ver_offset', 2,   'default lines to exclude from top and bottom when making profiles (10)') # 2 at least for iu22 (white line)
    _addparaminfo(qc_params, comments, 'hor_offset', 3,   'default lines to exclude from left and right when making profiles (10)') # with 2 kurtosis might explode
    _addparaminfo(qc_params, comments, 'fitcircle_frac', 1/3., 'use only this central fraction for circle fitting')
    _addparaminfo(qc_params, comments, 'cluster_fminsize', 300., 'ignore clusters smaller than imwidth*imheight/fminsize')
    _addparaminfo(qc_params, comments, 'signal_thresh', 0, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
    _addparaminfo(qc_params, comments, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
    _addparaminfo(qc_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
    _addparaminfo(ocr_params, comments, 'ocr_threshold', 0, 'threshold on pixel values to remove background gradient (0)')
    _addparaminfo(ocr_params, comments, 'ocr_zoom', 10, 'zoom factor before ocr analysis (10)')
    _addparaminfo(ocr_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
    _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')

    if m_id.startswith('epiq'):
        comments['description'] += 'Philips Epiq'
        comments['version'] = '20170907'
        _addaction(actions,'header_series', params=hdr_params)

        if 'L12_3' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '159;865;68;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L12_5' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '143;879;68;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L15_7io' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '204;818;129;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L17_5' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '152;870;68;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'eL18_4' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '159;865;68;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
            
        _addaction(actions,'qc_series', params=qc_params)

        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:xywh', '822;23;90;24', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:xywh', '913;23;80;24', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:xywh', '21;159;27;18', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_DynRange:xywh', '50;179;60;18', 'x0;y0;width;height of bounding box of OCR_DynRange')
        _addparaminfo(ocr_params, comments, 'OCR_DynRange:prefix', 'R', 'text in bounding box of OCR_DynRange starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_DynRange:type', 'float', 'contents minus prefix of bounding box of OCR_DynRange should be of this type')

        _addparaminfo(ocr_params, comments, 'OCR_FrameRate:xywh', '11;76;79;20', 'x0;y0;width;height of bounding box of OCR_FrameRate')
        _addparaminfo(ocr_params, comments, 'OCR_FrameRate:type', 'float', 'contents minus prefix of bounding box of OCR_FrameRate should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_FrameRate:suffix', 'Hz', 'text in bounding box of OCR_FrameRate starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_ResSpd:xywh', '11;97;59;20', 'x0;y0;width;height of bounding box of OCR_ResSpd')
        _addparaminfo(ocr_params, comments, 'OCR_ResSpd:type', 'string', 'contents minus prefix of bounding box of OCR_ResSpd should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_Persistence:xywh', '14;200;82;20', 'x0;y0;width;height of bounding box of OCR_Persistence')
        _addparaminfo(ocr_params, comments, 'OCR_Persistence:type', 'string', 'contents minus prefix of bounding box of OCR_Persistence should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_Persistence:prefix', 'P', 'text in bounding box of OCR_Persistence starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_PenResGen:xywh', '14;218;82;20', 'x0;y0;width;height of bounding box of OCR_PenResGen')
        _addparaminfo(ocr_params, comments, 'OCR_PenResGen:type', 'string', 'contents minus prefix of bounding box of OCR_PenResGen should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_LUT:xywh', '959;78;36;17', 'x0;y0;width;height of bounding box of OCR_PenResGen')
        _addparaminfo(ocr_params, comments, 'OCR_LUT:type', 'string', 'contents minus prefix of bounding box of OCR_PenResGen should be of this type')
        _addaction(actions,'ocr_series', params=ocr_params)
        if not 'usage' in comments:
            comments['usage'] = []
        comments['usage'].extend([
            '  "OCR_xxx:prefix": "mm" # text in bounding box of OCR_xxx starts with this and this should be stripped',
            '  "OCR_xxx:suffix": "mm" # text in bounding box of OCR_xxx ends with this and this should be stripped',
            '\n'
        ])

    elif m_id.startswith('iU22'):
        comments['description'] += 'Philips iU22'
        comments['version'] = '20170907'
        _addaction(actions,'header_series', params=hdr_params)

        if 'L9_3' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '217;876;125;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L12_5' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '202;891;125;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L15_7io' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '198;893;125;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L17_5' in m_id:
            _addparaminfo(qc_params, comments, 'rev_bbox', '212;881;125;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        _addaction(actions,'qc_series', params=qc_params)

        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:xywh', '824;6;86;26', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:xywh', '910;6;90;26', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:xywh', '14;159;28;19', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_DynRange:xywh', '28;179;35;19', 'x0;y0;width;height of bounding box of OCR_DynRange')
        _addparaminfo(ocr_params, comments, 'OCR_DynRange:type', 'float', 'contents minus prefix of bounding box of OCR_DynRange should be of this type')

        _addparaminfo(ocr_params, comments, 'OCR_FrameRate:xywh', '46;77;78;20', 'x0;y0;width;height of bounding box of OCR_FrameRate')
        _addparaminfo(ocr_params, comments, 'OCR_FrameRate:type', 'float', 'contents minus prefix of bounding box of OCR_FrameRate should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_FrameRate:suffix', 'Hz', 'text in bounding box of OCR_FrameRate starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_ResSpd:xywh', '14;97;59;20', 'x0;y0;width;height of bounding box of OCR_ResSpd')
        _addparaminfo(ocr_params, comments, 'OCR_ResSpd:type', 'string', 'contents minus prefix of bounding box of OCR_ResSpd should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_Persistence:xywh', '14;200;82;20', 'x0;y0;width;height of bounding box of OCR_Persistence')
        _addparaminfo(ocr_params, comments, 'OCR_Persistence:type', 'string', 'contents minus prefix of bounding box of OCR_Persistence should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_Persistence:prefix', 'P', 'text in bounding box of OCR_Persistence starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_PenResGen:xywh', '14;218;82;20', 'x0;y0;width;height of bounding box of OCR_PenResGen')
        _addparaminfo(ocr_params, comments, 'OCR_PenResGen:type', 'string', 'contents minus prefix of bounding box of OCR_PenResGen should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_LUT:xywh', '959;78;36;17', 'x0;y0;width;height of bounding box of OCR_PenResGen')
        _addparaminfo(ocr_params, comments, 'OCR_LUT:type', 'string', 'contents minus prefix of bounding box of OCR_PenResGen should be of this type')
        _addaction(actions,'ocr_series', params=ocr_params)

    elif m_id == 'CX50':
        comments['description'] += 'Philips CX50'
        comments['version'] = '20170503'
        _addaction(actions,'header_series', params=hdr_params)
        _addaction(actions,'qc_series', params=qc_params)

        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:xywh', '565;30;94;26', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:xywh', '565;4;94;26', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:xywh', '4;200;75;19', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:prefix', 'Gn', 'text in bounding box of OCR_Gain starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_DynRange:xywh', '4;219;75;19', 'x0;y0;width;height of bounding box of OCR_DynRange')
        _addparaminfo(ocr_params, comments, 'OCR_DynRange:prefix', 'C', 'text in bounding box of OCR_DynRange starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_DynRange:type', 'float', 'contents minus prefix of bounding box of OCR_DynRange should be of this type')
        _addaction(actions,'ocr_series', params=ocr_params)

    elif m_id == 'HD11':
        comments['description'] += 'Philips HD11'
        comments['version'] = '20170901'
        _addaction(actions,'header_series', params=hdr_params)
        _addaction(actions,'qc_series', params=qc_params)

        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:xywh', '565;30;94;26', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_TissueIndex:type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:xywh', '565;4;94;26', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_MechIndex:type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:xywh', '4;200;75;19', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:prefix', 'Gn', 'text in bounding box of OCR_Gain starts with this')
        _addparaminfo(ocr_params, comments, 'OCR_Gain:type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')
        _addaction(actions,'ocr_series', params=ocr_params)

    elif m_id == 'VolusonE8':
        comments['description'] += 'GE Voluson E8'
        comments['author'] = 'Pepijn van Horssen, VUmc'
        comments['version'] = '20180328'
        qc_params = {}
        hdr_params = {}
        ocr_params = {}

        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addaction(actions,'header_series', params=hdr_params)

        _addparaminfo(qc_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(qc_params, comments, 'uni_filter', 5,   'running average of this width before peak detection')
        _addparaminfo(qc_params, comments, 'uni_delta',  0.3, 'a dip in normalized reverb pattern must be at least <delta>')
        _addparaminfo(qc_params, comments, 'uni_depth',  5, 'depth in mm of size of ROI for reverb pattern analysis from top of profile. (5)')
        _addparaminfo(qc_params, comments, 'uni_start',  2, 'offset in mm to start of ROI for reverb pattern analysis from top of profile. (2)')
        _addparaminfo(qc_params, comments, 'sen_filter', 5,   'running average of this width for sensitivity data')
        _addparaminfo(qc_params, comments, 'sen_delta',  0.2, 'a peak in sensitivity profile must be at least <fdelta>*(max-noise)')
        _addparaminfo(qc_params, comments, 'ver_offset', 1,   'default lines to exclude from top and bottom when making profiles (10)')
        _addparaminfo(qc_params, comments, 'hor_offset', 5,   'default lines to exclude from left and right when making profiles (10)')
        _addparaminfo(qc_params, comments, 'fitcircle_frac', 1, 'use only this central fraction for circle fitting')
        _addparaminfo(qc_params, comments, 'cluster_fminsize', 5, 'ignore clusters smaller than imwidth*imheight/fminsize')
        _addparaminfo(qc_params, comments, 'signal_thresh', 25, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
        _addparaminfo(qc_params, comments, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
        _addparaminfo(qc_params, comments, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addaction(actions,'qc_series', params=qc_params)

        _addparaminfo(ocr_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(ocr_params, comments, 'ocr_threshold', 80, 'threshold on pixel values to remove background gradient (0)')
        _addparaminfo(ocr_params, comments, 'ocr_zoom', 2, 'zoom factor before ocr analysis (10)')
        _addparaminfo(ocr_params, comments, 'OCR_probeID:xywh', '334;2;139;20', 'bounding box of probeID if specified, this will be used as identifier for values, else from dicom headers')
        _addparaminfo(ocr_params, comments, 'OCR_probeID:type', 'string', 'contents of bounding box of OCR_probeID should be of this type')
        _addparaminfo(ocr_params, comments, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addaction(actions,'ocr_series', params=ocr_params)

    elif m_id == 'ALOKAG8':
        comments['description'] += 'ALOKA G8 C15'
        comments['author'] = 'Pepijn van Horssen, VUmc'
        comments['version'] = '20180328'
        qc_params = {}
        hdr_params = {}
        ocr_params = {}

        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addaction(actions,'header_series', params=hdr_params)
    
        _addparaminfo(qc_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(qc_params, comments, 'uni_filter', 5,   'running average of this width before peak detection')
        _addparaminfo(qc_params, comments, 'uni_delta',  0.3, 'a dip in normalized reverb pattern must be at least <delta>')
        _addparaminfo(qc_params, comments, 'uni_depth',  5, 'depth in mm of size of ROI for reverb pattern analysis from top of profile. (5)')
        _addparaminfo(qc_params, comments, 'uni_start',  2, 'offset in mm to start of ROI for reverb pattern analysis from top of profile. (2)')
        _addparaminfo(qc_params, comments, 'sen_filter', 5,   'running average of this width for sensitivity data')
        _addparaminfo(qc_params, comments, 'sen_delta',  0.2, 'a peak in sensitivity profile must be at least <fdelta>*(max-noise)')
        _addparaminfo(qc_params, comments, 'ver_offset', 1,   'default lines to exclude from top and bottom when making profiles (10)')
        _addparaminfo(qc_params, comments, 'hor_offset', 5,   'default lines to exclude from left and right when making profiles (10)')
        _addparaminfo(qc_params, comments, 'fitcircle_frac', 1, 'use only this central fraction for circle fitting')
        _addparaminfo(qc_params, comments, 'cluster_fminsize', 5, 'ignore clusters smaller than imwidth*imheight/fminsize')
        _addparaminfo(qc_params, comments, 'signal_thresh', 25, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
        _addparaminfo(qc_params, comments, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
        _addparaminfo(qc_params, comments, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addparaminfo(qc_params, comments, 'rev_bbox', '223;547;139;524', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        _addaction(actions,'qc_series', params=qc_params) 

        _addparaminfo(ocr_params, comments, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(ocr_params, comments, 'ocr_threshold', 80, 'threshold on pixel values to remove background gradient (0)')
        _addparaminfo(ocr_params, comments, 'ocr_zoom', 2, 'zoom factor before ocr analysis (10)')
        _addparaminfo(ocr_params, comments, 'OCR_probeID:xywh', '676;167;34;15', 'bounding box of probeID if specified, this will be used as identifier for values, else from dicom headers')
        _addparaminfo(ocr_params, comments, 'OCR_probeID:type', 'string', 'contents of bounding box of OCR_probeID should be of this type')
        _addparaminfo(ocr_params, comments, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addaction(actions,'ocr_series', params=ocr_params)

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)

def writeRFConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "RF/Pehamed for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})
    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime', 
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    _addresult(results, 'PhantomOrientation',
               display_name='Phantom Orientation', display_level=1, description='check if orientation of phantom is correct', units='')
    _addresult(results, 'AlignConfidence',
               display_name='Align Confidence', display_level=1, description='check if outline of phantom correctly located', units='%')
    _addresult(results, 'CuConfidence',
               display_name='Cu Confidence', display_level=1, description='check if copper wedge of phantom correctly located', units='%')

    if m_id == 'DDL':
        comments['description'] += 'Philips Omni Diagnost Eleva'
        comments['version'] = '20170828'
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'attempt to add suffix to results based on protocol name') 
        qc_params = hdr_params.copy()

        _addaction(actions,'header_series', params=hdr_params)
        _addaction(actions,'qc_series', params=qc_params)
        
        _addresult(results, 'PhantomOrientation', minlowhighmax=[-1, -1, 1, 1])
        _addresult(results, 'AlignConfidence', display_level=1, description='check if orientation of phantom is correct', 
                   units='', minlowhighmax=[80, 90, 100, 100])
        _addresult(results, 'CuConfidence', display_level=1, description='check if copper wedge of phantom is located correctly',
                   units='', minlowhighmax=[80, 90, 100, 100])
        _addresult(results, 'PhantomAngle', display_level=2, description='rotation angle of phantom',
                   units='deg', minlowhighmax=[-2.5, -2.5, 2.5, 2.5])
        _addresult(results, 'PhantomOrientation', display_level=1, description='check if orientation of phantom is correct',
                   units='deg', minlowhighmax=[-1, -1, 1, 1])
        _addresult(results, 'CuDR0.6_0.0', display_name='DynamicRange', display_level=2, 
                   description='dynamic range over Cu wedge', units='', minlowhighmax=[1.5, 1.5, 15., 15.])
        _addresult(results, 'CuSNR_0.6_0.0', display_name='SNR', display_level=2,
                   description='signal to noise ratio in Cu wedge thickness 0.0mm', units='', minlowhighmax=[40, 40, 100, 100])
        _addresult(results, 'ROIUniformity', display_name='Uniformity', display_level=2,
                   description='compare uniformity between ROIs left and right with cente', units='', minlowhighmax=[0, 0, 15, 20])

        # results to show, without action limits (yet)
        _addresult(results, "AnnotatedImage",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)


def writeCTConfig(m_id, anatomy, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "CT/PerformanceQuickIQ for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    actions = {}
    hdr_params = {}
    qc_params = {}
    qc_params = {}
    results_Head = {}
    results_Body = {} #will be added at the end if there is a body
    
    _addaction(actions, 'acqdatetime', params={})

    # declare all results here in order of appearance. Details can be added/altered later on
    ## Head phantom
    _addresult(results_Head, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    # results that are the same between scanners
    _addresult(results_Head, 'Patient Position', display_level=1, equals='HFS')
    _addresult(results_Head, 'MeanCenter',
               display_name='HU Water HEAD', display_level=2, description='mean HU value in ROI in center of phantom', 
               units='HU', minlowhighmax=[-4, -3, 3, 4])
    _addresult(results_Head, 'MeanAir',
               display_name='HU Air HEAD', display_level=2, description='mean HU value in ROI outside phantom', 
               units='HU', minlowhighmax=[-1004, -1003, -997, -996])
    _addresult(results_Head, 'maxdev',
               display_name='HU Max Deviation HEAD', display_level=2, description='max deviation from reference HU values for objects in phantom', 
               units='HU', minlowhighmax=[-8, -7, 7, 8])
    _addresult(results_Head, 'linearity',
               display_name='HU linearity fit HEAD', display_level=1, description='r2 coefficient of linear fit between measured and reference HU values', 
               units='', valpaccpcrit=[1., 7.5, 10.])

    # results that are different between scanners
    _addresult(results_Head, 'Protocol Name', display_level=1, display_name='Protocol HEAD')
    _addresult(results_Head, 'roisd',
               display_name='Noise HEAD', display_level=2, description='noise in ROI in center of phantom', units='HU')
    _addresult(results_Head, 'snr_hol',
               display_name='SNR HEAD', display_level=1, description='(mean+1000) to noise ratio in ROI in center of phantom', units='')
    _addresult(results_Head, 'unif',
               display_name='Non-uniformity HEAD', display_level=2, description='Non-uniformity of phantom', units='HU')
    _addresult(results_Head, 'MeanHigh',
               display_name='HU High HEAD', display_level=1, description='mean value for high HU object', 
               units='HU')

    ## Body phantom
    _addresult(results_Body, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    # results that are the same between scanners
    _addresult(results_Body, 'Patient Position', display_level=1, equals='HFS')
    _addresult(results_Body, 'MeanCenter',
               display_name='HU Center BODY', display_level=2, description='mean HU value in ROI in center of phantom', 
               units='HU', minlowhighmax=[-4, -3, 3, 4])
    _addresult(results_Body, 'MeanAir',
               display_name='HU Air BODY', display_level=2, description='mean HU value in ROI outside phantom', 
               units='HU', minlowhighmax=[-1004, -1003, -997, -996])
    _addresult(results_Body, 'maxdev',
               display_name='HU Max Deviation BODY', display_level=2, description='max deviation from reference HU values for objects in phantom', 
               units='HU', minlowhighmax=[-8, -4, 4, 8])
    _addresult(results_Body, 'linearity',
               display_name='HU linearity fit BODY', display_level=1, description='r2 coefficient of linear fit between measured and reference HU values', 
               units='', valpaccpcrit=[1., 7.5, 10.])
    _addresult(results_Body, 'MeanHigh',
               display_name='HU High BODY', display_level=1, description='mean value for high HU object', 
               units='HU')

    # results that are different between scanners
    _addresult(results_Body, 'Protocol Name', display_level=1, display_name='Protocol BODY')
    _addresult(results_Body, 'roisd',
               display_name='Noise BODY', display_level=2, description='noise in ROI in center of phantom', units='HU')
    _addresult(results_Body, 'snr_hol',
               display_name='SNR BODY', display_level=1, description='(mean+1000) to noise ratio in ROI in center of phantom', units='')
    _addresult(results_Body, 'unif',
               display_name='Non-uniformity BODY', display_level=2, description='Non-uniformity of phantom', units='HU')

    if m_id == 'CT1':
        comments['description'] += 'Philips Brilliance 64'
        comments['version'] = '20170622'
        
        _addaction(actions,'header_series', params=hdr_params)

        _addparam(qc_params, 'scannername', 'Brilliance 64')
        _addparam(qc_params, 'headHU_air',   -1000.)
        _addparam(qc_params, 'headHU_water',     1.2)
        _addparam(qc_params, 'headHU_pvc',    1185.)
        _addparam(qc_params, 'headdiammm_in',  188.)
        _addparam(qc_params, 'headdiammm_out', 192.)
        _addparam(qc_params, 'bodyHU_aculon',   98.)
        _addparam(qc_params, 'bodyHU_teflon',  878.)
        _addparam(qc_params, 'bodyHU_water',     1.2)
        _addparaminfo(qc_params, comments, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results_Head, 'Protocol Name', equals='IQ Check/Head')
        _addresult(results_Head, 'roisd', valpaccpcrit=[2.75, 7.5, 10.])
        _addresult(results_Head, 'snr_hol', valpaccpcrit=[360., 25., 30.])
        _addresult(results_Head, 'unif', minlowhighmax=[-3.7, -3.3, 0.3, 0.7])

    if m_id == 'CT2leen':
        comments['description'] += 'Philips Brilliance 64 leen'
        comments['version'] = '20160413'
        
        _addaction(actions,'header_series', params=hdr_params)

        _addparam(qc_params, 'scannername', 'Brilliance 64')
        _addparam(qc_params, 'headHU_air',   -1000.)
        _addparam(qc_params, 'headHU_water',     1.2)
        _addparam(qc_params, 'headHU_pvc',    1212.)
        _addparam(qc_params, 'headdiammm_in',  188.)
        _addparam(qc_params, 'headdiammm_out', 192.)
        _addparam(qc_params, 'bodyHU_aculon',   98.)
        _addparam(qc_params, 'bodyHU_teflon',  878.)
        _addparam(qc_params, 'bodyHU_water',     1.2)
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results_Head, 'Protocol Name', equals='IQ Check/Head')
        _addresult(results_Head, 'roisd',  valpaccpcrit=[2.75, 7.5, 10.])
        _addresult(results_Head, 'snr_hol', valpaccpcrit=[360., 25., 30.])
        _addresult(results_Head, 'unif', minlowhighmax=[-3.7, -3.2, 0.3, 0.7])

    if m_id == 'CT2':
        comments['description'] += 'Philips Mx8000IDT'
        comments['version'] = '20160413'
        
        _addaction(actions,'header_series', params=hdr_params)

        _addparam(qc_params, 'scannername', 'Mx8000IDT')
        _addparam(qc_params, 'headHU_air',   -1000.)
        _addparam(qc_params, 'headHU_water',     0.)
        _addparam(qc_params, 'headHU_pvc',    1205.)
        _addparam(qc_params, 'headdiammm_in',  188.)
        _addparam(qc_params, 'headdiammm_out', 192.)
        _addparam(qc_params, 'bodyHU_aculon',  100.)
        _addparam(qc_params, 'bodyHU_teflon',  915.)
        _addparam(qc_params, 'bodyHU_water',     0.)
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results_Head, 'Protocol Name', equals='Head QA/Head')
        _addresult(results_Head, 'roisd',  minlowhighmax=[4., 4.25, 4.75, 5.])
        _addresult(results_Head, 'snr_hol', valpaccpcrit=[222., 15., 30.])
        _addresult(results_Head, 'unif', minlowhighmax=[-2.5, -1.5, 0.5, 1.5])

        _addresult(results_Body, 'Protocol Name', equals='QA body/Abdomen/Ax')
        _addresult(results_Body, 'MeanCenter', minlowhighmax=[95, 97, 101, 102])
        _addresult(results_Body, 'roisd',  valpaccpcrit=[13.5, 5., 10.])
        _addresult(results_Body, 'snr_hol', valpaccpcrit=[80., 43.75, 87.5])
        _addresult(results_Body, 'unif', minlowhighmax=[-2.5, -1.5, 0.5, 1.5])

    if m_id == 'CT3':
        comments['description'] += 'Philips iCT 256'
        comments['version'] = '20160622'
        
        _addaction(actions,'header_series', params=hdr_params)

        _addparaminfo(qc_params, comments, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')
        _addparaminfo(qc_params, comments, 'use_bodyairdistmm', 130., 'distance in mm to ROI for Air for Body (def. 115.75)')

        _addparam(qc_params, 'scannername', 'iCT 256')
        _addparam(qc_params, 'headHU_air',   -1000.)
        _addparam(qc_params, 'headHU_water',     1.2)
        _addparam(qc_params, 'headHU_pvc',    1128.)
        _addparam(qc_params, 'headdiammm_in',  190.)
        _addparam(qc_params, 'headdiammm_out', 194.)
        _addparam(qc_params, 'bodyHU_aculon',    0.) # water!
        _addparam(qc_params, 'bodyHU_teflon',  920.) # 936 old phantom
        #_addparam(qc_params, 'bodyHU_water',     0.)
        _addparam(qc_params, 'bodyHU_air',   -1000.)
        _addaction(actions, 'qc_series', params=qc_params)

        # _addresult(results_Head, 'Protocol Name', equals='Head QA/Head') # Unknown
        _addresult(results_Head, 'roisd',  valpaccpcrit=[2.6, 7.5, 10.])
        _addresult(results_Head, 'snr_hol', valpaccpcrit=[385., 15., 20.])
        _addresult(results_Head, 'unif', minlowhighmax=[-3.5, -3.0, -0.5, 0.5])

        #_addresult(results_Body, 'Protocol Name', equals='QA body/Abdomen/Ax') # Unknown
        _addresult(results_Body, 'MeanCenter', minlowhighmax=[-4, -3, 3, 4])
        _addresult(results_Body, 'roisd',  valpaccpcrit=[7, 10., 15.])
        _addresult(results_Body, 'snr_hol', valpaccpcrit=[140., 15., 20.])
        _addresult(results_Body, 'unif', minlowhighmax=[-8, -8, 8, 8])

    if m_id.startswith('CT2_iqon'):
        comments['description'] += 'Philips IQON 64'
        comments['version'] = '20160622'
        
        _addaction(actions, 'header_series', params=hdr_params)

        _addparaminfo(qc_params, comments, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')
        _addparaminfo(qc_params, comments, 'use_bodyairdistmm', 130., 'distance in mm to ROI for Air for Body (def. 115.75)')
        _addparam(qc_params, 'scannername', 'IQon 64')
        _addparam(qc_params, 'headHU_air',   -1000.)
        _addparam(qc_params, 'headHU_water',    -0.5)
        _addparam(qc_params, 'headdiammm_in',  190.)
        _addparam(qc_params, 'headdiammm_out', 194.)
        _addparam(qc_params, 'bodyHU_aculon',    0.)  # water!
        _addparam(qc_params, 'bodyHU_air',   -1000.)
        #_addparam(qc_params, 'bodyHU_water',     0)


        # _addresult(results_Head, 'Protocol Name', equals='Head QA/Head') # Unknown
        _addresult(results_Head, 'snr_hol', valpaccpcrit=[385., 15., 20.])
        _addresult(results_Head, 'unif', minlowhighmax=[-4, -4, 4, 4])

        #_addresult(results_Body, 'Protocol Name', equals='QA body/Abdomen/Ax') # Unknown
        _addresult(results_Body, 'MeanCenter', minlowhighmax=[-4, -3, 3, 4])
        _addresult(results_Body, 'snr_hol', valpaccpcrit=[125., 15., 20.])
        _addresult(results_Body, 'unif', minlowhighmax=[-8, -8, 8, 8])
        
        if m_id.endswith('MonoE'):
            _addparam(qc_params, 'headHU_pvc',     161.)
            _addparam(qc_params, 'bodyHU_teflon',  866.)
            _addresult(results_Head, 'roisd',  valpaccpcrit=[3.3, 10., 15.]) # 2.5 for old iqon
            _addresult(results_Body, 'roisd',  valpaccpcrit=[9.5, 10., 15.]) 
        else:
            _addparam(qc_params, 'headHU_pvc',     122.)
            _addparam(qc_params, 'bodyHU_teflon',  923.)
            _addresult(results_Head, 'roisd',  valpaccpcrit=[3.5, 10., 15.]) # 2.5 for old iqon
            _addresult(results_Body, 'roisd',  valpaccpcrit=[11.5, 10., 15.]) # 8 for old iqon
            
        _addaction(actions, 'qc_series', params=qc_params)

    if m_id == 'CT4':
        comments['description'] += 'Philips Brilliance 16P'
        comments['version'] = '20160413'
        
        _addaction(actions, 'header_series', params=hdr_params)

        _addparam(qc_params, 'scannername', 'Brilliance 16P')
        _addparam(qc_params, 'headHU_air',   -1000.)
        _addparam(qc_params, 'headHU_water',     0.)
        _addparam(qc_params, 'headHU_pvc',    1206.)
        _addparam(qc_params, 'headdiammm_in',  188.)
        _addparam(qc_params, 'headdiammm_out', 192.)
        _addparam(qc_params, 'bodyHU_aculon',  105.)
        _addparam(qc_params, 'bodyHU_teflon',  908.)
        _addparam(qc_params, 'bodyHU_water',     0)
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results_Head, 'Protocol Name', equals='quick_proc/Head')
        _addresult(results_Head, 'roisd',  valpaccpcrit=[3.9, 5., 10.])
        _addresult(results_Head, 'snr_hol', valpaccpcrit=[260., 12.5, 25.])
        _addresult(results_Head, 'unif', minlowhighmax=[-2.5, -1.5, 0.5, 1.5])

    if m_id.startswith('CT4F'):
        comments['description'] += 'Siemens SOMATOM Force'
        comments['version'] = '20170621'
        
        _addaction(actions, 'header_series', params=hdr_params)

        _addparam(qc_params, 'scannername', 'SOMATOM Force') # do not try to determine anatomy, use this one
        _addparam(qc_params, 'use_anatomy', 'head')
        _addparam(qc_params, 'headHU_air',   -1000.)
        _addparam(qc_params, 'headHU_water',     0.)
        _addparam(qc_params, 'headdiammm_in',  193.)
        _addparam(qc_params, 'headdiammm_out', 196.)
        _addparam(qc_params, 'bodyHU_aculon',  105.) # dummy, only head phantom used
        _addparam(qc_params, 'bodyHU_teflon',  908.) # dummy, only head phantom used
        _addparam(qc_params, 'bodyHU_water',     0)  # dummy, only head phantom used
        _addparaminfo(qc_params, comments, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')
        _addparaminfo(qc_params, comments, 'use_bodyairdistmm', 93., 'distance in mm to ROI for Air for Body (def. 115.75)')
        
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results_Head, 'Protocol Name', equals='quick_proc/Head')
        _addresult(results_Head, 'snr_hol', valpaccpcrit=[260., 12.5, 25.])
        _addresult(results_Head, 'unif', minlowhighmax=[-2.5, -1.5, 0.5, 1.5])
        
        if m_id.endswith('_BodyAa'): # 6.5
            _addparam(qc_params, 'headHU_shell',   108.) # not pvc, but some other material for shell
            _addresult(results_Head, 'roisd',  valpaccpcrit=[6.5, 5., 10.]) 
        elif m_id.endswith('_BodyDual'):
            _addparam(qc_params, 'headHU_shell',   109.) # not pvc, but some other material for shell
            _addresult(results_Head, 'roisd',  valpaccpcrit=[5.25, 5., 10.]) 
        elif m_id.endswith('_BodyPedAa'): # 11.
            _addparam(qc_params, 'headHU_shell',   100.) # not pvc, but some other material for shell
            _addresult(results_Head, 'roisd',  valpaccpcrit=[11., 5., 10.]) 
        elif m_id.endswith('_HeadAa'):
            _addparam(qc_params, 'headHU_shell',   120.) # not pvc, but some other material for shell
            _addresult(results_Head, 'roisd',  valpaccpcrit=[10.5, 5., 10.]) 

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results_Head if anatomy == 'head' else results_Body
    }
    
    _writeConfig(filename, configdict)
    

def writeCRConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "CR_DR/Pehamed_Wellhofer for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})
    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(results, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    _addresult(results, 'PhantomOrientation_Table',
               display_name='Phantom Orientation Table', display_level=1, description='check if orientation of phantom is correct', 
               units='degrees', minlowhighmax=[-1, -1, 1, 1])
    _addresult(results, 'AlignConfidence_Table',
               display_name='Align Confidence Table', display_level=1, description='check if outline of phantom correctly located', 
               units='%', minlowhighmax=[70, 80, 100, 100])
    _addresult(results, 'xray[N]cm_Table',
               display_name='xray[N] Table', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[11.3, 11.9, 13.1, 13.7])
    _addresult(results, 'xray[E]cm_Table',
               display_name='xray[E] Table', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[11.3, 11.9, 13.1, 13.7])
    _addresult(results, 'xray[S]cm_Table',
               display_name='xray[S] Table', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[11.3, 11.9, 13.1, 13.7])
    _addresult(results, 'xray[W]cm_Table',
               display_name='xray[W] Table', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[11.3, 11.9, 13.1, 13.7])
    _addresult(results, 'xrayDev_Table',
               display_name='xrayDev Table', display_level=2, description='max deviation of x-ray field', 
               units='%', minlowhighmax=[-1.1, -.5, .5, 1.1])
    _addresult(results, 'ROIUniformity_Table',
               display_name='ROIUniformity Table', display_level=2, description='Uniformity difference accross horizontal ROI', 
               units='%', minlowhighmax=[-10, -5, 5, 10])
    _addresult(results, 'MTFPosConfidence_Table',
               display_name='MTFPosition Confidence Table', display_level=1, description='check if line pairs in phantom correctly located', 
               units='%', minlowhighmax=[70, 80, 100, 100])
    _addresult(results, 'MTFFreqConfidence_Table',
               display_name='MTFFequencies Confidence Table', display_level=1, description='check if line pairs in phantom correctly translated to frequencies', 
               units='%', minlowhighmax=[70, 80, 100, 100])

    _addresult(results, 'PhantomOrientation_Wall',
               display_name='Phantom Orientation Wall', display_level=1, description='check if orientation of phantom is correct', 
               units='degrees', minlowhighmax=[-1, -1, 1, 1])
    _addresult(results, 'AlignConfidence_Wall',
               display_name='Align Confidence Wall', display_level=1, description='check if outline of phantom correctly located', 
               units='%', minlowhighmax=[70, 80, 100, 100])
    _addresult(results, 'xray[N]cm_Wall',
               display_name='xray[N] Wall', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[10.5, 11.5, 13.5, 14.5])
    _addresult(results, 'xray[E]cm_Wall',
               display_name='xray[E] Wall', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[10.5, 11.5, 13.5, 14.5])
    _addresult(results, 'xray[S]cm_Wall',
               display_name='xray[S] Wall', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[10.5, 11.5, 13.5, 14.5])
    _addresult(results, 'xray[W]cm_Wall',
               display_name='xray[W] Wall', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[10.5, 11.5, 13.5, 14.5])
    _addresult(results, 'xrayDev_Wall',
               display_name='xrayDev Wall', display_level=2, description='max deviation of x-ray field', 
               units='%', minlowhighmax=[-1.1, -.5, .5, 1.1])
    _addresult(results, 'LRUniformity_Wall',
               display_name='ROIUniformity Wall', display_level=2, description='Uniformity difference between left and right', 
               units='%', minlowhighmax=[-10, -5, 5, 10])
    _addresult(results, 'MTFPosConfidence_Wall',
               display_name='MTFPosition Confidence Wall', display_level=1, description='check if line pairs in phantom correctly located', 
               units='%', minlowhighmax=[70, 80, 100, 100])
    _addresult(results, 'MTFFreqConfidence_Wall',
               display_name='MTFFequencies Confidence Wall', display_level=1, description='check if line pairs in phantom correctly translated to frequencies', 
               units='%', minlowhighmax=[70, 80, 100, 100])

    # changing limits
    _addresult(results, 'CuSNR_0.0_Table',
               display_name='CuSNR_0.0 Table', display_level=1, description='SNR in copper of thickness 0.0', units='')
    _addresult(results, 'CuCNRmin_Table',
               display_name='CuCNRmin Table', display_level=1, description='minimal CNR across copper wedge', units='')
    _addresult(results, 'CuDR0.6_0.0_Table',
               display_name='CuDR Table', display_level=1, description='dynamic range across copper wedge', units='')
    _addresult(results, 'lowCNR_0_Table',
               display_name='lowCNR Table', display_level=1, description='CNR of low contrast object', units='')
    _addresult(results, 'AreaContrast5_Table',
               display_name='AreaC5 Table', display_level=1, description='area under contrast curve', units='')
    _addresult(results, 'AreaMTF5_Table',
               display_name='AreaMTF5 Table', display_level=1, description='area under MTF curve', units='')
    _addresult(results, 'MTF10_Table',
               display_name='MTF10 Table', display_level=1, description='MTF 10 pct cut off', units='')

    _addresult(results, 'CuSNR_0.0_Wall',
               display_name='CuSNR_0.0 Wall', display_level=1, description='SNR in copper of thickness 0.0', units='')
    _addresult(results, 'CuCNRmin_Wall',
               display_name='CuCNRmin Wall', display_level=1, description='minimal CNR across copper wedge', units='')
    _addresult(results, 'CuDR0.6_0.0_Wall',
               display_name='CuDR Wall', display_level=1, description='dynamic range across copper wedge', units='')
    _addresult(results, 'lowCNR_0_Wall',
               display_name='lowCNR Wall', display_level=1, description='CNR of low contrast object', units='')
    _addresult(results, 'AreaContrast5_Wall',
               display_name='AreaC5 Wall', display_level=1, description='area under contrast curve', units='')
    _addresult(results, 'AreaMTF5_Wall',
               display_name='AreaMTF5 Wall', display_level=1, description='area under MTF curve', units='')
    _addresult(results, 'MTF10_Wall',
               display_name='MTF10 Wall', display_level=1, description='MTF 10 pct cut off', units='')

    if m_id == 'AZUDIDI':
        comments['description'] += 'Philips Digital Diagnost with Pehamed'
        comments['version'] = '20160413'
        _addparam(hdr_params, 'roomname', 'DiDi')
        _addparam(hdr_params, 'tablepidmm', 85)
        _addparam(hdr_params, 'wallpidmm', 65)
        _addparam(hdr_params, 'phantom', 'pehamed') # pehamed nr 100876
        _addparam(hdr_params, 'linepair_type', 'typ38') # pehamed nr 100876
        _addparam(hdr_params, 'xymm1.8', '53.1;27.1') # linepair_typ38
        _addparam(hdr_params, 'xymm0.6', '80.1;-04.5') # linepair_typ38
        _addparam(hdr_params, 'xymm1.4', '59.4;-24.7') # linepair_typ38
        _addparam(hdr_params, 'xymm4.6', '28.0;02.0') # linepair_typ38
        _addaction(actions,'header_series', params=hdr_params)

        qc_params = hdr_params.copy()
        _addaction(actions,'qc_series', params=qc_params)
        
        _addresult(results, 'Grid_Wall',
                   display_name='Grid Wall', display_level=1, description='', units='', equals='IN')
        _addresult(results, 'Postprocessing_Wall',
                   display_name='Postprocessing Wall', display_level=1, description='', units='', equals='constancy')
        _addresult(results, 'FocalSpot(s)_Wall',
                   display_name='FocalSpot(s) Wall', display_level=1, description='', units='', equals='2.000')
        _addresult(results, 'FilterType_Wall',
                   display_name='FilterType Wall', display_level=1, description='tube filtration', units='', equals='0.2Cu,1Al')
        _addresult(results, 'Grid_Table',
                   display_name='Grid Table', display_level=1, description='', units='', equals='IN')
        _addresult(results, 'Postprocessing_Table',
                   display_name='Postprocessing Table', display_level=1, description='', units='', equals='constancy')
        _addresult(results, 'FocalSpot(s)_Table',
                   display_name='FocalSpot(s) Table', display_level=1, description='', units='', equals='0.000')
        _addresult(results, 'FilterType_Table',
                   display_name='FilterType Table', display_level=1, description='tube filtration', units='', equals='02mmAl')

        _addresult(results, 'kVp_Wall',
                   display_name='kVp Wall', display_level=2, description='tube voltage', 
                   units='kV', minlowhighmax=[124.5, 124.99, 125.01,125.5])
        _addresult(results, 'kVp_Table',
                   display_name='kVp Table', display_level=2, description='tube voltage', 
                   units='kV', minlowhighmax=[72.5, 72.99, 73.01, 73.5])
        _addresult(results, 'ImageAreaDoseProduct_Wall',
                   display_name='DAP Wall', display_level=2, description='dose area product', 
                   units='dGy mm2', minlowhighmax=[.21, .25, .31, .35])
        _addresult(results, 'ImageAreaDoseProduct_Table',
                   display_name='DAP Table', display_level=2, description='dose area product', 
                   units='dGy mm2', minlowhighmax=[1.7, 1.8, 2.2, 2.3])
        _addresult(results, 'DistanceSourceToDetector (mm)_Wall',
                   display_name='distance Wall', display_level=2, description='distance between source and detector', 
                   units='mm', minlowhighmax=[1990., 1995., 2005., 2010.])
        _addresult(results, 'DistanceSourceToDetector (mm)_Table',
                   display_name='distance Table', display_level=2, description='distance between source and detector', 
                   units='mm', minlowhighmax=[1190., 1195., 1205., 1210.])
        _addresult(results, 'Exposure (mAs)_Wall',
                   display_name='mAs Wall', display_level=2, description='tube load', 
                   units='mAs', minlowhighmax=[2., 2., 3., 3.])
        _addresult(results, 'Exposure (mAs)_Table',
                   display_name='mAs Table', display_level=2, description='tube load', 
                   units='mAs', minlowhighmax=[11., 11., 14., 14.])
        _addresult(results, 'ExposureTime (ms)_Wall',
                   display_name='ms Wall', display_level=2, description='pulse length', 
                   units='ms', minlowhighmax=[4., 4., 7., 7.])
        _addresult(results, 'ExposureTime (ms)_Table',
                   display_name='ms Table', display_level=2, description='pulse length', 
                   units='ms', minlowhighmax=[27., 27., 43., 43.])

        _addresult(results, 'CuSNR_0.0_Table', minlowhighmax=[30., 50., 70., 90.])
        _addresult(results, 'CuCNRmin_Table', minlowhighmax=[3., 3.5, 5., 6.])
        _addresult(results, 'CuDR0.6_0.0_Table', minlowhighmax=[1.5, 3.25, 4.5, 6.])
        _addresult(results, 'lowCNR_0_Table', minlowhighmax=[2.0, 2.5, 4.5, 4.5])
        _addresult(results, 'AreaContrast5_Table', minlowhighmax=[0.05, 0.0625, .1, .1])
        _addresult(results, 'AreaMTF5_Table', minlowhighmax=[.45, .48, 1., 1.])
        _addresult(results, 'MTF10_Table', minlowhighmax=[3.1, 3.4, 5., 5.])

        _addresult(results, 'CuSNR_0.0_Wall', minlowhighmax=[10., 40., 70., 80.])
        _addresult(results, 'CuCNRmin_Wall', minlowhighmax=[1.1, 1.2, 2.5, 3.0])
        _addresult(results, 'CuDR0.6_0.0_Wall', minlowhighmax=[1.3, 1.6, 1.9, 2.0])
        _addresult(results, 'lowCNR_0_Wall', minlowhighmax=[1.25, 2.0, 4.0, 4.0])
        _addresult(results, 'AreaContrast5_Wall', minlowhighmax=[0.025, 0.0525, .1, .1])
        _addresult(results, 'AreaMTF5_Wall', minlowhighmax=[.40, .45, 1., 1.])
        _addresult(results, 'MTF10_Wall', minlowhighmax=[3.0, 3.325, 5., 5])
    
    elif 'WKZFCR' in m_id:
        comments['description'] += 'Siemens FCR with Wellhofer'
        comments['version'] = '20160413'
        
        _addparam(hdr_params, 'tablesidmm', 1150)
        _addparam(hdr_params, 'tablepidmm',   65)
        _addparam(hdr_params, 'wallsidmm',  2000)
        _addparam(hdr_params, 'wallpidmm',    50)
        _addparam(hdr_params, 'outvalue',   1023)
        _addparam(hdr_params, 'phantom', 'wellhofer') # 
        _addparam(hdr_params, 'linepair_type', 'typ38') # pehamed nr 100876
        _addparam(hdr_params, 'xymm1.8', '53.7;27.1') # linepair_typ38
        _addparam(hdr_params, 'xymm0.6', '80.6;-04.7') # linepair_typ38
        _addparam(hdr_params, 'xymm1.4', '59.9;-24.9') # linepair_typ38
        _addparam(hdr_params, 'xymm4.6', '28.4;01.8') # linepair_typ38
        if m_id == 'WKZFCR1':
            _addparam(hdr_params, 'roomname', 'WKZ1')
            datevalue =   '20110000;500'
            datevalue += '|20121130;450'
            datevalue += '|20130611;350'
            datevalue += '|40000000;403'
            #<sdthreshold>40</sdthreshold>
        elif m_id == 'WKZFCR2':
            _addparam(hdr_params, 'roomname', 'WKZ2')
            datevalue =   '20110500;700'
            datevalue += '|20110516;350'
            datevalue += '|20110625;600'
            datevalue += '|20111115;350'
            datevalue += '|20121130;400'
            datevalue += '|20110516;600'
            
        _addparam(hdr_params, 'sensitivitydatavalue', datevalue)
        _addaction(actions,'header_series', params=hdr_params)

        qc_params = hdr_params.copy()
        _addaction(actions,'qc_series', params=qc_params)
        
        _addresult(results, 'SoftwareVersions_Wall',
                   display_name='SoftwareVersions Wall', display_level=1, description='', units='', equals='A19')
        _addresult(results, 'Acquisition Device Processing Code_Wall',
                   display_name='Processing Wall', display_level=1, description='', units='', equals='0904')
        _addresult(results, 'SoftwareVersions_Table',
                   display_name='SoftwareVersions Table', display_level=1, description='', units='', equals='A19')
        _addresult(results, 'Acquisition Device Processing Code_Table',
                   display_name='Processing Table', display_level=1, description='', units='', equals='0904')

        _addresult(results, 'CuSNR_0.0_Table', minlowhighmax=[40.5, 42.75, 49.5, 49.5])
        _addresult(results, 'CuCNRmin_Table', minlowhighmax=[2.97, 3.135, 3.63, 3.63])
        _addresult(results, 'CuDR0.6_0.0_Table', minlowhighmax=[1.89, 1.995, 2.31, 2.31])
        _addresult(results, 'lowCNR_0_Table', minlowhighmax=[1.8, 1.9, 2.2, 2.2])
        _addresult(results, 'AreaContrast5_Table', minlowhighmax=[0.04, 0.0425, .05, .05])
        _addresult(results, 'AreaMTF5_Table', minlowhighmax=[.3, .325, .5, .5])
        _addresult(results, 'MTF10_Table', minlowhighmax=[3., 3.5, 5., 5.])

        _addresult(results, 'CuSNR_0.0_Wall', minlowhighmax=[31.5, 33.25, 40., 40.])
        _addresult(results, 'CuCNRmin_Wall', minlowhighmax=[1.17, 1.235, 2., 2.])
        _addresult(results, 'CuDR0.6_0.0_Wall', minlowhighmax=[1.35, 1.425, 2., 2.])
        _addresult(results, 'lowCNR_0_Wall', minlowhighmax=[1., 1.05, 2., 2.])
        _addresult(results, 'AreaContrast5_Wall', minlowhighmax=[0.03, 0.0325, .04, .04])
        _addresult(results, 'AreaMTF5_Wall', minlowhighmax=[.3, .325, .5, .5])
        _addresult(results, 'MTF10_Wall', minlowhighmax=[3., 3.25, 5., 5.])
    
        comments['usage'] = [
            'params that must be set for both actions below:',
            '  "roomname": "Bucky1" # any identifier',
            '  "tablepidmm": 85 # distance [mm] between phantom on table and image detector',
            '  "wallpidmm": 85 # distance [mm] between phantom on wall and image detector',
            '  "phantom": "pehamed" # type of phantom used, either pehamed or wellhofer',
            '\n' + \
            'params that must be set too for phosphor readers for both actions below:',
            '  "tablesidmm": 1150 # distance [mm] between source and table image detector',
            '  "wallsidmm": 2000 # distance [mm] between source and wall image detector',
            '  "outvalue": 1023 # pixelvalue of outside phantom bounds',
            '  "sdthreshold": 40 # threshold on standard deviation in uniform area to determine table or wall',
            '    or define a list of (date;threshold) on sensitivity:',
            '  "sensitivitydatavalue": "20110000;500|20130611;350|40000000:403',
            '     # threshold is 500 upto date 20110000',
            '     # threshold is 350 between 20110001 and 20130611',
            '     # threshold is 403 between 20130611 and 40000000',
            'params that can be set if the line pair phantom is not found correctly:',
            '  "linepair_type": "typ38" # linepair_typ38\n'+\
            '    "xymm1.8": "53.7;27.1" # the location of the dot in 1.8 on the line pair pattern',
            '    "xymm0.6": "80.6;-04.7" # the location of the dot in 0.6 on the line pair pattern',
            '    "xymm1.4": "59.9;-24.9" # the location of the dot in 1.4 on the line pair pattern',
            '    "xymm4.6": "28.4;01.8" # the location of the dot in 4.6 on the line pair pattern',
        ]
    elif 'YSIO' in m_id:
        comments['description'] += 'Siemens Ysio with Wellhofer'
        comments['version'] = '20161216'
        
        _addparam(hdr_params, 'roomname', 'YSIO')
        _addparam(hdr_params, 'use_mustbeinverted', 'False'), # override
        _addparam(hdr_params, 'tablepidmm',   58)
        _addparam(hdr_params, 'wallpidmm',    65)
        _addparam(hdr_params, 'outvalue',   20)
        _addparam(hdr_params, 'phantom', 'wellhofer') # 
        _addparam(hdr_params, 'linepair_type', 'typ38') # pehamed nr 100876
        _addparam(hdr_params, 'xymm1.8', '53.7;27.1') # linepair_typ38
        _addparam(hdr_params, 'xymm0.6', '80.6;-04.7') # linepair_typ38
        _addparam(hdr_params, 'xymm1.4', '59.9;-24.9') # linepair_typ38
        _addparam(hdr_params, 'xymm4.6', '28.4;01.8') # linepair_typ38

        _addaction(actions,'header_series', params=hdr_params)

        qc_params = hdr_params.copy()
        _addaction(actions,'qc_series', params=qc_params)

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    _writeConfig(filename, configdict)

    
def writeOTConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "OT/Generic for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})
    _addresult(results, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == 'input2dicom':
        comments['description'] += 'Input2DICOM from web2py'
        comments['version'] = '20160413'

        _addparam(qc_params, 'private_group', '0x0071')
        _addparam(qc_params, 'private_element', '0x9999')
        _addaction(actions,'qc_series', params=qc_params)

        _addresult(results, 'example string',
                   display_name='example string', display_level=2, description='example result string', 
                   units='', equals='WITHIN SPECS')
        _addresult(results, 'example float',
                   display_name='example float', display_level=2, description='example result float', 
                   units='', minlowhighmax=[40., 50., 70., 80.])

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename,configdict)


def writeNMConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "NM/Generic for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    qc_params = {}
    results = {}
    actions = {}
    qc_filters = {}

    _addaction(actions, 'acqdatetime', params={})
    _addresult(results, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == 'mCT':
        comments['description'] = 'PET/SiemensPETQCparser for Siemens Biograph mCT'
        comments['version'] = '20160413'
        comments['TODO'] = {
            'function':'PET.SiemensPETQCparser.parser.parseqcreport',
            'configtagvalues': 'params'
        }

        _addparam(qc_params, 'name', 'PET Siemens DailyQC')
        _addparam(qc_params, 'description', 'PET Siemens cfg')
        _addparam(qc_params, 'version', '20150131')
        _addparam(qc_params, 'use_private_tag', '0x0071,0x9999')
        _addaction(actions,'qc_series', params=qc_params)

        _addresult(results, 'Isotope',
                   display_name='isotope', display_level=2, description='isotope used', units='', equals='Ge-68')
        _addresult(results, 'System Quality Results',
                   display_name='quality', display_level=2, description='system quality', units='', equals='Passed')
        _addresult(results, 'QC Phantom Activity',
                   display_name='activity', display_level=2, description='activity in phantom', units='', equals='Passed')
        _addresult(results, 'Block Noise',
                   display_name='noise', display_level=2, description='block noise', units='', minlowhighmax=[0, 0, 3, 3])
        _addresult(results, 'Block Efficiency', # always zero, but passed!
                   display_name='block efficiency', display_level=2, description='block efficiency', units='', minlowhighmax=[0, 0, 120, 120])
        _addresult(results, 'Measured Randoms',
                   display_name='randoms', display_level=2, description='measured randoms', units='', minlowhighmax=[85, 85, 115, 115])
        _addresult(results, 'Scanner Efficiency',
                   display_name='scanner efficiency', display_level=2, description='scanner efficiency', units='', minlowhighmax=[25.48, 25.48, 47.32, 47.32])
        _addresult(results, 'Scatter Ratio',
                   display_name='scatter', display_level=2, description='scatter ratio', units='', minlowhighmax=[-28.8, -28.8, 35.2, 35.2])
        _addresult(results, 'ECF',
                   display_name='ECF', display_level=2, description='ECF', units='', minlowhighmax=[20000000, 20000000, 40000000, 40000000])
        _addresult(results, 'Plane efficiency',
                   display_name='plane efficiency', display_level=2, description='plane efficiency', units='', minlowhighmax=[-5, -5, 5, 5])
        _addresult(results, 'Time alignment residual',
                   display_name='residual', display_level=2, description='time alignment resdual', units='', minlowhighmax=[0, 0, 2, 2])

    elif m_id == 'spect2':
        comments['description'] = 'SPECT/NEMA_Uniformity for Siemens Symbia T2'
        comments['version'] = '20160413'
        comments['TODO'] = {
            'function':'NM.NM_NEMA_Uniformity.QCUnif_wadwrapper.nm_uniformity',
            'configtagvalues': 'params'
        }
        _addparam(qc_params, 'name', 'SPECT Siemens SymbiaT2')
        _addparam(qc_params, 'description', 'SPECT Siemens cfg')
        _addparam(qc_params, 'version', '20150429')
        _addparam(qc_params, 'function', 'unif1')
        _addparam(qc_params, 'modality', 'NM')
        _addparam(qc_params, 'perform_dome_correction', '0')
        _addparam(qc_params, 'instancetag', '0008,0060')
        _addfilter(qc_filters,{'seriesdescription':'Extrinsic Sweep Verification Flood [Extrinsic Sweep]'})
        #params_qc.append( ('unif1','NM',[('seriesdescription','Extrinsic Sweep Verification Flood [Extrinsic Sweep]'),('instancetag','0008,0060')]) )

        _addaction(actions,'qc_series', params=qc_params, filters=qc_filters)

        _addresult(results, 'det1_DU_x (UFOV)',
                   display_name='det1 DU x', display_level=2, description='det1_DU_x (UFOV)', units='', minlowhighmax=[0, 0, 5, 8])
        _addresult(results, 'det1_DU_y (UFOV)',
                   display_name='det1 DU y', display_level=2, description='det1_DU_y (UFOV)', units='', minlowhighmax=[0, 0, 5, 8])
        _addresult(results, 'det2_DU_x (UFOV)',
                   display_name='det2 DU x', display_level=2, description='det2_DU_x (UFOV)', units='', minlowhighmax=[0, 0, 5, 8])
        _addresult(results, 'det2_DU_y (UFOV)',
                   display_name='det2 DU y', display_level=2, description='det2_DU_y (UFOV)', units='', minlowhighmax=[0, 0, 5, 8])

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)

def writeMGCDConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MG/CDMAM for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})
    _addresult(results, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == 'CDMam32':
        comments['description'] += 'Philips MicrodoseL50'
        comments['version'] = '20170802'
        _addaction(actions,'header_series', params=hdr_params)

        _addparaminfo(qc_params, comments, 'modeCDCOM', 'False', 'use precompiled cdcom.exe for detection or use python implementation') 
        _addparaminfo(qc_params, comments, 'parsCDCOM', '', 'use extra flags for cdcom.exe, e.g use "c;high" to pass "c" and "high"') 
        _addparaminfo(qc_params, comments, 'phantomversion', '3.2', 'either 3.2 or 3.4') 
        _addaction(actions,'qc_series', params=qc_params)

        _addresult(results, 'EnergyComponent_ALsuproc',
                   display_name='energy component', display_level=2, description='displayed data', units='', equals='SUM FOR PROCESSING')
        _addresult(results, 'iqf_ALsuproc',
                   display_name='iqf', display_level=2, description='image quality factor', units='', minlowhighmax=[1., 1., 1.25, 1.5])

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)

def writeDXConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "DX/Normi13 for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})
    _addresult(results, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == "FULL": # make a config with all params
        comments['description'] += 'Generic'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'Generic', 'your identifier of this room') 

        # distances for auto suffix or if not provided properly
        _addparaminfo(hdr_params, comments, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results')
        _addparaminfo(hdr_params, comments, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
        _addparaminfo(hdr_params, comments, 'wallpidmm',     84, 'distance between phantom on wall stand and image detector')
        _addparaminfo(hdr_params, comments, 'pidmm',         70, 'distance between phantom and image detector (both table and wall)')
        _addparaminfo(hdr_params, comments, 'sidmm',       1000, 'distance between source and image detector (both table and wall)')
        _addparaminfo(hdr_params, comments, 'detector_names', 'SN152495;Tafel|SN152508;Wand', 'translation of serial numbers into your identifiers') 

        # for unif only
        unif_params = dict(hdr_params) # copy
        _addparaminfo(unif_params, comments, 'artefactborderpx', '35;35;35;35', 'number of pixels to skip on each side (xmin, xmax, ymin, ymax)') 
        # overrides
        _addparaminfo(unif_params, comments, 'use_pixmm',            0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(unif_params, comments, 'use_mustbeinverted',   False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(unif_params, comments, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to xmin_px;xmax_px;ymin_px;ymax_px before processing')
        _addparaminfo(unif_params, comments, 'use_mustbemirrored',   False, 'force mirroring of image before analysis.')
        
        # below is for QC only
        qc_params = dict(hdr_params) # copy
        # overrides
        _addparaminfo(qc_params, comments, 'use_pixmm',            0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',   False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to xmin_px;xmax_px;ymin_px;ymax_px before processing')
        _addparaminfo(qc_params, comments, 'use_mustbemirrored',   False, 'force mirroring of image before analysis.')


        # xray edges
        _addparaminfo(qc_params, comments, 'outvalue',   1023, 'value of pixels outside xray field')

        # line pair insert
        _addparaminfo(qc_params, comments, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6 (in RXT02 or typ38)') 
        _addparaminfo(qc_params, comments, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0 (in RXT02)') 
        _addparaminfo(qc_params, comments, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4 (in typ38)') 
        _addparaminfo(qc_params, comments, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8 (in typ38)') 
        _addparaminfo(qc_params, comments, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6 (in typ38)') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)
        _addaction(actions, 'uniformity_series', params=unif_params)

    elif 'AZUDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R3'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUDIDI':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results')
            _addparaminfo(hdr_params, comments, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, comments, 'wallpidmm',     84, 'distance between phantom on wall stand and image detector')
        elif m_id == 'AZUDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(results, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[13., 13., 20., 20.])
            _addresult(results, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.2, 1.2, 2.0, 2.0])
            _addresult(results, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.5, 3.5, 4.5, 4.5])
            _addresult(results, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(results, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])

            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1180, 1190, 1210, 1220])
            _addresult(results, 'FilterType',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="2mmAl")
            _addresult(results, 'FocalSpot(s)',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.000")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])

            # results to show, without action limits (yet)
            _addresult(results, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (mAs)',
                       display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(results, 'ExposureTime (ms)',
                       display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
        
        elif m_id == 'AZUDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(results, 'CuDR2.3_0.0',
                               display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2.5, 2.5, 3.5, 3.5])
            _addresult(results, 'lowCNR_0',
                               display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 1.5, 1.5])
            _addresult(results, 'MTF10',
                               display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.25, 4.25])
            _addresult(results, 'PhantomOrientation',
                               display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(results, 'xray[E]cm',
                               display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(results, 'xray[N]cm',
                               display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(results, 'xray[S]cm',
                               display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(results, 'xray[W]cm',
                               display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                               display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(results, 'FilterType',
                               display_name='Filtration', display_level=2, description='tube filtration', units='', equals="0.2Cu,1Al")
            _addresult(results, 'FocalSpot(s)',
                               display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="2.000")
            _addresult(results, 'Grid',
                               display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(results, 'kVp',
                               display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(results, 'CuSNR_0.0',
                               display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(results, 'Date of Last Calibration',
                               display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (mAs)',
                               display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(results, 'ExposureTime (ms)',
                               display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(results, 'ImageAreaDoseProduct',
                               display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                               display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "normi13",
                               display_name='overview', display_level=2, description='overview of analysis element', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)

    elif 'AZUR4DIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'
        comments['version'] = '20180417'
        _addparaminfo(hdr_params, comments, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUR4DIDITABLE':
            comments['version'] = '20180417'
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       70, 'distance between phantom and image detector')
    
            _addresult(results, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(results, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.5, 1.5, 3.0, 3.0])
            _addresult(results, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.24, 4.24])
            _addresult(results, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(results, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1090, 1090, 1110, 1110])
            _addresult(results, 'FilterMaterial',
                       display_name='FilterMaterial', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
    
            # results to show, without action limits (yet)
            _addresult(results, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (mAs)',
                       display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(results, 'ExposureTime (ms)',
                       display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
        
        elif m_id == 'AZUR4DIDIWALL':
            comments['version'] = '20180417'
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(results, 'CuDR2.3_0.0',
                               display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2.3, 2.3, 3.3, 3.3])
            _addresult(results, 'lowCNR_0',
                               display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 1.5, 1.5])
            _addresult(results, 'MTF10',
                               display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.25, 4.25])
            _addresult(results, 'PhantomOrientation',
                               display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(results, 'xray[E]cm',
                               display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(results, 'xray[N]cm',
                               display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(results, 'xray[S]cm',
                               display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(results, 'xray[W]cm',
                               display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                               display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(results, 'FilterMaterial',
                       display_name='FilterMaterial', display_level=2, description='tube filtration', units='', equals="['COPPER', 'ALUMINIUM']")
            _addresult(results, 'Grid',
                               display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(results, 'kVp',
                               display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(results, 'CuSNR_0.0',
                               display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(results, 'Date of Last Calibration',
                               display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (mAs)',
                               display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(results, 'ExposureTime (ms)',
                               display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(results, 'ImageAreaDoseProduct',
                               display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                               display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "normi13",
                               display_name='overview', display_level=2, description='overview of analysis element', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)
    
    elif 'WKZDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'

        if m_id == 'WKZDIDI1':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, comments, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, comments, 'wallpidmm',     50, 'distance between phantom on wall stand and image detector')
            _addparaminfo(hdr_params, comments, 'roomname', 'WKZ1', 'your identifier of this room')
            _addparaminfo(hdr_params, comments, 'detector_names', 'SN152495;Tafel|SN152508;Wand', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDI2':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, comments, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, comments, 'wallpidmm',     50, 'distance between phantom on wall stand and image detector')
            _addparaminfo(hdr_params, comments, 'roomname', 'WKZ2', 'your identifier of this room') 
            _addparaminfo(hdr_params, comments, 'detector_names', 'SN1525UU;Tafel|SN152590;Wand', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDILOS1': # Large!
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname', 'WKZLOS1', 'your identifier of this room') 
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(results, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(results, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 2., 2.])
            _addresult(results, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3., 3., 4., 4.])
            _addresult(results, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(results, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(results, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(results, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(results, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(results, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(results, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(results, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(results, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(results, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(results, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(results, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDILOS2': #Small!
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname', 'WKZLOS2', 'your identifier of this room') 
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(results, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(results, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 2., 2.])
            _addresult(results, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3., 3., 4., 4.])
            _addresult(results, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(results, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[10.8, 11., 13., 13.2])
            _addresult(results, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[7.8, 8., 10., 10.2])
            _addresult(results, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[7.8, 8., 10., 10.2])
            _addresult(results, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[10.8, 11., 13., 13.2])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(results, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(results, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(results, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(results, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(results, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(results, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(results, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(results, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(results, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(results, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDILOS':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname', 'WKZLOS', 'your identifier of this room') 
            _addparaminfo(hdr_params, comments, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
            _addparaminfo(hdr_params, comments, 'detector_names', 'SN1537DT;Klein2|SN143070;Klein2|SN1522YG;Klein2|SN151685;Groot1', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname',    'WKZ', 'your identifier of this room')
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(results, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(results, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.5, 1.5, 3.0, 3.0])
            _addresult(results, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.4, 3.4, 4.4, 4.4])
            _addresult(results, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(results, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(results, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(results, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(results, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(results, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(results, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(results, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(results, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(results, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(results, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(results, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(results, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname',    'WKZ', 'your identifier of this room') 
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       50, 'distance between phantom and image detector')

            _addresult(results, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2., 2., 4., 4.])
            _addresult(results, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.1, 1.1, 2.1, 2.1])
            _addresult(results, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.4, 3.4, 4.4, 4.4])
            _addresult(results, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(results, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(results, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(results, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(results, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(results, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="['COPPER', 'ALUMINIUM']")
            _addresult(results, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="LARGE")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(results, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(results, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(results, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(results, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(results, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(results, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(results, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(results, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, comments, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, comments, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)

    elif m_id == 'DRXLarge':
        comments['description'] += 'Carestream DRX-Revolution'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'DRXLARGE', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
    
        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, comments, 'sidmm', 1250, 'distance between source and image detector')
        _addparaminfo(qc_params, comments, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, comments, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, comments, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(results, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[1.75, 1.75, 4.5, 4.5])
        _addresult(results, 'lowCNR_0',
                   display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.3, 1.3, 3.0, 3.0])
        _addresult(results, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(results, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(results, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(results, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
    
        _addresult(results, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[105., 105., 105., 105.])
        _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs', minlowhighmax=[1599, 1599, 1601, 1601])
    
        # results to show, without action limits (yet)
        _addresult(results, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(results, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(results, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(results, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(results, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(results, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(results, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(results, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(results, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)


    elif m_id == 'DRXSmall':
        comments['description'] += 'Carestream DRX-Revolution'
        comments['version'] = '20180418'
        _addparaminfo(hdr_params, comments, 'roomname', 'DRXSMALL', 'your identifier of this room') 
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, comments, 'sidmm', 1250, 'distance between source and image detector')
        _addparaminfo(qc_params, comments, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, comments, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, comments, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 
    
        _addresult(results, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[1.75, 1.75, 4.5, 4.5])
        _addresult(results, 'lowCNR_0',
                   display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.15, 1.15, 3.0, 3.0])
        _addresult(results, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(results, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(results, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(results, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
    
        _addresult(results, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[105., 105., 105., 105.])
        _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs', minlowhighmax=[1599, 1599, 1601, 1601])
    
        # results to show, without action limits (yet)
        _addresult(results, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(results, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(results, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(results, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(results, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(results, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(results, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(results, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(results, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)


    elif m_id == 'ElevaAcq':
        comments['description'] += 'Philips MultiDiagnost Eleva'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'WKZEleva', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, comments, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, comments, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(results, 'PhantomOrientation',
                   display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="270.0")
        _addresult(results, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        _addresult(results, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(results, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(results, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
        _addresult(results, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(results, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[2.52, 2.6, 3., 3.08])
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 80., 80., 80.])
    
        # results to show, without action limits (yet)
        _addresult(results, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='')
        _addresult(results, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(results, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(results, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)
    elif m_id == 'ElevaCine':
        comments['description'] += 'Philips MultiDiagnost Eleva'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'WKZEleva', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, comments, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, comments, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(results, 'PhantomOrientation',
                   display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="270.0")
        _addresult(results, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        _addresult(results, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(results, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(results, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
        _addresult(results, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(results, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[2.52, 2.6, 3., 3.08])
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 81., 85., 86.])
        _addresult(results, 'num_slices',
                   display_name='frames', display_level=2, description='number of frames', units='kV', minlowhighmax=[9., 9.1, 12.9, 13.])
    
        # results to show, without action limits (yet)
        _addresult(results, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='')
        _addresult(results, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(results, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(results, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)
    elif m_id == 'FD20':
        comments['description'] += 'Philips AlluraXper FD20C'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'ANG1', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.302, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, comments, 'sidmm', 1000, 'distance between source and image detector')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, comments, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)
    elif m_id == 'FD2020_F':
        comments['description'] += 'Philips AlluraXper FD2020 Frontal'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.294, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, comments, 'sidmm', 1000, 'distance between source and image detector')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, comments, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)
    elif m_id == 'FD2020_L':
        comments['description'] += 'Philips AlluraXper FD2020 Lateral'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.249, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, comments, 'sidmm', 1300, 'distance between source and image detector')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbemirrored',  True, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, comments, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)



    elif m_id == 'HCK_WKZ_A':
        comments['description'] += 'Siemens AXIOM Artis Tube A'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'HCK_WKZ', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.279, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to [xmin_px, xmax_px, ymin_px, ymax_px] before processing')
        _addparaminfo(qc_params, comments, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, comments, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, comments, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, comments, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, comments, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(results, 'PhantomOrientation',
                   display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="270.0")
        _addresult(results, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        _addresult(results, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(results, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(results, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
        _addresult(results, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(results, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[2.52, 2.6, 3., 3.08])
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 80., 80., 80.])
    
        # results to show, without action limits (yet)
        _addresult(results, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='')
        _addresult(results, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(results, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(results, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'qc_series', params=qc_params)

    comments['usage'] = [
        'params that must be set for both actions below:',
        '  "roomname": "Bucky1" # any identifier',
        'and either (to set autodetection of table/wall stand):',
        '  "tablepidmm": 85 # distance [mm] between phantom on table and image detector',
        '  "wallpidmm": 85 # distance [mm] between phantom on wall and image detector',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "pidmm": 0 # distance [mm] between phantom and image detector',
        'params that can be set if the line pair phantom is not found correctly:',
        '  "linepair_typ": "RXT02"',
        '    "xymm0.6": "-83.0;-25.0" # the location of the dot in 0.6 on the line pair pattern',
        '    "xymm1.0": "-99.0;-8.0"  # the location of the dot in 1.0 on the line pair pattern',
        '  "linepair_typ": "typ38"',
        '    "xymm0.6": "-108.5;3.8"  # the location of the dot in 0.6 on the line pair pattern', 
        '    "xymm1.4": "-87.9;24.2"  # the location of the dot in 1.4 on the line pair pattern', 
        '    "xymm1.8": "-81.3;-27.3"  # the location of the dot in 1.8 on the line pair pattern', 
        '    "xymm4.6": "-56.2;-2.2"  # the location of the dot in 4.6 on the line pair pattern', 
        'params that can be set if the phantom orientation is not found correctly:',
        '  "use_mustbeprecropped": "100;1139;0;959" # crop to [xmin_px, xmax_px, ymin_px, ymax_px] before processing',
        '  "use_mustbemirrored": True # force mirroring of image before analysis',
        'optionally stands can be identified by their detectorId:',
        '  "detector_names": "SN1525UU;Tafel|SN152590;Wand"',
        'optionally the source to detector distance can be set manually (with autodetection of table/wall stand)',
        '  "tablesidmm": 1150 # distance [mm] between source and image detector in table',
        '  "wallsidmm": 2000 # distance [mm] between source and image detector in wall',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "sidmm": 1150 # distance [mm] between source and image detector',
        'the following params can be supplied to override automatic determination:',
        '  "use_pixmm": 0.262 # the size of one pixel in mm (on the phantom)',
        '  "use_mustbeinverted": True # higher pixelvalues should mean less attenuation',
        'optionally set the following parameters:',
        '  "auto_suffix": true or false [false]  # to set or suppress auto_suffix generation',

    ]

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)

def writeDXUConfig(m_id,filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "DX/Uniformity for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})
    _addresult(results, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    if 'AZUDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R3'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUDIDI':
            comments['version'] = '20160825'
            _addparaminfo(hdr_params, comments, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, comments, 'wallpidmm',     84, 'distance between phantom on wall stand and image detector')
            _addparaminfo(hdr_params, comments, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
        elif m_id == 'AZUDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(results, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(results, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1180, 1190, 1210, 1220])
            _addresult(results, 'FilterType',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="0mmAl")
            _addresult(results, 'FocalSpot(s)',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.000")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'AZUDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(results, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(results, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(results, 'FilterType',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="0mmAl")
            _addresult(results, 'FocalSpot(s)',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.000")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') # 35 px = 5mm at 0.143 mm resolution
        
        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)

    elif 'AZUR4DIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'
        comments['version'] = '20180417'
        _addparaminfo(hdr_params, comments, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUR4DIDITABLE':
            comments['version'] = '20180417'
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(results, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(results, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1090, 1090, 1110, 1110])
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'AZUR4DIDIWALL':
            comments['version'] = '20180417'
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(results, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(results, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') # 35 px = 5mm at 0.143 mm resolution
        
        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)

    elif 'WKZDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'

        if m_id == 'WKZDIDI1':
            comments['version'] = '20180206'
            _addparam(hdr_params, 'roomname', 'WKZ1') 
            _addparam(hdr_params, 'auto_suffix', True) 
            _addparam(hdr_params, 'tablepidmm',    70)
            _addparam(hdr_params, 'wallpidmm',     50)
            _addparam(hdr_params, 'detector_names', 'SN152495;Tafel|SN152508;Wand') 
        elif m_id == 'WKZDIDI2':
            comments['version'] = '20180206'
            _addparam(hdr_params, 'roomname', 'WKZ2') 
            _addparam(hdr_params, 'auto_suffix', True) 
            _addparam(hdr_params, 'tablepidmm',    70)
            _addparam(hdr_params, 'wallpidmm',     50)
            _addparam(hdr_params, 'detector_names', 'SN1525UU;Tafel|SN152590;Wand') 
        elif m_id == 'WKZDIDILOS':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname', 'WKZLOS', 'your identifier of this room') 
            _addparaminfo(hdr_params, comments, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
            _addparaminfo(hdr_params, comments, 'detector_names', 'SN1537DT;Klein2|SN143070;Klein2|SN1522YG;Klein2|SN151685;Groot1', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDILOS1':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname', 'WKZLOS1', 'your identifier of this room')
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            
            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(results, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(results, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(results, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='')
            _addresult(results, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(results, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(results, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(results, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(results, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(results, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(results, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
            _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'WKZDIDILOS2':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname', 'WKZLOS2', 'your identifier of this room')
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(results, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(results, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(results, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='')
            _addresult(results, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(results, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(results, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(results, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(results, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(results, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(results, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
            _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'WKZDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname',    'WKZ', 'your identifier of this room') 
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(results, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(results, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(results, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='')
            _addresult(results, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(results, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(results, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(results, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(results, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(results, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(results, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
            _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'WKZDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, comments, 'roomname',    'WKZ', 'your identifier of this room')
            _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            _addparaminfo(qc_params, comments, 'pidmm',       50, 'distance between phantom and image detector')

            _addresult(results, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(results, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(results, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(results, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='')
            _addresult(results, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(results, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(results, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(results, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(results, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(results, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(results, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(results, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(results, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(results, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(results, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(results, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(results, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(results, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
            _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)

    elif m_id == 'DRXLarge':
        comments['description'] += 'Carestream DRX-Revolution with large detector'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'DRXLARGE', 'your identifier of this room') 
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, comments, 'sidmm', 1250, 'distance between source and image detector')

        _addresult(results, 'Artefacts',
                   display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 5])
        _addresult(results, 'Uniformity_(%)',
                   display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
    
        _addresult(results, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[90., 90., 90., 90.])
    
        # results to show, without action limits (yet)
        _addresult(results, 'Exposure (uAs)',
                   display_name='uAs', display_level=2, description='tube load', units='uAs')
        _addresult(results, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(results, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(results, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(results, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(results, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(results, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(results, "artefacts",
                   display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(results, "uniformity",
                   display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)

    elif m_id == 'DRXSmall':
        comments['description'] += 'Carestream DRX-Revolution with small detector'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'DRXSMALL', 'your identifier of this room') 
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, comments, 'sidmm', 1250, 'distance between source and image detector')

        _addresult(results, 'Artefacts',
                   display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 5])
        _addresult(results, 'Uniformity_(%)',
                   display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 12.5, 12.5])
    
        _addresult(results, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[90., 90., 90., 90.])
    
        # results to show, without action limits (yet)
        _addresult(results, 'Exposure (uAs)',
                   display_name='uAs', display_level=2, description='tube load', units='uAs')
        _addresult(results, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(results, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(results, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(results, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(results, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(results, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(results, "artefacts",
                   display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(results, "uniformity",
                   display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 
    
        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)

    elif m_id == 'ElevaAcq':
        comments['description'] += 'Philips MultiDiagnost Eleva'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'WKZEleva', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')

        _addresult(results, 'Artefacts',
                   display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
        _addresult(results, 'Uniformity_(%)',
                   display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 9, 10])

        _addresult(results, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(results, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[0.79, 0.8, 0.96, 0.97])
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 80., 80., 80.])
    
        # results to show, without action limits (yet)
        _addresult(results, "artefacts",
                   display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(results, "uniformity",
                   display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)
    elif m_id == 'FD20':
        comments['description'] += 'Philips AlluraXper FD20C'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'ANG1', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.302, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')

        # results to show, without action limits (yet)
        _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)
    elif m_id == 'FD2020_F':
        comments['description'] += 'Philips AlluraXper FD2020 Frontal'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.294, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')

        # results to show, without action limits (yet)
        _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)
        
    elif m_id == 'FD2020_L':
        comments['description'] += 'Philips AlluraXper FD2020 Lateral'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.249, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbemirrored',  True, 'force mirroring of image before analysis.')

        # results to show, without action limits (yet)
        _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;95;135') 

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)


    elif m_id == 'HCK_WKZ_A':
        comments['description'] += 'Siemens AXIOM Artis Tube A'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, comments, 'roomname', 'HCK_WKZ', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'use_pixmm',    0.279, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, comments, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to [xmin_px, xmax_px, ymin_px, ymax_px] before processing')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        # results to show, without action limits (yet)
        _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)

    elif m_id == 'HCK_WKZ_B':
        comments['description'] += 'Siemens AXIOM Artis Tube B'
        comments['version'] = '20180312'
        _addparaminfo(hdr_params, comments, 'roomname', 'HCK_WKZ', 'your identifier of this room')
        _addparaminfo(hdr_params, comments, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        _addparaminfo(qc_params, comments, 'pidmm',    0., 'distance [mm] between phantom and image detector')
        #_addparaminfo(qc_params, comments, 'use_pixmm',    0.279, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, comments, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        # results to show, without action limits (yet)
        _addresult(results, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(results, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addaction(actions, 'header_series', params=hdr_params)
        _addaction(actions, 'uniformity_series', params=qc_params)

    comments['usage'] = [
        'params that must be set for both actions below:',
        '  "roomname": "Bucky1" # any identifier',
        'and either (to set autodetection of table/wall stand):'
        '  "tablepidmm": 85 # distance [mm] between phantom on table and image detector',
        '  "wallpidmm": 85 # distance [mm] between phantom on wall and image detector',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "pidmm": 0 # distance [mm] between phantom and image detector',
        'optionally stands can be identified by their detectorId:',
        '  "detector_names": "SN1525UU;Tafel|SN152590;Wand"',
        'optionally the source to detector distance can be set manually (with autodetection of table/wall stand)',
        '  "tablesidmm": 1150 # distance [mm] between source and image detector in table',
        '  "wallsidmm": 2000 # distance [mm] between source and image detector in wall',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "sidmm": 1150 # distance [mm] between source and image detector',
        'the following params can be supplied to override automatic determination:',
            '  "use_pixmm": 0.262 # the size of one pixel in mm (on the phantom)',
            '  "use_mustbeinverted": True # higher pixelvalues should mean less attenuation',
            '  "use_mustbemirrored": True # force mirroring of image before analysis',
        'optionally set the following parameters:',
        '  "auto_suffix": true or false [false]  # to set or suppress auto_suffix generation',
    ]

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)
    

def writeMGConfig(m_id, tubefilter, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MG/Flatfield for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})
    _addresult(results, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    _addresult(results, 'unif_pct',
               display_name='Non-uniformity', display_level=2, description='Non-uniformity of phantom', units='%')
    _addresult(results, 'snr_hol',
               display_name='SNR', display_level=2, description='mean to noise ratio in ROI in center chestside of phantom', units='')
    _addresult(results, 'doseratio',
               display_name='dose ratio', display_level=1, description='ratio of calculated to agd dose', units='')
    _addresult(results, 'art_clusters',
               display_name='artefacts', display_level=2, description='number of clusters of artefacts', units='')
    _addresult(results, 'expert_inoutoverin',
               display_name='phantom fraction', display_level=1, description='usable fraction of phantom over image', units='')

    _addparam(hdr_params, 'info', 'qc')
    if m_id == 'Selenia':
        comments['description'] += 'Hologic Selenia'
        comments['version'] = '20161219'
        _addaction(actions, 'header_series', params=hdr_params)

        qc_params = hdr_params.copy()
        _addparaminfo(qc_params, comments, 'art_borderpx_lrtb', '0;0;12;12', 'skip this number of pixels in artefact evaluation for left, right, top, bottom of image') 
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results, 'Grid',
                   display_name='grid', display_level=1, description='is grid used', units='', equals='HTC_IN')
        _addresult(results, 'ProtocolName',
                   display_name='protocol', display_level=1, description='protocol used', units='', equals='Flat Field')

        _addresult(results, 'unif_pct',  minlowhighmax=[0., 0., 7.5,  10.])
        _addresult(results, 'snr_hol',   minlowhighmax=[60., 60., 80., 100.])
        _addresult(results, 'doseratio', valpaccpcrit=[1., 5., 10.])
        _addresult(results, 'art_clusters',       minlowhighmax=[0, 0,  0, 1])
        _addresult(results, 'expert_inoutoverin', minlowhighmax=[-.2, -.1, .5, 1.])

    elif m_id == 'MicrodoseL50':
        comments['description'] += 'Philips Microdose L50'
        comments['version'] = '20161219'
        _addaction(actions, 'header_series', params=hdr_params)

        qc_params = hdr_params.copy()
        _addparaminfo(qc_params, comments, 'art_borderpx_lrtb', '0;0;0;0', 'skip this number of pixels in artefact evaluation for left, right, top, bottom of image') 
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results, 'BodyPartThickness',
                   display_name='thickness', display_level=2, description='compressed thickness', units='', equals='42')
        _addresult(results, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='', equals='32')

        _addresult(results, 'unif_pct',  minlowhighmax=[-5.0, -2.5, 2.5, 5.0])
        _addresult(results, 'snr_hol',   valpaccpcrit=[23.9, 10., 20.])
        _addresult(results, 'doseratio', valpaccpcrit=[1., 5., 10.])
        _addresult(results, 'art_clusters',       minlowhighmax=[0, 0,  0, 1])
        _addresult(results, 'expert_inoutoverin', minlowhighmax=[-.2, -.1, .5, 1.])

    elif m_id == 'Dimensions':
        comments['description'] += 'Hologic Dimensions'
        comments['version'] = '20161219'
        _addaction(actions, 'header_series', params=hdr_params)

        qc_params = hdr_params.copy()
        _addparaminfo(qc_params, comments, 'art_borderpx_lrtb', '0;0;12;12', 'skip this number of pixels in artefact evaluation for left, right, top, bottom of image') 
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results, 'Grid',
                   display_name='grid', display_level=1, description='is grid used', units='', equals='IN')
        _addresult(results, 'ProtocolName',
                   display_name='protocol', display_level=1, description='protocol used', units='', equals='Flat Field Conv')

        _addresult(results, 'unif_pct',  minlowhighmax=[0., 0., 7.5,  10.])
        _addresult(results, 'snr_hol',   minlowhighmax=[60., 61., 74., 75.])
        _addresult(results, 'doseratio', valpaccpcrit=[1., 5., 10.])
        _addresult(results, 'art_clusters',       minlowhighmax=[0, 0,  0, 1])
        _addresult(results, 'expert_inoutoverin', minlowhighmax=[-.2, -.1, .5, 1.])

    elif m_id == 'AffirmProne':
        comments['description'] += 'Hologic Affirm Prone'
        comments['version'] = '20180417'
        _addaction(actions, 'header_series', params=hdr_params)

        qc_params = hdr_params.copy()
        _addparaminfo(qc_params, comments, 'art_borderpx_lrtb', '0;0;16;16', 'skip this number of pixels in artefact evaluation for left, right, top, bottom of image') 
        _addaction(actions, 'qc_series', params=qc_params)

        _addresult(results, 'ProtocolName',
                   display_name='protocol', display_level=1, description='protocol used', units='', equals='PMMA 4,5 cm')

        _addresult(results, 'unif_pct',  minlowhighmax=[0., 0., 7.5,  10.])
        _addresult(results, 'snr_hol',   minlowhighmax=[80., 81., 99., 100.])
        _addresult(results, 'doseratio', valpaccpcrit=[1., 5., 10.])
        _addresult(results, 'art_clusters',       minlowhighmax=[0, 0,  0, 1])
        _addresult(results, 'expert_inoutoverin', minlowhighmax=[-.2, -.1, .5, 1.])

    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)

def writeMRConfig(m_id, series, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/PIQT for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    hdr_params = {}
    qc_params = {}
    results = {}
    actions = {}

    _addaction(actions, 'acqdatetime', params={})
    _addresult(results, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    if series == 'QA1':
        range1 = ['3_1_30', '3_2_100']
        range2 = ['2_1_30']
        range3 = ['4_1_30', '4_2_100']
        range4 = ['5_1_30', '5_2_100']
    elif series == 'QA2':
        range1 = ['2_1_15']
        range2 = ['3_1_15']
        range3 = []
        range4 = []
    elif series == 'QA3':
        range1 = ['1_1_50', '1_2_100', '1_3_150']
        range2 = []
        range3 = []
        range4 = []

    if series == 'QA1': # just add to user view, no limits
        _addresult(results, 'Central_freq_QA1_2_1_30',
                   display_name='Central_freq_QA1_2_1_30', display_level=DISPLAY_USER, description='Central Frequency', units='MHz')

    for model in range1:
        _addresult(results, 'Patients Name_%s_%s'%(series, model),
                   display_name='patient name_%s'%model, display_level=DISPLAY_KEYUSER, description='patient name', units='', equals='PIQT')
        _addresult(results, 'S/N (B)_%s_%s'%(series, model),
                   display_name='S/N (B)_%s'%model, display_level=DISPLAY_USER, description='signal to noise ratio', units='%')
        _addresult(results, 'C-10/C+10_%s_%s'%(series, model),
                   display_name='C-10/C+10_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
        _addresult(results, 'Rad 10%%_%s_%s'%(series, model),
                   display_name='Rad 10%%_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
        _addresult(results, 'T/C-20_%s_%s'%(series, model),
                   display_name='T/C-20_%s'%model, display_level=DISPLAY_USER, description='uniformity measure', units='%')
        _addresult(results, 'C-20/C-10_%s_%s'%(series, model),
                   display_name='C-20/C-10_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')

        if series == 'QA1':     
            _addresult(results, 'T/C-20_%s_%s'%(series, model),
                           display_name='T/C-20_%s'%model, display_level=DISPLAY_USER, description='uniformity measure', units='%')
            _addresult(results, 'C+10/C+20_%s_%s'%(series, model),
                       display_name='C+10/C+20_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
        else:
            _addresult(results, 'T/C-20_%s_%s'%(series, model),
                           display_name='T/C-20_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
            _addresult(results, 'C+10/C+20_%s_%s'%(series, model),
                               display_name='C+10/C+20_%s'%model, display_level=DISPLAY_USER, description='uniformity measure', units='%')

        _addresult(results, 'C+20/Max_%s_%s'%(series, model),
                   display_name='C+20/Max_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
        _addresult(results, 'FFU__%s_%s'%(series, model),
                   display_name='FFU__%s'%model, display_level=DISPLAY_USER, description='Flood Field Uniformity', units='')

    for model in range2:
        _addresult(results, 'Patients Name_%s_%s'%(series, model),
                   display_name='patient name_%s'%model, display_level=DISPLAY_KEYUSER, description='patient name', units='', equals='PIQT')
        
        if series == 'QA1':
            _addresult(results, 'phant_rot_QA1_%s'%model,
                       display_name='phant rot_%s'%model, display_level=DISPLAY_KEYUSER, description='phantom rotation', units='degrees')
            #_addresult(results, 'm/p_angle_QA1_%s'%model,
            #           display_name='m/p angle_%s'%model, display_level=1, description='m/p angle', units='')
            _addresult(results, 'size_hor_QA1_%s'%model,
                       display_name='hor size_%s'%model, display_level=DISPLAY_USER, description='horizontal size', units='mm')
            _addresult(results, 'size_ver_QA1_%s'%model,
                       display_name='ver size_%s'%model, display_level=DISPLAY_USER, description='vertical size', units='mm')

            _addresult(results, 'hor_int_av_QA1_%s'%model,
                       display_name='hor int av_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='mm')
            _addresult(results, 'hor_int_dev_QA1_%s'%model,
                       display_name='hor int dev_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='mm')
            _addresult(results, 'hor_max_right_QA1_%s'%model,
                       display_name='hor max right_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='mm')
            _addresult(results, 'hor_max_left_QA1_%s'%model,
                       display_name='hor max left_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='mm')
            _addresult(results, 'hor_diff_av_QA1_%s'%model,
                       display_name='hor diff av_%s'%model, display_level=DISPLAY_USER, description='horizontal geometry measure', units='%')
            _addresult(results, 'hor_diff_dev_QA1_%s'%model,
                       display_name='hor diff dev_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='%')
            _addresult(results, 'hor_max_QA1_%s'%model,
                       display_name='hor max_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='%')
            _addresult(results, 'hor_min_QA1_%s'%model,
                       display_name='hor min_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='%')

            _addresult(results, 'ver_int_av_QA1_%s'%model,
                       display_name='ver int av_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='mm')
            _addresult(results, 'ver_int_dev_QA1_%s'%model,
                       display_name='ver int dev_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='mm')
            _addresult(results, 'ver_max_up_QA1_%s'%model,
                       display_name='ver max up_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='mm')
            _addresult(results, 'ver_max_down_QA1_%s'%model,
                       display_name='ver max down_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='mm')
            _addresult(results, 'ver_diff_av_QA1_%s'%model,
                       display_name='ver diff av_%s'%model, display_level=DISPLAY_USER, description='vertical geometry measure', units='%')
            _addresult(results, 'ver_diff_dev_QA1_%s'%model,
                       display_name='ver diff dev_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='%')
            _addresult(results, 'ver_max_QA1_%s'%model,
                       display_name='ver max_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='%')
            _addresult(results, 'ver_min_QA1_%s'%model,
                       display_name='ver min_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='%')
            _addresult(results, 'lin_NEMA_QA1_%s'%model,
                        display_name='lin_NEMA_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='%')
        
            _addresult(results, 'LIN__%s_%s'%(series, model),
                        display_name='LIN__%s'%model, display_level=DISPLAY_USER, description='Spatial linearity', units='')

        elif series == 'QA2':
            _addresult(results, 'FWHM_QA2_%s'%model,
                       display_name='FWHM_%s'%model, display_level=DISPLAY_KEYUSER, description='full width half maximum', units='mm')
            _addresult(results, 'Slice_int_QA2_%s'%model,
                       display_name='Slice_int_%s'%model, display_level=DISPLAY_KEYUSER, description='slice integral', units='mm')
            _addresult(results, 'SLP__%s_%s'%(series, model),
                       display_name='SLP__%s'%model, display_level=DISPLAY_USER, description='Slice Profile', units='')
            
    for model in range3:
        if series == 'QA1':
            _addresult(results, 'Patients Name_%s_%s'%(series, model),
                       display_name='patient name_%s'%model, display_level=DISPLAY_KEYUSER, description='patient name', units='', equals='PIQT')

            _addresult(results, 'FWHM_QA1_%s'%model,
                       display_name='FWHM_%s'%model, display_level=DISPLAY_KEYUSER, description='full width half maximum', units='mm')
            _addresult(results, 'FWTM_QA1_%s'%model,
                       display_name='FWTM_%s'%model, display_level=DISPLAY_USER, description='full width tenth maximum', units='mm')
            _addresult(results, 'Slice_int_QA1_%s'%model,
                       display_name='Slice_int_%s'%model, display_level=DISPLAY_KEYUSER, description='slice integral', units='mm')
            #_addresult(results, 'Phase_Shift_QA1_%s'%model,
            #           display_name='Phase_Shift_%s'%model, display_level=1, description='phase shift', units='')
            _addresult(results, 'SLP__%s_%s'%(series, model),
                        display_name='SLP__%s'%model, display_level=DISPLAY_USER, description='Slice Profile', units='')
    
    for model in range4:
        if series == 'QA1':
            _addresult(results, 'Patient Name_%s_%s'%(series, model),
                       display_name='patient name_%s'%model, display_level=DISPLAY_KEYUSER, description='patient name', units='', equals='PIQT')
            _addresult(results, 'Hor_pxl_size_QA1_%s'%model,
                       display_name='Hor_pxl_size_%s'%model, display_level=DISPLAY_USER, description='horizontal pixel size', units='mm')
            _addresult(results, 'Ver_pxl_size_QA1_%s'%model,
                       display_name='Ver_pxl_size_%s'%model, display_level=DISPLAY_USER, description='vertical pixel size', units='mm')
            _addresult(results, 'MTF__%s_%s'%(series, model),
                       display_name='MTF__%s'%model, display_level=DISPLAY_USER, description='Spatial Resolution', units='')


    if m_id == 'Achieva15sHB': # uses receive/transmit ='SENSE-Head-8'/B
        comments['description'] += 'Philips Achieva 1.5T receive/transmit=SENSE-Head-8/B'
        comments['specfiles'] = 'S: t15r3v2l3_gr4_rf0_l01_nt.spec, C: t15r3v2l3_gr4_rf0_l01_nt.cust'
        comments['version'] = '20180418'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[125., 135., 175., 175.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[96., 101., 125., 125.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[61., 71., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[61., 71., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[45., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[45., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 7., 9.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 7., 9.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0., 9., 13.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0., 9., 13.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 15., 20.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 15., 20.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5])
            
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[0., 0., 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[0., 0., 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[0., 0., 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[0., 0., 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[0., 0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[0., 0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2.0, -1.0, 0.00, 0.0]) # only crit_low given
        
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[0., 0., 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[0., 0., 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[0., 0., 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[0., 0., 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[0., 0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[0., 0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.0, -1.0, 0.00, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[4.65, 4.70, 5.10, 5.15]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[4.30, 4.35, 4.75, 4.80]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 7., 8.]) # before 20180418 [0., 0., 4., 8.])
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 7., 8.]) # before 20180418 [0., 0., 4., 8.])
        
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[4.55, 4.65, 4.95, 5.05]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[4.30, 4.35, 4.75, 4.80]) 
        
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 10., 15.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 15., 20.]) # only crit_high given

            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given

        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[89., 95., 120., 120.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[61., 71., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[45., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 7., 9.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 9., 13.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 15., 20.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 4., 5.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[4.80, 4.85, 5.25, 5.30]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[4.85, 4.95, 5.25, 5.35]) 
            
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[60., 60.,  80., 80.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[52., 52.,  70., 70.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[42., 42.,  60., 60.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[86., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0., 2., 3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0., 2., 3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
    
    elif m_id == 'Intera15HB': # uses receive/transmit ='Head'/B
        comments['description'] += 'Philips Intera 1.5T receive/transmit=Head/B'
        comments['specfiles'] = 'S : acsr11v8l1_03_l01_nt.spec C : none'
        comments['version'] = '20160413'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[92., 97., 125., 125.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[64., 68., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 5., 6.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 5., 6.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.75, 150.25, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[0., 0., 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[0., 0., 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[0., 0., 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[0., 0., 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[0., 0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[0., 0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2.0, -1.0, 0.00, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[0., 0., 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[0., 0., 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[0., 0., 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[0., 0., 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[0., 0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[0., 0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.0, -1.0, 0.00, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[4.70, 4.80, 5.10, 5.20]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[4.40, 4.55, 4.75, 4.90]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 4., 8.]) 
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 4., 8.]) 
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[4.60, 4.75, 4.95, 5.10]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[4.40, 4.55, 4.75, 4.90]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 10., 15.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 10., 15.]) # only crit_high given
        
            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[60., 64.,  90.,  90.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 5., 6.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[4.80, 4.85, 5.25, 5.30]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[4.90, 5.05, 5.25, 5.40]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[60., 62.,  80.,  80.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[52., 52.,  70.,  70.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[42., 42.,  60.,  60.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[86., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0., 2., 3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0., 2., 3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

    elif m_id == 'Panorama10sHB': # uses receive/transmit = 'SENSE-Head-8'/B
        comments['description'] += 'Philips Panorama HFO 1.0T receive/transmit=SENSE-Head-8/B'
        comments['specfiles'] = 'S: p10r2v6l3_02_l01_nt.spec, C: p10r2v6l3_02_l13_nt.cust'
        comments['version'] = '20180418'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[174., 185., 225., 225.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[131., 138., 180., 180.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[35., 37.,  70.,  70.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[35., 37.,  70.,  70.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[33., 35.,  60.,  60.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[33., 35.,  60.,  60.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 11., 12.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 11., 12.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0.,  9., 10.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0.,  9., 10.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 11., 22.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 11., 22.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0., 25., 28.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0., 25., 28.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ 0.,  0.0, 1.00, 1.5]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, 0.00, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 1.50, 2.0]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 1.00, 1.5]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[ 0.0,  0.0, 1.50, 2.5]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-3.5, -2.5, 0.00, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[4.55, 4.60, 5.00, 5.05]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[4.25, 4.30, 4.70, 4.75]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 7., 8.]) # before 20180418 [0., 0., 4., 8.]) 
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 7., 8.]) # before 20180418 [0., 0., 4., 8.]) 
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[4.55, 4.65, 4.95, 5.05]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[4.25, 4.35, 4.65, 4.75]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 10., 15.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 10., 15.]) # only crit_high given

            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[125., 135., 180., 180.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[33., 35.,  70.,  70.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[30., 32.,  60.,  60.]) # only crit_low given
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 11., 12.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 10., 11.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 11., 22.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 25., 28.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[4.70, 4.75, 5.15, 5.20]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[4.75, 4.85, 5.15, 5.25]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[ 49.,  51.,  75.,  75.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[ 41.,  43.,  70.,  70.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 35.,  37.,  70.,  70.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[85., 87., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[85., 87., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[82., 84., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[70., 75., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[60., 65., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[55., 60.,  90.,  90.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0.,  5.,  6.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0.,  6.,  7.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0.,  7.,  8.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0.,  9., 10.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0.,  4.,  6.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0.,  4.,  6.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0.,  6.,  7.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0.,  1.,  2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0.,  1.,  2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0.,  1.,  2.]) # only crit_high given

    elif m_id == 'MR7_l01HS': #older version used receive/transmit = 'T/R-head'/S, now uses 'SENSE-Head-8'/B
        comments['description'] += 'Philips Achieva 3.0T receive/transmit=T/R-head/S'
        comments['specfiles'] = 'S: t30r3v2l3_gr5_rf0_l02_nt.spec, C: t30r3v2l3_gr5_rf0_l02_nt.cust'
        comments['version'] = '20160413'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[106., 107., 120., 120.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 78.,  79., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[92., 94., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[88., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[87., 88., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0.,  2.5,  3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0.,  4.5,  5.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.75, 150.25, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, 0.00, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[ 0.0,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.5, -1.5, 0.00, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[1.85, 1.94, 1.96, 2.05]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.85, 1.94, 1.96, 2.05]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 7.5, 10.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 7.5, 10.]) # only crit_high given
        
            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.0, 1.3]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.0, 1.3]) # only crit_high given

        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 72.,  73.,  90.,  90.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[87., 88., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0.,  4.5,  5.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[ 92.,  93., 110., 110.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[ 81.,  82., 100., 100.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 63.,  64.,  75.,  75.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[91., 93., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[83., 85.,  90.,  90.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[88., 90., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[84., 86., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0., 4., 5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0.,  3.5,  4.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0.,  4.5,  5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0., 10.0, 12.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

    elif m_id == 'Achieva30sHB': #newer software uses receive/transmit = 'SENSE-Head-8'/B
        comments['description'] += 'Philips Achieva 3.0T receive/transmit=SENSE-Head-8/B'
        comments['specfiles'] = 'S: t30r3v2l3_gr5_rf0_l02_nt.spec, C: t30r3v2l3_gr5_rf0_l02_nt.cust'
        comments['version'] = '20160413'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[102., 104., 120., 120.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 77.,  79., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[60., 62., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[60., 62., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 42., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 42., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0., 16., 18.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0., 16., 18.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 11., 12.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 11., 12.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.75, 150.25, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, 0.00, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[ 0.0,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.5, -1.5, 0.00, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[1.85, 1.94, 1.96, 2.05]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 3., 4.]) 
            #_addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.85, 1.94, 1.96, 2.05]) 
            #_addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            #_addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.8, 1.9, 2.2, 2.2]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 10., 15.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 10., 15.]) # only crit_high given

            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.0, 1.3]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.0, 1.3]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 73.,  75.,  90.,  90.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[60., 62., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[40., 42., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 16., 18.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 11., 12.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[2.2, 2.2, 2.5, 2.5]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[115., 117., 120., 120.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[100., 102., 120., 120.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 80.,  82., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[85., 87., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[85., 87., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[70., 72., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0.,  3.,  5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0.,  3.,  5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0.,  3.,  5.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0., 1.5,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0., 1.5,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0., 2.0,  3.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

    elif m_id == 'Achieva30sHS': # receive/transmit ='SENSE-Head-8'/S
        comments['description'] += 'Philips Achieva 3.0T receive/transmit=SENSE-Head-8/S'
        comments['specfiles'] = 'S: t30r3v2l3_gr8_rf1_l02_nt.spec, S: t30r3v2l3_gr8_rf1_l03_nt.spec'
        comments['version'] = '20180418'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[102., 120., 175., 175.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 77.,  90., 125., 125.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[60., 70., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[60., 70., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0., 15., 18.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0., 15., 18.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 10., 12.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 10., 12.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, 0.00, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.25]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[ 0.0,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.5, -1.5, 0.00, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 3.5, 4.]) # before 20180418 [0., 0., 3., 4.])
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 3.5, 4.]) # before 20180418 [0., 0., 3., 4.])
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 2.10]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 10., 15.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 20., 25.]) # only crit_high given

            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.3]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.2, 1.3]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 73.,  85., 120., 120.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[60., 70., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[40., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 15., 18.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 10., 12.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[1.85, 1.92, 1.97, 2.10]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.50]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[115., 115., 150., 150.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[100., 100., 150., 150.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 65.,  65., 115., 115.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[85., 90., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[85., 90., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[70., 80., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0.,  2.,  3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0.,  2.,  3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0.,  2.,  3.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0., 1.5,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0., 1.5,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0., 2.5,  3.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

    elif m_id == 'Achieva70HS': # receive/transmit ='T/R-head'/S
        comments['description'] += 'Philips Achieva 7.0T receive/transmit=T/R-head/S' # copy of Achieva30sHS
        comments['specfiles'] = ''
        comments['version'] = '20160413'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[102., 120., 175., 175.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 77.,  90., 125., 125.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[60., 70., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[60., 70., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0., 15., 18.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0., 15., 18.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 10., 12.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 10., 12.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.75, 150.25, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, 0.00, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.0,  0.0, 0.25, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.0,  0.0, 0.50, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[ 0.0,  0.0, 1.00, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.5, -1.5, 0.00, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 10., 15.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 20., 25.]) # only crit_high given

            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.0, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.0, 1.3]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.0, 1.3]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 73.,  85., 120., 120.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[60., 70., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[40., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 13., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 15., 18.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 10., 12.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 3.0, 4.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[115., 120., 150., 150.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[100., 110., 150., 150.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 80.,  90., 115., 115.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[85., 90., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[85., 90., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[70., 80., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0.,  4.,  5.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0.,  2.,  3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0.,  2.,  3.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0.,  2.,  3.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0., 1.5,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0., 1.5,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0., 2.5,  3.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0., 1.5, 2.]) # only crit_high given

    elif m_id == 'Ingenia15MB': # receive/transmit=MULTI COIL/B
        comments['description'] += 'Philips Ingenia 1.5T receive/transmit=MULTI COIL/B'
        comments['specfiles'] = 'S: wa15r5v1l7_gr4_rf2_l01_irf.spec, T: wa15r5v1l7_gr4_rf2_l01_irf.typ'
        comments['version'] = '20160413'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[115., 115., 160., 160.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 87.,  87., 122., 122.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[65., 65., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[65., 65., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 40., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 40., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 6., 6.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 6., 6.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0., 4., 4.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0., 4., 4.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 20., 20.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 20., 20.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0., 10., 10.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0., 10., 10.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -5.0, 5.0, 5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.0, 91.0, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            #_addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-1., -1., 0.0, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.15]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0., 2.0, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2., -2., 0.0, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[4.65, 4.65, 5.15, 5.25]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[4.40, 4.40, 4.85, 4.95]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 7., 7.]) 
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 7., 7.]) 
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[4.60, 4.60, 5.10, 5.10]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[4.35, 4.35, 4.80, 4.80]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 11., 11.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 15., 15.]) # only crit_high given

            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 79.,  79., 111., 111.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[65., 65., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[40., 40., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 6., 6.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 4., 4.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 20., 20.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 10., 10.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[4.70, 4.70, 5.20, 5.20]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[4.80, 4.80, 5.30, 5.30]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[ 44.,  44.,  62.,  62.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[ 39.,  39.,  55.,  55.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 33.,  33.,  47.,  47.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[90., 90., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[90., 90., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[90., 90., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0., 4., 4.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0., 4., 4.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0., 4., 4.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0., 2., 2.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0., 2., 2.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0., 2., 2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0.,  1.,  1.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0.,  1.,  1.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0.,  1.,  1.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0.,  1.,  1.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0.,  1.,  1.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0.,  1.,  1.]) # only crit_high given

    elif m_id == 'Ingenia30MS': # receive/transmit=MULTI COIL/S
        comments['description'] += 'Philips Ingenia 3.0T receive/transmit=MULTI COIL/S'
        comments['specfiles'] = 'S: wa30r5v1l7_gr8_rf3_l08_irf.spec, T: wa30r5v1l7_gr8_rf3_l08_irf.typ'
        comments['version'] = '20160413'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[ 81.,  81., 114., 114.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 59.,  59.,  83.,  83.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[55., 55., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[55., 55., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 40., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 40., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 10., 10.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 10., 10.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 20., 20.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 20., 20.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0.,  4.,  4.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0.,  4.,  4.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -5.0, 5.0, 5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.0, 91.0, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            #_addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-1., -1., 0.0, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0., 2.0, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2., -2., 0.0, 0.0]) # only crit_low given

            # range3
            _addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            _addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[1.8, 1.8, 2.0, 2.0]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 4., 4.]) 
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 4., 4.]) 
            #_addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            #_addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.8, 1.8, 2.0, 2.0]) 
            #_addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 10., 10.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 25., 25.]) # only crit_high given

            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 2.4, 2.4]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.25, 1.25]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 2.5, 2.5]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 56.,  56.,  79.,  79.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[45., 45., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[ 0.,  0., 100., 100.]) # only crit_low given # not given!
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 12., 12.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 15., 15.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 25., 25.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 12., 12.]) # only crit_high given

            # range2
            _addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[2.2, 2.2, 2.5, 2.5]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[126., 126., 177., 177.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[104., 104., 146., 146.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 94.,  94., 132., 132.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[78., 78., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[78., 78., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[78., 78., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[60., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[60., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[60., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0.,  7.,  7.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0.,  7.,  7.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0.,  7.,  7.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0.,  2.,  3.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given

    elif m_id == 'Ingenia30MS_YC': # receive/transmit=MULTI COIL/S special for Youth Cohort
        comments['description'] += 'Philips Ingenia 3.0T receive/transmit=MULTI COIL/S'
        comments['specfiles'] = 'S: wa30r5v1l7_gr8_rf3_l08_irf.spec, T: wa30r5v1l7_gr8_rf3_l08_irf.typ, M:20161118'
        comments['version'] = '20161118'

        if series == 'QA1':
            # range1
            _addresult(results, 'S/N (B)_QA1_3_1_30', minlowhighmax=[ 81.,  81., 114., 114.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 56.,  59.,  83.,  83.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[55., 55., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[55., 55., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 40., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 40., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA1_3_1_30', minlowhighmax=[0., 0., 10., 10.]) # only crit_high given
            _addresult(results, 'T/C-20_QA1_3_2_100',minlowhighmax=[0., 0., 10., 10.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[0., 0., 20., 20.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[0., 0., 20., 20.]) # only crit_high given
            #_addresult(results, 'C+20/Max_QA1_3_1_30', minlowhighmax=[0., 0.,  4.,  4.]) # only crit_high given
            #_addresult(results, 'C+20/Max_QA1_3_2_100',minlowhighmax=[0., 0.,  4.,  4.]) # only crit_high given

            # range2
            _addresult(results, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -5.0, 5.0, 5.0]) # only crit_high given
            #_addresult(results, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.0, 91.0, 91.0]) 
            _addresult(results, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(results, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            #_addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'hor_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'hor_min_QA1_2_1_30',      minlowhighmax=[-1., -1., 0.0, 0.0]) # only crit_low given
            _addresult(results, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ 0.,  0., 0.5, 0.5]) # only crit_high given
            _addresult(results, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.0]) # only crit_high given
            _addresult(results, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            _addresult(results, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ 0.,  0., 1.0, 1.5]) # only crit_high given
            _addresult(results, 'ver_max_QA1_2_1_30',      minlowhighmax=[ 0.,  0., 2.0, 2.0]) # only crit_high given
            _addresult(results, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2., -2., 0.0, 0.0]) # only crit_low given

            # range3
            #_addresult(results, 'FWHM_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            #_addresult(results, 'FWHM_QA1_4_2_100',minlowhighmax=[1.8, 1.8, 2.0, 2.0]) 
            _addresult(results, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 4., 4.]) 
            _addresult(results, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 4., 4.]) 
            #_addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            #_addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.8, 1.8, 2.0, 2.0]) 
            #_addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            _addresult(results, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.8, 2.8]) 
            _addresult(results, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.9, 1.9, 2.8, 2.8]) 
            #_addresult(results, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[0., 0., 10., 10.]) # only crit_high given
            #_addresult(results, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[0., 0., 25., 25.]) # only crit_high given

            # range4
            _addresult(results, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.2, 1.2]) # only crit_high given
            _addresult(results, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 2.4, 2.4]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[0., 0., 1.25, 1.25]) # only crit_high given
            _addresult(results, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[0., 0., 2.5, 2.5]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(results, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 56.,  56.,  79.,  79.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[45., 45., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA2_2_1_15', minlowhighmax=[ 0.,  0., 100., 100.]) # only crit_low given # not given!
            _addresult(results, 'T/C-20_QA2_2_1_15', minlowhighmax=[0., 0., 12., 12.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[0., 0., 15., 15.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[0., 0., 25., 25.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA2_2_1_15', minlowhighmax=[0., 0., 12., 12.]) # only crit_high given

            # range2
            #_addresult(results, 'FWHM_QA2_3_1_15', minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            _addresult(results, 'Slice_int_QA2_3_1_15', minlowhighmax=[2.2, 2.2, 2.8, 2.8]) 
        elif series == 'QA3':
            # range1
            _addresult(results, 'S/N (B)_QA3_1_1_50', minlowhighmax=[55., 55., 130., 130.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_2_100',minlowhighmax=[55., 55., 130., 130.]) # only crit_low given
            _addresult(results, 'S/N (B)_QA3_1_3_150',minlowhighmax=[40., 40.,  95.,  95.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[78., 78., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[78., 78., 100., 100.]) # only crit_low given
            _addresult(results, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[78., 78., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_1_50', minlowhighmax=[60., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_2_100',minlowhighmax=[60., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'Rad 10%_QA3_1_3_150',minlowhighmax=[60., 60., 100., 100.]) # only crit_low given
            _addresult(results, 'T/C-20_QA3_1_1_50', minlowhighmax=[0., 0.,  7.,  7.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_2_100',minlowhighmax=[0., 0.,  7.,  7.]) # only crit_high given
            _addresult(results, 'T/C-20_QA3_1_3_150',minlowhighmax=[0., 0.,  7.,  7.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[0., 0., 14., 14.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given
            _addresult(results, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[0., 0.,  2.,  3.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_1_50', minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_2_100',minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given
            _addresult(results, 'C+20/Max_QA3_1_3_150',minlowhighmax=[0., 0.,  2.,  2.]) # only crit_high given

    _addaction(actions, 'header_series', params=hdr_params)
    _addaction(actions, 'qc_series', params=qc_params)
    configdict = {
        'comments': comments,
        'actions' : actions,
        'results' : results
    }
    
    _writeConfig(filename, configdict)
    

# main
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='WAD Config.json generator version %s'%__version__)
    destfolder = None
    parser.add_argument('-d','--dest',
                        default=destfolder,type=str,
                        help='the destination folder',dest='destfolder')
    
    args = parser.parse_args()
    if args.destfolder is  None:
        parser.print_help()
        sys.exit()

    allMRObjects = [ # machine, outname
        ('Achieva15sHB', 'mr_philips_achieva15_sHB'),
        ('Intera15HB' , 'mr_philips_intera15_HB'),
        ('Panorama10sHB','mr_philips_panorama10_sHB'),
        ('Achieva30sHB', 'mr_philips_achieva30_sHB'),
        ('Achieva30sHS', 'mr_philips_achieva30_sHS'),
        ('Ingenia15MB', 'mr_philips_ingenia15_MB'),
        ('Ingenia30MS', 'mr_philips_ingenia30_MS'),
        ('Ingenia30MS_YC','mr_philips_ingenia30_MS_YC'),
        ('Achieva70HS', 'mr_philips_achieva70_HS'),
    ]
    allObjects = [ # modality, id, outname
        ('CT', 'CT1', 'ct_philips_brilliance64_head.json', 'head'),
        ('CT', 'CT2', 'ct_philips_mx8000idt_head.json', 'head'),
        ('CT', 'CT2', 'ct_philips_mx8000idt_body.json', 'body'),
        ('CT', 'CT2leen', 'ct_philips_brilliance64b_head.json', 'head'),
        ('CT', 'CT2_iqon', 'ct_philips_iqon_head.json', 'head'),
        ('CT', 'CT2_iqon', 'ct_philips_iqon_body.json', 'body'),
        ('CT', 'CT2_iqon_MonoE', 'ct_philips_iqon_head_monoe.json', 'head'),
        ('CT', 'CT2_iqon_MonoE', 'ct_philips_iqon_body_monoe.json', 'body'),
        ('CT', 'CT3', 'ct_philips_ict_head.json', 'head'),
        ('CT', 'CT3', 'ct_philips_ict_body.json', 'body'),
        ('CT', 'CT4', 'ct_philips_brilliance16p_head.json', 'head'),
        ('CT', 'CT4F_HeadAa','ct_siemens_force_head_headAa.json', 'head'),
        ('CT', 'CT4F_BodyAa','ct_siemens_force_head_bodyAa.json', 'head'),
        ('CT', 'CT4F_BodyDual','ct_siemens_force_head_dual.json', 'head'),
        ('CT', 'CT4F_BodyPedAa','ct_siemens_force_head_pedAa.json', 'head'),
        ('US', 'epiq', 'us_philips_epiq_instance.json'),
        ('US', 'epiqeL18_5', 'us_philips_epiqeL18_4_instance.json'),
        ('US', 'epiqL12_3', 'us_philips_epiqL12_3_instance.json'),
        ('US', 'epiqL12_5', 'us_philips_epiqL12_5_instance.json'),
        ('US', 'epiqL15_7io', 'us_philips_epiqL15_7io_instance.json'),
        ('US', 'epiqL17_5', 'us_philips_epiqL17_5_instance.json'),
        ('US', 'iU22', 'us_philips_iu22_instance.json'),
        ('US', 'iU22L9_3', 'us_philips_iu22L9_3_instance.json'),
        ('US', 'iU22L12_5', 'us_philips_iu22L12_5_instance.json'),
        ('US', 'iU22L15_7io', 'us_philips_iu22L15_7io_instance.json'),
        ('US', 'iU22L17_5', 'us_philips_iu22L17_5_instance.json'),
        ('US', 'CX50', 'us_philips_cx50_instance.json'),
        ('US', 'HD11', 'us_philips_hd11_instance.json'),
        ('US', 'VolusonE8', 'us_ge_volusone8_instance.json'),
        ('US', 'ALOKAG8', 'us_aloka_g8_instance.json'),
        ('RF', 'DDL', 'rf_philips_omni.json'),
        ('CR', 'AZUDIDI','cr_pehamed_philips_didi.json'),
        ('CR', 'WKZFCR1','cr_wellhofer_siemens_fcr1.json'),
        ('CR', 'WKZFCR2','cr_wellhofer_siemens_fcr2.json'),
        ('CR', 'YSIO','cr_wellhofer_siemens_ysio.json'),
        ('OT', 'input2dicom','input2dicom_example.json'),
        ('NM', 'mCT','nm_dailyqc_siemens_mct.json'),
        ('NM', 'spect2','nm_uniformity_siemens_symbia_t2.json'),
        ('MGCD', 'CDMam32','mg_cdmam32_L50.json'),
        ('DX', 'AZUDIDI', 'dx_normi13_philips_umcu.json'),
        ('DX', 'AZUDIDITABLE', 'dx_normi13_philips_umcutable.json'),
        ('DX', 'AZUDIDIWALL', 'dx_normi13_philips_umcuwall.json'),
        ('DX', 'AZUR4DIDITABLE', 'dx_normi13_philipsr4_umcutable.json'),
        ('DX', 'AZUR4DIDIWALL', 'dx_normi13_philipsr4_umcuwall.json'),
        ('DX', 'WKZDIDI1','dx_normi13_philips_wkz1.json'),
        ('DX', 'WKZDIDI2','dx_normi13_philips_wkz2.json'),
        ('DX', 'WKZDIDILOS','dx_normi13_philips_wkzlos.json'),
        ('DX', 'WKZDIDILOS1','dx_normi13_philips_wkzlos1.json'),
        ('DX', 'WKZDIDILOS2','dx_normi13_philips_wkzlos2.json'),
        ('DX', 'WKZDIDITABLE','dx_normi13_philips_wkztable.json'),
        ('DX', 'WKZDIDIWALL', 'dx_normi13_philips_wkzwall.json'),
        ('DXU', 'AZUDIDI', 'dx_uniformity_philips_umcu.json'),
        ('DXU', 'AZUDIDITABLE', 'dx_uniformity_philips_umcutable.json'),
        ('DXU', 'AZUDIDIWALL', 'dx_uniformity_philips_umcuwall.json'),
        ('DXU', 'AZUR4DIDITABLE', 'dx_uniformity_philipsr4_umcutable.json'),
        ('DXU', 'AZUR4DIDIWALL', 'dx_uniformity_philipsr4_umcuwall.json'),
        ('DXU', 'WKZDIDI1','dx_uniformity_philips_wkz1.json'),
        ('DXU', 'WKZDIDI2','dx_uniformity_philips_wkz2.json'),
        ('DXU', 'WKZDIDILOS','dx_uniformity_philips_wkzlos.json'),
        ('DXU', 'WKZDIDILOS1','dx_uniformity_philips_wkzlos1.json'),
        ('DXU', 'WKZDIDILOS2','dx_uniformity_philips_wkzlos2.json'),
        ('DXU', 'WKZDIDITABLE','dx_uniformity_philips_wkztable.json'),
        ('DXU', 'WKZDIDIWALL', 'dx_uniformity_philips_wkzwall.json'),
        ('MG', 'Selenia', 'mg_hologic_selenia_MO.json', 'MO'),
        ('MG', 'Selenia', 'mg_hologic_selenia_RH.json', 'RH'),
        ('MG', 'Dimensions', 'mg_hologic_dimensions_RH.json', 'RH'),
        ('MG', 'Dimensions', 'mg_hologic_dimensions_AG.json', 'AG'),
        ('MG', 'AffirmProne', 'mg_hologic_affirmprone_AG.json', 'AG'),
        ('MG', 'MicrodoseL50', 'mg_philips_L50.json', None),
        ('OCR', 'epiq', 'ocr_philips_epiq_instance.json'),
        ('OCR', 'Intevo_LEHR', 'ocr_intevo_lehr.json'),
        ('OCR', 'ALOKAG8', 'ocr_alokag8_instance.json'),
        ('FBIRNQC', 'philips', 'mr_fbirn_qc_philips.json'),
        ('FBIRNB0', 'philips', 'mr_fbirn_b0_philips.json'),
        ('FBIRNB1', 'philips', 'mr_fbirn_b1_philips.json'),
        ('FBIRNSNR', 'philips', 'mr_fbirn_snr_philips.json'),
        ('FBIRNReceive', 'philips', 'mr_fbirn_receive_philips.json'),
        ('DX', 'FULL','dx_normi13_uniformity_generic.json'),
        ('DX', 'ElevaAcq', 'rf_normi13_philips_elevaacq.json'),
        ('DX', 'ElevaCine','rf_normi13_philips_elevacine.json'),
        ('DXU', 'ElevaAcq','rf_uniformity_philips_elevaacq.json'),
        ('DX', 'DRXLarge','dx_normi13_carestream_drxlarge.json'),
        ('DX', 'DRXSmall','dx_normi13_carestream_drxsmall.json'),
        ('DXU', 'DRXLarge','dx_uniformity_carestream_drxlarge.json'),
        ('DXU', 'DRXSmall','dx_uniformity_carestream_drxsmall.json'),
        ('DX', 'FD20', 'xa_normi13_philips_fd20.json'),
        ('DX', 'FD2020_F', 'xa_normi13_philips_fd2020frontal.json'),
        ('DX', 'FD2020_L', 'xa_normi13_philips_fd2020lateral.json'),
        ('DXU', 'FD20', 'xa_uniformity_philips_fd20.json'),
        ('DXU', 'FD2020_F', 'xa_uniformity_philips_fd2020frontal.json'),
        ('DXU', 'FD2020_L', 'xa_uniformity_philips_fd2020lateral.json'),
        ('DX', 'HCK_WKZ_A',  'xa_normi13_siemens_axiom_artisA.json'),
        ('DXU', 'HCK_WKZ_A', 'xa_uniformity_siemens_axiom_artisA.json'),
        ('DXU', 'HCK_WKZ_B', 'xa_uniformity_siemens_axiom_artisB.json'),
    ]
    for machine, fname in allMRObjects:
        for series in ['QA1', 'QA2', 'QA3']:
            allObjects.append( ('MR', machine, fname+'_%s.json'%series, series))

    for obj in allObjects:
        if len(obj) ==3:
            mod, m_id, filename = obj
        else:
            mod, m_id, filename, model = obj
        if args.destfolder:
            try:
                os.makedirs(os.path.join(args.destfolder, 'meta'))
            except OSError as e:
                print(e.errno,e)
                if e.errno != errno.EEXIST:
                    raise

            fname = os.path.expanduser(os.path.join(args.destfolder,filename))
        else:
            fname = filename
        if mod == 'CT':
            writeCTConfig(m_id, filename=fname, anatomy=model)
        elif mod == 'CR':
            writeCRConfig(m_id, filename=fname)
        elif mod == 'MG':
            writeMGConfig(m_id, filename=fname, tubefilter=model)
        elif mod == 'MGCD':
            writeMGCDConfig(m_id, filename=fname)
        elif mod == 'MR':
            writeMRConfig(m_id, filename=fname, series=model)
        elif mod == 'NM':
            writeNMConfig(m_id, filename=fname)
        elif mod == 'US':
            writeUSConfig(m_id, filename=fname)
        elif mod == 'RF':
            writeRFConfig(m_id, filename=fname)
        elif mod == 'OT':
            writeOTConfig(m_id, filename=fname)
        elif mod == 'DX':
            writeDXConfig(m_id, filename=fname)
        elif mod == 'DXU':
            writeDXUConfig(m_id, filename=fname)
        elif mod == 'OCR':
            writeOCRConfig(m_id, filename=fname)
        elif mod == 'FBIRNQC':
            writeFBIRNQCConfig(m_id, filename=fname)
        elif mod == 'FBIRNB0':
            writeFBIRNB0Config(m_id, filename=fname)
        elif mod == 'FBIRNB1':
            writeFBIRNB1Config(m_id, filename=fname)
        elif mod == 'FBIRNSNR':
            writeFBIRNSNRConfig(m_id, filename=fname)
        elif mod == 'FBIRNReceive':
            writeFBIRNReceiveConfig(m_id, filename=fname)
